import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:leadcalls/Home.dart';
import 'package:leadcalls/LoginPage.dart';
import 'package:leadcalls/Reused/LeadResponse.dart';
import 'package:leadcalls/dashboardPage.dart';
import 'package:leadcalls/leadListPage.dart';
import 'package:leadcalls/objects/DropdownResponse.dart';
import 'package:leadcalls/objects/UpdateLeadRecord.dart';
import 'package:leadcalls/objects/createResponse.dart';
import 'package:leadcalls/objects/leads.dart';
import 'package:leadcalls/utils/ColorConstants.dart';
import 'package:leadcalls/utils/Constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LeadEdit extends StatefulWidget {
  final String firstnametext,createdby,modifyby,assusrname,desc;
  LeadEdit({Key key, @required this.firstnametext, @required this.createdby,
          @required this.modifyby, @required this.assusrname,@required this.desc}) : super(key: key);

  static const id = 'leadedit';

  @override
  _LeadEditState createState() => _LeadEditState();

}

class _LeadEditState extends State<LeadEdit> {

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  final _idController = TextEditingController();
  final _firstnameController = TextEditingController();
  final _lastnameController = TextEditingController();
  final _phoneworkController = TextEditingController();
  final _primaryAddressStreetController = TextEditingController();
  final _primaryAddressCityController = TextEditingController();
  final _primaryAddressStateController = TextEditingController();
  final _primaryAddressPostalcodeController = TextEditingController();
  final _primaryAddressCountryController = TextEditingController();
  final _altAddressCityController = TextEditingController();
  final _altAddressStreetController = TextEditingController();
  final _altAddressStateController = TextEditingController();
  final _altAddressPostalcodeController = TextEditingController();
  final _altAddressCountryController = TextEditingController();
  final _phoneFaxController = TextEditingController();
  final _referedByController = TextEditingController();
  final _leadSourceDescriptionController = TextEditingController();
  final _modifiedByNameController = TextEditingController();
  final _dateModifiedController = TextEditingController();
  final _accountNameController = TextEditingController();
  final _opportunityAmountController = TextEditingController();
  final _campaignNameontroller = TextEditingController();
  final _websiteController = TextEditingController();
  final _statusDiscriptionController = TextEditingController();
  final _cretedByController = TextEditingController();
  final _assignByController = TextEditingController();

  String selectedTitle;
  String selectedSalutation;
  String selectedLeadSource;
  String selectedStatus;
  String selectedProperty;
  String selectedSubPropertyType;
  String selectedPropertyStatus;

  List<Map> emailSectionList;
  List<String> sal_options = ['please wait..'];
  List<String> leadsourceoptions = ['please wait..'];
  List<String> statusoptions = ['please wait..'];
  List<String> property_options = ['please wait..'];
  List<String> propetystatus_options = ['please wait..'];
  List<String> subproperty_options = ['please wait..'];

  Map emailSectionDefaultValue = {
    'position': 0,
    'optedOut': false,
    'invalid': false,
    'email': null
  };

  bool addressSame = true;
  List<dynamic> _dataTitle = List();

  DropDownLeadResponse dropDownLeadResponse;
  EntryList leads;
  @override
  void initState() {
    super.initState();
    getdropdownValues();

    emailSectionList = [emailSectionDefaultValue];

    _firstnameController.text = widget.firstnametext;
    _cretedByController.text = widget.createdby;
  ///  _idController.text = widget.recordId;
    _modifiedByNameController.text = widget.modifyby;
    _assignByController.text = widget.assusrname;
  }

  @override
  Widget build(BuildContext context) {

    final Size screenSize = MediaQuery.of(context).size;
    Widget overViewSection = Container(
        padding: new EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),

        child: new ListView(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          children: <Widget>[
            Text(
              'Salutation',
              style: TextStyle(fontSize: 12),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 8),
                  child: DropdownButton(
                    value: selectedSalutation,
                    items: sal_options.map((title) {
                      return DropdownMenuItem(
                        value: title,
                        child: Text(title),
                      );
                    }).toList(),
                    onChanged: (changed) {
                      setState(() {
                        selectedSalutation = changed;
                      }
                      );
                    },
                  ),
                ),

                Expanded(
                  child: TextFormField(
                      controller: _firstnameController,
                      decoration:  InputDecoration(
                          labelText: "First Name",)),
                ),

              ],
            ),
             TextFormField(
                controller: _lastnameController,
                decoration: new InputDecoration(
                    hintText: 'Last Name', labelText: 'Last Name')),
             TextFormField(
                controller: _phoneworkController,
                decoration: new InputDecoration(
                    hintText: 'Office Phone', labelText: 'Office Phone')),
            Container(
              height: 12,
            ),
            Text(
              'Lead Source',
              style: TextStyle(fontSize: 12),
            ),
            DropdownButton(
              value: selectedLeadSource,
              items: leadsourceoptions.map((title) {
                return DropdownMenuItem(
                  value: title,
                  child: Text(title),
                );
              }).toList(),
              onChanged: (changed) {
                setState(() {
                  selectedLeadSource = changed;

                });
              },
            ),
            Container(
              height: 12,
            ),
            Text(
              'Status',
              style: TextStyle(fontSize: 12),
            ),
            DropdownButton(
              value: selectedStatus,
              items: statusoptions.map((title) {
                return DropdownMenuItem(
                  value: title,
                  child: Text(title),
                );
              }).toList(),
              onChanged: (changed) {
                setState(() {
                  selectedStatus = changed;

                });
              },
            ),
            ListView.builder(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemCount: emailSectionList.length,
                itemBuilder: (BuildContext ctxt, int index) {
                  return emailSection(
                      index: index, emailSectionData: emailSectionList[index]);
                }),
            Container(
              alignment: Alignment.centerRight,
              child: IconButton(
                icon: Icon(Icons.add_box),
                onPressed: () {
                  setState(() {
                    emailSectionList.add(emailSectionDefaultValue);
                  });
                },
              ),
            ),
          ],
        ),

    );
    Widget primaryAddressSection = Container(
      padding: new EdgeInsets.all(20.0),
      child: new ListView(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          children: <Widget>[
            Text(
              "Primary Address",
              style: TextStyle(
                  color: ColorConstants.primaryColorDark,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            new TextFormField(
                controller: _primaryAddressStreetController,
                decoration: new InputDecoration(
                  hintText: 'Address',
                  labelText: 'Address',
                )),
            new TextFormField(
                controller: _primaryAddressCityController,
                decoration:
                new InputDecoration(hintText: 'City', labelText: 'City')),
            new TextFormField(
                controller: _primaryAddressStateController,
                decoration: new InputDecoration(
                    hintText: 'State / Region', labelText: 'State / Region')),
            new TextFormField(
                controller: _primaryAddressPostalcodeController,
                decoration: new InputDecoration(
                    hintText: 'Postal Code', labelText: 'Postal Code')),
            new TextFormField(
                controller: _primaryAddressCountryController,
                decoration: new InputDecoration(
                    hintText: 'Country', labelText: 'Country')),
            Container(
              height: 12,
            ),
          ]),
    );
    Widget createButton = Container(
      alignment: Alignment.bottomCenter,
      height: 60,
      width: screenSize.width,
      child: Row(
        children: <Widget>[
          Expanded(
            child: new RaisedButton(
              child: new Text(
                'Cancel',
                style: new TextStyle(color: Colors.white),
              ),
              onPressed: () => {
                Navigator.pushNamed(context, DashboardPage.id)
              },
              color: Colors.grey,
            ),
          ),
          Expanded(
            child: new RaisedButton(
              child: new Text(
                'Update',
                style: new TextStyle(color: Colors.white),
              ),
              onPressed: () => onCreateLead(),
              color: Colors.blue,
            ),
          ),
        ],
      ),
      margin: new EdgeInsets.only(top: 20.0),
    );
    Widget otherAddressSection = Container(
      padding: new EdgeInsets.all(20.0),
      child: new ListView(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          children: <Widget>[
            Text(
              "Secondary Address",
              style: TextStyle(
                  color: ColorConstants.primaryColorDark,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            new TextFormField(
                controller: _altAddressStreetController,
                decoration: new InputDecoration(
                  hintText: 'Address',
                  labelText: 'Address',
                )),
            new TextFormField(
                controller: _altAddressCityController,
                decoration:
                new InputDecoration(hintText: 'City', labelText: 'City')),
            new TextFormField(
                controller: _altAddressStateController,
                decoration: new InputDecoration(
                    hintText: 'State / Region', labelText: 'State / Region')),
            new TextFormField(
                controller: _altAddressPostalcodeController,
                decoration: new InputDecoration(
                    hintText: 'Postal Code', labelText: 'Postal Code')),
            new TextFormField(
                controller: _altAddressCountryController,
                decoration: new InputDecoration(
                    hintText: 'Country', labelText: 'Country')),
            Container(
              height: 12,
            ),
          ]),
    );
    Widget addressesSameCheck = Container(
      padding: new EdgeInsets.all(20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
              child: Text('Primary Address and Residential Address are same ')),
          Checkbox(
            value: addressSame,
            onChanged: ((val) {
              setState(() {
                addressSame = val;
              });
            }),
          )
        ],
      ),
    );
    Widget moreInfoSection = Container(
        padding: new EdgeInsets.all(20.0),
        child: new ListView(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          children: <Widget>[
            Text(
              "More Information",
              style: TextStyle(
                  color: ColorConstants.primaryColorDark,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
             TextFormField(
                controller: _accountNameController,
                decoration: new InputDecoration(
                  hintText: 'Account Name',
                  labelText: 'Account Name',
                )),
             TextFormField(
                controller: _websiteController,
                decoration: new InputDecoration(
                    hintText: 'Website', labelText: 'Website')),
             TextFormField(
                controller: _statusDiscriptionController,
                decoration: new InputDecoration(
                    hintText: 'Status Description',
                    labelText: 'Status Description')),
             TextFormField(
                controller: _leadSourceDescriptionController,
                decoration: new InputDecoration(
                    hintText: 'Lead Source Description',
                    labelText: 'Lead Source Description')),
             TextFormField(
                controller: _opportunityAmountController,
                decoration: new InputDecoration(
                    hintText: 'Opportunity Amount',
                    labelText: 'Opportunity Amount')),
             TextFormField(
                controller: _referedByController,
                decoration: new InputDecoration(
                    hintText: 'Referred By', labelText: 'Referred By')),
             TextFormField(
                controller: _campaignNameontroller,
                decoration: new InputDecoration(
                    hintText: 'Campaign', labelText: 'Campaign')),
             TextFormField(
                controller: _phoneFaxController,
                decoration:
                new InputDecoration(hintText: 'Fax', labelText: 'Fax')),
            Container(
              height: 12,
            ),
          ],
        ));

//    final format = DateFormat("yyyy-MM-dd");
    DateTime dateCreated = DateTime.now();
    DateTime dateModified = DateTime.now();
    Widget internalSection = Container(
        padding:  EdgeInsets.all(20.0),
        child:  ListView(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          children: <Widget>[
            Text(
              "Internal",
              style: TextStyle(
                  color: ColorConstants.primaryColorDark,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            Text(
              "date entered",
            ), ////created date
            Container(
              height: 10,
            ),
            GestureDetector(
                onTap: () async {
                  await DatePicker.showDatePicker(
                    context,
                    minTime: DateTime(1900),
                    currentTime: dateCreated,
                    maxTime: DateTime(2200),
                    onChanged: ((picked) {
                      dateCreated = picked;
                    }),
                  );

                  setState(() {

                  });
                },
                child: Text(
                  '$dateCreated',
                  style: TextStyle(color: Colors.black, fontSize: 18.0),
                )),
            Container(
              height: 20,
            ),
            Text(
              "Date Modified",
            ),
            Container(
              height: 10,
            ),
            GestureDetector(
                onTap: () async {
                  DatePicker.showDatePicker(
                    context,
                    minTime: DateTime(1900),
                    currentTime: dateCreated,
                    maxTime: DateTime(2200),
                    onChanged: ((picked) {
                      setState(() {
                        dateCreated = picked;
                      });
                    }),
                  );
                },
                child: Text(
                  '$dateModified',
                  style: TextStyle(color: Colors.black, fontSize: 18.0),
                )),

             TextFormField(
                controller: _cretedByController,
                decoration: new InputDecoration(
                  hintText: "Created By",
                  labelText: "Created By",
                )),
             TextFormField(
                controller: _dateModifiedController,
                decoration: new InputDecoration(
                  hintText: "Modified By Name",
                  labelText: "Modified By Name",
                )),
            /* new TextFormField(
              controller: _altAddressPostalcodeController,
                decoration: new InputDecoration(
                    hintText: 'Alt Address Postal Code',
                    labelText: 'Alt Address Postal Code')),*/
            Text(
              'Property',
              style: TextStyle(fontSize: 12),
            ),
            DropdownButton(
              value: selectedProperty,
              items: property_options.map((title) {
                return DropdownMenuItem(
                  value: title,
                  child: Text(title),
                );
              }).toList(),
              onChanged: (changed) {
                setState(() {
                  selectedProperty = changed;

                });
              },
            ),
            Text(
              'Sub Property Type',
              style: TextStyle(fontSize: 12),
            ),
            DropdownButton(
              value: selectedSubPropertyType,
              items: subproperty_options.map((title) {
                return DropdownMenuItem(
                  value: title,
                  child: Text(title),
                );
              }).toList(),
              onChanged: (changed) {
                setState(() {
                  selectedSubPropertyType = changed;

                });
              },
            ),
            Text(
              'Property Status',
              style: TextStyle(fontSize: 12),
            ),
            DropdownButton(
              value: selectedPropertyStatus,
              items: propetystatus_options.map((title) {
                return DropdownMenuItem(
                  value: title,
                  child: Text(title),
                );
              }).toList(),
              onChanged: (changed) {
                setState(() {
                  selectedPropertyStatus = changed;
                });
              },
            ),
            Container(
              height: 12,
            ),
          ],
        ));
    Widget divider = Container(
      width: screenSize.width,
      height: .2,
      color: Colors.grey,
    );
    Widget body = Form(
        key: this._formKey,
        child: Stack(alignment: Alignment.bottomCenter, children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 60),
            child: new ListView(
              children: <Widget>[
                overViewSection,
                divider,
                primaryAddressSection,
                addressesSameCheck,
                addressSame ? new Container() : otherAddressSection,
                divider,
                moreInfoSection,
                divider,
                internalSection,
                divider,
              ],
            ),
          ),
          createButton
        ]));
    return Scaffold(
      appBar: (
      AppBar(title: Text("Edit Lead"),)
      ),
      body: body,
    );
  }

  Widget emailSection({int index, Map emailSectionData}) {
    if (index == null) {
      index = emailSectionList.length;
    }

    if (emailSectionData == null) {
      emailSectionData = {'optedOut': false, 'invalid': false, 'email': null};
    }

    TextEditingController _textEmailController =
    new TextEditingController(text: emailSectionData['email']);

    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: new TextFormField(
                controller: _textEmailController,
                decoration:
                new InputDecoration(hintText: "Email", labelText: "Email"),
                onChanged: (text) {
                  emailSectionData['email'] = text;
                  emailSectionList[index] = emailSectionData;
                },
              ),
            ),
            IconButton(
              icon: Icon(Icons.remove_circle),
              color: Colors.red,
              onPressed: () {
                setState(() {
                  emailSectionList.removeAt(index);
                });
              },
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Checkbox(
                value: emailSectionData['optedOut'],
                onChanged: ((change) {
                  emailSectionData['optedOut'] = change;
                })),
            Checkbox(
              value: emailSectionData['invalid'],
              onChanged: ((change) {
                emailSectionData['invalid'] = change;
              }),
            )
          ],
        )
      ],
    );
  }

  void onCreateLead() {
    String id = _idController.text;
    String firstNameValue = _firstnameController.text;

    String lastNameValue = _lastnameController.text;

    String phoneWorkValue = _phoneworkController.text;
    String primaryAddresStreetValue = _primaryAddressStreetController.text;
    String primaryAddresCityValue = _primaryAddressCityController.text;
    String primaryAddresStateValue = _primaryAddressStateController.text;
    String primaryAddresPostalCodeValue =
        _primaryAddressPostalcodeController.text;
    String primaryAddresCountryValue = _primaryAddressCountryController.text;
    String altAddresStreetValue = _altAddressStreetController.text;
    String altAddresCityValue = _altAddressCityController.text;
    String altAddresStateValue = _altAddressCityController.text;
    String altAddresPostalCodeValue = _altAddressPostalcodeController.text;
    String altAddresCountryValue = _altAddressCountryController.text;
    String accountNameValue = _accountNameController.text;
    String websiteValue = _websiteController.text;
    String statusDescriptionValue = _statusDiscriptionController.text;
    String lead_source_descriptionValue = _leadSourceDescriptionController.text;
    String opportunity_amountValue = _opportunityAmountController.text;
    String refered_byValue = _referedByController.text;
    String campaign_nameValue = _campaignNameontroller.text;
    String phone_faxValue = _phoneFaxController.text;
    String created_by_nameValue = _cretedByController.text;
    String modified_by_nameValue = _modifiedByNameController.text;

    if (firstNameValue.isEmpty &&
        accountNameValue.isEmpty &&
        refered_byValue.isEmpty &&
        phoneWorkValue.isEmpty &&
        lastNameValue.isEmpty &&
        created_by_nameValue.isEmpty) {
      setState(() {
        String desc = "Please enter details";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      });
    }

    _saveLead(
      id,
        firstNameValue, lastNameValue, phoneWorkValue, modified_by_nameValue,
        created_by_nameValue, phone_faxValue, campaign_nameValue,
        refered_byValue, opportunity_amountValue,
        lead_source_descriptionValue, statusDescriptionValue, websiteValue, accountNameValue,
        primaryAddresStreetValue, altAddresCountryValue,
        altAddresPostalCodeValue, altAddresStateValue,
        altAddresCityValue, primaryAddresPostalCodeValue,
        altAddresStreetValue, primaryAddresStateValue,
        primaryAddresCountryValue, primaryAddresCityValue,
        selectedSalutation, selectedProperty, selectedSubPropertyType,
        selectedPropertyStatus, selectedLeadSource, selectedStatus
    );
  }
  _saveLead(String id,
      String firstNameValue, String lastNameValue, String phoneWorkValue,
      String modified_by_nameValue, String created_by_nameValue,
      String phone_faxValue, String campaign_nameValue,
      String refered_byValue, String opportunity_amountValue,
      String lead_source_descriptionValue, String statusDescriptionValue,
      String websiteValue, String accountNameValue,
      String primaryAddresStreetValue, String altAddresCountryValue,
      String altAddresPostalCodeValue, String altAddresStateValue,
      String altAddresCityValue, String primaryAddresPostalCodeValue,
      String altAddresStreetValue, String primaryAddresStateValue,
      String primaryAddresCountryValue, String primaryAddresCityValue,
      String selectedSalutation, String selectedProperty,
      String selectedSubPropertyType, String selectedPropertyStatus,
      String selectedLeadSource, String selectedStatus) async {
// set up POST request arguments
    String url = 'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    String leadString = '{"session":"$userSession", "module_name":"Leads", "name_value_list":[{"name":"id","value":"$id"},{"name":"assigned_user_id","value":"$userID"},{"name":"first_name","value":"$firstNameValue"},{"name":"status","value":"$selectedStatus"},{"name":"alt_address_street","value":"$altAddresStreetValue"},{"name":"alt_address_city","value":"$altAddresCityValue"},{"name":"phone_work","value":"$phoneWorkValue"},{"name":"alt_address_state","value":"$altAddresStateValue"},{"name":"alt_address_postalcode","value":"$altAddresPostalCodeValue"},{"name":"alt_address_country","value":"$altAddresCountryValue"},{"name":"website","value":"$websiteValue"},{"name":"status_description","value":"$statusDescriptionValue"},{"name":"lead_source_description","value":"$lead_source_descriptionValue"},{"name":"opportunity_amount","value":"$opportunity_amountValue"},{"name":"refered_by","value":"$refered_byValue"},{"name":"campaign_name","value":"$campaign_nameValue"},{"name":"lead_source","value":"$selectedLeadSource"},{"name":"date_entered","value":""}]}';

    Map<String, Object> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "set_entry",
      "rest_data": "$leadString"
    };

    FormData formData = FormData.fromMap(body);

    // make POST request
    Dio dio = new Dio();

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      return options; //continue
    }, onResponse: (Response response) async {
      return response; // continue
    }, onError: (DioError e) async {
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
    int statusCode = response.statusCode;
    try {
      if (statusCode == 200) {
        Map responseMap = json.decode(response.toString());
        if (responseMap != null) {
          UpdateRecordLead leadResponse = UpdateRecordLead.fromJson(json.decode(response.toString()));

          String desc = "Updated Successfully";
          Fluttertoast.showToast(
              msg: "$desc",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIos: 1,
              backgroundColor: Colors.greenAccent,
              textColor: Colors.white,
              fontSize: 16.0);

          Navigator.of(context).pop();


        } else {
          String desc = "Something went wrong. Try again.";

          Fluttertoast.showToast(
              msg: "$desc",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIos: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      }
    } catch (e) {
    }
  }

  // ignore: missing_return
  String generateMd5(String input) {
    /////    return md5.convert(utf8.encode(input)).toString();
  }

  Future<dynamic> readResponse(HttpClientResponse response) {
    var completer = new Completer();
    var contents = new StringBuffer();
    response.transform(utf8.decoder).listen((data) {
      contents.write(data);
    }, onDone: () => completer.complete(contents.toString()));
    return completer.future;
  }

  Future<void> getdropdownValues() async {
    //loading(context);
    // set up POST request arguments
    String url =
        'http://uat.ideadunes.com/projects/devs/testapi_giproperties/dom.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    int offset = 0;
    String restData =
        '{"session":"$userSession","module_name":"Leads","query":"assigned_user_id=\'$userID\'","order_by":"status","offset":"$offset","select_fields":[],"link_name_to_fields_array":[],"max_results":"50","deleted":"0","Favorites":false}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_module_fields",
      "rest_data": "$restData"
    };

    FormData formData = FormData.fromMap(body);
    // make POST request
    Dio dio = new Dio();
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // Do something before request is sen
      return options; //continue
    }, onResponse: (Response response) async {
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
    // check the status code for the result
    int statusCode = response.statusCode;


    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());
      if (responseMap != null) {
        ///write a code from response
        DropDownLeadResponse dropDownLeadResponse = await DropDownLeadResponse.fromJson(jsonDecode(response.toString()));
        setState(() {
          sal_options = dropDownLeadResponse.moduleFields.salutation.options;
          selectedSalutation = sal_options[0];
          leadsourceoptions = dropDownLeadResponse.moduleFields.leadSource.options;
          selectedLeadSource = leadsourceoptions[0];
          statusoptions = dropDownLeadResponse.moduleFields.status.options;
          selectedStatus = statusoptions[0];
          property_options = dropDownLeadResponse.moduleFields.propertyC.options;
          selectedProperty = property_options[0];
          propetystatus_options = dropDownLeadResponse.moduleFields.propertyStatusC.options;
          selectedPropertyStatus = propetystatus_options[0];
          subproperty_options = dropDownLeadResponse.moduleFields.subPropertyTypeC.options;
          selectedSubPropertyType = subproperty_options[0];
        });

      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);

        if (desc.contains("session ID is invalid")) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
            ModalRoute.withName('/'),
          );
        }
      }
    }
  }
}
