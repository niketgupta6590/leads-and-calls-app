import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:leadcalls/Home.dart';
import 'package:leadcalls/LoginPage.dart';
import 'package:leadcalls/Models/LeadsResponseModule.dart';
import 'package:leadcalls/Res/Colors.dart';
import 'package:leadcalls/Screens/Details.dart';
import 'package:leadcalls/Screens/RelatedToSelection.dart';
import 'package:leadcalls/callsListPage.dart';
import 'package:leadcalls/objects/DropDownCalls.dart';
import 'package:leadcalls/objects/updateRecordCall.dart';
import 'package:leadcalls/utils/Constants.dart';
import 'package:leadcalls/utils/dropDownLists.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

// ignore: must_be_immutable
class CallEdit extends StatefulWidget {
  final String firstnametext,
      recordId,
      createdby,
      modifyby,
      assusrname,
      duration;

  CallEdit(
      {Key key,
      @required this.firstnametext,
      @required this.recordId,
      @required this.createdby,
      @required this.modifyby,
      @required this.assusrname,
      @required this.duration})
      : super(key: key);

  EntryList calls;
  EntryList details;

  @override
  _CallEditState createState() => _CallEditState();
}

class _CallEditState extends State<CallEdit> {
  FlutterLocalNotificationsPlugin fltrNotification;

  TextEditingController _idController;
  TextEditingController _subjectController;
  TextEditingController descController;
  TextEditingController locController;
  TextEditingController createdByController;
  TextEditingController modifyByController;
  TextEditingController assignUserNameController;
  TextEditingController _durationController;

  String selectedStatus1;
  String selectedPopupReminder;
  String _selectedRemainder;
  String selectedDurationHours;
  String selectedDurationMinutes;
  String formattedStartDate;
  String selectedRelatedTo;
  String selectText = '';
  String callRecordId;

  List<String> popup_options = ['please wait..'];
  List<String> remainder_options = ['please wait..'];
  List<String> related_to_options = ['please wait..'];
  List<String> status_options = ['please wait..'];
  List<String> duration_options = ['please wait..'];
  List<String> direction_options = ['please wait..'];

  DateTime dateCreated = DateTime.now();

  @override
  void initState() {
    super.initState();

    getdropdownValues();

    var androidInitialize = new AndroidInitializationSettings('icon');
    var iOSinitialize = new IOSInitializationSettings();
    var initilizationSettings =
        new InitializationSettings(androidInitialize, iOSinitialize);
    fltrNotification = new FlutterLocalNotificationsPlugin();
    fltrNotification.initialize(initilizationSettings,
        onSelectNotification: notificationSelected);

    _subjectController = TextEditingController();
    descController = TextEditingController();
    locController = TextEditingController();
    createdByController = TextEditingController();
    modifyByController = TextEditingController();
    assignUserNameController = TextEditingController();
    _idController = TextEditingController();
    _durationController = TextEditingController();

    _subjectController.text = widget.firstnametext;
    createdByController.text = widget.createdby;
    modifyByController.text = widget.modifyby;
    assignUserNameController.text = widget.assusrname;
    _idController.text = widget.recordId;
    _durationController.text = widget.duration;
  }

  @override
  Widget build(BuildContext context) {
    Future _showNotification() async {
      var androidDetails = new AndroidNotificationDetails(
          "channel Id ", "desi programmer", "this is my channel",
          importance: Importance.Max);
      var iosDetails = new IOSNotificationDetails();
      var generalNotificationDetails =
          new NotificationDetails(androidDetails, iosDetails);

      // await fltrNotification.show(
      //     0, "Reminder", "Schedule Notifications", generalNotificationDetails,payload:'Welcome to the Local Notification demo');

      var scheduledTime = DateTime.now().add(Duration(seconds: 5));
      fltrNotification.schedule(1, "Task", "scheduled Notification",
          scheduledTime, generalNotificationDetails);

      // var date = DateTime.parse(useDateTime);
      // var scheduledTime = date.subtract(Duration(minutes: 15));  // selected popup value write here
      // fltrNotification.schedule(1, "Task", "scheduled Notification", scheduledTime, generalNotificationDetails);
    }

    final Size screenSize = MediaQuery.of(context).size;
    Widget overViewSection = Container(
      child: SingleChildScrollView(
          padding:
              EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0, bottom: 50.0),
          child: Column(
            children: <Widget>[
              Row(
                children: [
                  Flexible(child:
                  TextFormField(
                      controller: _subjectController,
                      decoration:  InputDecoration(
                          hintText: 'Subject', labelText: 'Subject')),),
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Text(
                      'Related To',
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  DropdownButton(
                    value: selectedRelatedTo,
                    items: related_to_options.map((String title) {
                      return DropdownMenuItem(
                        value: title,
                        child: Text(title),
                      );
                    }).toList(),
                    onChanged: (changed) {
                      setState(() {
                        selectedRelatedTo = changed;
                      });
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text(selectText),
                  ),
                  Spacer(),
                  IconButton(
                      icon: Icon(Icons.attachment),
                      onPressed: () async {
                        final text = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    RelatedToSelection(selectedRelatedTo)));
                        setState(() {
                          selectText = text ?? '';
                        });
                      })
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text('Date Created '),
                      Container(
                        height: 10,
                      ),
                      GestureDetector(
                          onTap: () async {
                            await DatePicker.showDateTimePicker(
                              context,
                              minTime: DateTime(1900),
                              currentTime: dateCreated,
                              maxTime: DateTime(2200),
                              onChanged: ((picked) {
                                dateCreated = picked;
                              }),
                            );
                            setState(() {});
                          },
                          child: Text(
                            '$dateCreated',
                            style:
                                TextStyle(color: Colors.black, fontSize: 18.0),
                          )),
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Text(
                      'Duration(Hours/Minutes)',
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: DropdownButton(
                      value: selectedDurationHours,
                      items: duration_options.map((title) {
                        return DropdownMenuItem(
                          value: title,
                          child: Text(title),
                        );
                      }).toList(),
                      onChanged: (changed) {
                        setState(() {
                          selectedDurationHours = changed;
                        });
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: DropdownButton(
                      value: selectedDurationMinutes,
                      items: duration_options.map((title) {
                        return DropdownMenuItem(
                          value: title,
                          child: Text(title),
                        );
                      }).toList(),
                      onChanged: (changed) {
                        setState(() {
                          selectedDurationMinutes = changed;
                        });
                      },
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    'Status',
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
              Row(
                children: [
                  DropdownButton(
                    value: selectedStatus1,
                    items: status_options.map((title) {
                      return DropdownMenuItem(
                        value: title,
                        child: Text(title),
                      );
                    }).toList(),
                    onChanged: (changed) {
                      setState(() {
                        selectedStatus1 = changed;
                      });
                    },
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    'Remainder',
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
              Row(
                children: [
                  Text('Popup Remainder  : '),
                  DropdownButton(
                    value: selectedPopupReminder,
                    items: popup_options.map((String title) {
                      return DropdownMenuItem(
                        value: title,
                        child: Text(title),
                      );
                    }).toList(),
                    onChanged: (changed) {
                      setState(() {
                        selectedPopupReminder = changed;
                        _showNotification();
                      });
                    },
                  ),
                ],
              ),
              Row(
                children: [
                  Text('Email Remainder  : '),
                  DropdownButton(
                    value: _selectedRemainder,
                    items: remainder_options.map((title) {
                      return DropdownMenuItem(
                        value: title,
                        child: Text(title),
                      );
                    }).toList(),
                    onChanged: (changed) {
                      setState(() {
                        _selectedRemainder = changed;
                        _showNotification();
                      });
                    },
                  ),
                ],
              ),
              Row(
                children: [
                  Flexible(
                    child: TextFormField(
                      controller: descController,
                      decoration: InputDecoration(
                        labelText: 'Short Description',
                        hintText: 'Short Description',
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Flexible(
                      child: TextFormField(
                    controller: createdByController,
                    decoration: InputDecoration(
                        labelText: 'Created by', hintText: 'Created by'),
                  )),
                ],
              ),
              Row(
                children: [
                  Flexible(
                      child: TextFormField(
                    controller: modifyByController,
                    decoration: InputDecoration(
                        labelText: 'Modified by', hintText: 'Modified by'),
                  )),
                ],
              ),
              Row(
                children: [
                  Flexible(
                      child: TextFormField(
                    controller: assignUserNameController,
                    decoration: InputDecoration(
                        labelText: 'Assigned by', hintText: 'Assigned by'),
                  )),
                ],
              ),
            ],
          )),
    );
    Widget createButton = Container(
      alignment: Alignment.bottomCenter,
      height: 60,
      width: screenSize.width,
      child: Row(
        children: <Widget>[
          Expanded(
            child: RaisedButton(
              child: Text(
                'Cancel',
                style: new TextStyle(color: Colors.white),
              ),
              onPressed: () => Navigator.of(context).pop(),
              color: Colors.grey,
            ),
          ),
          Expanded(
            child: RaisedButton(
              child: Text(
                'Update',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => onCreateCall(),
              color: Colors.blue,
            ),
          ),
        ],
      ),
      margin: new EdgeInsets.only(top: 20.0),
    );

    Widget body = Stack(
      children: <Widget>[overViewSection, createButton],
    );

    EntryList details;
    return Scaffold(
        appBar: AppBar(
          title: Text('Edit Call'),
        ),
        body: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            overViewSection,
            createButton,
          ],
        ));
  }

  onCreateCall() {
    String subjectValue = _subjectController.text;
    String createdby = createdByController.text;
    String modifyby = modifyByController.text;
    String assignusername = assignUserNameController.text;
    String recordID = _idController.text;
    String durationValue = _durationController.text;

    if (subjectValue.isEmpty && subjectValue.isEmpty) {
      setState(() {
        String desc = "Please enter details";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      });
      return;
    }

    _onSave(subjectValue, durationValue, createdby, modifyby, assignusername,
        recordID);
  }

  _onSave(String subjectValue, String durationValue, String createdby,
      String modifyby, String assignusername, String recordID) async {
    // set up POST request arguments
    String url =
        'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    String callString =
        '{"session":"$userSession","module_name":"Calls","name_value_list":[{"name":"id","value":"$recordID"},{"name":"name","value":"$subjectValue"},{"name":"assigned_user_id","value":"$userID"},{"name":"date_start","value":""},{"name":"duration_hours","value":""},{"name":"parent_type","value":""},{"name":"status","value":""},{"name":"description","value":""},{"name":"date_entered","value":"description"},{"name":"date_modified","value":""},{"name":"created_by_name","value":"$createdby"},{"name":"modified_by_name","value":"$modifyby"},{"name":"assigned_user_name","value":"$assignusername"}]}';

    Map<String, Object> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "set_entry",
      "rest_data": "$callString"
    };

    FormData formData = FormData.fromMap(body);
    // make POST request
    Dio dio = new Dio();

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // Do something before request is sent
      print(options.data);
      print(options);
      return options; //continue
    }, onResponse: (Response response) async {
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
//    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());

      if (responseMap != null) {
        UpdateRecordCall callresponse =
            UpdateRecordCall.fromJson(json.decode(response.toString()));

        String desc = "Update Successfully";
        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.greenAccent,
            textColor: Colors.black,
            fontSize: 16.0);

        Navigator.pushNamed(context, CallsListPage.id);
      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    }
  }

  // ignore: missing_return
  String generateMd5(String input) {
    /////    return md5.convert(utf8.encode(input)).toString();
  }

  Future<dynamic> readResponse(HttpClientResponse response) {
    var completer = new Completer();
    var contents = new StringBuffer();
    response.transform(utf8.decoder).listen((data) {
      contents.write(data);
    }, onDone: () => completer.complete(contents.toString()));
    return completer.future;
  }

  Future<void> getdropdownValues() async {
    //loading(context);
    // set up POST request arguments
    String url =
        'http://uat.ideadunes.com/projects/devs/testapi_giproperties/call_dom.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    int offset = 0;
    String restData =
        '{"session":"$userSession","module_name":"Calls","query":"assigned_user_id=\'$userID\'","order_by":"status","offset":"$offset","select_fields":[],"link_name_to_fields_array":[],"max_results":"50","deleted":"0","Favorites":false}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_module_fields",
      "rest_data": "$restData"
    };

    FormData formData = FormData.fromMap(body);
    Dio dio = new Dio();
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      return options; //continue
    }, onResponse: (Response response) async {
      return response; // continue
    }, onError: (DioError e) async {
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());
      if (responseMap != null) {
        ///write a code from response
        ///write a code from response
        DropDownCalls dropDowncalls =
            await DropDownCalls.fromJson(jsonDecode(response.toString()));
        setState(() {
          popup_options = dropDowncalls.moduleFields.reminderTime.options;
          selectedPopupReminder = popup_options[0];

          remainder_options =
              dropDowncalls.moduleFields.emailReminderTime.options;
          _selectedRemainder = remainder_options[0];

          status_options = dropDowncalls.moduleFields.status.options;
          selectedStatus1 = status_options[0];

          related_to_options = dropDowncalls.moduleFields.parentName.options;
          selectedRelatedTo = related_to_options[0];

          duration_options = dropDowncalls.moduleFields.durationMinutes.options;
          selectedDurationMinutes = duration_options[0];
          selectedDurationHours = duration_options[0];

          // subproperty_options = dropDowncalls.moduleFields.subPropertyTypeC.options;
          // selectedSubPropertyType = subproperty_options[0];
        });
      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);

        if (desc.contains("session ID is invalid")) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
            ModalRoute.withName('/'),
          );
        }
      }
    }
  }

  Future notificationSelected(String payload) async {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text("Notification clicked $payload"),
      ),
    );
  }
}
