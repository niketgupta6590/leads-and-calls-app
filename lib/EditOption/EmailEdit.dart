// import 'package:flutter/material.dart';
// import 'package:leadcalls/Res/Colors.dart';
// import 'package:leadcalls/objects/emails.dart';
//
// class EmailEdit extends StatefulWidget {
//
//   final String firstnametext,recordId,createdby,modifyby,assusrname,duration;
//
//   EmailEdit({Key key, @required this.firstnametext, @required this.recordId, @required this.createdby
//     ,@required this.modifyby, @required this.assusrname,@required this.duration}) : super(key: key);
//
//   EntryList calls;
//
//
//   @override
//   _EmailEditState createState() => _EmailEditState();
// }
//
// class _EmailEditState extends State<EmailEdit> {
//
//   String callRecordId;
//
//   TextEditingController _idController;
//   TextEditingController _fromController;
//   TextEditingController _toController;
//   TextEditingController locController;
//   TextEditingController createdByController;
//   TextEditingController modifyByController;
//   TextEditingController assignUserNameController;
//   TextEditingController _durationController;
//
//   String selectedRelatedTo;
//   DateTime dateCreated = DateTime.now();
//
//   bool _isVisible = false;
//
//
//   @override
//   void initState() {
//     super.initState();
//
//
//
//   //  selectedRelatedTo = DropDownLists.relatedTo[0];
//
//     _fromController = TextEditingController();
//   //  descController = TextEditingController();
//     _toController = TextEditingController();
//     createdByController = TextEditingController();
//     modifyByController = TextEditingController();
//     assignUserNameController = TextEditingController();
//     _idController = TextEditingController();
//     _durationController = TextEditingController();
//
//     _fromController.text = widget.firstnametext;
//     _toController.text = widget.firstnametext;
//     createdByController.text = widget.createdby;
//     modifyByController.text = widget.modifyby;
//     assignUserNameController.text = widget.assusrname;
//     _idController.text = widget.recordId;
//     _durationController.text = widget.duration;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Create Email'),
//       ),
//       body: Container(
//         padding: EdgeInsets.all(10.0),
//         child: _fromController.text != null
//             ? ListView(physics: ClampingScrollPhysics(), children: [
//           Row(
//             children: [
//               Padding(
//                 padding: const EdgeInsets.only(right: 12.0),
//                 child: Text(
//                   'From',
//                   style: TextStyle(fontSize: 18.0),
//                 ),
//               ),
//               Flexible(
//                 child: TextFormField(
//                   controller: _fromController,
//                   decoration: InputDecoration(
//                       border: InputBorder.none,
//                       focusedBorder: InputBorder.none,
//                       enabledBorder: InputBorder.none),
//                 ),
//               ),
//             ],
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceAround,
//             children: [
//               Padding(
//                 padding: const EdgeInsets.only(right: 12.0),
//                 child: Text(
//                   'To',
//                   style: TextStyle(fontSize: 18.0),
//                 ),
//               ),
//               Flexible(
//                 child: TextFormField(
//                   controller: _toController,
//                   decoration: InputDecoration(
//                       border: InputBorder.none,
//                       focusedBorder: InputBorder.none,
//                       enabledBorder: InputBorder.none),
//                 ),
//               ),
//               IconButton(
//                 icon: Icon(
//                     _isVisible ? Icons.expand_less : Icons.expand_more),
//                 onPressed: showToast,
//               ),
//             ],
//           ),
//           Container(
//             width: double.infinity,
//             height: 0.3,
//             color: cBlack.withOpacity(0.4),
//           ),
//           Visibility(
//             visible: _isVisible,
//             child: Row(
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.only(right: 12.0),
//                   child: Text(
//                     'Cc',
//                     style: TextStyle(fontSize: 18.0, color: Colors.grey),
//                   ),
//                 ),
//                 Flexible(
//                   child: TextFormField(
//                     controller: _ccController,
//                     decoration: InputDecoration(
//                         border: InputBorder.none,
//                         focusedBorder: InputBorder.none,
//                         enabledBorder: InputBorder.none),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           Visibility(
//             visible: _isVisible,
//             child: Row(
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.only(right: 12.0),
//                   child: Text(
//                     'Bcc',
//                     style: TextStyle(fontSize: 18.0, color: Colors.grey),
//                   ),
//                 ),
//                 Flexible(
//                   child: TextFormField(
//                     controller: _bccController,
//                     decoration: InputDecoration(
//                         border: InputBorder.none,
//                         focusedBorder: InputBorder.none,
//                         enabledBorder: InputBorder.none),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           Row(
//             children: [
//               Padding(
//                 padding: const EdgeInsets.only(right: 12.0),
//                 child: Text(
//                   'Subject',
//                   style: TextStyle(fontSize: 18.0),
//                 ),
//               ),
//               Flexible(
//                 child: TextFormField(
//                   controller: _subjectController,
//                   decoration: InputDecoration(
//                       border: InputBorder.none,
//                       focusedBorder: InputBorder.none,
//                       enabledBorder: InputBorder.none),
//                 ),
//               ),
//             ],
//           ),
//           Container(
//             width: double.infinity,
//             height: 0.3,
//             color: cBlack.withOpacity(0.4),
//           ),
//           Row(
//             children: [
//               Padding(
//                 padding: const EdgeInsets.only(right: 12.0),
//                 child: Text(
//                   'Compose email',
//                   style: TextStyle(fontSize: 18.0),
//                 ),
//               ),
//               Flexible(
//                 child: TextFormField(
//                   controller: _bodyController,
//                   decoration: InputDecoration(
//                       border: InputBorder.none,
//                       focusedBorder: InputBorder.none,
//                       enabledBorder: InputBorder.none),
//                 ),
//               ),
//             ],
//           ),
//           Container(
//             width: double.infinity,
//             height: 0.3,
//             color: cBlack.withOpacity(0.4),
//           ),
//           Container(
//             height: 12,
//           ),
//           RaisedButton(
//             child: Text('Send'),
//             color: Colors.blue,
//             textColor: Colors.white,
//             onPressed: () => onCreateEmail(),
//           )
//         ])
//             : Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               const CircularProgressIndicator(),
//               SizedBox(
//                 height: 10,
//               ),
//               Text('Please wait while we get your contacts...')
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
