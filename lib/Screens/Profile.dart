import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'dart:math';
import 'dart:ui';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:leadcalls/Models/LeadsResponseModule.dart';
import 'package:leadcalls/Models/UserDetailsResponse.dart';
import 'package:leadcalls/Network/Apis.dart';
import 'package:leadcalls/Network/Apis.dart';
import 'package:leadcalls/Network/Apis.dart';
import 'package:leadcalls/Network/ServiceGenerator.dart';
import 'package:leadcalls/Temps.dart';
import 'package:leadcalls/objects/UpdateLeadRecord.dart';
import 'package:leadcalls/objects/UserResponse.dart';
import 'package:leadcalls/utils/ColorConstants.dart';
import 'package:path/path.dart';

// ignore: camel_case_types
class Edit_profile extends StatefulWidget {
   static const id = 'profile';
  // EntryList details;
  // String recordId;
  // String selectedModule;
  // Map<String, String> labelValues = {};
  // bool hasData = false;

  @override
  State<StatefulWidget> createState() {
    return _Edit_Profile();
  }
}

// class User {
//   const User(this.name);
//   final String name;
// }
//
// class Remainder {
//   const Remainder(this.name);
//   final String name;
// }

class _Edit_Profile extends State<Edit_profile> {
  File _image;
  var imageFile;
  // User selectedUser;
  // Remainder selectedRemainder;
  // List<User> users = <User>[const User('Foo'), const User('Bar')];
  // List<Remainder> remainder = <Remainder>[
  //   const Remainder('popup'),
  //   const Remainder('email invitees')
  // ];

  TextEditingController
      firstNameCtrl = TextEditingController(),
      lastNameCtrl = TextEditingController(),
      phoneNoCtrl = TextEditingController(),
      timeZoneCtrl = TextEditingController(),
      departmentCtrl = TextEditingController(),
      statusCtrl = TextEditingController(),
      usernameCtrl = TextEditingController(),
      jobTitleCtrl = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  FormData restData() {
    String userAuthString =
        '{"session":"$tUserSession","module_name":"Users","id":"$tUserId","select_fields":[],"link_name_to_fields_array":[],"track_view":false}';
    Map<String, Object> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_entry",
      "rest_data": "$userAuthString"
    };
    return FormData.fromMap(body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Profile'),
        ),
        body: FutureBuilder(
            future: ServiceGenerator.getReqWithParams(Apis.rest, restData()),
            // ignore: missing_return
            builder: (context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else if (snapshot.hasData) {
                final PEntryList entryListData = UserDetailsResponse
                    .fromJson(json.decode(snapshot.data.toString()))
                    .entryList[0];
                usernameCtrl.text = entryListData.nameValueList.userName.value;
                firstNameCtrl.text = entryListData.nameValueList.firstName.value;
                lastNameCtrl.text = entryListData.nameValueList.lastName.value;
                phoneNoCtrl.text = entryListData.nameValueList.phoneMobile.value;
                departmentCtrl.text = entryListData.nameValueList.department.value;
                jobTitleCtrl.text = entryListData.nameValueList.title.value;
                statusCtrl.text = entryListData.nameValueList.status.value;

                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 40.0),
                          child: Container(
                            child: Text(
                              "Edit Profile",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.black87),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: CircleAvatar(
                            radius: 35,
                            backgroundColor: Colors.white,
                            child: Text(
                               'W',
                              style: TextStyle(
                                  fontSize: 20.0,
                                  color: ColorConstants.primaryColor),
                            ),
                          )
                        ),
                        Row(
                          children: <Widget>[
                            Text("User Name"),
                            Flexible(
                              child: Padding(
                                padding: EdgeInsets.only(left: 12.0),
                                child: TextField(
                                  controller: usernameCtrl,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text("First Name"),
                            Flexible(
                              child: Padding(
                                padding: EdgeInsets.only(left: 12.0),
                                child: TextField(
                                  controller: firstNameCtrl,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text("Last Name"),
                            Flexible(
                              child: Padding(
                                padding: EdgeInsets.only(left: 12.0),
                                child: TextField(
                                  controller: lastNameCtrl,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text("Phone No"),
                            Flexible(
                              child: Padding(
                                padding: EdgeInsets.only(left: 12.0),
                                child: TextField(
                                controller: phoneNoCtrl,
                              ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text("Department"),
                            Flexible(
                              child: Padding(
                                padding: EdgeInsets.only(left: 12.0),
                                child: TextField(
                                controller: departmentCtrl,
                              ),
                            ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text("Job Title"),
                            Flexible(
                              child: Padding(
                                padding: EdgeInsets.only(left: 12.0),
                                child: TextField(
                                controller: jobTitleCtrl,

                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text("Status"),
                            Flexible(
                              child: Padding(
                                padding: EdgeInsets.only(left: 12.0),
                                child: TextField(
                                controller: statusCtrl,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          width: 400,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: RaisedButton(
                              color: Colors.cyan,
                              onPressed: () => _onSave(),
                              child: Text('Save'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }
            }));
  }

  _imgFromCamera() async {
    _image = await ImagePicker.pickImage(source: ImageSource.camera,imageQuality: 50);
    setState(() {
      _image = _image;
      print(_image);
      print('----------');
    });
  }

  _imgFromGallery() async {

      _image = await ImagePicker.pickImage(source: ImageSource.gallery,imageQuality: 50);
      setState(() {
        _image = _image;
        print(_image);
        print('----------');
      });

  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child:  Wrap(
                children: <Widget>[
                   ListTile(
                      leading:  Icon(Icons.photo_library),
                      title:  Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                   ListTile(
                    leading:  Icon(Icons.photo_camera),
                    title:  Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  _onSave(){
    String usernameValue = usernameCtrl.text;
    String firstValue = firstNameCtrl.text;
    String lastValue = lastNameCtrl.text;
    String departmentValue = departmentCtrl.text;
    String jobtitleValue = jobTitleCtrl.text;
    String phoneValue = phoneNoCtrl.text;
    String statusValue = statusCtrl.text;
   // String file = _image.toString();


    _onUpload(firstValue, lastValue, usernameValue,departmentValue,jobtitleValue,phoneValue,statusValue);
}

   _onUpload(String firstValue, String lastValue, String username, String department, String jobtitle, String phone, String status) async

  {

    String url = 'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';

  String restData = '{"session":"$tUserSession","module_name":"Users","name_value_list":[{"name":"id","value":"$tUserId"},{"name":"first_name","value":"$firstValue"},{"name":"last_name","value":"$lastValue"},{"name":"title","value":"$jobtitle"},{"name":"department","value":"$department"},{"name":"user_name","value":"$username"},{"name":"phone_mobile","value":"$phone"},{"name":"status","value":"$status"}]}';

  Map<String, dynamic> body = {
    "input_type": "JSON",
    "response_type": "JSON",
    "method": "set_entry",
    "rest_data": "$restData"
  };

  print('===========');
  print(restData);

  FormData formData = FormData.fromMap(body);

  Dio dio = new Dio();
    dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      print(options.data);
      print(options);
      return options; //continue
    }, onResponse: (Response response) async {
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      return e; //continue
    }));
  Response response = await dio.post(url, data: formData);
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());
  if (responseMap != null) {
    UserDetailsResponse detailsResponse = UserDetailsResponse.fromJson(json.decode(response.toString()));

    print('----------uploaded data------');
    print(detailsResponse.toString());
    String desc = "User Details Updated";

    Fluttertoast.showToast(
        msg: "$desc",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.greenAccent,
        textColor: Colors.white,
        fontSize: 16.0);
  } else {
    String desc = 'Error occured';

    Fluttertoast.showToast(
        msg: "$desc",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

    }
  }

  String generateMd5(String input) {
    /////    return md5.convert(utf8.encode(input)).toString();
  }

  Future<dynamic> readResponse(HttpClientResponse response) {
    var completer = Completer();
    var contents = StringBuffer();
    response.transform(utf8.decoder).listen((data) {
      contents.write(data);
    }, onDone: () => completer.complete(contents.toString()));
    return completer.future;
  }








  Future<dynamic> _upload() async {

    //  ServiceGenerator.getReqWithParams(Apis.rest,restData());

      if (_image == null) return;
      String fileName = _image.path.split('/').last;
      print(fileName);
      print(fileName.toString());
      print('---------------------------');
      Map<String, dynamic> formData = {
        "image": await MultipartFile.fromFile(_image.path,filename: fileName),
      };
      String url = 'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';
      return await Dio()
          .post(url,data:formData).
      then((dynamic result){
        print('------------result file path-------');
        print(result.toString());
        String results = result.toString();
        print(results.toString());
      });


    }
}



//Center(
//                             child: GestureDetector(
//                               onTap: () {
//                                 _showPicker(context);
//                               },
//                               child: CircleAvatar(
//                                 radius: 52,
//                                 backgroundColor: Colors.black87,
//                                 child: _image != null
//                                     ? ClipRRect(
//                                   borderRadius:
//                                   BorderRadius.circular(50),
//                                   child: Image.file(
//                                     _image,
//                                     width: 100,
//                                     height: 100,
//                                     fit: BoxFit.fitHeight,
//                                   ),
//                                 )
//                                     : Container(
//                                   decoration: BoxDecoration(
//                                       color: Colors.grey[200],
//                                       borderRadius:
//                                       BorderRadius.circular(50)),
//                                   width: 100,
//                                   height: 100,
//                                   child: Icon(
//                                     Icons.camera_alt,
//                                     color: Colors.grey[800],
//                                   ),
//                                 ),
//                               ),
//                             ),
//                           ),


  //--------------------------------------------------------------------------

