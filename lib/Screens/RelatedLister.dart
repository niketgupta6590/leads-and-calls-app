import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:leadcalls/Models/LeadsResponseModule.dart';
import 'package:leadcalls/Network/Apis.dart';
import 'package:leadcalls/Network/ServiceGenerator.dart';
import 'package:leadcalls/utils/ColorConstants.dart';
import '../Temps.dart';
import 'Details.dart';

class RelatedLister extends StatefulWidget {

  RelatedLister(this.selectedModule,this.mainmodule,this.recordId);
  final String selectedModule;
  final String mainmodule;
  final String recordId;

  @override
  _RelatedListerState createState() => _RelatedListerState();
}

class _RelatedListerState extends State<RelatedLister> {
  String selectedModule;
  String selectedModuleotherletters;
  String mainmodule;
  String recordId;

  FormData restData() {
    selectedModule = widget.selectedModule;
    selectedModuleotherletters = selectedModule.substring(0,1).toUpperCase()+selectedModule.substring(1);
    mainmodule = widget.mainmodule;
    recordId = widget.recordId;

    String restData =
        '{"session":"$tUserSession","module_name":"$mainmodule","module_id":"$recordId","link_field_name":"$selectedModule","related_module_query":"","related_fields":["id","name"],"related_module_link_name_to_fields_array":[],"deleted":0,"order_by":"","offset":0,"limit":false}';
    print('-----------------------------------------------RESTDATA-----------------------------------------------------------------');
    print(restData);

    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_relationships",
      "rest_data": "$restData"
    };

    return FormData.fromMap(body);
  }

  Widget getListByModule(dynamic data){
    List<EntryList> entryList = LeadsResponseModule.fromJson(jsonDecode(data.toString())).entryList;
    return entryList != null ? ListView.builder(
      itemCount: entryList.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: ()  {
            Navigator.pop(context, entryList[index]);
            print(entryList[index]);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              color:
              Colors.white ,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Not Found',
                            textAlign: TextAlign.end,
                          ),
                        ),
                      ],
                    ),
                    ListTile(
                      title: Text(
                          '${entryList[index].nameValueList.name.value}'),
                    ),
                    // ListTile(
                    //   title: Text(
                    //       '${entryList[index].nameValueList.dateEntered.value}') ?? 'not found'
                    //   // trailing: Text('${entryList[index].nameValueList.status.value}') ,
                    // ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    )
        : 'Data Not Found';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${widget.selectedModule}'),
      ),
      body: SafeArea(
        child: FutureBuilder(
            future: ServiceGenerator.getReqWithParams(Apis.rest,restData()),
            builder: (context, snapshot){
              if(snapshot.connectionState == ConnectionState.waiting){
                return Center(child: CircularProgressIndicator());
              }
              else if(snapshot.hasData){
                return getListByModule(snapshot.data);
              }
              else{
                return Center(child: Text('Something went wrong...'));
              }
            }
        ),
      ),
    );
  }
}


