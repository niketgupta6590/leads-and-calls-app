import 'package:flutter/material.dart';

ThemeData darkTheme = ThemeData.dark().copyWith(
    primaryColor: Color(0xff1f655d),
    accentColor: Color(0xff40bf7a),
    buttonTheme: ButtonThemeData(
      buttonColor: Colors.red,
      textTheme: ButtonTextTheme.primary,
    ),
    textTheme: TextTheme(
        headline1: TextStyle(color: Color(0xff40bf7a)),
        headline2: TextStyle(color: Color(0xff40bf7a)),
        headline3: TextStyle(color: Color(0xff40bf7a))
    ),
    appBarTheme: AppBarTheme(color: Color(0xff1f655d)
    )
);

ThemeData lightTheme = ThemeData.light().copyWith(
    primaryColor: Color(0xff40bf7a),
    accentColor: Color(0xff40bf7a),
    appBarTheme: AppBarTheme(
        color: Color(0xff1f655d),
        actionsIconTheme: IconThemeData(color: Colors.white)
    )
);

final bluePrimary = Color(0xFF3F51B5);
final blueAccent = Color(0xFFFF9800);

final blueTheme = ThemeData(
  primaryColor: bluePrimary,
  accentColor: blueAccent,
);


ThemeData redTheme = ThemeData.light().copyWith(
    primaryColor: Color(0xFFE57373),
    accentColor: Color(0xFFE57373),
    appBarTheme: AppBarTheme(
        color: Colors.red,
        actionsIconTheme: IconThemeData(color: Colors.white)));

ThemeData greenTheme = ThemeData.light().copyWith(
    primaryColor: Color(0xff20c6b6),
    accentColor: Color(0xff20c6b6),
    appBarTheme: AppBarTheme(
        color: Color(0xff20c6b6),
        actionsIconTheme: IconThemeData(color: Colors.white)));
