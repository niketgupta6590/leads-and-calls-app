import 'package:flutter/material.dart';
import 'package:leadcalls/utils/ColorConstants.dart';

import 'SetNewPassword.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {

  TextEditingController emailIdorMobile = TextEditingController();
  TextEditingController sendOTP = TextEditingController();
  TextEditingController resendOTP = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
            'Forgot Password',
            style: TextStyle(
                color: Colors.white,
            fontWeight: FontWeight.w400,
            fontSize: 20),
        ),

       // textColor: Colors.white,
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(

            children: <Widget>[
        Container(
        width: double.infinity,
          alignment: Alignment.center,
          height: 100,
          color: ColorConstants.primaryColor,
          child: Padding(
              padding: const EdgeInsets.all(16.0),
              child:
              Image.asset('images/lock.png', color: Colors.white, height: 75,)

          ),
        ),
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  child: Text(
                    'Forgot Your Password?',
                    style: TextStyle(
                        color: ColorConstants.primaryColor,
                        fontWeight: FontWeight.w400,
                        fontSize: 30),
                  )),
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  child: Text(
                    'If you have forgot your password',
                    style: TextStyle(
                       // color: ColorConstants.primaryColor,
                        fontSize: 15),
                  )),
               Container(
                   padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                   child: TextField(
                   controller: emailIdorMobile,
                   decoration: InputDecoration(
                   border: OutlineInputBorder(),
                   labelText: 'Enter EmailId or Mobile No.',
                  ),
                ),
              ),
                Container(
                   padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                   child: TextField(
                   controller: sendOTP,
                     decoration: InputDecoration(
                       border: OutlineInputBorder(),
                    labelText: 'Enter OTP',
                  ),
                ),
              ),

               Container(
                  height: 50,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: RaisedButton(
                  textColor: Colors.white,
                  color: ColorConstants.primaryColor,
                  child: Text('Resend OTP'),
                    onPressed: () {
                      Navigator.pushReplacement(context,
                     new PageRouteBuilder(pageBuilder: (_, __, ___) => SetNewPassword()));
              },
           )),
       ],
     )));
  }
}
