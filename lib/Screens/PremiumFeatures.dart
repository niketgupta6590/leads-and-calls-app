import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:leadcalls/utils/ColorConstants.dart';

class PremiumFeatures extends StatefulWidget {

  _PremiumFeaturesState createState() => _PremiumFeaturesState();
}

class _PremiumFeaturesState extends State<PremiumFeatures> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle()
        .copyWith(statusBarColor: ColorConstants.primaryColorDark));
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
      child: Scaffold(
      appBar: AppBar(
        title: Text(
          'Premium Features',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w400, fontSize: 20),
        ),
        bottom: TabBar(
          tabs:<Widget> [
            Text('By Feature'),
            Text('By Product Item')
          ],
        ),
      ),
        body: Padding(
          padding: EdgeInsets.all(8),
          child: ListView(
              children: ListTile.divideTiles(
                  context: context,
                  tiles: [
              ListTile(
              trailing: Icon(Icons.keyboard_arrow_right),
              leading: Icon(Icons.edit),
              title: Text('Create, edit and delete records'),
          ),
                    ListTile(
                      trailing: Icon(Icons.keyboard_arrow_right),
                      leading: Icon(Icons.add_box),
                      title: Text('Use custom modules and custom fields'),
                    ),
                    ListTile(
                      trailing: Icon(Icons.keyboard_arrow_right),
                      leading: Icon(Icons.keyboard_arrow_right),
                      title: Text('Common modules'),
                    ),
                    ListTile(
                      trailing: Icon(Icons.keyboard_arrow_right),
                      leading: Icon(Icons.keyboard_arrow_right),
                      title: Text('Sugar dashlets'),
                    ),
                    ListTile(
                      trailing: Icon(Icons.keyboard_arrow_right),
                      leading: Icon(Icons.calendar_today),
                      title: Text('Calendar'),
                    ),
                    ListTile(
                      trailing: Icon(Icons.keyboard_arrow_right),
                      leading: Icon(Icons.blur_circular),
                      title: Text('App localization'),
                    ),
                    ListTile(
                      trailing: Icon(Icons.keyboard_arrow_right),
                      leading: Icon(Icons.keyboard_arrow_right),
                      title: Text('Call logging'),
                    ),
                    ListTile(
                      trailing: Icon(Icons.keyboard_arrow_right),
                      leading: Icon(Icons.edit_attributes),
                      title: Text('Inline editing'),
                    ),
                    ListTile(
                      trailing: Icon(Icons.keyboard_arrow_right),
                      leading: Icon(Icons.add_location),
                      title: Text('Lod location'),
                    ),
                    ListTile(
                      trailing: Icon(Icons.keyboard_arrow_right),
                      leading: Icon(Icons.keyboard_arrow_right),
                      title: Text('Import or export phone contact to sugar'),
                    ),
        // body: TabBarView(
        //   children: [
        //     Text('by features1'),
        //     Text('by features2')
           ],
        ).toList(),
      ),
      ),
      ),
    ),
    );
  }
}
