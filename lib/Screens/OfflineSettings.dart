import 'package:flutter/material.dart';



class OfflineSettings extends StatefulWidget {
  @override
  _OfflineSettingsState createState() => _OfflineSettingsState();
}


class _OfflineSettingsState extends State<OfflineSettings> {
  bool _isSelected = false;

  void _onChanged(bool value) {
    setState(() {
      _isSelected = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          'Offline Settings',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w400, fontSize: 20),
        ),
      ),
        body: Padding(
          padding: EdgeInsets.all(8),
          child: ListView(
            children: ListTile.divideTiles(
                context: context,
                tiles: [
                  SwitchListTile(
                      value: _isSelected,
                      title: Text('Enable Offline Editing'),
                      secondary: Icon(Icons.offline_bolt),
                      onChanged: (bool value) {
                        _onChanged(value);
                      }),
                  ListTile(
                    trailing: Icon(Icons.keyboard_arrow_right),
                    leading: Icon(Icons.offline_pin),
                    title: Text('Pending Offline Update'),
                  ),
                  ListTile(
                    trailing: Icon(Icons.keyboard_arrow_right),
                    leading: Icon(Icons.cached),
                    title: Text('Manage Cache'),
                  ),
                  ListTile(
                    leading: Icon(Icons.cached),
                    title: Text('1.5742 KB '),
                    subtitle: Text('Database File Size'),
                  ),
        ],
    ).toList(),
          ),

    ),
    );

  }
}
