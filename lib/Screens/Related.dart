import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:leadcalls/Models/LeadsResponseModule.dart';
import 'package:leadcalls/Network/Apis.dart';
import 'package:leadcalls/Network/ServiceGenerator.dart';
import 'package:leadcalls/Screens/Lister.dart';
import 'package:leadcalls/Screens/RelatedLister.dart';
import 'package:leadcalls/utils/ColorConstants.dart';
import '../Temps.dart';
import 'Details.dart';
import 'package:expandable/expandable.dart';

class Related extends StatefulWidget {
  Related(this.moduleName, this.recordId);
  final String moduleName;
  final String recordId;

  _RelatedState createState() => _RelatedState();
}

class _RelatedState extends State<Related> {
  bool hasData = false;
  String selectedModule;
  String recordId;

  List<String> names = [], values = [], type = [];
  Map<String, dynamic> maps = {};

  @override
  void initState() {
    super.initState();
    getRelatedModules();
  }

  Future<void> getRelatedModules() async {
    recordId = widget.recordId;
    selectedModule = widget.moduleName;
    String restData =
        '{"session":"$tUserSession","module_name":"$selectedModule","fields":[]}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_module_fields",
      "rest_data": "$restData"
    };
    try {
      final namesM = await ServiceGenerator.getReqWithParams(
          Apis.rest, FormData.fromMap(body));
      final Map<String, dynamic> namesModuleJson =
          jsonDecode(namesM.toString());
      Map<String, String> names_modules = {};
      final moduleNValue = namesModuleJson['link_fields'];
      (moduleNValue as Map<String, dynamic>).forEach((key2, value2) {
         print(moduleNValue);
        if (value2 is! String) {
          (value2 as Map<String, dynamic>).forEach((key3, value3) {
            if (key3 == 'type' && value3 == 'link') {
              type.add(value3);
              print(type);

            }
            if (key3 == 'name') {
              names.add(value3);
              print(names);

            }
          });
        }
      });

      print(
          '======================================================================');

      for (int i = 0; i < names.length; i++) {
        names_modules[names[i]] = type[i];
      }
      // print(names_modules);

      setState(() {
        hasData = true;
      });
    } catch (err) {
      throw err;
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return ExpandableListView(
              title: names[index],
              mainModule: selectedModule,
              recordId: recordId);
        },
        itemCount: names.length,
      ),
    );
  }
}

// return !hasData
//     ? Center(child: CircularProgressIndicator())
//     : ListView.builder(
//         itemCount: names.length,
//         itemBuilder: (BuildContext context, int index) {
//           return RelatedTile(
//               name: names[index],
//               index: index,
//               text: selectedModule,
//               recordId: recordId);
//         },
//       );

class ExpandableListView extends StatefulWidget {
  final String title;
  final String mainModule;
  final String recordId;

  const ExpandableListView(
      {Key key, this.title, this.mainModule, this.recordId})
      : super(key: key);

  @override
  _ExpandableListViewState createState() => _ExpandableListViewState();
}

class _ExpandableListViewState extends State<ExpandableListView> {
  bool expandFlag = false;
  List<EntryList> entrylist = [];

  @override
  Widget build(BuildContext context) {
    final sWidth = MediaQuery.of(context).size.width;

    Column _buildBottomNavigationMenu() {
      return Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text("Add a related Calls by"),
          ),
          ListTile(
            title: Text("Create"),
            onTap: () {},
          ),
          ListTile(
              title: Text("Select"),
              onTap: () async {
                final EntryList entry = await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => RelatedLister(
                            widget.title, widget.mainModule, widget.recordId)));

                setState(() {
                  entrylist.add(entry);
                });
              })
        ],
      );
    }

    void _onButtonPressed() {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              color: Color(0xFF737373),
              height: 140,
              child: Container(
                child: _buildBottomNavigationMenu(),
                decoration: BoxDecoration(
                    color: Theme.of(context).canvasColor,
                    borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(10),
                      topRight: const Radius.circular(10),
                    )),
              ),
            );
          });
    }

    return Container(
      margin: EdgeInsets.only(top: 5.0),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5.0),
            child: SingleChildScrollView(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 20.0, left: 10.0),
                    child: CircleAvatar(
                        radius: sWidth * 0.04,
                        child: Text(widget.title.split('')[0].toUpperCase()),
                        backgroundColor:
                            ColorConstants.primaryColor.withOpacity(0.4)),
                  ),
                  Text(
                    widget.title,
                    style:  TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
                  ),
                  Spacer(),
                  IconButton(
                    icon: Icon(Icons.add),
                    onPressed: () => _onButtonPressed(),
                  ),
                  Spacer(),
                  IconButton(
                      icon: Container(
                        height: 50.0,
                        width: 50.0,
                        child: Center(
                          child: Icon(
                            expandFlag
                                ? Icons.keyboard_arrow_up
                                : Icons.keyboard_arrow_down,
                            color: Colors.black,
                            size: 30.0,
                          ),
                        ),
                      ),
                      onPressed: () {
                        setState(() {
                          expandFlag = !expandFlag;
                        });
                      }),
                ],
              ),
            ),
          ),
          ExpandableContainer(
              expanded: expandFlag,
              child: entrylist!=null ? ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 1.0, color: Colors.greenAccent),
                        color: Colors.white12),
                    child: ListTile(
                      title: Text(
                        '${entrylist[index].nameValueList.name.value}',
                        style: TextStyle(
                            color: Colors.black),
                      ),
                      // leading: Text(
                      //   '${entrylist[index].nameValueList.dateEntered.value}',
                      //   style: TextStyle(
                      //       fontWeight: FontWeight.bold, color: Colors.black),
                      // ),
                    ),
                  );
                },
                itemCount: entrylist.length,
              ):
                  'Data Not Available'
          ),

        ],
      ),
    );
  }
}

class ExpandableContainer extends StatelessWidget {
  final bool expanded;
  final double collapsedHeight;
  final double expandedHeight;
  final Widget child;

  ExpandableContainer({
    @required this.child,
    this.collapsedHeight = 0.0,
    this.expandedHeight = 200.0,
    this.expanded = true,
  });

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      curve: Curves.easeInOut,
      width: screenWidth,
      height: expanded ? expandedHeight : collapsedHeight,
      child: Container(
        child: child,
            decoration:  BoxDecoration(border:  Border.all(width: 0.10, color: Colors.blue)),
      ),
    );
  }

/*class RelatedTile extends StatefulWidget {
  // RelatedTile(this.name);
  // final String selecteModule;
  const RelatedTile({
    @required this.name,
    @required this.index,
    @required this.text,
    @required this.recordId,
    Key key,
  }) : super(key: key);

  final String recordId;
  final String text;
  final String name;
  final int index;

  @override
  _RelatedTileState createState() => _RelatedTileState();
}

class _RelatedTileState extends State<RelatedTile> {
  bool showDetails = false;
  // String selectedModule = widget.name;
  List<EntryList> entryList = [];

  FormData restData() {
    // selectedModule = widget.moduleName;
    String restData = '';
    ////  '{"session":"$tUserSession","module_name":"module name","query":"${selectedModule.toLowerCase() + '.'}assigned_user_id=\'$tUserId\'"}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_entry_list",
      "rest_data": "$restData"
    };
    print(restData);
    return FormData.fromMap(body);
  }

  Widget getListByModule(data) {
    // List<EntryList> entryList =
    //     LeadsResponseModule.fromJson(jsonDecode(data.toString())).entryList;
    return entryList != null
        ? Expanded(
            child: ListView.builder(
              itemCount: 5,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.pop(context, entryList[index]);
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    'not found',
                                    textAlign: TextAlign.end,
                                  ),
                                ),
                              ],
                            ),
                            ListTile(
                              title: Text('nbdhb'),
                              // trailing: Text(
                              //     '${entryList[index].nameValueList.status.value}'),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          )
        : 'Data Not Found';
  }

  @override
  Widget build(BuildContext context) {
    final sWidth = MediaQuery.of(context).size.width;
    bool expandFlag = false;

    return Scaffold(
      body : Container(
        margin:  EdgeInsets.symmetric(vertical: 1.0),
        child:  Column(
          children: <Widget>[
            Container(
              color: Colors.blue,
              padding:  EdgeInsets.symmetric(horizontal: 5.0),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                      icon:  Container(
                        height: 50.0,
                        width: 50.0,
                        decoration:  BoxDecoration(
                          color: Colors.orange,
                          shape: BoxShape.circle,
                        ),
                        child:  Center(
                          child:  Icon(
                            expandFlag ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,
                            color: Colors.white,
                            size: 30.0,
                          ),
                        ),
                      ),
                      onPressed: () {
                        setState(() {
                          expandFlag = !expandFlag;
                        });
                      }),
                  Text(
                    'title',
                    style:  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                  )
                ],
              ),
            ),
            ExpandableContainer(
                expanded: expandFlag,
                child:  ListView.builder(
                  itemBuilder: (BuildContext context, int index) {
                    return  Container(
                      decoration:
                      BoxDecoration(border:  Border.all(width: 1.0, color: Colors.grey), color: Colors.black),
                      child:  ListTile(
                        title:  Text(
                          "Cool $index",
                          style:  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        leading:  Icon(
                          Icons.local_pizza,
                          color: Colors.white,
                        ),
                      ),
                    );
                  },
                  itemCount: 15,
                ))
          ],
        ),
      ),
    );
  }
}

class ExpandableContainer extends StatelessWidget {
  final bool expanded;
  final double collapsedHeight;
  final double expandedHeight;
  final Widget child;

  ExpandableContainer({
    @required this.child,
    this.collapsedHeight = 0.0,
    this.expandedHeight = 300.0,
    this.expanded = true,
  });

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return  AnimatedContainer(
      duration:  Duration(milliseconds: 500),
      curve: Curves.easeInOut,
      width: screenWidth,
      height: expanded ? expandedHeight : collapsedHeight,
      child:  Container(
        child: child,
        decoration:  BoxDecoration(border:  Border.all(width: 1.0, color: Colors.blue)),
      ),
    );
  }
}*/
}

// Column(
// crossAxisAlignment: CrossAxisAlignment.stretch,
// mainAxisAlignment: MainAxisAlignment.start,
// children: [
// Row(children: [
// Padding(
// padding: EdgeInsets.only(top: 14.0),
// child: Row(
// children: [
// Padding(
// padding: EdgeInsets.only(right: 20.0, left: 10.0),
// child: CircleAvatar(
// radius: sWidth * 0.04,
// child: Text(widget.name.split('')[0].toUpperCase()),
// backgroundColor: widget.index % 2 == 0
// ? ColorConstants.primaryColor.withOpacity(0.4)
// : ColorConstants.primaryColor.withOpacity(0.2),
// ),
// ),
// Padding(
// padding: EdgeInsets.only(right: 8.0),
// child: Text('0'),
// ),
// Text(widget.name ?? 'Not found'),
// ],
// )),
// Spacer(),
// IconButton(
// onPressed: () => _onButtonPressed(),
// icon: Icon(Icons.add),
// ),
// IconButton(
// onPressed: () => setState(() {
// showDetails = !showDetails ? true : false;
// }),
// icon: Icon(showDetails ? Icons.expand_less : Icons.expand_more),
// )
// ]),
//
// Visibility(
// visible: showDetails,
// child:
// Expanded(
// child: ListView.builder(
// itemCount: 5,
// itemBuilder: (context, index) {
// return GestureDetector(
// onTap: () {
// //   Navigator.pop(context, entryList[index]);
// },
// child: Padding(
// padding: const EdgeInsets.symmetric(horizontal: 8.0),
// child: Card(
// child: Padding(
// padding: const EdgeInsets.all(8.0),
// child: Column(
// children: <Widget>[
// Row(
// children: <Widget>[
// Expanded(
// child: Text(
// 'hjvj',
// textAlign: TextAlign.end,
// ),
// ),
// ],
// ),
// ListTile(
// title: Text('hb'),
// trailing: Text('gjfj'),
// ),
// ],
// ),
// ),
// ),
// ),
// );
// }),
// ),
// // FutureBuilder(
// //     future:
// //     ServiceGenerator.getReqWithParams(Apis.rest, restData()),
// //     builder: (context, snapshot) {
// //       if (snapshot.connectionState == ConnectionState.waiting) {
// //         return Center(child: CircularProgressIndicator());
// //       } else if (snapshot.hasData) {
// //         return getListByModule(snapshot.data);
// //       } else {
// //         return Center(child: Text('Something went wrong...'));
// //       }
// //     }),
// ),
// ],
// ),
