import 'package:flutter/material.dart';
import 'package:leadcalls/Screens/FontScreen.dart';
import 'package:leadcalls/Screens/ThemeModel.dart';
import 'package:provider/provider.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // title: 'Flutter Demo',
      theme: Provider.of<ThemeModel>(context).currentTheme,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  // final String title;
  //
  // MyHomePage({this.title});

  bool _isVisible = false;

  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 14.0, bottom: 10.0),
                  child: Text(
                    'Select Theme',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                  ),
                )
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  height: 30.0,
                  width: 30.0,
                  child: FlatButton(
                    child: null,
                    onPressed: () {
                      Provider.of<ThemeModel>(context, listen: false)
                          .setDarkTheme();
                    },
                    color: Colors.black,
                  ),
                ),
                // const SizedBox(height: 30),
                Container(
                  height: 30.0,
                  width: 30.0,
                  child: RaisedButton(
                    onPressed: () {
                      Provider.of<ThemeModel>(context, listen: false)
                          .setLightTheme();
                    },
                    color: Colors.white,
                  ),
                ),
                Container(
                  height: 30.0,
                  width: 30.0,
                  child: RaisedButton(
                    onPressed: () {
                      Provider.of<ThemeModel>(context, listen: false)
                          .setRedTheme();
                    },
                    color: Colors.red,
                  ),
                ),
                Container(
                  height: 30.0,
                  width: 30.0,
                  child: RaisedButton(
                    onPressed: () {
                      Provider.of<ThemeModel>(context, listen: false)
                          .setBlueTheme();
                    },
                    color: Colors.blue,
                  ),
                ),
                Container(
                  height: 30.0,
                  width: 30.0,
                  child: RaisedButton(
                    onPressed: () {
                      Provider.of<ThemeModel>(context, listen: false)
                          .setGreenTheme();
                    },
                    color: Colors.greenAccent,
                  ),
                )
              ],
            ),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 14.0, bottom: 10.0,top: 14.0),
                  child: Text(
                    'Notification',
                    style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  static void setAppFontFamily(BuildContext context, String element) {}
}
