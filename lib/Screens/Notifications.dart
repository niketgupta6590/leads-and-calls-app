import 'package:flutter/material.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {

  bool _value = false;

  void _onChanged(bool value) {
    setState(() {
      _value = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home : Scaffold (
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          'Notification',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w400, fontSize: 20),
        ),
      ),
      body: Container(
          padding: EdgeInsets.all(8),
          child: Column(
            children:
            ListTile.divideTiles(
                context: context,
                tiles: [
                  Expanded(
                    child:Column (
                      children: [
                        // Switch(value: _value,
                        //     onChanged: (bool value) {_onChanged(value);}),
                        SwitchListTile(value: _value,
                            title: Text("Enable Notifications"),
                            activeColor: Colors.blue,
                            secondary: Icon(Icons.notifications),
                           // subtitle: Text("For my small print"),
                            onChanged: (bool value) {_onChanged(value);}),
                        //trailing: Icon(Icons.keyboard_arrow_right),
                        //leading: Icon(Icons.notifications),
                        //title: Text('Enable Notifications'),
                      ],
                    )
                  ),
          ListTile(
              trailing: Icon(Icons.keyboard_arrow_right),
              leading: Icon(Icons.list),
              title: Text('Reminder List'
              )

            // onTap: () {
            // Navigator.pushReplacement(context,
            // new PageRouteBuilder(pageBuilder: (_, __, ___) => Appearance()));
            // }
          )
      ],
    ).toList()
    ),
    ),
      ),
    );
  }
}
