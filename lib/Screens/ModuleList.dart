import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:leadcalls/Models/Modules.dart';
import 'package:leadcalls/Network/Apis.dart';
import 'package:leadcalls/Network/ServiceGenerator.dart';
import 'package:leadcalls/Res/Colors.dart';
import 'package:leadcalls/db/DatabaseHelper.dart';
import 'package:leadcalls/db/ModuleDb.dart';
import 'package:leadcalls/utils/ColorConstants.dart';

class ModuleList extends StatefulWidget {

  static const id = 'module_list';

  @override
  _ModuleListState createState() => _ModuleListState();
}

class _ModuleListState extends State<ModuleList> {
  List<String> mKeys = [];

  @override
  void initState() {
    super.initState();
    _getLists();
  }

  _getLists() async {
    await DatabaseHelper.instance.get().then((value) {
      if (value.length > 0) {
        mKeys.clear();
        for (ModuleDb module in value) {
          mKeys.add(module.moduleKey);
        }
      }
    }).catchError((onError) {
      print(onError);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context,true);
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Module Selection'),
        ),
        body: SafeArea(
          child: FutureBuilder(
            future: ServiceGenerator.getReq(Apis.moduleList),
            builder: (context, snapshot){
              if(snapshot.connectionState == ConnectionState.waiting){
                return Center(child: CircularProgressIndicator());
              }
              else if(snapshot.hasData){
                List<Module> modules = Modules.fromJson(jsonDecode(snapshot.data.toString())).modules;
                return ListView.builder(
                  physics: BouncingScrollPhysics(),
                  itemCount: 10,
                  itemBuilder: (context, index){
                    return ModuleItem(
                      module: modules[index],
                      isChecked: mKeys.contains(modules[index].moduleKey) ? true : false,
                      index: index,
                    );
                  }
                );
              }
              else{
                return Center(child: Text('Something went wrong...'));
              }
            }
          ),
        ),
      ),
    );
  }

}

class ModuleItem extends StatefulWidget {
  ModuleItem({this.module, this.isChecked, this.index});
  final Module module;
  final bool isChecked;
  final int index;
  @override
  _ModuleItemState createState() => _ModuleItemState();
}

class _ModuleItemState extends State<ModuleItem> {

  bool isChecked;
  ModuleDb mDb;

  @override
  void initState() {
    super.initState();

    isChecked = widget.isChecked;

  }

  @override
  Widget build(BuildContext context) {
    final sWidth = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () async{
        setState(() {
          isChecked = isChecked ? true : false;
          /////////////changed by oshu false = true
        });
        mDb = ModuleDb(
          moduleKey: widget.module.moduleKey,
          moduleName: widget.module.moduleLabel,
        );
        isChecked
        ? await DatabaseHelper.instance.insert(mDb)
        : await DatabaseHelper.instance.delete(mDb);
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 2, horizontal: 10),
        margin: EdgeInsets.only(top: 8,left: 10, right: 10),
         decoration: BoxDecoration(
            color: cWhite,
            borderRadius: BorderRadius.all(Radius.circular(5))
        ),
        child: Row(
          children: [
            Padding(
                padding: EdgeInsets.symmetric(horizontal: sWidth * 0.01),
                child: CircleAvatar(
                  backgroundColor: widget.index % 2 == 0 ? ColorConstants.primaryColor.withOpacity(0.4) : ColorConstants.primaryColor.withOpacity(0.2),
                  radius: sWidth * 0.045,
                  child: Text(
                    widget.module.moduleKey.split('')[0],
                    style: TextStyle(
                      color: cBlack
                    ),
                  ),
                )
            ),
            SizedBox(
              width: sWidth * 0.02,
            ),
            Text(
              widget.module.moduleKey ?? 'Not found',
              style: TextStyle(
                fontSize: sWidth * 0.04,
              ),
            ),
            Spacer(),
            Checkbox(value: isChecked, onChanged: (val) async {
              setState(() {
                isChecked = val;
              });
              isChecked
              ? await DatabaseHelper.instance.insert(mDb)
              : await DatabaseHelper.instance.delete(mDb);
            })
          ],
        ),
      ),
    );
  }
}