import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:leadcalls/Models/LeadsResponseModule.dart';
import 'package:leadcalls/Network/Apis.dart';
import 'package:leadcalls/Network/ServiceGenerator.dart';
import 'package:leadcalls/Temps.dart';

class RelatedToSelection extends StatefulWidget {
  RelatedToSelection(this.changedVal);
  String changedVal;

  @override
  _RelatedToSelectionState createState() => _RelatedToSelectionState();
}

class _RelatedToSelectionState extends State<RelatedToSelection> {
  @override
  void initState() {
    super.initState();
  }

  Widget listMaker(dynamic data) {
    List<EntryList> entryList =
        LeadsResponseModule.fromJson(jsonDecode(data.toString())).entryList;
    return ListView.builder(
        itemCount: entryList.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.pop(context,entryList[index]);
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              '${entryList[index].nameValueList.dateEntered.value}',
                              textAlign: TextAlign.end,
                            ),
                          ),
                        ],
                      ),
                      ListTile(
                        title: Text(
                            '${entryList[index].nameValueList.name.value}'),
                        trailing: Text(
                            '${entryList[index].nameValueList.description.value}'),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  FormData restData() {
    String restData =
        '{"session":"$tUserSession","module_name":"${widget.changedVal + 's'}","query":"${widget.changedVal.toLowerCase() + 's.'}assigned_user_id=\'$tUserId\'"}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_entry_list",
      "rest_data": "$restData"
    };
    return FormData.fromMap(body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.changedVal),
      ),
      body: SafeArea(
        child: FutureBuilder(
            future: ServiceGenerator.getReqWithParams(Apis.rest, restData()),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else if (snapshot.hasData) {
                return listMaker(snapshot.data);
              } else {
                return Center(child: Text('Something went wrong...'));
              }
            }),
      ),
    );
  }
}
