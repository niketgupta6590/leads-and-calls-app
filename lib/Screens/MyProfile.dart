import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:leadcalls/Models/LeadsResponseModule.dart';
import 'package:leadcalls/Network/Apis.dart';
import 'package:leadcalls/Network/ServiceGenerator.dart';
import 'package:leadcalls/Temps.dart';

class MyProfile extends StatefulWidget {

  static const id = 'profilemy';

  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  EntryList details;
  String recordId;
  String selectedModule;
  Map<String, String> labelValues = {};
  bool hasData = false;
  List<String> names = [], values = [];

  TextEditingController
  firstNameCtrl = TextEditingController(),
      lastNameCtrl = TextEditingController(),
      phoneNoCtrl = TextEditingController(),
      timeZoneCtrl = TextEditingController(),
      departmentCtrl = TextEditingController(),
      currencyCtrl = TextEditingController();



  @override
  void initState() {
    super.initState();
    getAll();

  }

  getAll() async {
    Map<String, String> labels, values;
    labels = await getLabels();
    values = await getValues();
    values.forEach((key, value) {
      if (labels.containsKey(key)) {
        labelValues[labels[key]] = value;
      }
    });
    print("==========labelValues-------------------------");
    print(labelValues);
    setState(() {
      hasData = true;
    });
  }

  Future<Map<String, String>> getLabels() async {
    String restData =
        '{"session":"$tUserSession","module_name":"Users","fields":[]}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_module_fields",
      "rest_data": "$restData"
    };
    try {
      final labelsD = await ServiceGenerator.getReqWithParams(
          Apis.rest, FormData.fromMap(body));
      final Map<String, dynamic> labelsJson = jsonDecode(labelsD.toString());
      Map<String, String> labels = {};
      final labelValue = labelsJson['module_fields'];
      (labelValue as Map<String, dynamic>).forEach((key2, value2) {
        if (value2 is! String) {
          (value2 as Map<String, dynamic>).forEach((key3, value3) {
            if (key3 == 'name') {
              names.add(value3.toString());
            }
            if (key3 == 'label') {
              values.add(value3.toString());
            }
          });
        }
      });
      for (int i = 0; i < names.length; i++) {
        labels[names[i]] = values[i];
      }
      return labels;
    } catch (err) {
      throw err;
    }
  }

  Future<Map<String, String>> getValues() async {
    String restData =
        '{"session":"$tUserSession","module_name":"Users","id":"$tUserId","select_fields":[],"link_name_to_fields_array":[],"track_view":false}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_entry",
      "rest_data": "$restData"
    };
    try {
      final valuesD = await ServiceGenerator.getReqWithParams(Apis.rest, FormData.fromMap(body));
      final Map<String, dynamic> valuesJson = jsonDecode(valuesD.toString());
      List<String> names = [], values = [];
      Map<String, String> entries = {};
      final entryValue = valuesJson['entry_list'][0]['name_value_list'];
      (entryValue as Map<String, dynamic>).forEach((key2, value2) {
        if (value2 is! String) {
          (value2 as Map<String, dynamic>).forEach((key3, value3) {
            if (key3 == 'name') {
              names.add(value3.toString());
            }
            if (key3 == 'value') {
              values.add(value3.toString());
            }
          });
        }
      });
      for (int i = 0; i < names.length; i++) {
        entries[names[i]] = values[i];
      }
      return entries;
    } catch (err) {
      throw err;
    }
  }

  @override
  Widget build(BuildContext context) {
    // details = widget.details;
    // selectedModule = widget.details.moduleName;
    Map<String, TextEditingController> textEditingControllers = {};
    var textFields = <TextField>[];
    values.forEach((str) {
      var textEditingController = new TextEditingController(text: str);
      textEditingControllers.putIfAbsent(str, () => textEditingController);
      return textFields.add(TextField(controller: textEditingController));
    });

    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
      body: SafeArea(
        child: Column(
          children: [
            // Column(children: textFields),

            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                'Edit Profie',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
            ),

            Expanded(
              child: hasData
                  ? Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 12, horizontal: 18),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("name", style: TextStyle(fontSize: 16)),
                            ],
                          ),
                        ),
                      ],
                    )
                  : Center(child: CircularProgressIndicator()),
            ),
          ],
        ),
      ),
    );
  }
}

class OverviewWidget extends StatefulWidget {
  const OverviewWidget({
    Key key,
    @required this.details,
  }) : super(key: key);
  final Map<String, String> details;

  @override
  _OverviewWidgetState createState() => _OverviewWidgetState();
}

class _OverviewWidgetState extends State<OverviewWidget> {
  List<Widget> tiles = [];
  TextEditingController
  firstNameCtrl = TextEditingController(),
      lastNameCtrl = TextEditingController(),
      phoneNoCtrl = TextEditingController(),
      timeZoneCtrl = TextEditingController(),
      departmentCtrl = TextEditingController(),
      currencyCtrl = TextEditingController();

  @override
  void initState() {
    super.initState();
    widget.details.forEach((key, value) {
      tiles.add(
        NameValTile(
          name: key,
          value: value,
        ),
      );
    });
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch, children: tiles);
  }
}

class NameValTile extends StatelessWidget {
  const NameValTile({
    @required this.name,
    @required this.value,
    this.mycontroller,
    this.onTap,
    this.isLastChild = false,
    Key key,
  }) : super(key: key);

  final bool isLastChild;
  final String name, value;
  final Function onTap;
  final TextEditingController mycontroller;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(name, style: TextStyle(fontSize: 16)),
                TextField(
                    controller: mycontroller, style: TextStyle(fontSize: 17)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
