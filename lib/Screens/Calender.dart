import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:leadcalls/Home.dart';
import 'package:leadcalls/Network/Apis.dart';
import 'package:leadcalls/Network/ServiceGenerator.dart';
import 'package:leadcalls/Screens/Contacts.dart';
import 'package:leadcalls/callsDetailPage.dart';
import 'package:leadcalls/createCallPage.dart';
import 'package:leadcalls/createMeetingPage.dart';
import 'package:leadcalls/objects/UpcomingEvents.dart';
import 'package:table_calendar/table_calendar.dart';
import '../meetingsDetailPage.dart';

class Calender extends StatefulWidget {
  static const id = 'calendar';

  @override
  _CalenderState createState() => _CalenderState();
}

class _CalenderState extends State<Calender> {
  CalendarController _calendarController;
  UpcomingEvents events;

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TableCalendar(calendarController: _calendarController),
        Expanded(
            child: SafeArea(
                child: Column(
          children: [
            Expanded(
              child: FutureBuilder(
                  future: ServiceGenerator.getReq(Apis.upcomingEventsList),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(child: CircularProgressIndicator());
                    } else if (snapshot.hasData) {
                      List<UpcomingEvents> eventsList =
                          (jsonDecode(snapshot.data.toString()) as List)
                              .map((e) => UpcomingEvents.fromJson(e))
                              .toList();

                      print(eventsList.length.toString() + 'ss');
                      return eventsList != null
                          ? ListView.builder(
                              physics: BouncingScrollPhysics(),
                              itemCount: eventsList.length,
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                     eventsList[index].module == "Calls" ?

                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Home()))
                                     :
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Contacts()));
                                  },
                                  child: Container(
                                    child: Card(
                                      color: Colors.pink,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Text(
                                              eventsList[index].summary,
                                              style: TextStyle(
                                                  fontSize: 22,
                                                  color: Colors.black),
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Text(eventsList[index].dateDue),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              })
                          : "No Upcoming events found";
                    } else {
                      return Center(child: Text('Something went wrong...'));
                    }
                  }),
            ),
          ],
        ))),
        Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(7.0),
                child: FlatButton.icon(
                  icon: Icon(Icons.add), //`Icon` to display
                  label: Text('Calls'), //`Text` to display
                  onPressed: () {
                    Navigator.pushNamed(context, CreateCallPage.id);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(7.0),
                child: FlatButton.icon(
                  icon: Icon(Icons.add), //`Icon` to display
                  label: Text('Meetings'), //`Text` to display
                  onPressed: () {
                    Navigator.pushNamed(context, CreateMeetingPage.id);
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
