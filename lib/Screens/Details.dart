import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:leadcalls/EditOption/CallsEdit.dart';
import 'package:leadcalls/EditOption/LeadEdit.dart';
import 'package:leadcalls/EditOption/MeetingEdit.dart';
import 'package:leadcalls/Models/LeadsResponseModule.dart';
import 'package:leadcalls/Network/Apis.dart';
import 'package:leadcalls/Network/ServiceGenerator.dart';
import 'package:leadcalls/Temps.dart';
import 'package:leadcalls/menus/webview.dart';
import 'package:leadcalls/objects/DeleteRecord.dart';
import 'package:leadcalls/objects/DropDownCalls.dart';
import 'package:leadcalls/utils/ColorConstants.dart';
import 'package:leadcalls/utils/Constants.dart';
import 'package:share/share.dart';
import '../LoginPage.dart';
import 'NotesList.dart';
import 'Related.dart';
import 'package:leadcalls/Screens/History.dart';

class Details extends StatefulWidget {
  const Details(this.details);
  final EntryList details;

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  EntryList details;
  String recordId;
  String selectedModule;
  Map<String, String> labelValues = {};
  List moduleName = [];
  bool hasData = false;
  bool _isVisible = false;
  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  List<String> popup_options = ['please wait..'];
  List<String> email_remainder_options = ['please wait..'];

  String _selectedPopup, _selectedRemainder;
  bool _isPopupSelected = true;
  bool _isEmailRemainderSelected = false;

  @override
  void initState() {
    super.initState();
    getAll();
  }

  getAll() async {
    selectedModule = widget.details.moduleName;
    recordId = widget.details.nameValueList.id.value;
    Map<String, String> labels, values;
    labels = await getLabels();
    values = await getValues(widget.details.nameValueList.id.value);
    values.forEach((key, value) {
      if (labels.containsKey(key)) {
        labelValues[labels[key]] = value;
      }
    });
    print("==========labelValues");
    print(labelValues);
    setState(() {
      hasData = true;
    });
  }

  Future<Map<String, String>> getLabels() async {
    String restData =
        '{"session":"$tUserSession","module_name":"$selectedModule","fields":[]}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_module_fields",
      "rest_data": "$restData"
    };
    try {
      final labelsD = await ServiceGenerator.getReqWithParams(Apis.rest, FormData.fromMap(body));
      final Map<String, dynamic> labelsJson = jsonDecode(labelsD.toString());
      List<String> names = [], values = [];
      Map<String, String> labels = {};
      final labelValue = labelsJson['module_fields'];
      (labelValue as Map<String, dynamic>).forEach((key2, value2) {
        if (value2 is! String) {
          (value2 as Map<String, dynamic>).forEach((key3, value3) {
            if (key3 == 'name') {
              names.add(value3.toString());
            }
            if (key3 == 'label') {
              values.add(value3.toString());
            }
          });
        }
      });
      for (int i = 0; i < names.length; i++) {
        labels[names[i]] = values[i];
      }
      return labels;
    } catch (err) {
      throw err;
    }
  }

  Future<Map<String, String>> getValues(String recordId) async {
    String restData =
        '{"session":"$tUserSession","module_name":"$selectedModule","id":"$recordId","select_fields":[],"track_view":false}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_entry",
      "rest_data": "$restData"
    };
    try {
      final valuesD = await ServiceGenerator.getReqWithParams(
          Apis.rest, FormData.fromMap(body));
      final Map<String, dynamic> valuesJson = jsonDecode(valuesD.toString());
      List<String> names = [], values = [];
      Map<String, String> entries = {};
      final entryValue = valuesJson['entry_list'][0]['name_value_list'];
      (entryValue as Map<String, dynamic>).forEach((key2, value2) {
        if (value2 is! String) {
          (value2 as Map<String, dynamic>).forEach((key3, value3) {
            if (key3 == 'name') {
              names.add(value3.toString());
            }
            if (key3 == 'value') {
              values.add(value3.toString());
            }
          });
        }
      });
      for (int i = 0; i < names.length; i++) {
        entries[names[i]] = values[i];
      }
      return entries;
    } catch (err) {
      throw err;
    }
  }

  @override
  Widget build(BuildContext context) {
    details = widget.details;
    selectedModule = widget.details.moduleName;
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text(details.moduleName),
          actions: [
            PopupMenuButton<String>(
              onSelected: choiceAction,
              itemBuilder: (BuildContext context) {
                return Constants.choices.map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  );
                }).toList();
              },
            ),
          ],
          bottom: TabBar(
            tabs: [
              Tab(text: 'DETAILS'),
              Tab(text: 'RELATED'),
              Tab(text: 'HISTORY'),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            SafeArea(
                child: hasData
                    ? SingleChildScrollView(
                        child: Column(
                          children: [
                            ExpansionTile(
                                title: Text('Reminder', style: TextStyle()),
                                backgroundColor: Colors.grey[100],
                                children: <Widget>[
                                  InkWell(
                                    onTap: () => showToast(),
                                    splashColor: ColorConstants.primaryColor
                                        .withOpacity(0.3),
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colors.grey[500]),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(3))),
                                      child: Text('Add reminder'),
                                    ),
                                  ),
                                  Visibility(
                                    visible: _isVisible,
                                child:  Container(
                                      child: Card(
                                        child:
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Row(children: [

                                                 Flexible(
                                                 child: Padding(
                                                    padding: EdgeInsets.all(8.0),
                                                  child: Text(
                                                    "Actions : ",
                                                    style: TextStyle(
                                                        fontSize: 20.0,
                                                        color: Colors.black),
                                                  ),
                                                ),
                                              ),
                                            ],),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Switch(
                                                  value: _isPopupSelected,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _isPopupSelected = value;
                                                     // _showNotification();
                                                      print(_isPopupSelected);
                                                    }
                                                    );
                                                  },
                                                  activeTrackColor:
                                                      ColorConstants
                                                          .primaryColor,
                                                  activeColor: ColorConstants
                                                      .primaryColorDark,
                                                ),
                                                SizedBox(
                                                  width: 20,
                                                ),
                                                Text('Popup'),
                                                Spacer(),
                                                DropdownButton(
                                                  value: _selectedPopup,
                                                  items: popup_options.map((title) {
                                                    return DropdownMenuItem(
                                                      value: title,
                                                      child: Text(title),
                                                    );
                                                  }).toList(),
                                                  onChanged: (changed) {
                                                    // setState(() {
                                                    //   _selectedPopup = changed;
                                                    // });
                                                  },
                                                ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Switch(
                                                  value:
                                                      _isEmailRemainderSelected,
                                                  onChanged: (value) {
                                                    // setState(() {
                                                    //   _isEmailRemainderSelected = value;
                                                    //   _showNotification();
                                                    //   print(_isEmailRemainderSelected);
                                                    // });
                                                  },
                                                  activeTrackColor:
                                                      ColorConstants
                                                          .primaryColor,
                                                  activeColor: ColorConstants
                                                      .primaryColorDark,
                                                ),
                                                SizedBox(
                                                  width: 20,
                                                ),
                                                Text("Remainder"),
                                                Spacer(),
                                                DropdownButton(
                                                  value: _selectedRemainder,
                                                  items: popup_options
                                                      .map((title) {
                                                    return DropdownMenuItem(
                                                      value: title,
                                                      child: Text(title),
                                                    );
                                                  }).toList(),
                                                  onChanged: (changed) {
                                                    // setState(() {
                                                    //   _selectedRemainder = changed;
                                                    // });
                                                  },
                                                ),
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                                  EdgeInsets.only(top: 18.0),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  FlatButton(
                                                      onPressed: () {},
                                                      child: Text(
                                                          "ADD ALL INVITEES")),
                                                  FlatButton(
                                                      onPressed: () {},
                                                      child: Text(
                                                          "REMOVE REMAINDER")),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                        ),
                                      ),
                                    ),


                                ]),
                            OverviewWidget(details: labelValues),
                          ],
                        ),
                      )
                    : Center(child: CircularProgressIndicator())),
            Related(selectedModule,recordId),
            History(),
          ],
        ),
      ),
    );
  }

  choiceAction(String choice) {
    if (choice == Constants.webView) {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => webview(),
          ));
    } else if (choice == Constants.share) {
      share(context);
    } else if (choice == Constants.edit) {
      _editData();
    } else if (choice == Constants.delete) {
      deletedialog(context);
    } else if (choice == Constants.scanQr) {
      ////   String cameraScabResult = await scanner.scan();
    } else if (choice == Constants.metadata) {}
  }

  share(BuildContext context) {
    Share.share(
        '      http://hosting.ideadunes.com/hosting/giproperties.ae/index.php?module=Calls&offset=1&stamp=1603537262099946500&return_module=Calls&action=DetailView&record=' +
            recordId);
  }

  _editData() {
    String subject = widget.details.nameValueList.name.value;
    String id = widget.details.nameValueList.id.value;
    String desc = widget.details.nameValueList.description.value;
    String createdby = widget.details.nameValueList.createdByName.value;
    String modifyby = widget.details.nameValueList.modifiedByName.value;
    String assignusername = widget.details.nameValueList.assignedUserName.value;
    String duration = widget.details.nameValueList.name.value;

    print('--------------------values for editing -----------------');
    print(subject);
    print(id);
    print(createdby);
    print(modifyby);
    print(assignusername);

    switch (selectedModule) {
      case 'Leads':
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => LeadEdit(
              firstnametext: subject,
              createdby: createdby,
              modifyby: modifyby,
              assusrname: assignusername,
              desc: desc,
            ),
          ),
        );
        break;

      case 'Calls':
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CallEdit(
                firstnametext: subject,
                recordId: id,
                createdby: createdby,
                modifyby: modifyby,
                assusrname: assignusername,
                duration: duration),
          ),
        );
        break;

      case 'Meetings':
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => MeetingEdit(
                firstnametext: subject,
                recordId: id,
                createdby: createdby,
                modifyby: modifyby,
                assusrname: assignusername,
                desc: desc),
          ),
        );
        break;

      case 'Contacts':
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CallEdit(
                firstnametext: subject,
                recordId: id,
                createdby: createdby,
                modifyby: modifyby,
                assusrname: assignusername,
                duration: duration),
          ),
        );
        break;
    }
  }

  Future<void> deletedialog(context) {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Do you want to delete?"),
            actions: [
              FlatButton(
                child: Text("Yes"),
                onPressed: () {
                  _deleteItem();
                },
              ),
              FlatButton(
                child: Text("Cancel"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          );
        });
  }

  FormData restData() {
    String restData =
        '{"session":"$tUserSession","module_name":"$selectedModule","query":"${selectedModule.toLowerCase() + '.'}assigned_user_id=\'$tUserId\'"}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_entry_list",
      "rest_data": "$restData"
    };
    print(restData);
    return FormData.fromMap(body);
  }

  Future<void> _deleteItem() async {
    //  ServiceGenerator.getReqWithParams(Apis.rest,restData);

    String url =
        'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';
    String restData =
        '{"session":"$tUserSession","module_name":"$selectedModule","name_value_list":[{"name":"id","value":"$recordId"},{"name":"deleted","value":"1"}]}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "set_entry",
      "rest_data": "$restData"
    };
    print(
        '-----------------------------deleting..............................');
    print(recordId);

    FormData formData = FormData.fromMap(body);
    Dio dio = new Dio();
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      return options; //continue
    }, onResponse: (Response response) async {
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
    int statusCode = response.statusCode;
    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());
      if (responseMap != null) {
        DeleteRecord deleteRecord =
            DeleteRecord.fromJson(json.decode(response.toString()));

        String desc = "Delete successfully..";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.greenAccent,
            textColor: Colors.white,
            fontSize: 16.0);

        Navigator.of(context).pop();
        Navigator.pop(context, true);
      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        if (desc.contains("session ID is invalid")) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
            ModalRoute.withName('/'),
          );
        }
      }
    }
  }
}

class OverviewWidget extends StatefulWidget {
  const OverviewWidget({
    Key key,
    @required this.details,
  }) : super(key: key);
  final Map<String, String> details;

  @override
  _OverviewWidgetState createState() => _OverviewWidgetState();
}

class _OverviewWidgetState extends State<OverviewWidget> {
  List<Widget> tiles = [];

  @override
  void initState() {
    super.initState();
    widget.details.forEach((key, value) {
      tiles.add(
        NameValTile(
          name: key,
          value: value,
        ),
      );
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch, children: tiles);
  }
}

class NameValTile extends StatelessWidget {
  const NameValTile({
    @required this.name,
    @required this.value,
    this.onTap,
    this.isLastChild = false,
    Key key,
  }) : super(key: key);

  final bool isLastChild;
  final String name, value;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(name, style: TextStyle(fontSize: 16)),

                Flexible(
                  child: Padding(
                    padding: EdgeInsets.only(left: 20.0),
                    child:
                    Text(value,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.w500,
                            color: onTap == null
                                ? Colors.black
                                : ColorConstants.primaryColor)),
                  )

                ),
              ],
            ),
          ),
          Visibility(
            visible: !isLastChild,
            child: Divider(
              color: Colors.black26,
              height: 1,
              thickness: 1,
            ),
          )
        ],
      ),
    );
  }
}

class MarkerTile extends StatelessWidget {
  const MarkerTile({
    this.text,
    Key key,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(color: ColorConstants.primaryColor),
        child: Padding(
          padding: EdgeInsets.only(left: 15),
          child: Text(text.toUpperCase(),
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 17,
                  fontWeight: FontWeight.w500)),
        ));
  }
}

class Reminder extends StatefulWidget {
  @override
  _ReminderState createState() => _ReminderState();
}

class _ReminderState extends State<Reminder> {
  bool _isVisible = false;
  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  List<String> popup_options = ['please wait..'];
  List<String> email_remainder_options = ['please wait..'];

  String _selectedPopup, _selectedRemainder;
  bool _isPopupSelected = true;
  bool _isEmailRemainderSelected = false;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 210.0,
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Flexible(
                child: Text(
                  "Actions : ",
                  style: TextStyle(fontSize: 20.0, color: Colors.black),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Switch(
                  value: _isPopupSelected,
                  onChanged: (value) {
                    // setState(() {
                    //   _isPopupSelected = value;
                    //   _showNotification();
                    //   print(_isPopupSelected);
                    // }
                    // );
                  },
                  activeTrackColor: ColorConstants.primaryColor,
                  activeColor: ColorConstants.primaryColorDark,
                ),
                SizedBox(
                  width: 20,
                ),
                Text('Popup'),
                Spacer(),
                DropdownButton(
                  value: _selectedPopup,
                  items: popup_options.map((title) {
                    return DropdownMenuItem(
                      value: title,
                      child: Text(title),
                    );
                  }).toList(),
                  onChanged: (changed) {
                    // setState(() {
                    //   _selectedPopup = changed;
                    // });
                  },
                ),
              ],
            ),
            Row(
              children: [
                Switch(
                  value: _isEmailRemainderSelected,
                  onChanged: (value) {
                    // setState(() {
                    //   _isEmailRemainderSelected = value;
                    //   _showNotification();
                    //   print(_isEmailRemainderSelected);
                    // });
                  },
                  activeTrackColor: ColorConstants.primaryColor,
                  activeColor: ColorConstants.primaryColorDark,
                ),
                SizedBox(
                  width: 20,
                ),
                Text("Remainder"),
                Spacer(),
                DropdownButton(
                  value: _selectedRemainder,
                  items: popup_options.map((title) {
                    return DropdownMenuItem(
                      value: title,
                      child: Text(title),
                    );
                  }).toList(),
                  onChanged: (changed) {
                    // setState(() {
                    //   _selectedRemainder = changed;
                    // });
                  },
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 18.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FlatButton(onPressed: () {}, child: Text("ADD ALL INVITEES")),
                  FlatButton(onPressed: () {}, child: Text("REMOVE REMAINDER")),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}




//class InternalWidget extends StatelessWidget {
//   const InternalWidget({
//     Key key,
//     @required this.details,
//   }) : super(key: key);
//
//   final EntryList details;
//
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.stretch,
//       children: [
//         MarkerTile(
//           text: 'Internal',
//         ),
//         NameValTile(
//           name: 'Date Created',
//           value: details.nameValueList.dateEntered.value,
//         ),
//         NameValTile(
//           name: 'Date Modified',
//           value: details.nameValueList.dateModified.value,
//         ),
//         NameValTile(
//           name: 'Created by',
//           value: details.nameValueList.createdByName.value,
//         ),
//         NameValTile(
//           name: 'Modified by',
//           value: details.nameValueList.modifiedByName.value,
//         ),
//         NameValTile(
//           onTap: () {},
//           isLastChild: true,
//           name: 'Assigned to',
//           value: details.nameValueList.assignedUserName.value ?? 'Not found',
//         ),
//       ],
//     );
//   }
// }
