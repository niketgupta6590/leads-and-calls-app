import 'package:flutter/material.dart';

class SugarSettings extends StatefulWidget {
  @override
  _SugarSettingsState createState() => _SugarSettingsState();
}

class _SugarSettingsState extends State<SugarSettings> {
  bool _isSelected = false;

  void _onChanged(bool value) {
    if (_isSelected) {
      setState(() {
        _isSelected = value;
      });
    }
  }

  _createSugarListWidget(BuildContext context) {
    return ListView(
      children: ListTile.divideTiles(context: this.context, tiles: [
        ListTile(
          leading: Icon(Icons.keyboard_arrow_right),
          title: Text('Destroy PHP session (debug)'),
        ),
        ListTile(
          leading: Icon(Icons.keyboard_arrow_right),
          title: Text('Logout'),
        ),
        ListTile(
          leading: Icon(Icons.star),
          title: Text('Site License: No'),
        ),
        ListTile(
          trailing: Icon(Icons.keyboard_arrow_right),
          leading: Icon(Icons.keyboard_arrow_right),
          title: Text('CRM meta data'),
        ),
        SwitchListTile(
            value: _isSelected,
            title: Text('Do not adjust time zone'),
            secondary: Icon(Icons.timer),
            onChanged: (bool value) {
              _onChanged(value);
            }),
        ListTile(
          trailing: Icon(Icons.keyboard_arrow_right),
          leading: Icon(Icons.account_circle),
          title: Text('Accounts'),
        ),
      ]).toList(),
    );
  }

  var containerWidget = Container(
    height: 200,
    child: Card(
      clipBehavior: Clip.antiAlias,
      child: ListView(
        children: [
          ListTile(
            title: Center(
              child: Text('Login Credential'),
            ),
          ),
          ListTile(
            title: Text('https://hosting.ideadunes.com/hosting/giproperties'),
            trailing: Text('Sugar CE 6.5.25'),
          ),
          ListTile(
              title: Text("Webapp-api@ideadunes.com"), trailing: Text("user")),
          ListTile(title: Text('en_us'), trailing: Text('Language')),
        ].toList(),
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        title: Text(
          'Sugar Settings',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w400, fontSize: 20),
        ),
      ),
      // body: _createSugarListWidget(context),

      body: Column(
        children: [
          containerWidget,
          new Expanded(child: _createSugarListWidget(context))
        ],
      ),
    );
  }
}
