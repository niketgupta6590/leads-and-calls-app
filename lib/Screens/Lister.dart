import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:leadcalls/Models/LeadsResponseModule.dart';
import 'package:leadcalls/Network/Apis.dart';
import 'package:leadcalls/Network/ServiceGenerator.dart';
import 'package:leadcalls/utils/ColorConstants.dart';
import '../Temps.dart';
import 'Details.dart';

class Lister extends StatefulWidget {

  Lister(this.selectedModule);
  final String selectedModule;

  @override
  _ListerState createState() => _ListerState();
}

class _ListerState extends State<Lister> {
  String selectedModule;

  getColor(String status) {
    switch (status.toLowerCase().replaceAll(" ", "")) {
      case 'planned':
        return ColorConstants.assigned;
        break;
      case 'open':
        return ColorConstants.open;
        break;
      case 'followup':
        return ColorConstants.followUp;
        break;
      case 'hot':
        return ColorConstants.hot;
        break;
      case 'held':
        return ColorConstants.cold;
        break;
      case 'new':
        return ColorConstants.newStatus;
        break;
      case 'recycled':
        return ColorConstants.recycled;
        break;
      case 'sold':
        return ColorConstants.soldSatus;
        break;
      case 'dead':
        return ColorConstants.dead;
        break;
      case 'replied':
        return ColorConstants.assigned;
        break;
      case 'sent':
        return ColorConstants.open;
        break;
      case 'read':
        return ColorConstants.followUp;
        break;
      case 'unread':
        return ColorConstants.hot;
        break;
      case 'held':
        return ColorConstants.cold;
        break;
      case 'new':
        return ColorConstants.newStatus;
        break;
      case 'recycled':
        return ColorConstants.recycled;
        break;
      case 'sold':
        return ColorConstants.soldSatus;
        break;
      case 'dead':
        return ColorConstants.dead;
        break;
      default:
        return Colors.white;
    }
  }

  FormData restData() {
    selectedModule = widget.selectedModule;

    String restData =
        '{"session":"$tUserSession","module_name":"$selectedModule","query":"${selectedModule.toLowerCase() + '.'}assigned_user_id=\'$tUserId\'"}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_entry_list",
      "rest_data": "$restData"
    };
    print(restData);
    return FormData.fromMap(body);
  }

  Widget getListByModule(dynamic data){
    List<EntryList> entryList = LeadsResponseModule.fromJson(jsonDecode(data.toString())).entryList;

    return entryList != null ? ListView.builder(

      shrinkWrap: true,
      itemCount: entryList.length,
      itemBuilder: (context, index) {

        return GestureDetector(
          onTap: ()  async{
            final isDeleted = await
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        Details(entryList[index])));
            if (isDeleted) {
              setState(() {
                entryList.removeAt(index);
              });
            }
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              color:
              selectedModule == 'Accounts' || selectedModule == 'Contacts' || selectedModule == 'Notes' ?
              Colors.white :
              getColor(entryList[index].nameValueList.status.value) ,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            '${entryList[index].nameValueList.dateEntered.value}',
                            textAlign: TextAlign.end,
                          ),
                        ),
                      ],
                    ),
                   selectedModule == 'Accounts' || selectedModule == 'Contacts' || selectedModule == 'Notes' ?

                   ListTile(
                     title: Text('${entryList[index].nameValueList.name.value}'),
                     trailing: Text('Not found') ,
                   )
                       :

                    ListTile(
                      title: Text('${entryList[index].nameValueList.name.value}'),
                      trailing: Text('${entryList[index].nameValueList.status.value}') ,
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    )
    : 'Data Not Found';

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      // appBar:
      // AppBar !=null ?? AppBar(
      //  title: Text('hgfd'),
      // ),
      body: SafeArea(
        child:
            FutureBuilder(
                future: ServiceGenerator.getReqWithParams(Apis.rest,restData()),
                builder: (context, snapshot){
                  if(snapshot.connectionState == ConnectionState.waiting){
                    return Center(child: CircularProgressIndicator());
                  }
                  else if(snapshot.hasData){
                    return getListByModule(snapshot.data);
                  }
                  else{
                    return Center(child: Text('Something went wrong...'));
                  }
                }
            ),
        ),
    );
  }
}


