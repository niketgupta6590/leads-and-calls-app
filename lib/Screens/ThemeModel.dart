import 'package:flutter/material.dart';
import 'Theme.dart';

class ThemeModel extends ChangeNotifier {
  ThemeData currentTheme = greenTheme;
  // ThemeType _themeType = ThemeType.Dark;

  setDarkTheme() {
    currentTheme = darkTheme;
    // _themeType = ThemeType.Dark;
    return notifyListeners();
  }

  setLightTheme() {
    currentTheme = lightTheme;
    // _themeType = ThemeType.Light;
    return notifyListeners();
  }

  setRedTheme() {
    currentTheme = redTheme;
    // _themeType = ThemeType.Red;
    return notifyListeners();
  }
 // settext(){
 //    currentTheme = text;
 //    return notifyListeners();
 // }
  setBlueTheme() {
    currentTheme = blueTheme;
    // _themeType = ThemeType.Blue;
    return notifyListeners();
  }

  setGreenTheme() {
    currentTheme = greenTheme;
    // _themeType = ThemeType.Green;
    return notifyListeners();
  }
}