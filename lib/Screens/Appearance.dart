import 'package:flutter/material.dart';
// import 'package:leadcalls/Screens/MultipleThemes.dart';
import 'package:leadcalls/main.dart';
import 'dart:io';

class Appearance extends StatefulWidget {
  @override
  _AppearanceState createState() => _AppearanceState();
}

class _AppearanceState extends State<Appearance> {
  @override
  Widget build(BuildContext context) {
    Widget expansion = Container(
      child: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, i) {
          return ExpansionTile(
            title: Text(
              items[i].title,
              style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                  color: Colors.black),
            ),
            children: [
              Column(
                children: _buildExpandableContent(items[i]),
              ),
            ],
          );
        },
      ),
    );

    // Widget theme =  Column(
    //   children: [
    //     RaisedButton(
    //       onPressed: () {
    //         Navigator.pushReplacement(
    //             context,
    //             new PageRouteBuilder(
    //                 pageBuilder: (_, __, ___) => MultipleThemes()));
    //       },
    //       child: const Text('Change Theme',
    //           style: TextStyle(fontSize: 20)),
    //     ),
    //   ],
    // );
    //

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Appearance',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w400, fontSize: 20),
        ),
      ),
      body: SafeArea(child: expansion),
    );
  }

  _buildExpandableContent(ListItems fontItem) {
    List<Widget> columnContent = [];
    fontItem.contents.map((element) {
      columnContent.add(
        new ListTile(
            title: new Text(
              element,
              style: new TextStyle(fontSize: 18.0, color: Colors.black),
            ),
            onTap: () {

            }),
      );
    }).toList();
    return columnContent;
  }

  _selected(int index) {}
}

class ListItems {
  final String title;
  List<String> contents = [];
  ListItems(this.title, this.contents);
}

final titles = [
  'Font',
  'Font Size',
];
List<ListItems> items = [
  new ListItems(titles[0], ['Arial', 'Helvetica', 'sans-serif', 'Roman']),
  new ListItems(titles[1], ['Small', 'Medium', 'Large']),
];