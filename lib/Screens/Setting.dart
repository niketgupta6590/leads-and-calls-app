// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:leadcalls/Screens/SugarSettings.dart';
// import 'package:leadcalls/Screens/ThemeModel.dart';
// import 'DeveloperSettings.dart';
// import 'Notifications.dart';
// import 'Appearance.dart';
// import 'OfflineSettings.dart';
// import 'PremiumFeatures.dart';
//
// class Setting extends StatefulWidget {
//   @override
//   _SettingState createState() => _SettingState();
//
// }
//
// class _SettingState extends State<Setting> {
//   final _themeController = StreamController<ThemeData>.broadcast();
//
//   Sink<ThemeData> get inputter => _themeController.sink;
//
//   Stream<ThemeData> get outputter => _themeController.stream;
//   ThemeModel themeModel = new ThemeModel();
//
//   ThemeBloc() {
//     // when the app loads put the initial themeData that
//     //you want to show
//     // inputter.add(initialThemeData);
//     inputter.add(themeModel.currentTheme);
//   }
//
//   //use this to dispose the controller whenever the app gets closed
//   void dispose() {
//     _themeController.close();
//   }
//
//   bool _valueBio = false;
//   bool _value = false;
//
//   void _onChangedBoi(bool value) {
//     setState(() {
//       _valueBio = value;
//     });
//   }
//
//   void _onChanged(bool value) {
//     setState(() {
//       _value = value;
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Padding(
//         padding: EdgeInsets.all(8),
//         child: ListView(
//           children: [
//             ListTile(
//                 trailing: Icon(Icons.keyboard_arrow_right),
//                 leading: Icon(Icons.pan_tool),
//                 title: Text('Premium Feature'),
//                 subtitle: Text('buy or purchases'),
//                 onTap: () {
//                   Navigator.pushReplacement(
//                       context,
//                       PageRouteBuilder(
//                           pageBuilder: (_, __, ___) => PremiumFeatures()));
//                 }
//                 ),
//             ListTile(
//                 trailing: Icon(Icons.keyboard_arrow_right),
//                 leading: Icon(Icons.keyboard_arrow_right),
//                 title: Text('Appearance'),
//                 onTap: () {
//                   Navigator.pushReplacement(
//                       context,
//                       PageRouteBuilder(
//                           pageBuilder: (_, __, ___) => Appearance()));
//                 }
//                 ),
//             Expanded(
//                 child: Column(
//               children: [
//                 SwitchListTile(
//                     value: _valueBio,
//                     title: Text('Biometric Authentication'),
//                     secondary: Icon(Icons.settings),
//                     // subtitle: Text("For my small print"),
//                     onChanged: (bool value) {
//                       _onChangedBoi(value);
//                     }),
//               ],
//             )),
//             ListTile(
//                 trailing: Icon(Icons.arrow_forward_ios),
//                 leading: Icon(Icons.arrow_forward_ios),
//                 title: Text('Sugar Settings'),
//                 onTap: () {
//                   Navigator.pushReplacement(
//                       context,
//                       new PageRouteBuilder(
//                           pageBuilder: (_, __, ___) => SugarSettings()));
//                 }),
//             ListTile(
//               trailing: Icon(Icons.arrow_forward_ios),
//               leading: Icon(Icons.offline_pin),
//               title: Text('Offline Settings'),
//               onTap: () {
//                 Navigator.pushReplacement(
//                     context,
//                     PageRouteBuilder(
//                         pageBuilder: (_, __, ___) => OfflineSettings()));
//               },
//             ),
//             ListTile(
//                 trailing: Icon(Icons.keyboard_arrow_right),
//                 leading: Icon(Icons.add),
//                 title: Text('Notification'),
//                 onTap: () {
//                   Navigator.pushReplacement(
//                       context,
//                       PageRouteBuilder(
//                           pageBuilder: (_, __, ___) => Notifications()));
//                 }),
//             Expanded(
//                 child: Column(
//               children: [
//                 SwitchListTile(
//                     value: _value,
//                     title: Text('Automatic Log Call'),
//                     secondary: Icon(Icons.ac_unit),
//                     onChanged: (bool value) {
//                       _onChanged(value);
//                     }),
//               ],
//             )
//             ),
//             ListTile(
//               trailing: Icon(Icons.keyboard_arrow_right),
//               leading: Icon(Icons.keyboard),
//               title: Text('Developer'),
//               onTap: () {
//                 Navigator.pushReplacement(
//                     context,
//                     PageRouteBuilder(
//                         pageBuilder: (_, __, ___) => DeveloperSettings()));
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
//
// class ThemeBloc {
//   final _themeController = StreamController<ThemeData>.broadcast();
//
//   Sink<ThemeData> get inputter => _themeController.sink;
//
//   Stream<ThemeData> get outputter => _themeController.stream;
//   ThemeModel themeModel = new ThemeModel();
//
//   ThemeBloc() {
//     // when the app loads put the initial themeData that
//     //you want to show
//     // inputter.add(initialThemeData);
//     inputter.add(themeModel.currentTheme);
//   }
//
//   //use this to dispose the controller whenever the app gets closed
//   void dispose() {
//     _themeController.close();
//   }
// }
