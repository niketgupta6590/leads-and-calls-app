import 'package:flutter/material.dart';

class DeveloperSettings extends StatefulWidget {
  @override
  _DeveloperSettingsState createState() => _DeveloperSettingsState();
}

class _DeveloperSettingsState extends State<DeveloperSettings> {
  bool _isSelected = false;

  void _onChanged(bool value) {
    if(_isSelected){
      setState(() {
        _isSelected = value;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text(
            'Developer Settings',
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w400, fontSize: 20),
          ),
        ),
        body: ListView(
          children: ListTile.divideTiles(
            context: context,
            tiles: [
              SwitchListTile(
                  value: _isSelected,
                  title: Text('Developer Mode'),
                  subtitle: Text('show more settings for developers'),
                  secondary: Icon(Icons.settings),
                  onChanged: (bool value) {
                    _onChanged(value);
                  }),
              SwitchListTile(
                  value: _isSelected,
                  title: Text('Show network request status bar'),
                  secondary: Icon(Icons.network_check),
                  onChanged: (bool value) {
                    _onChanged(value);
                  }),
              SwitchListTile(
                  value: _isSelected,
                  title: Text('Show filter panal in fullscreen'),
                  secondary: Icon(Icons.add_alert),
                  onChanged: (bool value) {
                    _onChanged(value);
                  }
                  ),
              SwitchListTile(
                  value: _isSelected,
                  title: Text('Show Preview Features'),
                  subtitle: Text('Quick filter'),
                  secondary: Icon(Icons.filter),
                  onChanged: (bool value) {
                    _onChanged(value);
                  }),
              ListTile(
                trailing: Icon(Icons.arrow_forward_ios),
                leading: Icon(Icons.add_location),
                title: Text('Debug'),
              ),
              ListTile(
                trailing: Icon(Icons.keyboard_arrow_right),
                leading: Icon(Icons.settings),
                title: Text('Developer settings(sugar)'),
              ),
              ListTile(
                trailing: Icon(Icons.keyboard_arrow_down),
                leading: Icon(Icons.list),
                title: Text('Logging setting'),
              ),
              ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text('Exit'),
              ),
            ],
          ).toList(),
        ),
      ),
    );
  }
}
