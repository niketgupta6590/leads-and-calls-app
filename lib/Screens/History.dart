
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:leadcalls/CreateModuleEntry/CreateNote.dart';
import 'package:leadcalls/Screens/NotesList.dart';
import 'package:leadcalls/Models/LeadsResponseModule.dart';

class History extends StatefulWidget {
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  String _selectedItem = " ";
  String selectText = 'testing';
  List<EntryList> entryList = [];

  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          child: Column(
            children: <Widget>[
              Center(
                child: FlatButton.icon(
                    onPressed: () => _onButtonPressed(),
                    icon: Icon(Icons.add),
                    label: Text("Notes")),
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: entryList.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                       //   Navigator.pop(context, entryList[index]);
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Card(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          '${entryList[index].nameValueList.dateEntered.value}',
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ],
                                  ),
                                  ListTile(
                                    title: Text(
                                        '${entryList[index].nameValueList.name.value}'),
                                    trailing: Text(
                                        '${entryList[index].nameValueList.description.value}'),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    }),
              )
            ],
          ),
        ));
  }

  void _onButtonPressed() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            color: Color(0xFF737373),
            height: 140,
            child: Container(
              child: _buildBottomNavigationMenu(),
              decoration: BoxDecoration(
                  color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                    topLeft: const Radius.circular(10),
                    topRight: const Radius.circular(10),
                  )),
            ),
          );
        });
  }

  Column _buildBottomNavigationMenu() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Text("Add a related Calls by"),
        ),
        ListTile(
          title: Text("Create"),
          onTap: () {
            Navigator.pushNamed(context, CreateNotes.id);
          },
        ),
        ListTile(
            title: Text("Select"),
            onTap: () async {
              final EntryList entry = await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => NotesList(_selectedItem)));
              setState(() {
                entryList.add(entry);
              });
            })
      ],
    );
  }

  void _selectItem(String name) {
    Navigator.pop(context);
    setState(() {
      _selectedItem = name;
    });
  }
}