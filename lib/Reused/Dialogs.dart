import 'package:flutter/material.dart';

class Dialogs{
  Future<void> loading(context){
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
                SizedBox(
                  height: 20,
                ),
                Text(
                    'Please wait..'
                ),
              ],
            ),
          );
        }
    );
  }

  dismiss(context) => Navigator.of(context).pop();

}

