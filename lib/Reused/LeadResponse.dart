import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:leadcalls/Models/LeadsResponseModule.dart';
import 'package:leadcalls/Network/Apis.dart';
import 'package:leadcalls/Network/ServiceGenerator.dart';
import 'package:leadcalls/loginPage.dart';
import 'package:leadcalls/utils/Constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Temps.dart';

class LeadResponse {
  static LeadResponse _instance;
  factory LeadResponse() => _instance ??= new LeadResponse._();

  LeadResponse._();
  List modules = ['Leads','Users','Contacts'];

  FormData restData(){
   String restData = '{"session":"$tUserSession","module_name":"Leads","query":"leads.assigned_user_id=\'$tUserId\'"}';

   Map<String, dynamic> body = {
     "input_type": "JSON",
     "response_type": "JSON",
     "method": "get_entry_list",
     "rest_data": "$restData"
   };
   return FormData.fromMap(body);
  }

  Future<void> getList(context) async {

    ServiceGenerator.getReqWithParams(Apis.rest, restData());
  }
}