import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class getNotification{

  FlutterLocalNotificationsPlugin  fltrNotification;


  Future<void> _showNotification(context) async {

    var androidDetails = new AndroidNotificationDetails(
        "channel Id ", "desi programmer", "this is my channel",
        importance: Importance.Max);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationDetails = new NotificationDetails(androidDetails, iosDetails);

    // await fltrNotification.show(
    //     0, "Reminder", "Schedule Notifications", generalNotificationDetails,payload:'Welcome to the Local Notification demo');

    var scheduledTime = DateTime.now().add(Duration(seconds: 5));
    fltrNotification.schedule(1, "Task", "scheduled Notification", scheduledTime, generalNotificationDetails);

    // var date = DateTime.parse(useDateTime);
    // var scheduledTime = date.subtract(Duration(minutes: 15));  // selected popup value write here
    // fltrNotification.schedule(1, "Task", "scheduled Notification", scheduledTime, generalNotificationDetails);
  }
}