import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:leadcalls/LoginPage.dart';
import 'package:leadcalls/Screens/RelatedToSelection.dart';
import 'package:leadcalls/leadListPage.dart';
import 'Res/Colors.dart';
import 'objects/DropDownCalls.dart';
import 'objects/callResponse.dart';
import 'utils/Constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

import 'utils/dropDownLists.dart';

class CreateCallPage extends StatefulWidget {
  static const id = 'createcall';
   String selectModule;
  CreateCallPage(this.selectModule);
  @override
  _CreateCallState createState() => _CreateCallState();
}

class _CreateCallState extends State<CreateCallPage> {
  FlutterLocalNotificationsPlugin fltrNotification;

  final _durationController = TextEditingController();
  final _subjectController = TextEditingController();
  final createdByController = TextEditingController();
  final descController = TextEditingController();
  final modifyByController = TextEditingController();
  final assignUserNameController = TextEditingController();

  List<String> popup_options = ['please wait..'];
  List<String> remainder_options = ['please wait..'];
  List<String> related_to_options = ['please wait..'];
  List<String> status_options = ['please wait..'];
  List<String> duration_options = ['please wait..'];
  List<String> direction_options = ['please wait..'];

  DateTime dateCreated = DateTime.now();

  String selectedStatus1;
  String selectedPopupReminder;
  String _selectedRemainder;
  String selectedDurationHours;
  String selectedDurationMinutes;
  String formattedStartDate;
  String selectedRelatedTo;
  String selectText = '';

  @override
  void initState() {
    super.initState();
    getdropdownValues();

    var androidInitialize = new AndroidInitializationSettings('icon');
    var iOSinitialize = new IOSInitializationSettings();
    var initilizationSettings =
        new InitializationSettings(androidInitialize, iOSinitialize);
    fltrNotification = new FlutterLocalNotificationsPlugin();
    fltrNotification.initialize(initilizationSettings,
        onSelectNotification: notificationSelected);
  }

  @override
  Widget build(BuildContext context) {
    Future _showNotification() async {
      var androidDetails = new AndroidNotificationDetails(
          "channel Id ", "desi programmer", "this is my channel",
          importance: Importance.Max);
      var iosDetails = new IOSNotificationDetails();
      var generalNotificationDetails =
          new NotificationDetails(androidDetails, iosDetails);

      // await fltrNotification.show(
      //     0, "Reminder", "Schedule Notifications", generalNotificationDetails,payload:'Welcome to the Local Notification demo');

      var scheduledTime = DateTime.now().add(Duration(seconds: 5));
      fltrNotification.schedule(1, "Task", "scheduled Notification",
          scheduledTime, generalNotificationDetails);

      // var date = DateTime.parse(useDateTime);
      // var scheduledTime = date.subtract(Duration(minutes: 15));  // selected popup value write here
      // fltrNotification.schedule(1, "Task", "scheduled Notification", scheduledTime, generalNotificationDetails);
    }
    final Size screenSize = MediaQuery.of(context).size;
    Widget overViewSection = Container(
      child: SingleChildScrollView(
          padding:
              EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0, bottom: 50.0),
          child: Column(
            children: <Widget>[
              Row(children: [
                Flexible(child:
                TextFormField(
                    controller: _subjectController,
                    decoration: new InputDecoration(
                        hintText: 'Subject', labelText: 'Subject')),),
              ],),
              Row(children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Text(
                    'Related To',
                    style: TextStyle(fontSize: 12),
                  ),
                ),
              ],),
              Row(
                children: [
                  DropdownButton(
                    value: selectedRelatedTo,
                    items: related_to_options.map((String title) {
                      return DropdownMenuItem(
                        value: title,
                        child: Text(title),
                      );
                    }).toList(),
                    onChanged: (changed) {
                      setState(() {
                        selectedRelatedTo = changed;
                      });
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text(selectText),
                  ),
                  Spacer(),
                  IconButton(
                      icon: Icon(Icons.attachment),
                      onPressed: () async {
                        final text = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    RelatedToSelection(selectedRelatedTo)));
                        setState(() {
                          selectText = text ?? '';
                        });
                      })
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text('Date Created '),
                      Container(
                        height: 10,
                      ),
                      GestureDetector(
                          onTap: () async {
                            await DatePicker.showDateTimePicker(
                              context,
                              minTime: DateTime(1900),
                              currentTime: dateCreated,
                              maxTime: DateTime(2200),
                              onChanged: ((picked) {
                                dateCreated = picked;
                              }),
                            );
                            setState(() {});
                          },
                          child: Text(
                            '$dateCreated',
                            style:
                            TextStyle(color: Colors.black, fontSize: 18.0),
                          )),
                    ],
                  ),
                ],
              ),
              Row(children: [
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Text(
                    'Duration(Hours/Minutes)',
                    style: TextStyle(fontSize: 12),
                  ),
                ),
              ],),
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: DropdownButton(
                      value: selectedDurationHours,
                      items: duration_options.map((title) {
                        return DropdownMenuItem(
                          value: title,
                          child: Text(title),
                        );
                      }).toList(),
                      onChanged: (changed) {
                        setState(() {
                          selectedDurationHours = changed;
                        });
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: DropdownButton(
                      value: selectedDurationMinutes,
                      items: duration_options.map((title) {
                        return DropdownMenuItem(
                          value: title,
                          child: Text(title),
                        );
                      }).toList(),
                      onChanged: (changed) {
                        setState(() {
                          selectedDurationMinutes = changed;
                        });
                      },
                    ),
                  ),
                ],
              ),
              Row(children: [

                Text(
                  'Status',
                  style: TextStyle(fontSize: 12),
                ),
              ],),
              Row(children: [
                DropdownButton(
                  value: selectedStatus1,
                  items: status_options.map((title) {
                    return DropdownMenuItem(
                      value: title,
                      child: Text(title),
                    );
                  }).toList(),
                  onChanged: (changed) {
                    setState(() {
                      selectedStatus1 = changed;
                    });
                  },
                ),
              ],),
              Row(children: [
                Text(
                  'Remainder',
                  style: TextStyle(fontSize: 12),
                ),
              ],),
              Row(
                children: [
                  Text('Popup Remainder  : '),
                  DropdownButton(
                    value: selectedPopupReminder,
                    items: popup_options.map((String title) {
                      return DropdownMenuItem(
                        value: title,
                        child: Text(title),
                      );
                    }).toList(),
                    onChanged: (changed) {
                      setState(() {
                        selectedPopupReminder = changed;
                        _showNotification();
                      });
                    },
                  ),
                ],
              ),
              Row(
                children: [
                  Text('Email Remainder  : '),
                  DropdownButton(
                    value: _selectedRemainder,
                    items: remainder_options.map((title) {
                      return DropdownMenuItem(
                        value: title,
                        child: Text(title),
                      );
                    }).toList(),
                    onChanged: (changed) {
                      setState(() {
                        _selectedRemainder = changed;
                        _showNotification();
                      });
                    },
                  ),
                ],
              ),
              Row(children: [
                Flexible(
                  child: TextFormField(
                    controller: descController,
                    decoration: InputDecoration(
                      labelText: 'Short Description',
                      hintText: 'Short Description',
                    ),
                  ),
                ),
              ],),
              Row(children: [
                Flexible(
                    child: TextFormField(
                      controller: createdByController,
                      decoration: InputDecoration(
                          labelText: 'Created by', hintText: 'Created by'),
                    )),
              ],),
              Row(children: [
                Flexible(
                    child: TextFormField(
                      controller: modifyByController,
                      decoration: InputDecoration(
                          labelText: 'Modified by', hintText: 'Modified by'),
                    )),
              ],),
              Row(children: [
                Flexible(
                    child: TextFormField(
                      controller: assignUserNameController,
                      decoration: InputDecoration(
                          labelText: 'Assigned by', hintText: 'Assigned by'),
                    )),
              ],),

            ],
          )),
    );

    Widget createButton = Container(
      alignment: Alignment.bottomCenter,
      height: 60,
      width: screenSize.width,
      child: Row(
        children: <Widget>[
          Expanded(
            child: new RaisedButton(
              child: new Text(
                'Cancel',
                style: new TextStyle(color: Colors.white),
              ),
              onPressed: () => Navigator.of(context).pop(),
              color: Colors.grey,
            ),
          ),
          Expanded(
            child: new RaisedButton(
              child: new Text(
                'Save',
                style: new TextStyle(color: Colors.white),
              ),
              onPressed: () => _onCreateCall(),
              color: Colors.blue,
            ),
          ),
        ],
      ),
      margin: new EdgeInsets.only(top: 20.0),
    );
    Widget body = Stack(alignment: Alignment.bottomCenter, children: [
      SingleChildScrollView(
        child: Column(
          children: [overViewSection],
        ),
      ),
      createButton,
    ]);
    return Scaffold(
      appBar: AppBar(
        title: Text('Create Call'),
      ),
      body: body,
    );
  }

  _onCreateCall() {
    String subjectValue = _subjectController.text;
    String durationValue = _durationController.text;
    String dateStartValue = _durationController.text;

    if (subjectValue.isEmpty && subjectValue.isEmpty) {
      setState(() {
        String desc = "Please enter details";
        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      });
      return;
    }

    _onSave(subjectValue, durationValue, dateStartValue);
  }

  _onSave(
      String subjectValue, String durationValue, String dateStartValue) async {
    // set up POST request arguments
    String url =
        'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    String callString =
        '{"session":"$userSession","module_name":"Calls","name_value_list":[{"name":"assigned_user_id","value":"$userID"},{"name":"parent_type","value":"$subjectValue" },{"name":"name","value":"$subjectValue"},{"name":"date_start","value":"$dateStartValue"},{"name":"duration_hours","value":"$durationValue"}]}';

    Map<String, Object> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "set_entry",
      "rest_data": "$callString"
    };
    FormData formData = FormData.fromMap(body);
    // make POST request
    Dio dio = new Dio();

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // Do something before request is sent
      print(options.data);
      print(options);
      return options; //continue
    }, onResponse: (Response response) async {
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
//    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());
      if (responseMap != null) {
        CallResponse callresponse = CallResponse.fromJson(json.decode(response.toString()));
        String desc = "Call Created Successfully";
        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.greenAccent,
            textColor: Colors.black,
            fontSize: 16.0);

        _subjectController.clear();
        _durationController.clear();
      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    }
  }

  String generateMd5(String input) {
    /////    return md5.convert(utf8.encode(input)).toString();
  }
  Future<dynamic> readResponse(HttpClientResponse response) {
    var completer = new Completer();
    var contents = new StringBuffer();
    response.transform(utf8.decoder).listen((data) {
      contents.write(data);
    }, onDone: () => completer.complete(contents.toString()));
    return completer.future;
  }

  Future<void> getdropdownValues() async {
    //loading(context);
    // set up POST request arguments
    String url =
        'http://uat.ideadunes.com/projects/devs/testapi_giproperties/call_dom.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    int offset = 0;
    String restData =
        '{"session":"$userSession","module_name":"Calls","query":"assigned_user_id=\'$userID\'","order_by":"status","offset":"$offset","select_fields":[],"link_name_to_fields_array":[],"max_results":"50","deleted":"0","Favorites":false}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_module_fields",
      "rest_data": "$restData"
    };

    FormData formData = FormData.fromMap(body);
    Dio dio = new Dio();
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      return options; //continue
    }, onResponse: (Response response) async {
      return response; // continue
    }, onError: (DioError e) async {
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());
      if (responseMap != null) {
        ///write a code from response
        ///write a code from response
        DropDownCalls dropDowncalls =
            await DropDownCalls.fromJson(jsonDecode(response.toString()));
        setState(() {
          popup_options = dropDowncalls.moduleFields.reminderTime.options;
          selectedPopupReminder = popup_options[0];

          remainder_options =
              dropDowncalls.moduleFields.emailReminderTime.options;
          _selectedRemainder = remainder_options[0];

          status_options = dropDowncalls.moduleFields.status.options;
          selectedStatus1 = status_options[0];

          related_to_options = dropDowncalls.moduleFields.parentName.options;
          selectedRelatedTo = related_to_options[0];

          duration_options = dropDowncalls.moduleFields.durationMinutes.options;
          selectedDurationMinutes = duration_options[0];
          selectedDurationHours = duration_options[0];

          // subproperty_options = dropDowncalls.moduleFields.subPropertyTypeC.options;
          // selectedSubPropertyType = subproperty_options[0];
        });
      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);

        if (desc.contains("session ID is invalid")) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
            ModalRoute.withName('/'),
          );
        }
      }
    }
  }

  Future notificationSelected(String payload) async {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text("Notification clicked $payload"),
      ),
    );
  }
}
