import 'package:flutter/material.dart';
import 'package:leadcalls/createLeadPage.dart';
import 'package:leadcalls/utils/ColorConstants.dart';
import 'leadDetailPage.dart';
import 'objects/leads.dart';

class LeadListPage extends StatefulWidget {
  static const id = "leadList";

  Leads leads;
  LeadListPage(this.leads);

  @override
  _LeadListPageState createState() => _LeadListPageState();
}

class _LeadListPageState extends State<LeadListPage> {
  @override
  Widget build(BuildContext context) {
    Widget body = ListView.builder(
      itemCount: widget.leads.entryList.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () async {
            final isDeleted = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        LeadDetailPage(widget.leads.entryList[index])));
            if (isDeleted) {
              setState(() {
                widget.leads.entryList.removeAt(index);
              });
            }
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              color: getColor(
                  widget.leads.entryList[index].nameValueList.status.value),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            '${widget.leads.entryList[index].nameValueList.dateEntered.value}',
                            textAlign: TextAlign.end,
                          ),
                        ),
                      ],
                    ),
                    ListTile(
                      title: Text(
                          '${widget.leads.entryList[index].nameValueList.name.value}'),
                      subtitle: Text(
                          '${widget.leads.entryList[index].nameValueList.phoneMobile != null ? widget.leads.entryList[index].nameValueList.phoneMobile.value : (widget.leads.entryList[index].nameValueList.email1 != null ? widget.leads.entryList[index].nameValueList.email1.value : " ... ")}'),
                      trailing: Text(
                          '${widget.leads.entryList[index].nameValueList.status.value}'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: (){

             Navigator.pushNamed(context, CreateLeadPage.id);
            },
          )
        ],
        title: Text('Leads'),
      ),
      body: body,
    );
  }

  getColor(String status) {
    switch (status.toLowerCase().replaceAll(" ", "")) {
      case 'assigned':
        return ColorConstants.assigned;
        break;
      case 'open':
        return ColorConstants.open;
        break;
      case 'followup':
        return ColorConstants.followUp;
        break;
      case 'hot':
        return ColorConstants.hot;
        break;
      case 'cold':
        return ColorConstants.cold;
        break;
      case 'new':
        return ColorConstants.newStatus;
        break;
      case 'recycled':
        return ColorConstants.recycled;
        break;
      case 'sold':
        return ColorConstants.soldSatus;
        break;
      case 'dead':
        return ColorConstants.dead;
        break;
      default:
        return Colors.white;
    }
  }
}
