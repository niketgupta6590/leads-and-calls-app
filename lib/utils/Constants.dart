class Constants{
  static const String loginStatusPrefKey = "loginStatus";

  static const String id = 'id';
  static const String userId = 'userID';
  static const String userName = 'name';

  static const String webView = 'Open in web';
  static const String edit = 'Edit';
  static const String share = 'Share';
  static const String delete = 'Delete';
  static const String scanQr = 'Scan QR/Barcode';
  static const String metadata = 'CRM metadata';


  static const String recordId = 'recordId';


  static const List<String> choices = <String>[
    webView,
    edit,
    share,
    delete,
    // scanQr,
    // metadata
  ];

  static const String module = 'module_name';
  static const String module_lead = 'Leads';
  static const String module_calls = 'Calls';
  static const String module_meetings = 'Meetings';
  static const String module_emails = 'Emails';




}