import 'dart:core';

import 'package:flutter/cupertino.dart';

class DropDownLists extends ChangeNotifier {

  static final List<String> titleList = ['QA Analyst', 'Designer', 'Software Developer', 'Mobile Developer', 'Truck Driver'];

  static final List<String> relatedTo = ['Account','Contact','Task', 'Opportunity','Bug','Case','Lead','Project','Project Task','Target','Contract','Invoice','Quote','Product'];

  ////added by oshin
  static final List<String> callStatus1 = ['Contact','Task', 'Opportunity'];
  static final List<String> callStatus2 = ['Contact','Task', 'Opportunity'];

  static final List<String> popuppreminder = ['1 minute prior','5 minute prior', '10 minute prior','15 minute prior','30 minute prior','1 hour prior','1.30 minute prior'];
  static final List<String> emailreminder = ['1 minute prior','5 minute prior', '10 minute prior','15 minute prior','30 minute prior','1 hour prior','30 minute prior'];


}