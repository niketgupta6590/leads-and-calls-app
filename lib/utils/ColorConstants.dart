import 'dart:ui';
import 'package:flutter/material.dart';

class ColorConstants{

  static const Color primaryColor = Color(0xff20c6b6);
  static const Color primaryColorDark = Color(0xff009586);
  static const Color primaryColorLight = Color(0xff68fae8);

  static const Color blueLight = Color(0xffe1f5fe);
  static const Color redLight = Color(0xffffebee);
  static const Color greyLight = Color(0xffeceff1);
  static const Color greenLight = Color(0xffe8f5e9);

//  1. Assigned - #AF7AC5
//  2. Open - #76D7C4
//  3. Follow up - #F4D03F
//  4.Hot - #E74C3C
//  5.Cold  - #52BE80
//  6.New - #3498DB
//  7.Recycled - #D35400
//  8.sold - #CCFF66
//  9. Dead - #BFC9CA

  static const Color assigned = Color(0xffAF7AC5);
  static const Color open = Color(0xff76D7C4);
  static const Color followUp = Color(0xffF4D03F);
  static const Color hot = Color(0xffE74C3C);
  static const Color cold = Color(0xff52BE80);
  static const Color newStatus = Color(0xff3498DB);
  static const Color recycled = Color(0xffD35400);
  static const Color soldSatus = Color(0xffCCFF66);
  static const Color dead = Color(0xffBFC9CA);



  static const MaterialColor primaryColorSwatch = const MaterialColor(
    0xff20c6b6,
    const <int, Color>{
      50: const Color(0xff20c6b6),
      100: const Color(0xff20c6b6),
      200: const Color(0xff20c6b6),
      300: const Color(0xff20c6b6),
      400: const Color(0xff20c6b6),
      500: const Color(0xff20c6b6),
      600: const Color(0xff20c6b6),
      700: const Color(0xff20c6b6),
      800: const Color(0xff20c6b6),
      900: const Color(0xff20c6b6),
    },
  );
}