import 'package:call_log/call_log.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';

class CallLogs extends StatefulWidget {
  @override
  _CallLogsState createState() => _CallLogsState();
}

class _CallLogsState extends State<CallLogs> {
  Iterable<CallLogEntry> _callLogEntries;

  void initState() {
    super.initState();
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text('Permissions error'),
          content: Text('Please enable contacts access '
              'permission in system settings'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () => _setEntriesToServer(),
            )
          ],
        ));
    _callLogs();
  }

  _callLogs() async {
    await _getCallLogs();
  }

  Future<void> _getCallLogs() async {
    final Iterable<CallLogEntry> calllogs = await CallLog.query();
    setState(() {
      _callLogEntries = calllogs;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
        child: _callLogEntries != null
            ? ListView.builder(
                itemCount: _callLogEntries?.length ?? 0,
                itemBuilder: (BuildContext context, int index) {
                  CallLogEntry callLog = _callLogEntries?.elementAt(index);
                  print('===================');
                  print(callLog.name);
                  return Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Icon(callLog.callType == CallType.incoming
                                ? Icons.call_received
                                : Icons.call_made)),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                callLog.name ?? 'Name not found',
                                style: TextStyle(fontSize: 20),
                              ),
                              Text(
                                callLog.number ?? '',
                                style: TextStyle(fontSize: 16),
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: IconButton(
                              icon: Icon(Icons.call),
                              onPressed: () async {
                                FlutterPhoneDirectCaller.callNumber(
                                    callLog.number);
                              }),
                        ),
                      ],
                    ),
                  );
                },
              )
            : Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Please wait while we get your call logs...')
                  ],
                ),
              ),
      )),
    );
  }

  _setEntriesToServer() {
    CallLogEntry callLog ;

    List names = [callLog];
    print(names.length);

  }
}
