import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:leadcalls/menus/sharePage.dart';
import 'package:leadcalls/objects/DeleteRecord.dart';
import 'package:leadcalls/utils/Constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OptionMenu extends StatefulWidget {
  @override
  _OptionMenuState createState() => _OptionMenuState();
}

class _OptionMenuState extends State<OptionMenu> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
        title: Text('Call Details'),
        actions: <Widget>[
        PopupMenuButton<String>(
          onSelected: choiceAction,
          itemBuilder: (BuildContext context) {
            return Constants.choices.map((String choice) {
              return PopupMenuItem<String>(
                value: choice,
                child: Text(choice),
              );
            }).toList();
          },
        ),
      ],
    ),
    );
  }

  choiceAction(String choice) {
    if (choice == Constants.webView) {
      // return WebviewPage();
    } else if (choice == Constants.share) {

    } else if (choice == Constants.edit) {
      String desc = "Copied...";

      Fluttertoast.showToast(
          msg: "$desc",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }

    else if (choice == Constants.delete) {
      AlertDialog(
        title: Text('Do you want to delete?'),
        content: Container(width: 200, height: 200, color: Colors.green),
        actions: <Widget>[
          RaisedButton(onPressed: () => cancelClick(), child: Text('Cancel')),
          RaisedButton(onPressed: () => _deleteRecord(), child: Text('Yes')),
        ],
        actionsPadding: EdgeInsets.symmetric(horizontal: 8.0),
      );
    } else if (choice == Constants.scanQr) {
      ////   String cameraScabResult = await scanner.scan();

    } else if (choice == Constants.metadata) {}
  }
}

cancelClick() {



}

Future<void> _deleteRecord() async {
  // set up POST request arguments
  String url =
      'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';

  SharedPreferences prefs = await SharedPreferences.getInstance();
  String userSession = prefs.getString(Constants.id);
  String userID = prefs.getString(Constants.userId);

  String restData =
      '{"session":"$userSession","module_name":"Calls", "name_value_list":[{"name":"id","value":"$userID" },{"name":"deleted","value": "1"}]}}';
  Map<String, dynamic> body = {
    "input_type": "JSON",
    "response_type": "JSON",
    "method": "set_entry",
    "rest_data": "$restData"
  };

  FormData formData = FormData.fromMap(body);
  // make POST request
  Dio dio = new Dio();

  dio.interceptors
      .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
    // Do something before request is sent
    print(options.data);
    print(options);
    return options; //continue
    // If you want to resolve the request with some custom data，
    // you can return a `Response` object or return `dio.resolve(data)`.
    // If you want to reject the request with a error message,
    // you can return a `DioError` object or return `dio.reject(errMsg)`
  }, onResponse: (Response response) async {
    // Do something with response data
    return response; // continue
  }, onError: (DioError e) async {
    // Do something with response error
    return e; //continue
  }));

  Response response = await dio.post(url, data: formData);
//    // check the status code for the result
  int statusCode = response.statusCode;

  if (statusCode == 200) {
    Map responseMap = json.decode(response.toString());

    if (responseMap != null && responseMap.containsKey('result_count')) {

      DeleteRecord deleteRecord = DeleteRecord.fromJson(json.decode(response.toString()));



    } else {
      String desc = responseMap.containsKey('description')
          ? responseMap['description']
          : "Something went wrong. Try again.";

      Fluttertoast.showToast(
          msg: "$desc",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);

      if (desc.contains("session ID is invalid")) {
        // Navigator.pushAndRemoveUntil(
        //   context,
        //   MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
        //   ModalRoute.withName('/'),
        // );
      }
    }
  }
}