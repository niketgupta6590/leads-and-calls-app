class BugsResponseModule {
  int resultCount;
  String totalCount;
  int nextOffset;
  List<EntryList> entryList;
  ///List<Null> relationshipList;

  BugsResponseModule(
      {this.resultCount,
        this.totalCount,
        this.nextOffset,
        this.entryList,
    ///    this.relationshipList
      });

  BugsResponseModule.fromJson(Map<String, dynamic> json) {
    resultCount = json['result_count'];
    totalCount = json['total_count'];
    nextOffset = json['next_offset'];
    if (json['entry_list'] != null) {
      entryList = new List<EntryList>();
      json['entry_list'].forEach((v) {
        entryList.add(new EntryList.fromJson(v));
      });
    }
    // if (json['relationship_list'] != null) {
    //   relationshipList = new List<Null>();
    //   json['relationship_list'].forEach((v) {
    //     relationshipList.add(new Null.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result_count'] = this.resultCount;
    data['total_count'] = this.totalCount;
    data['next_offset'] = this.nextOffset;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.map((v) => v.toJson()).toList();
    }
    // if (this.relationshipList != null) {
    //   data['relationship_list'] =
    //       this.relationshipList.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

class EntryList {
  String id;
  String moduleName;
  NameValueList nameValueList;

  EntryList({this.id, this.moduleName, this.nameValueList});

  EntryList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    moduleName = json['module_name'];
    nameValueList = json['name_value_list'] != null
        ? new NameValueList.fromJson(json['name_value_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['module_name'] = this.moduleName;
    if (this.nameValueList != null) {
      data['name_value_list'] = this.nameValueList.toJson();
    }
    return data;
  }
}

class NameValueList {
  AssignedUserName assignedUserName;
  AssignedUserName modifiedByName;
  AssignedUserName createdByName;
  AssignedUserName id;
  AssignedUserName name;
  AssignedUserName dateEntered;
  AssignedUserName dateModified;
  AssignedUserName modifiedUserId;
  AssignedUserName createdBy;
  AssignedUserName description;
  AssignedUserName deleted;
  AssignedUserName assignedUserId;
  AssignedUserName bugNumber;
  AssignedUserName type;
  AssignedUserName status;
  AssignedUserName priority;
  AssignedUserName resolution;
  AssignedUserName workLog;
  AssignedUserName foundInRelease;
  AssignedUserName releaseName;
  AssignedUserName fixedInRelease;
  AssignedUserName fixedInReleaseName;
  AssignedUserName source;
  AssignedUserName productCategory;

  NameValueList(
      {this.assignedUserName,
        this.modifiedByName,
        this.createdByName,
        this.id,
        this.name,
        this.dateEntered,
        this.dateModified,
        this.modifiedUserId,
        this.createdBy,
        this.description,
        this.deleted,
        this.assignedUserId,
        this.bugNumber,
        this.type,
        this.status,
        this.priority,
        this.resolution,
        this.workLog,
        this.foundInRelease,
        this.releaseName,
        this.fixedInRelease,
        this.fixedInReleaseName,
        this.source,
        this.productCategory});

  NameValueList.fromJson(Map<String, dynamic> json) {
    assignedUserName = json['assigned_user_name'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_name'])
        : null;
    modifiedByName = json['modified_by_name'] != null
        ? new AssignedUserName.fromJson(json['modified_by_name'])
        : null;
    createdByName = json['created_by_name'] != null
        ? new AssignedUserName.fromJson(json['created_by_name'])
        : null;
    id = json['id'] != null ? new AssignedUserName.fromJson(json['id']) : null;
    name = json['name'] != null
        ? new AssignedUserName.fromJson(json['name'])
        : null;
    dateEntered = json['date_entered'] != null
        ? new AssignedUserName.fromJson(json['date_entered'])
        : null;
    dateModified = json['date_modified'] != null
        ? new AssignedUserName.fromJson(json['date_modified'])
        : null;
    modifiedUserId = json['modified_user_id'] != null
        ? new AssignedUserName.fromJson(json['modified_user_id'])
        : null;
    createdBy = json['created_by'] != null
        ? new AssignedUserName.fromJson(json['created_by'])
        : null;
    description = json['description'] != null
        ? new AssignedUserName.fromJson(json['description'])
        : null;
    deleted = json['deleted'] != null
        ? new AssignedUserName.fromJson(json['deleted'])
        : null;
    assignedUserId = json['assigned_user_id'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_id'])
        : null;
    bugNumber = json['bug_number'] != null
        ? new AssignedUserName.fromJson(json['bug_number'])
        : null;
    type = json['type'] != null
        ? new AssignedUserName.fromJson(json['type'])
        : null;
    status = json['status'] != null
        ? new AssignedUserName.fromJson(json['status'])
        : null;
    priority = json['priority'] != null
        ? new AssignedUserName.fromJson(json['priority'])
        : null;
    resolution = json['resolution'] != null
        ? new AssignedUserName.fromJson(json['resolution'])
        : null;
    workLog = json['work_log'] != null
        ? new AssignedUserName.fromJson(json['work_log'])
        : null;
    foundInRelease = json['found_in_release'] != null
        ? new AssignedUserName.fromJson(json['found_in_release'])
        : null;
    releaseName = json['release_name'] != null
        ? new AssignedUserName.fromJson(json['release_name'])
        : null;
    fixedInRelease = json['fixed_in_release'] != null
        ? new AssignedUserName.fromJson(json['fixed_in_release'])
        : null;
    fixedInReleaseName = json['fixed_in_release_name'] != null
        ? new AssignedUserName.fromJson(json['fixed_in_release_name'])
        : null;
    source = json['source'] != null
        ? new AssignedUserName.fromJson(json['source'])
        : null;
    productCategory = json['product_category'] != null
        ? new AssignedUserName.fromJson(json['product_category'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.assignedUserName != null) {
      data['assigned_user_name'] = this.assignedUserName.toJson();
    }
    if (this.modifiedByName != null) {
      data['modified_by_name'] = this.modifiedByName.toJson();
    }
    if (this.createdByName != null) {
      data['created_by_name'] = this.createdByName.toJson();
    }
    if (this.id != null) {
      data['id'] = this.id.toJson();
    }
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.dateEntered != null) {
      data['date_entered'] = this.dateEntered.toJson();
    }
    if (this.dateModified != null) {
      data['date_modified'] = this.dateModified.toJson();
    }
    if (this.modifiedUserId != null) {
      data['modified_user_id'] = this.modifiedUserId.toJson();
    }
    if (this.createdBy != null) {
      data['created_by'] = this.createdBy.toJson();
    }
    if (this.description != null) {
      data['description'] = this.description.toJson();
    }
    if (this.deleted != null) {
      data['deleted'] = this.deleted.toJson();
    }
    if (this.assignedUserId != null) {
      data['assigned_user_id'] = this.assignedUserId.toJson();
    }
    if (this.bugNumber != null) {
      data['bug_number'] = this.bugNumber.toJson();
    }
    if (this.type != null) {
      data['type'] = this.type.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.priority != null) {
      data['priority'] = this.priority.toJson();
    }
    if (this.resolution != null) {
      data['resolution'] = this.resolution.toJson();
    }
    if (this.workLog != null) {
      data['work_log'] = this.workLog.toJson();
    }
    if (this.foundInRelease != null) {
      data['found_in_release'] = this.foundInRelease.toJson();
    }
    if (this.releaseName != null) {
      data['release_name'] = this.releaseName.toJson();
    }
    if (this.fixedInRelease != null) {
      data['fixed_in_release'] = this.fixedInRelease.toJson();
    }
    if (this.fixedInReleaseName != null) {
      data['fixed_in_release_name'] = this.fixedInReleaseName.toJson();
    }
    if (this.source != null) {
      data['source'] = this.source.toJson();
    }
    if (this.productCategory != null) {
      data['product_category'] = this.productCategory.toJson();
    }
    return data;
  }
}

class AssignedUserName {
  String name;
  String value;

  AssignedUserName({this.name, this.value});

  AssignedUserName.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
