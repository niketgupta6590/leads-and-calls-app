class UserDetailsResponse {
  List<PEntryList> entryList;
//  List<Null> relationshipList;

  UserDetailsResponse({this.entryList,
  ///  this.relationshipList
  });

  UserDetailsResponse.fromJson(Map<String, dynamic> json) {
    if (json['entry_list'] != null) {
      entryList = new List<PEntryList>();
      json['entry_list'].forEach((v) {
        entryList.add(new PEntryList.fromJson(v));
      });
    }
    // if (json['relationship_list'] != null) {
    //   relationshipList = new List<Null>();
    //   json['relationship_list'].forEach((v) {
    //     relationshipList.add(new Null.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.map((v) => v.toJson()).toList();
    }
    // if (this.relationshipList != null) {
    //   data['relationship_list'] =
    //       this.relationshipList.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

class PEntryList {
  String id;
  String moduleName;
  NameValueList nameValueList;

  PEntryList({this.id, this.moduleName, this.nameValueList});

  PEntryList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    moduleName = json['module_name'];
    nameValueList = json['name_value_list'] != null
        ? new NameValueList.fromJson(json['name_value_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['module_name'] = this.moduleName;
    if (this.nameValueList != null) {
      data['name_value_list'] = this.nameValueList.toJson();
    }
    return data;
  }
}

class NameValueList {
  ModifiedByName modifiedByName;
  ModifiedByName createdByName;
  ModifiedByName id;
  ModifiedByName userName;
  ModifiedByName userHash;
  ModifiedByName systemGeneratedPassword;
  ModifiedByName pwdLastChanged;
  ModifiedByName authenticateId;
  ModifiedByName sugarLogin;
  ModifiedByName firstName;
  ModifiedByName lastName;
  ModifiedByName fullName;
  ModifiedByName name;
  ModifiedByName isAdmin;
  ModifiedByName externalAuthOnly;
  ModifiedByName receiveNotifications;
  ModifiedByName description;
  ModifiedByName dateEntered;
  ModifiedByName dateModified;
  ModifiedByName modifiedUserId;
  ModifiedByName createdBy;
  ModifiedByName title;
  ModifiedByName photo;
  ModifiedByName department;
  ModifiedByName phoneHome;
  ModifiedByName phoneMobile;
  ModifiedByName phoneWork;
  ModifiedByName phoneOther;
  ModifiedByName phoneFax;
  ModifiedByName status;
  ModifiedByName addressStreet;
  ModifiedByName addressCity;
  ModifiedByName addressState;
  ModifiedByName addressCountry;
  ModifiedByName addressPostalcode;
  ModifiedByName userType;
  ModifiedByName deleted;
  ModifiedByName portalOnly;
  ModifiedByName showOnEmployees;
  ModifiedByName employeeStatus;
  ModifiedByName messengerId;
  ModifiedByName messengerType;
  ModifiedByName reportsToId;
  ModifiedByName reportsToName;
  ModifiedByName email1;
  ModifiedByName emailLinkType;
  ModifiedByName editorType;
  ModifiedByName isGroup;
  ModifiedByName cAcceptStatusFields;
  ModifiedByName mAcceptStatusFields;
  ModifiedByName acceptStatusId;
  ModifiedByName acceptStatusName;
  ModifiedByName securitygroupNoninherFields;
  ModifiedByName securitygroupNoninheritId;
  ModifiedByName securitygroupNoninheritable;
  ModifiedByName securitygroupPrimaryGroup;
  ModifiedByName factorAuth;
  ModifiedByName factorAuthInterface;

  NameValueList(
      {this.modifiedByName,
        this.createdByName,
        this.id,
        this.userName,
        this.userHash,
        this.systemGeneratedPassword,
        this.pwdLastChanged,
        this.authenticateId,
        this.sugarLogin,
        this.firstName,
        this.lastName,
        this.fullName,
        this.name,
        this.isAdmin,
        this.externalAuthOnly,
        this.receiveNotifications,
        this.description,
        this.dateEntered,
        this.dateModified,
        this.modifiedUserId,
        this.createdBy,
        this.title,
        this.photo,
        this.department,
        this.phoneHome,
        this.phoneMobile,
        this.phoneWork,
        this.phoneOther,
        this.phoneFax,
        this.status,
        this.addressStreet,
        this.addressCity,
        this.addressState,
        this.addressCountry,
        this.addressPostalcode,
        this.userType,
        this.deleted,
        this.portalOnly,
        this.showOnEmployees,
        this.employeeStatus,
        this.messengerId,
        this.messengerType,
        this.reportsToId,
        this.reportsToName,
        this.email1,
        this.emailLinkType,
        this.editorType,
        this.isGroup,
        this.cAcceptStatusFields,
        this.mAcceptStatusFields,
        this.acceptStatusId,
        this.acceptStatusName,
        this.securitygroupNoninherFields,
        this.securitygroupNoninheritId,
        this.securitygroupNoninheritable,
        this.securitygroupPrimaryGroup,
        this.factorAuth,
        this.factorAuthInterface});

  NameValueList.fromJson(Map<String, dynamic> json) {
    modifiedByName = json['modified_by_name'] != null
        ? new ModifiedByName.fromJson(json['modified_by_name'])
        : null;
    createdByName = json['created_by_name'] != null
        ? new ModifiedByName.fromJson(json['created_by_name'])
        : null;
    id = json['id'] != null ? new ModifiedByName.fromJson(json['id']) : null;
    userName = json['user_name'] != null
        ? new ModifiedByName.fromJson(json['user_name'])
        : null;
    userHash = json['user_hash'] != null
        ? new ModifiedByName.fromJson(json['user_hash'])
        : null;
    systemGeneratedPassword = json['system_generated_password'] != null
        ? new ModifiedByName.fromJson(json['system_generated_password'])
        : null;
    pwdLastChanged = json['pwd_last_changed'] != null
        ? new ModifiedByName.fromJson(json['pwd_last_changed'])
        : null;
    authenticateId = json['authenticate_id'] != null
        ? new ModifiedByName.fromJson(json['authenticate_id'])
        : null;
    sugarLogin = json['sugar_login'] != null
        ? new ModifiedByName.fromJson(json['sugar_login'])
        : null;
    firstName = json['first_name'] != null
        ? new ModifiedByName.fromJson(json['first_name'])
        : null;
    lastName = json['last_name'] != null
        ? new ModifiedByName.fromJson(json['last_name'])
        : null;
    fullName = json['full_name'] != null
        ? new ModifiedByName.fromJson(json['full_name'])
        : null;
    name =
    json['name'] != null ? new ModifiedByName.fromJson(json['name']) : null;
    isAdmin = json['is_admin'] != null
        ? new ModifiedByName.fromJson(json['is_admin'])
        : null;
    externalAuthOnly = json['external_auth_only'] != null
        ? new ModifiedByName.fromJson(json['external_auth_only'])
        : null;
    receiveNotifications = json['receive_notifications'] != null
        ? new ModifiedByName.fromJson(json['receive_notifications'])
        : null;
    description = json['description'] != null
        ? new ModifiedByName.fromJson(json['description'])
        : null;
    dateEntered = json['date_entered'] != null
        ? new ModifiedByName.fromJson(json['date_entered'])
        : null;
    dateModified = json['date_modified'] != null
        ? new ModifiedByName.fromJson(json['date_modified'])
        : null;
    modifiedUserId = json['modified_user_id'] != null
        ? new ModifiedByName.fromJson(json['modified_user_id'])
        : null;
    createdBy = json['created_by'] != null
        ? new ModifiedByName.fromJson(json['created_by'])
        : null;
    title = json['title'] != null
        ? new ModifiedByName.fromJson(json['title'])
        : null;
    photo = json['photo'] != null
        ? new ModifiedByName.fromJson(json['photo'])
        : null;
    department = json['department'] != null
        ? new ModifiedByName.fromJson(json['department'])
        : null;
    phoneHome = json['phone_home'] != null
        ? new ModifiedByName.fromJson(json['phone_home'])
        : null;
    phoneMobile = json['phone_mobile'] != null
        ? new ModifiedByName.fromJson(json['phone_mobile'])
        : null;
    phoneWork = json['phone_work'] != null
        ? new ModifiedByName.fromJson(json['phone_work'])
        : null;
    phoneOther = json['phone_other'] != null
        ? new ModifiedByName.fromJson(json['phone_other'])
        : null;
    phoneFax = json['phone_fax'] != null
        ? new ModifiedByName.fromJson(json['phone_fax'])
        : null;
    status = json['status'] != null
        ? new ModifiedByName.fromJson(json['status'])
        : null;
    addressStreet = json['address_street'] != null
        ? new ModifiedByName.fromJson(json['address_street'])
        : null;
    addressCity = json['address_city'] != null
        ? new ModifiedByName.fromJson(json['address_city'])
        : null;
    addressState = json['address_state'] != null
        ? new ModifiedByName.fromJson(json['address_state'])
        : null;
    addressCountry = json['address_country'] != null
        ? new ModifiedByName.fromJson(json['address_country'])
        : null;
    addressPostalcode = json['address_postalcode'] != null
        ? new ModifiedByName.fromJson(json['address_postalcode'])
        : null;
    userType = json['UserType'] != null
        ? new ModifiedByName.fromJson(json['UserType'])
        : null;
    deleted = json['deleted'] != null
        ? new ModifiedByName.fromJson(json['deleted'])
        : null;
    portalOnly = json['portal_only'] != null
        ? new ModifiedByName.fromJson(json['portal_only'])
        : null;
    showOnEmployees = json['show_on_employees'] != null
        ? new ModifiedByName.fromJson(json['show_on_employees'])
        : null;
    employeeStatus = json['employee_status'] != null
        ? new ModifiedByName.fromJson(json['employee_status'])
        : null;
    messengerId = json['messenger_id'] != null
        ? new ModifiedByName.fromJson(json['messenger_id'])
        : null;
    messengerType = json['messenger_type'] != null
        ? new ModifiedByName.fromJson(json['messenger_type'])
        : null;
    reportsToId = json['reports_to_id'] != null
        ? new ModifiedByName.fromJson(json['reports_to_id'])
        : null;
    reportsToName = json['reports_to_name'] != null
        ? new ModifiedByName.fromJson(json['reports_to_name'])
        : null;
    email1 = json['email1'] != null
        ? new ModifiedByName.fromJson(json['email1'])
        : null;
    emailLinkType = json['email_link_type'] != null
        ? new ModifiedByName.fromJson(json['email_link_type'])
        : null;
    editorType = json['editor_type'] != null
        ? new ModifiedByName.fromJson(json['editor_type'])
        : null;
    isGroup = json['is_group'] != null
        ? new ModifiedByName.fromJson(json['is_group'])
        : null;
    cAcceptStatusFields = json['c_accept_status_fields'] != null
        ? new ModifiedByName.fromJson(json['c_accept_status_fields'])
        : null;
    mAcceptStatusFields = json['m_accept_status_fields'] != null
        ? new ModifiedByName.fromJson(json['m_accept_status_fields'])
        : null;
    acceptStatusId = json['accept_status_id'] != null
        ? new ModifiedByName.fromJson(json['accept_status_id'])
        : null;
    acceptStatusName = json['accept_status_name'] != null
        ? new ModifiedByName.fromJson(json['accept_status_name'])
        : null;
    securitygroupNoninherFields = json['securitygroup_noninher_fields'] != null
        ? new ModifiedByName.fromJson(json['securitygroup_noninher_fields'])
        : null;
    securitygroupNoninheritId = json['securitygroup_noninherit_id'] != null
        ? new ModifiedByName.fromJson(json['securitygroup_noninherit_id'])
        : null;
    securitygroupNoninheritable = json['securitygroup_noninheritable'] != null
        ? new ModifiedByName.fromJson(json['securitygroup_noninheritable'])
        : null;
    securitygroupPrimaryGroup = json['securitygroup_primary_group'] != null
        ? new ModifiedByName.fromJson(json['securitygroup_primary_group'])
        : null;
    factorAuth = json['factor_auth'] != null
        ? new ModifiedByName.fromJson(json['factor_auth'])
        : null;
    factorAuthInterface = json['factor_auth_interface'] != null
        ? new ModifiedByName.fromJson(json['factor_auth_interface'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.modifiedByName != null) {
      data['modified_by_name'] = this.modifiedByName.toJson();
    }
    if (this.createdByName != null) {
      data['created_by_name'] = this.createdByName.toJson();
    }
    if (this.id != null) {
      data['id'] = this.id.toJson();
    }
    if (this.userName != null) {
      data['user_name'] = this.userName.toJson();
    }
    if (this.userHash != null) {
      data['user_hash'] = this.userHash.toJson();
    }
    if (this.systemGeneratedPassword != null) {
      data['system_generated_password'] = this.systemGeneratedPassword.toJson();
    }
    if (this.pwdLastChanged != null) {
      data['pwd_last_changed'] = this.pwdLastChanged.toJson();
    }
    if (this.authenticateId != null) {
      data['authenticate_id'] = this.authenticateId.toJson();
    }
    if (this.sugarLogin != null) {
      data['sugar_login'] = this.sugarLogin.toJson();
    }
    if (this.firstName != null) {
      data['first_name'] = this.firstName.toJson();
    }
    if (this.lastName != null) {
      data['last_name'] = this.lastName.toJson();
    }
    if (this.fullName != null) {
      data['full_name'] = this.fullName.toJson();
    }
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.isAdmin != null) {
      data['is_admin'] = this.isAdmin.toJson();
    }
    if (this.externalAuthOnly != null) {
      data['external_auth_only'] = this.externalAuthOnly.toJson();
    }
    if (this.receiveNotifications != null) {
      data['receive_notifications'] = this.receiveNotifications.toJson();
    }
    if (this.description != null) {
      data['description'] = this.description.toJson();
    }
    if (this.dateEntered != null) {
      data['date_entered'] = this.dateEntered.toJson();
    }
    if (this.dateModified != null) {
      data['date_modified'] = this.dateModified.toJson();
    }
    if (this.modifiedUserId != null) {
      data['modified_user_id'] = this.modifiedUserId.toJson();
    }
    if (this.createdBy != null) {
      data['created_by'] = this.createdBy.toJson();
    }
    if (this.title != null) {
      data['title'] = this.title.toJson();
    }
    if (this.photo != null) {
      data['photo'] = this.photo.toJson();
    }
    if (this.department != null) {
      data['department'] = this.department.toJson();
    }
    if (this.phoneHome != null) {
      data['phone_home'] = this.phoneHome.toJson();
    }
    if (this.phoneMobile != null) {
      data['phone_mobile'] = this.phoneMobile.toJson();
    }
    if (this.phoneWork != null) {
      data['phone_work'] = this.phoneWork.toJson();
    }
    if (this.phoneOther != null) {
      data['phone_other'] = this.phoneOther.toJson();
    }
    if (this.phoneFax != null) {
      data['phone_fax'] = this.phoneFax.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.addressStreet != null) {
      data['address_street'] = this.addressStreet.toJson();
    }
    if (this.addressCity != null) {
      data['address_city'] = this.addressCity.toJson();
    }
    if (this.addressState != null) {
      data['address_state'] = this.addressState.toJson();
    }
    if (this.addressCountry != null) {
      data['address_country'] = this.addressCountry.toJson();
    }
    if (this.addressPostalcode != null) {
      data['address_postalcode'] = this.addressPostalcode.toJson();
    }
    if (this.userType != null) {
      data['UserType'] = this.userType.toJson();
    }
    if (this.deleted != null) {
      data['deleted'] = this.deleted.toJson();
    }
    if (this.portalOnly != null) {
      data['portal_only'] = this.portalOnly.toJson();
    }
    if (this.showOnEmployees != null) {
      data['show_on_employees'] = this.showOnEmployees.toJson();
    }
    if (this.employeeStatus != null) {
      data['employee_status'] = this.employeeStatus.toJson();
    }
    if (this.messengerId != null) {
      data['messenger_id'] = this.messengerId.toJson();
    }
    if (this.messengerType != null) {
      data['messenger_type'] = this.messengerType.toJson();
    }
    if (this.reportsToId != null) {
      data['reports_to_id'] = this.reportsToId.toJson();
    }
    if (this.reportsToName != null) {
      data['reports_to_name'] = this.reportsToName.toJson();
    }
    if (this.email1 != null) {
      data['email1'] = this.email1.toJson();
    }
    if (this.emailLinkType != null) {
      data['email_link_type'] = this.emailLinkType.toJson();
    }
    if (this.editorType != null) {
      data['editor_type'] = this.editorType.toJson();
    }
    if (this.isGroup != null) {
      data['is_group'] = this.isGroup.toJson();
    }
    if (this.cAcceptStatusFields != null) {
      data['c_accept_status_fields'] = this.cAcceptStatusFields.toJson();
    }
    if (this.mAcceptStatusFields != null) {
      data['m_accept_status_fields'] = this.mAcceptStatusFields.toJson();
    }
    if (this.acceptStatusId != null) {
      data['accept_status_id'] = this.acceptStatusId.toJson();
    }
    if (this.acceptStatusName != null) {
      data['accept_status_name'] = this.acceptStatusName.toJson();
    }
    if (this.securitygroupNoninherFields != null) {
      data['securitygroup_noninher_fields'] =
          this.securitygroupNoninherFields.toJson();
    }
    if (this.securitygroupNoninheritId != null) {
      data['securitygroup_noninherit_id'] =
          this.securitygroupNoninheritId.toJson();
    }
    if (this.securitygroupNoninheritable != null) {
      data['securitygroup_noninheritable'] =
          this.securitygroupNoninheritable.toJson();
    }
    if (this.securitygroupPrimaryGroup != null) {
      data['securitygroup_primary_group'] =
          this.securitygroupPrimaryGroup.toJson();
    }
    if (this.factorAuth != null) {
      data['factor_auth'] = this.factorAuth.toJson();
    }
    if (this.factorAuthInterface != null) {
      data['factor_auth_interface'] = this.factorAuthInterface.toJson();
    }
    return data;
  }
}

class ModifiedByName {
  String name;
  String value;

  ModifiedByName({this.name, this.value});

  ModifiedByName.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
