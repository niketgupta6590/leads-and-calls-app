// class RelatedModules {
//   String moduleName;
//   String tableName;
//   List<Null> moduleFields;
//   LinkFields linkFields;
//
//   RelatedModules(
//       {this.moduleName, this.tableName, this.moduleFields, this.linkFields});
//
//   RelatedModules.fromJson(Map<String, dynamic> json) {
//     moduleName = json['module_name'];
//     tableName = json['table_name'];
//     if (json['module_fields'] != null) {
//       moduleFields = new List<Null>();
//       json['module_fields'].forEach((v) {
//         moduleFields.add(new Null.fromJson(v));
//       });
//     }
//     linkFields = json['link_fields'] != null
//         ? new LinkFields.fromJson(json['link_fields'])
//         : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['module_name'] = this.moduleName;
//     data['table_name'] = this.tableName;
//     if (this.moduleFields != null) {
//       data['module_fields'] = this.moduleFields.map((v) => v.toJson()).toList();
//     }
//     if (this.linkFields != null) {
//       data['link_fields'] = this.linkFields.toJson();
//     }
//     return data;
//   }
// }
//
// class LinkFields {
//   Contacts contacts;
//   Contacts accounts;
//   Contacts contact;
//   Contacts opportunity;
//   Contacts campaignLeads;
//   Contacts tasks;
//   Contacts notes;
//   Contacts meetings;
//   Contacts calls;
//   Contacts emails;
//   Contacts campaigns;
//
//   LinkFields(
//       {this.contacts,
//         this.accounts,
//         this.contact,
//         this.opportunity,
//         this.campaignLeads,
//         this.tasks,
//         this.notes,
//         this.meetings,
//         this.calls,
//         this.emails,
//         this.campaigns});
//
//   LinkFields.fromJson(Map<String, dynamic> json) {
//     contacts = json['contacts'] != null
//         ? new Contacts.fromJson(json['contacts'])
//         : null;
//     accounts = json['accounts'] != null
//         ? new Contacts.fromJson(json['accounts'])
//         : null;
//     contact =
//     json['contact'] != null ? new Contacts.fromJson(json['contact']) : null;
//     opportunity = json['opportunity'] != null
//         ? new Contacts.fromJson(json['opportunity'])
//         : null;
//     campaignLeads = json['campaign_leads'] != null
//         ? new Contacts.fromJson(json['campaign_leads'])
//         : null;
//     tasks = json['tasks'] != null ? new Contacts.fromJson(json['tasks']) : null;
//     notes = json['notes'] != null ? new Contacts.fromJson(json['notes']) : null;
//     meetings = json['meetings'] != null
//         ? new Contacts.fromJson(json['meetings'])
//         : null;
//     calls = json['calls'] != null ? new Contacts.fromJson(json['calls']) : null;
//     emails =
//     json['emails'] != null ? new Contacts.fromJson(json['emails']) : null;
//     campaigns = json['campaigns'] != null
//         ? new Contacts.fromJson(json['campaigns'])
//         : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.contacts != null) {
//       data['contacts'] = this.contacts.toJson();
//     }
//     if (this.accounts != null) {
//       data['accounts'] = this.accounts.toJson();
//     }
//     if (this.contact != null) {
//       data['contact'] = this.contact.toJson();
//     }
//     if (this.opportunity != null) {
//       data['opportunity'] = this.opportunity.toJson();
//     }
//     if (this.campaignLeads != null) {
//       data['campaign_leads'] = this.campaignLeads.toJson();
//     }
//     if (this.tasks != null) {
//       data['tasks'] = this.tasks.toJson();
//     }
//     if (this.notes != null) {
//       data['notes'] = this.notes.toJson();
//     }
//     if (this.meetings != null) {
//       data['meetings'] = this.meetings.toJson();
//     }
//     if (this.calls != null) {
//       data['calls'] = this.calls.toJson();
//     }
//     if (this.emails != null) {
//       data['emails'] = this.emails.toJson();
//     }
//     if (this.campaigns != null) {
//       data['campaigns'] = this.campaigns.toJson();
//     }
//     return data;
//   }
// }
//
// class Contacts {
//   String name;
//   String type;
//   String group;
//   String idName;
//   String relationship;
//   String module;
//   String beanName;
//
//   Contacts(
//       {this.name,
//         this.type,
//         this.group,
//         this.idName,
//         this.relationship,
//         this.module,
//         this.beanName});
//
//   Contacts.fromJson(Map<String, dynamic> json) {
//     name = json['name'];
//     type = json['type'];
//     group = json['group'];
//     idName = json['id_name'];
//     relationship = json['relationship'];
//     module = json['module'];
//     beanName = json['bean_name'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['name'] = this.name;
//     data['type'] = this.type;
//     data['group'] = this.group;
//     data['id_name'] = this.idName;
//     data['relationship'] = this.relationship;
//     data['module'] = this.module;
//     data['bean_name'] = this.beanName;
//     return data;
//   }
// }
