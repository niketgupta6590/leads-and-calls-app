class Modules {
  List<Module> modules;

  Modules({this.modules});

  Modules.fromJson(Map<String, dynamic> json) {
    if (json['modules'] != null) {
      modules = new List<Module>();
      json['modules'].forEach((v) {
        modules.add(new Module.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.modules != null) {
      data['modules'] = this.modules.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Module {
  String moduleKey;
  String moduleLabel;
  bool favoriteEnabled;
  List<Acls> acls;

  Module({this.moduleKey, this.moduleLabel, this.favoriteEnabled, this.acls});

  Module.fromJson(Map<String, dynamic> json) {
    moduleKey = json['module_key'];
    moduleLabel = json['module_label'];
    favoriteEnabled = json['favorite_enabled'];
    if (json['acls'] != null) {
      acls = new List<Acls>();
      json['acls'].forEach((v) {
        acls.add(new Acls.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['module_key'] = this.moduleKey;
    data['module_label'] = this.moduleLabel;
    data['favorite_enabled'] = this.favoriteEnabled;
    if (this.acls != null) {
      data['acls'] = this.acls.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Acls {
  String action;
  bool access;

  Acls({this.action, this.access});

  Acls.fromJson(Map<String, dynamic> json) {
    action = json['action'];
    access = json['access'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['action'] = this.action;
    data['access'] = this.access;
    return data;
  }
}
