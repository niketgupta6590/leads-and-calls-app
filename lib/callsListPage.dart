import 'package:flutter/material.dart';
import 'package:leadcalls/callsDetailPage.dart';
import 'createCallPage.dart';
import 'objects/callListResponse.dart';
import 'objects/calls.dart';
import 'utils/ColorConstants.dart';

class CallsListPage extends StatefulWidget {

  static  const id = 'callList';

  CallsListResponse calls;
  CallsListPage(this.calls);

  @override
  _CallsListPageState createState() => _CallsListPageState();
}

class _CallsListPageState extends State<CallsListPage> {
  @override
  Widget build(BuildContext context) {
    Widget body = ListView.builder(
      itemCount: widget.calls.entryList.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () async{
            final isDeleted = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CallsDetailPage(widget.calls.entryList[index])
                )
            );
            if(isDeleted){
              setState(() {
                widget.calls.entryList.removeAt(index);
              });
            }
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              color: getColor(widget.calls.entryList[index].nameValueList.status),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text('${widget.calls.entryList[index].nameValueList.status} / ${widget.calls.entryList[index].nameValueList.direction}',
                        style: TextStyle(fontWeight: FontWeight.w500),),
                        Expanded(
                          child: Text(
                              '${widget.calls.entryList[index].nameValueList.dateEntered}',
                            textAlign: TextAlign.end,
                          ),
                        ),
                      ],
                    ),
                    ListTile(
                      title: Text(
                          '${widget.calls.entryList[index].nameValueList.name}'),
                      subtitle: Text(
                          '${widget.calls.entryList[index].nameValueList.description != null ? widget.calls.entryList[index].nameValueList.description : " ... "}'),
                      trailing: Text(
                          '${widget.calls.entryList[index].nameValueList.status}'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
    return Scaffold(
    appBar: AppBar(
      actions: [
        IconButton(
          icon: Icon(Icons.add),
          onPressed: (){
            Navigator.pushNamed(context, CreateCallPage.id);

          },
        )
      ],
      title: Text('Calls'),
    ),
        body: body
    );
  }

  getColor(String status) {
    switch (status.toLowerCase().replaceAll(" ", "")) {
      case 'planned':
        return ColorConstants.assigned;
        break;
      case 'open':
        return ColorConstants.open;
        break;
      case 'followup':
        return ColorConstants.followUp;
        break;
      case 'hot':
        return ColorConstants.hot;
        break;
      case 'held':
        return ColorConstants.cold;
        break;
      case 'new':
        return ColorConstants.newStatus;
        break;
      case 'recycled':
        return ColorConstants.recycled;
        break;
      case 'sold':
        return ColorConstants.soldSatus;
        break;
      case 'dead':
        return ColorConstants.dead;
        break;
      default:
        return Colors.white;
    }
  }
}
