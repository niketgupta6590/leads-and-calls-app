import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:leadcalls/CallLogs.dart';
import 'package:leadcalls/CreateModuleEntry/CreateContact.dart';
import 'package:leadcalls/Home.dart';
import 'package:leadcalls/LoginPage.dart';
import 'package:leadcalls/Screens/Calender.dart';
import 'package:leadcalls/Screens/Lister.dart';
import 'package:leadcalls/Screens/ModuleList.dart';
import 'package:leadcalls/Screens/Profile.dart';
import 'package:leadcalls/createCallPage.dart';
import 'package:leadcalls/createEmailPage.dart';
import 'package:leadcalls/createLeadPage.dart';
import 'package:leadcalls/createMeetingPage.dart';
import 'package:leadcalls/utils/ColorConstants.dart';
import 'package:leadcalls/utils/Constants.dart';
import 'package:leadcalls/Res/Styles.dart';
import 'package:leadcalls/Res/Colors.dart';
import 'About.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Screens/Contacts.dart';
import 'Screens/Seeting.dart';
import 'dart:io';
import 'db/DatabaseHelper.dart';
import 'db/ModuleDb.dart';

class DashboardPage extends StatefulWidget {
  static const id = 'dashboard';

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  String userName = '';
  bool fromDb = false;
  int currentIndex = 0, currItem = 0;
  List<ModuleDb> modules = [];
  final List<Widget> screens = [
    Home(),
    Contacts(),
    CallLogs(),
    Calender(),
    Settings(),
    About(),
  ];
  final List<String> menus = [
    'Home',
    'Contacts',
    'Call Log',
    'Calender',
    'Setting',
    'About',
  ];
  final List<String> icons = [
    'icons8-circled-h-100.png',
    'icons8-circled-c-100.png',
    'icons8-circled-c-100.png',
    'icons8-circled-c-100.png',
    'icons8-circled-s-100.png',
    'icons8-circled-s-100.png',
  ];
  @override
  void initState() {
    super.initState();
    _getLists();
    getUserName();
  }

  _getLists() async {
    await DatabaseHelper.instance.get().then((value) {
      if (value.length > 0) {
        modules.clear();
        for (ModuleDb module in value) {
          modules.add(module);
        }
      }
      setState(() {

      });
    }).catchError((onError) {
      print(onError);
    });
  }

  Future<void> _confirmDialog(context, isLogout) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Confirmation'),
            content: Text(
                isLogout ? 'Are you want to logout?' : 'Are you want to Exit?'),
            actions: [
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('No')),
              FlatButton(
                  onPressed: () async {
                    if (isLogout) {
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      prefs.clear();
                      Navigator.pushNamedAndRemoveUntil(
                          context, LoginPage.id, (route) => false);
                    } else
                      exit(0);
                  },
                  child: Text('Yes'))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final sWidth = MediaQuery.of(context).size.width;
    return WillPopScope(
        onWillPop: () async {
          _confirmDialog(context, false);
          return false;
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              fromDb ? modules[currItem].moduleKey : menus[currentIndex],
            ),
            actions: [
              // modules[currItem].moduleKey == 'Home'?
              //
              // IconButton(
              //   icon: Icon(Icons.keyboard_arrow_right), onPressed: () {  },
              // ):
              IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  switch (modules[currItem].moduleKey) {
                    case 'Leads':
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  CreateLeadPage(modules[currItem].moduleKey)));
                      break;
                    case 'Calls':
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  CreateCallPage(modules[currItem].moduleKey)));                      break;
                    case 'Meetings':
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  CreateMeetingPage(modules[currItem].moduleKey)));                      break;
                    case 'Emails':
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  CreateEmailPage(modules[currItem].moduleKey)));                      break;
                    case 'Contacts':
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  CreateContact(modules[currItem].moduleKey)));                      break;
                  }
                },
              )
            ],
          ),
          body: SafeArea(
            child: fromDb
                ? Lister(modules[currItem].moduleKey)
                : screens[currentIndex],
          ),
          drawer: Container(
            color: Colors.white,
            width: sWidth * 0.75,
            child: Column(mainAxisSize: MainAxisSize.max, children: [
              Container(
                width: double.infinity,
                height: sWidth * 0.6,
                color: ColorConstants.primaryColor,
                child: SafeArea(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 15, 15, 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        InkWell(
                          onTap: () {
                            _confirmDialog(context, true);
                          },
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: EdgeInsets.only(right: sWidth * 0.02),
                              child: Icon(
                                FontAwesomeIcons.signOutAlt,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, Edit_profile.id);
                          },
                          child: Column(
                            children: [
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CircleAvatar(
                                    radius: 35,
                                    backgroundColor: Colors.white,
                                    child: Text(
                                      userName.split('')[0] ?? '',
                                      style: TextStyle(
                                          fontSize: sWidth * 0.1,
                                          color: ColorConstants.primaryColor),
                                    ),
                                  )),
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(userName ?? 'no found',
                                    overflow: TextOverflow.ellipsis,
                                    style: sIdleText.copyWith(
                                        fontSize: sWidth * 0.04)),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: sWidth * 0.03,
              ),
              // SizedBox(
              //   height: sWidth * 0.03,
              // ),
              MenuTiles(
                currentIndex: currentIndex,
                activeIndex: (i) {
                  print(i);
                  setState(() {
                    fromDb = false;
                    currentIndex = i;
                  });
                  Navigator.of(context).pop();
                },
                menus: menus,
                icons: icons,
                sWidth: sWidth,
              ),
              Divider(
                thickness: 1.5,
                height: 1.5,
              ),
              Container(
                padding: EdgeInsets.fromLTRB(5, 0, 20, 0),
                child: InkWell(
                  onTap: () async {
                    final res =
                        await Navigator.pushNamed(context, ModuleList.id);
                    if (res) _getLists();
                  },
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                            padding:
                                EdgeInsets.symmetric(horizontal: sWidth * 0.01),
                            child: CircleAvatar(
                              radius: sWidth * 0.04,
                              child: Text('C'),
                            )),
                        Padding(
                          padding: EdgeInsets.only(left: sWidth * 0.02),
                          child: Text(
                            'Customize Tiles',
                            style: TextStyle(
                                fontWeight: FontWeight.w800,
                                fontSize: sWidth * 0.045),
                          ),
                        ),
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Icon(Icons.dashboard, color: cBlack),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Divider(
                thickness: 1.5,
                height: 1.5,
              ),

              Expanded(
                child: ListView.builder(
                    physics: BouncingScrollPhysics(),
                    itemCount: modules.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          setState(() {
                            Navigator.of(context).pop();
                            currItem = index;
                            fromDb = true;
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.fromLTRB(5, 0, 20, 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: sWidth * 0.01),
                                  child: CircleAvatar(
                                    radius: sWidth * 0.04,
                                    child: Text(
                                        modules[index].moduleKey.split('')[0]),
                                    backgroundColor: index % 2 == 0
                                        ? ColorConstants.primaryColor
                                        .withOpacity(0.4)
                                        : ColorConstants.primaryColor
                                        .withOpacity(0.2),
                                  )),
                              Padding(
                                padding: EdgeInsets.only(left: sWidth * 0.02),
                                child: Text(
                                  modules[index].moduleKey,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      fontSize: sWidth * 0.045,
                                      color: currItem == index
                                          ? ColorConstants.primaryColor
                                          : cBlack),
                                ),
                              ),
                              Spacer(),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Icon(Icons.navigate_next,
                                    color: currItem == index
                                        ? ColorConstants.primaryColor
                                        : cBlack),
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
              ),
            ]),
          ),
        ));
  }

  Future<void> getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      userName = prefs.getString(Constants.userName);
    });
  }
}

class MenuTiles extends StatelessWidget {
  const MenuTiles({
    this.currentIndex,
    this.activeIndex,
    this.menus,
    this.icons,
    Key key,
    @required this.sWidth,
  }) : super(key: key);

  final double sWidth;
  final int currentIndex;
  final List<String> menus;
  final List<String> icons;
  final Function(int) activeIndex;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        child: Column(
          children: [
            for (int i = 0; i < menus.length; i++)
              Container(
                padding: EdgeInsets.fromLTRB(5, 0, 20, 0),
                child: InkWell(
                  onTap: () => activeIndex(i),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: sWidth * 0.01),
                          child: Image.asset(
                            'images/${icons[i]}',
                            height: 26.0,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: sWidth * 0.03),
                          child: Text(
                            menus[i],
                            style: sIdleText.copyWith(
                                fontSize: sWidth * 0.05,
                                color: i == currentIndex
                                    ? ColorConstants.primaryColor
                                    : cBlack),
                          ),
                        ),
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Icon(Icons.navigate_next,
                              color: i == currentIndex
                                  ? ColorConstants.primaryColor
                                  : cBlack),
                        ),
                      ],
                    ),
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }
}
