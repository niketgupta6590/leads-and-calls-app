import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'ModuleDb.dart';

class DatabaseHelper {
  static final _databaseName = "modules.db";
  static final _databaseVersion = 1;

  static final table = 'module';

  static final colId = 'id';
  static final colModuleKey = 'moduleKey';
  static final colModuleName = 'moduleName';

  DatabaseHelper._();
  static final DatabaseHelper instance = DatabaseHelper._();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await createDatabase();
    return _database;
  }

  Future<Database> createDatabase() async {
    String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $table (
            $colId INTEGER PRIMARY KEY AUTOINCREMENT,
            $colModuleKey TEXT NOT NULL,
            $colModuleName TEXT NOT NULL
          )
          ''');
  }

  Future<List<ModuleDb>> get() async {
    final db = await instance.database;
    var modules = await db.query(table);
    List<ModuleDb> modulesList = [];
    modules.forEach((element) {
      ModuleDb module = ModuleDb.fromMap(element);
      modulesList.add(module);
    });
    return modulesList;
  }

  Future<ModuleDb> insert(ModuleDb module) async {
    final db = await instance.database;
    module.id = await db.insert(table, module.toMap());
    return module;
  }

  Future<int> update(ModuleDb module) async {
    Database db = await instance.database;
    String mKey = module.moduleKey;
    return await db.update(table, module.toMap(),
        where: '$mKey = ?', whereArgs: [mKey]);
  }

  Future<int> delete(ModuleDb module) async {
    Database db = await instance.database;
    String mKey = module.moduleKey;
    return await db
        .delete(table, where: '$colModuleKey = ?', whereArgs: [mKey]);
  }
}
