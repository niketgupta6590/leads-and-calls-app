import 'DatabaseHelper.dart';
class ModuleDb {
  int id;
  String moduleKey, moduleName;

  ModuleDb({this.id, this.moduleKey, this.moduleName});

  ModuleDb.fromMap(Map<String, dynamic> map) {
    id = map[DatabaseHelper.colId];
    moduleKey = map[DatabaseHelper.colModuleKey];
    moduleName = map[DatabaseHelper.colModuleName];
  }

  Map<String, dynamic> toMap() {
    var map = <String,dynamic>{
      DatabaseHelper.colModuleKey: moduleKey,
      DatabaseHelper.colModuleName: moduleName,
    };
    if(id!=null){
      map[DatabaseHelper.colId] = id;
    }
    return map;
  }
}
