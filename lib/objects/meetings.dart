class Meetings {
  int resultCount;
  String totalCount;
  int nextOffset;
  List<EntryList> entryList;
//  List<Null> relationshipList;

  Meetings(
      {this.resultCount,
        this.totalCount,
        this.nextOffset,
        this.entryList,
   ///     this.relationshipList
      });

  Meetings.fromJson(Map<String, dynamic> json) {
    resultCount = json['result_count'];
    totalCount = json['total_count'];
    nextOffset = json['next_offset'];
    if (json['entry_list'] != null) {
      entryList = new List<EntryList>();
      json['entry_list'].forEach((v) {
        entryList.add(new EntryList.fromJson(v));
      });
    }
    // if (json['relationship_list'] != null) {
    //   relationshipList = new List<Null>();
    //   json['relationship_list'].forEach((v) {
    //     relationshipList.add(new Null.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result_count'] = this.resultCount;
    data['total_count'] = this.totalCount;
    data['next_offset'] = this.nextOffset;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.map((v) => v.toJson()).toList();
    }
    // if (this.relationshipList != null) {
    //   data['relationship_list'] =
    //       this.relationshipList.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

class EntryList {
  String id;
  String moduleName;
  NameValueList nameValueList;

  EntryList({this.id, this.moduleName, this.nameValueList});

  EntryList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    moduleName = json['module_name'];
    nameValueList = json['name_value_list'] != null
        ? new NameValueList.fromJson(json['name_value_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['module_name'] = this.moduleName;
    if (this.nameValueList != null) {
      data['name_value_list'] = this.nameValueList.toJson();
    }
    return data;
  }
}

class NameValueList {
  AssignedUserName assignedUserName;
  AssignedUserName modifiedByName;
  AssignedUserName createdByName;
  AssignedUserName id;
  AssignedUserName name;
  AssignedUserName dateEntered;
  AssignedUserName dateModified;
  AssignedUserName modifiedUserId;
  AssignedUserName createdBy;
  AssignedUserName description;
  AssignedUserName deleted;
  AssignedUserName assignedUserId;
  AssignedUserName acceptStatus;
  AssignedUserName location;
  AssignedUserName password;
  AssignedUserName joinUrl;
  AssignedUserName hostUrl;
  AssignedUserName displayedUrl;
  AssignedUserName creator;
  AssignedUserName externalId;
  AssignedUserName durationHours;
  AssignedUserName durationMinutes;
  AssignedUserName dateStart;
  AssignedUserName dateEnd;
  AssignedUserName parentType;
  AssignedUserName status;
  AssignedUserName type;
  AssignedUserName direction;
  AssignedUserName parentId;
  ReminderChecked reminderChecked;
  AssignedUserName reminderTime;
  ReminderChecked emailReminderChecked;
  AssignedUserName emailReminderTime;
  AssignedUserName emailReminderSent;
  AssignedUserName reminders;
  AssignedUserName outlookId;
  AssignedUserName sequence;
  AssignedUserName contactName;
  AssignedUserName parentName;
  AssignedUserName contactId;
  AssignedUserName repeatType;
  AssignedUserName repeatInterval;
  AssignedUserName repeatDow;
  AssignedUserName repeatUntil;
  AssignedUserName repeatCount;
  AssignedUserName repeatParentId;
  AssignedUserName recurringSource;
  AssignedUserName duration;
  AssignedUserName gsyncId;
  AssignedUserName gsyncLastsync;
  AssignedUserName jjwgMapsLngC;
  AssignedUserName jjwgMapsGeocodeStatusC;
  AssignedUserName jjwgMapsAddressC;
  AssignedUserName jjwgMapsLatC;

  NameValueList(
      {this.assignedUserName,
        this.modifiedByName,
        this.createdByName,
        this.id,
        this.name,
        this.dateEntered,
        this.dateModified,
        this.modifiedUserId,
        this.createdBy,
        this.description,
        this.deleted,
        this.assignedUserId,
        this.acceptStatus,
        this.location,
        this.password,
        this.joinUrl,
        this.hostUrl,
        this.displayedUrl,
        this.creator,
        this.externalId,
        this.durationHours,
        this.durationMinutes,
        this.dateStart,
        this.dateEnd,
        this.parentType,
        this.status,
        this.type,
        this.direction,
        this.parentId,
        this.reminderChecked,
        this.reminderTime,
        this.emailReminderChecked,
        this.emailReminderTime,
        this.emailReminderSent,
        this.reminders,
        this.outlookId,
        this.sequence,
        this.contactName,
        this.parentName,
        this.contactId,
        this.repeatType,
        this.repeatInterval,
        this.repeatDow,
        this.repeatUntil,
        this.repeatCount,
        this.repeatParentId,
        this.recurringSource,
        this.duration,
        this.gsyncId,
        this.gsyncLastsync,
        this.jjwgMapsLngC,
        this.jjwgMapsGeocodeStatusC,
        this.jjwgMapsAddressC,
        this.jjwgMapsLatC});

  NameValueList.fromJson(Map<String, dynamic> json) {
    assignedUserName = json['assigned_user_name'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_name'])
        : null;
    modifiedByName = json['modified_by_name'] != null
        ? new AssignedUserName.fromJson(json['modified_by_name'])
        : null;
    createdByName = json['created_by_name'] != null
        ? new AssignedUserName.fromJson(json['created_by_name'])
        : null;
    id = json['id'] != null ? new AssignedUserName.fromJson(json['id']) : null;
    name = json['name'] != null
        ? new AssignedUserName.fromJson(json['name'])
        : null;
    dateEntered = json['date_entered'] != null
        ? new AssignedUserName.fromJson(json['date_entered'])
        : null;
    dateModified = json['date_modified'] != null
        ? new AssignedUserName.fromJson(json['date_modified'])
        : null;
    modifiedUserId = json['modified_user_id'] != null
        ? new AssignedUserName.fromJson(json['modified_user_id'])
        : null;
    createdBy = json['created_by'] != null
        ? new AssignedUserName.fromJson(json['created_by'])
        : null;
    description = json['description'] != null
        ? new AssignedUserName.fromJson(json['description'])
        : null;
    deleted = json['deleted'] != null
        ? new AssignedUserName.fromJson(json['deleted'])
        : null;
    assignedUserId = json['assigned_user_id'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_id'])
        : null;
    acceptStatus = json['accept_status'] != null
        ? new AssignedUserName.fromJson(json['accept_status'])
        : null;
    location = json['location'] != null
        ? new AssignedUserName.fromJson(json['location'])
        : null;
    password = json['password'] != null
        ? new AssignedUserName.fromJson(json['password'])
        : null;
    joinUrl = json['join_url'] != null
        ? new AssignedUserName.fromJson(json['join_url'])
        : null;
    hostUrl = json['host_url'] != null
        ? new AssignedUserName.fromJson(json['host_url'])
        : null;
    displayedUrl = json['displayed_url'] != null
        ? new AssignedUserName.fromJson(json['displayed_url'])
        : null;
    creator = json['creator'] != null
        ? new AssignedUserName.fromJson(json['creator'])
        : null;
    externalId = json['external_id'] != null
        ? new AssignedUserName.fromJson(json['external_id'])
        : null;
    durationHours = json['duration_hours'] != null
        ? new AssignedUserName.fromJson(json['duration_hours'])
        : null;
    durationMinutes = json['duration_minutes'] != null
        ? new AssignedUserName.fromJson(json['duration_minutes'])
        : null;
    dateStart = json['date_start'] != null
        ? new AssignedUserName.fromJson(json['date_start'])
        : null;
    dateEnd = json['date_end'] != null
        ? new AssignedUserName.fromJson(json['date_end'])
        : null;
    parentType = json['parent_type'] != null
        ? new AssignedUserName.fromJson(json['parent_type'])
        : null;
    status = json['status'] != null
        ? new AssignedUserName.fromJson(json['status'])
        : null;
    type = json['type'] != null
        ? new AssignedUserName.fromJson(json['type'])
        : null;
    direction = json['direction'] != null
        ? new AssignedUserName.fromJson(json['direction'])
        : null;
    parentId = json['parent_id'] != null
        ? new AssignedUserName.fromJson(json['parent_id'])
        : null;
    reminderChecked = json['reminder_checked'] != null
        ? new ReminderChecked.fromJson(json['reminder_checked'])
        : null;
    reminderTime = json['reminder_time'] != null
        ? new AssignedUserName.fromJson(json['reminder_time'])
        : null;
    emailReminderChecked = json['email_reminder_checked'] != null
        ? new ReminderChecked.fromJson(json['email_reminder_checked'])
        : null;
    emailReminderTime = json['email_reminder_time'] != null
        ? new AssignedUserName.fromJson(json['email_reminder_time'])
        : null;
    emailReminderSent = json['email_reminder_sent'] != null
        ? new AssignedUserName.fromJson(json['email_reminder_sent'])
        : null;
    reminders = json['reminders'] != null
        ? new AssignedUserName.fromJson(json['reminders'])
        : null;
    outlookId = json['outlook_id'] != null
        ? new AssignedUserName.fromJson(json['outlook_id'])
        : null;
    sequence = json['sequence'] != null
        ? new AssignedUserName.fromJson(json['sequence'])
        : null;
    contactName = json['contact_name'] != null
        ? new AssignedUserName.fromJson(json['contact_name'])
        : null;
    parentName = json['parent_name'] != null
        ? new AssignedUserName.fromJson(json['parent_name'])
        : null;
    contactId = json['contact_id'] != null
        ? new AssignedUserName.fromJson(json['contact_id'])
        : null;
    repeatType = json['repeat_type'] != null
        ? new AssignedUserName.fromJson(json['repeat_type'])
        : null;
    repeatInterval = json['repeat_interval'] != null
        ? new AssignedUserName.fromJson(json['repeat_interval'])
        : null;
    repeatDow = json['repeat_dow'] != null
        ? new AssignedUserName.fromJson(json['repeat_dow'])
        : null;
    repeatUntil = json['repeat_until'] != null
        ? new AssignedUserName.fromJson(json['repeat_until'])
        : null;
    repeatCount = json['repeat_count'] != null
        ? new AssignedUserName.fromJson(json['repeat_count'])
        : null;
    repeatParentId = json['repeat_parent_id'] != null
        ? new AssignedUserName.fromJson(json['repeat_parent_id'])
        : null;
    recurringSource = json['recurring_source'] != null
        ? new AssignedUserName.fromJson(json['recurring_source'])
        : null;
    duration = json['duration'] != null
        ? new AssignedUserName.fromJson(json['duration'])
        : null;
    gsyncId = json['gsync_id'] != null
        ? new AssignedUserName.fromJson(json['gsync_id'])
        : null;
    gsyncLastsync = json['gsync_lastsync'] != null
        ? new AssignedUserName.fromJson(json['gsync_lastsync'])
        : null;
    jjwgMapsLngC = json['jjwg_maps_lng_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_lng_c'])
        : null;
    jjwgMapsGeocodeStatusC = json['jjwg_maps_geocode_status_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_geocode_status_c'])
        : null;
    jjwgMapsAddressC = json['jjwg_maps_address_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_address_c'])
        : null;
    jjwgMapsLatC = json['jjwg_maps_lat_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_lat_c'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.assignedUserName != null) {
      data['assigned_user_name'] = this.assignedUserName.toJson();
    }
    if (this.modifiedByName != null) {
      data['modified_by_name'] = this.modifiedByName.toJson();
    }
    if (this.createdByName != null) {
      data['created_by_name'] = this.createdByName.toJson();
    }
    if (this.id != null) {
      data['id'] = this.id.toJson();
    }
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.dateEntered != null) {
      data['date_entered'] = this.dateEntered.toJson();
    }
    if (this.dateModified != null) {
      data['date_modified'] = this.dateModified.toJson();
    }
    if (this.modifiedUserId != null) {
      data['modified_user_id'] = this.modifiedUserId.toJson();
    }
    if (this.createdBy != null) {
      data['created_by'] = this.createdBy.toJson();
    }
    if (this.description != null) {
      data['description'] = this.description.toJson();
    }
    if (this.deleted != null) {
      data['deleted'] = this.deleted.toJson();
    }
    if (this.assignedUserId != null) {
      data['assigned_user_id'] = this.assignedUserId.toJson();
    }
    if (this.acceptStatus != null) {
      data['accept_status'] = this.acceptStatus.toJson();
    }
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    if (this.password != null) {
      data['password'] = this.password.toJson();
    }
    if (this.joinUrl != null) {
      data['join_url'] = this.joinUrl.toJson();
    }
    if (this.hostUrl != null) {
      data['host_url'] = this.hostUrl.toJson();
    }
    if (this.displayedUrl != null) {
      data['displayed_url'] = this.displayedUrl.toJson();
    }
    if (this.creator != null) {
      data['creator'] = this.creator.toJson();
    }
    if (this.externalId != null) {
      data['external_id'] = this.externalId.toJson();
    }
    if (this.durationHours != null) {
      data['duration_hours'] = this.durationHours.toJson();
    }
    if (this.durationMinutes != null) {
      data['duration_minutes'] = this.durationMinutes.toJson();
    }
    if (this.dateStart != null) {
      data['date_start'] = this.dateStart.toJson();
    }
    if (this.dateEnd != null) {
      data['date_end'] = this.dateEnd.toJson();
    }
    if (this.parentType != null) {
      data['parent_type'] = this.parentType.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.type != null) {
      data['type'] = this.type.toJson();
    }
    if (this.direction != null) {
      data['direction'] = this.direction.toJson();
    }
    if (this.parentId != null) {
      data['parent_id'] = this.parentId.toJson();
    }
    if (this.reminderChecked != null) {
      data['reminder_checked'] = this.reminderChecked.toJson();
    }
    if (this.reminderTime != null) {
      data['reminder_time'] = this.reminderTime.toJson();
    }
    if (this.emailReminderChecked != null) {
      data['email_reminder_checked'] = this.emailReminderChecked.toJson();
    }
    if (this.emailReminderTime != null) {
      data['email_reminder_time'] = this.emailReminderTime.toJson();
    }
    if (this.emailReminderSent != null) {
      data['email_reminder_sent'] = this.emailReminderSent.toJson();
    }
    if (this.reminders != null) {
      data['reminders'] = this.reminders.toJson();
    }
    if (this.outlookId != null) {
      data['outlook_id'] = this.outlookId.toJson();
    }
    if (this.sequence != null) {
      data['sequence'] = this.sequence.toJson();
    }
    if (this.contactName != null) {
      data['contact_name'] = this.contactName.toJson();
    }
    if (this.parentName != null) {
      data['parent_name'] = this.parentName.toJson();
    }
    if (this.contactId != null) {
      data['contact_id'] = this.contactId.toJson();
    }
    if (this.repeatType != null) {
      data['repeat_type'] = this.repeatType.toJson();
    }
    if (this.repeatInterval != null) {
      data['repeat_interval'] = this.repeatInterval.toJson();
    }
    if (this.repeatDow != null) {
      data['repeat_dow'] = this.repeatDow.toJson();
    }
    if (this.repeatUntil != null) {
      data['repeat_until'] = this.repeatUntil.toJson();
    }
    if (this.repeatCount != null) {
      data['repeat_count'] = this.repeatCount.toJson();
    }
    if (this.repeatParentId != null) {
      data['repeat_parent_id'] = this.repeatParentId.toJson();
    }
    if (this.recurringSource != null) {
      data['recurring_source'] = this.recurringSource.toJson();
    }
    if (this.duration != null) {
      data['duration'] = this.duration.toJson();
    }
    if (this.gsyncId != null) {
      data['gsync_id'] = this.gsyncId.toJson();
    }
    if (this.gsyncLastsync != null) {
      data['gsync_lastsync'] = this.gsyncLastsync.toJson();
    }
    if (this.jjwgMapsLngC != null) {
      data['jjwg_maps_lng_c'] = this.jjwgMapsLngC.toJson();
    }
    if (this.jjwgMapsGeocodeStatusC != null) {
      data['jjwg_maps_geocode_status_c'] = this.jjwgMapsGeocodeStatusC.toJson();
    }
    if (this.jjwgMapsAddressC != null) {
      data['jjwg_maps_address_c'] = this.jjwgMapsAddressC.toJson();
    }
    if (this.jjwgMapsLatC != null) {
      data['jjwg_maps_lat_c'] = this.jjwgMapsLatC.toJson();
    }
    return data;
  }
}

class AssignedUserName {
  String name;
  String value;

  AssignedUserName({this.name, this.value});

  AssignedUserName.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

class ReminderChecked {
  String name;
  bool value;

  ReminderChecked({this.name, this.value});

  ReminderChecked.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
