class UserResponse {
  String id;
  String moduleName;
  NameValueList nameValueList;

  UserResponse({this.id, this.moduleName, this.nameValueList});

  UserResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    moduleName = json['module_name'];
    nameValueList = json['name_value_list'] != null
        ? new NameValueList.fromJson(json['name_value_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['module_name'] = this.moduleName;
    if (this.nameValueList != null) {
      data['name_value_list'] = this.nameValueList.toJson();
    }
    return data;
  }
}

class NameValueList {
  UserId userId;
  UserId userName;
  UserId userLanguage;
  UserId userCurrencyId;
  UserIsAdmin userIsAdmin;
  UserDefaultTeamId userDefaultTeamId;
  UserId userDefaultDateformat;
  UserId userDefaultTimeformat;
  UserId userNumberSeparator;
  UserId userDecimalSeparator;
  UserDefaultTeamId mobileMaxListEntries;
  UserDefaultTeamId mobileMaxSubpanelEntries;
  UserId userCurrencyName;

  NameValueList(
      {this.userId,
        this.userName,
        this.userLanguage,
        this.userCurrencyId,
        this.userIsAdmin,
        this.userDefaultTeamId,
        this.userDefaultDateformat,
        this.userDefaultTimeformat,
        this.userNumberSeparator,
        this.userDecimalSeparator,
        this.mobileMaxListEntries,
        this.mobileMaxSubpanelEntries,
        this.userCurrencyName});

  NameValueList.fromJson(Map<String, dynamic> json) {
    userId =
    json['user_id'] != null ? new UserId.fromJson(json['user_id']) : null;
    userName = json['user_name'] != null
        ? new UserId.fromJson(json['user_name'])
        : null;
    userLanguage = json['user_language'] != null
        ? new UserId.fromJson(json['user_language'])
        : null;
    userCurrencyId = json['user_currency_id'] != null
        ? new UserId.fromJson(json['user_currency_id'])
        : null;
    userIsAdmin = json['user_is_admin'] != null
        ? new UserIsAdmin.fromJson(json['user_is_admin'])
        : null;
    userDefaultTeamId = json['user_default_team_id'] != null
        ? new UserDefaultTeamId.fromJson(json['user_default_team_id'])
        : null;
    userDefaultDateformat = json['user_default_dateformat'] != null
        ? new UserId.fromJson(json['user_default_dateformat'])
        : null;
    userDefaultTimeformat = json['user_default_timeformat'] != null
        ? new UserId.fromJson(json['user_default_timeformat'])
        : null;
    userNumberSeparator = json['user_number_separator'] != null
        ? new UserId.fromJson(json['user_number_separator'])
        : null;
    userDecimalSeparator = json['user_decimal_separator'] != null
        ? new UserId.fromJson(json['user_decimal_separator'])
        : null;
    mobileMaxListEntries = json['mobile_max_list_entries'] != null
        ? new UserDefaultTeamId.fromJson(json['mobile_max_list_entries'])
        : null;
    mobileMaxSubpanelEntries = json['mobile_max_subpanel_entries'] != null
        ? new UserDefaultTeamId.fromJson(json['mobile_max_subpanel_entries'])
        : null;
    userCurrencyName = json['user_currency_name'] != null
        ? new UserId.fromJson(json['user_currency_name'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userId != null) {
      data['user_id'] = this.userId.toJson();
    }
    if (this.userName != null) {
      data['user_name'] = this.userName.toJson();
    }
    if (this.userLanguage != null) {
      data['user_language'] = this.userLanguage.toJson();
    }
    if (this.userCurrencyId != null) {
      data['user_currency_id'] = this.userCurrencyId.toJson();
    }
    if (this.userIsAdmin != null) {
      data['user_is_admin'] = this.userIsAdmin.toJson();
    }
    if (this.userDefaultTeamId != null) {
      data['user_default_team_id'] = this.userDefaultTeamId.toJson();
    }
    if (this.userDefaultDateformat != null) {
      data['user_default_dateformat'] = this.userDefaultDateformat.toJson();
    }
    if (this.userDefaultTimeformat != null) {
      data['user_default_timeformat'] = this.userDefaultTimeformat.toJson();
    }
    if (this.userNumberSeparator != null) {
      data['user_number_separator'] = this.userNumberSeparator.toJson();
    }
    if (this.userDecimalSeparator != null) {
      data['user_decimal_separator'] = this.userDecimalSeparator.toJson();
    }
    if (this.mobileMaxListEntries != null) {
      data['mobile_max_list_entries'] = this.mobileMaxListEntries.toJson();
    }
    if (this.mobileMaxSubpanelEntries != null) {
      data['mobile_max_subpanel_entries'] =
          this.mobileMaxSubpanelEntries.toJson();
    }
    if (this.userCurrencyName != null) {
      data['user_currency_name'] = this.userCurrencyName.toJson();
    }
    return data;
  }
}

class UserId {
  String name;
  String value;

  UserId({this.name, this.value});

  UserId.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

class UserIsAdmin {
  String name;
  bool value;

  UserIsAdmin({this.name, this.value});

  UserIsAdmin.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

class UserDefaultTeamId {
  String name;
  Null value;

  UserDefaultTeamId({this.name, this.value});

  UserDefaultTeamId.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}