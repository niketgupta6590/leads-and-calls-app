class UpdateRecordLead {
  String id;
  EntryListm entryList;

  UpdateRecordLead({this.id, this.entryList});

  UpdateRecordLead.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    entryList = json['entry_list'] != null
        ? new EntryListm.fromJson(json['entry_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.toJson();
    }
    return data;
  }
}

class EntryListm {
  AssignedUserId assignedUserId;
  AssignedUserId firstName;
  AssignedUserId status;
  AssignedUserId altAddressStreet;
  AssignedUserId altAddressCity;
  AssignedUserId phoneWork;
  AssignedUserId altAddressState;
  AssignedUserId altAddressPostalcode;
  AssignedUserId altAddressCountry;
  AssignedUserId website;
  AssignedUserId statusDescription;
  AssignedUserId leadSourceDescription;
  AssignedUserId opportunityAmount;
  AssignedUserId referedBy;
  AssignedUserId campaignName;
  AssignedUserId leadSource;
  AssignedUserId id;

  EntryListm(
      {this.assignedUserId,
        this.firstName,
        this.status,
        this.altAddressStreet,
        this.altAddressCity,
        this.phoneWork,
        this.altAddressState,
        this.altAddressPostalcode,
        this.altAddressCountry,
        this.website,
        this.statusDescription,
        this.leadSourceDescription,
        this.opportunityAmount,
        this.referedBy,
        this.campaignName,
        this.leadSource,
        this.id});

  EntryListm.fromJson(Map<String, dynamic> json) {
    assignedUserId = json['assigned_user_id'] != null
        ? new AssignedUserId.fromJson(json['assigned_user_id'])
        : null;
    firstName = json['first_name'] != null
        ? new AssignedUserId.fromJson(json['first_name'])
        : null;
    status = json['status'] != null
        ? new AssignedUserId.fromJson(json['status'])
        : null;
    altAddressStreet = json['alt_address_street'] != null
        ? new AssignedUserId.fromJson(json['alt_address_street'])
        : null;
    altAddressCity = json['alt_address_city'] != null
        ? new AssignedUserId.fromJson(json['alt_address_city'])
        : null;
    phoneWork = json['phone_work'] != null
        ? new AssignedUserId.fromJson(json['phone_work'])
        : null;
    altAddressState = json['alt_address_state'] != null
        ? new AssignedUserId.fromJson(json['alt_address_state'])
        : null;
    altAddressPostalcode = json['alt_address_postalcode'] != null
        ? new AssignedUserId.fromJson(json['alt_address_postalcode'])
        : null;
    altAddressCountry = json['alt_address_country'] != null
        ? new AssignedUserId.fromJson(json['alt_address_country'])
        : null;
    website = json['website'] != null
        ? new AssignedUserId.fromJson(json['website'])
        : null;
    statusDescription = json['status_description'] != null
        ? new AssignedUserId.fromJson(json['status_description'])
        : null;
    leadSourceDescription = json['lead_source_description'] != null
        ? new AssignedUserId.fromJson(json['lead_source_description'])
        : null;
    opportunityAmount = json['opportunity_amount'] != null
        ? new AssignedUserId.fromJson(json['opportunity_amount'])
        : null;
    referedBy = json['refered_by'] != null
        ? new AssignedUserId.fromJson(json['refered_by'])
        : null;
    campaignName = json['campaign_name'] != null
        ? new AssignedUserId.fromJson(json['campaign_name'])
        : null;
    leadSource = json['lead_source'] != null
        ? new AssignedUserId.fromJson(json['lead_source'])
        : null;
    id = json['id'] != null ? new AssignedUserId.fromJson(json['id']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.assignedUserId != null) {
      data['assigned_user_id'] = this.assignedUserId.toJson();
    }
    if (this.firstName != null) {
      data['first_name'] = this.firstName.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.altAddressStreet != null) {
      data['alt_address_street'] = this.altAddressStreet.toJson();
    }
    if (this.altAddressCity != null) {
      data['alt_address_city'] = this.altAddressCity.toJson();
    }
    if (this.phoneWork != null) {
      data['phone_work'] = this.phoneWork.toJson();
    }
    if (this.altAddressState != null) {
      data['alt_address_state'] = this.altAddressState.toJson();
    }
    if (this.altAddressPostalcode != null) {
      data['alt_address_postalcode'] = this.altAddressPostalcode.toJson();
    }
    if (this.altAddressCountry != null) {
      data['alt_address_country'] = this.altAddressCountry.toJson();
    }
    if (this.website != null) {
      data['website'] = this.website.toJson();
    }
    if (this.statusDescription != null) {
      data['status_description'] = this.statusDescription.toJson();
    }
    if (this.leadSourceDescription != null) {
      data['lead_source_description'] = this.leadSourceDescription.toJson();
    }
    if (this.opportunityAmount != null) {
      data['opportunity_amount'] = this.opportunityAmount.toJson();
    }
    if (this.referedBy != null) {
      data['refered_by'] = this.referedBy.toJson();
    }
    if (this.campaignName != null) {
      data['campaign_name'] = this.campaignName.toJson();
    }
    if (this.leadSource != null) {
      data['lead_source'] = this.leadSource.toJson();
    }
    if (this.id != null) {
      data['id'] = this.id.toJson();
    }
    return data;
  }
}

class AssignedUserId {
  String name;
  String value;

  AssignedUserId({this.name, this.value});

  AssignedUserId.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
