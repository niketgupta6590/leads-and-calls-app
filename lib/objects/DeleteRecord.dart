class DeleteRecord {
  String id;
  ResEntryList resEntryList;

  DeleteRecord({this.id, this.resEntryList});

  DeleteRecord.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    resEntryList = json['entry_list'] != null
        ? new ResEntryList.fromJson(json['entry_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.resEntryList != null) {
      data['entry_list'] = this.resEntryList.toJson();
    }
    return data;
  }
}

class ResEntryList {
  Id id;
  Id deleted;

  ResEntryList({this.id, this.deleted});

  ResEntryList.fromJson(Map<String, dynamic> json) {
    id = json['id'] != null ? new Id.fromJson(json['id']) : null;
    deleted = json['deleted'] != null ? new Id.fromJson(json['deleted']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.id != null) {
      data['id'] = this.id.toJson();
    }
    if (this.deleted != null) {
      data['deleted'] = this.deleted.toJson();
    }
    return data;
  }
}

class Id {
  String name;
  String value;

  Id({this.name, this.value});

  Id.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
