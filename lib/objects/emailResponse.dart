class EmailResponse {
  String id;
  pEntryList entryList;

  EmailResponse({this.id, this.entryList});

  EmailResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    entryList = json['entry_list'] != null
        ? new pEntryList.fromJson(json['entry_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.toJson();
    }
    return data;
  }
}

class pEntryList {
  ToAddrsNames toAddrsNames;
  ToAddrsNames bccAddrsNames;
  ToAddrsNames ccAddrsNames;
  ToAddrsNames name;
  ToAddrsNames description;
  ToAddrsNames assignedUserId;

  pEntryList(
      {this.toAddrsNames,
        this.bccAddrsNames,
        this.ccAddrsNames,
        this.name,
        this.description,
        this.assignedUserId});

  pEntryList.fromJson(Map<String, dynamic> json) {
    toAddrsNames = json['to_addrs_names'] != null
        ? new ToAddrsNames.fromJson(json['to_addrs_names'])
        : null;
    bccAddrsNames = json['bcc_addrs_names'] != null
        ? new ToAddrsNames.fromJson(json['bcc_addrs_names'])
        : null;
    ccAddrsNames = json['cc_addrs_names'] != null
        ? new ToAddrsNames.fromJson(json['cc_addrs_names'])
        : null;
    name =
    json['name'] != null ? new ToAddrsNames.fromJson(json['name']) : null;
    description = json['description'] != null
        ? new ToAddrsNames.fromJson(json['description'])
        : null;
    assignedUserId = json['assigned_user_id'] != null
        ? new ToAddrsNames.fromJson(json['assigned_user_id'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.toAddrsNames != null) {
      data['to_addrs_names'] = this.toAddrsNames.toJson();
    }
    if (this.bccAddrsNames != null) {
      data['bcc_addrs_names'] = this.bccAddrsNames.toJson();
    }
    if (this.ccAddrsNames != null) {
      data['cc_addrs_names'] = this.ccAddrsNames.toJson();
    }
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.description != null) {
      data['description'] = this.description.toJson();
    }
    if (this.assignedUserId != null) {
      data['assigned_user_id'] = this.assignedUserId.toJson();
    }
    return data;
  }
}

class ToAddrsNames {
  String name;
  String value;

  ToAddrsNames({this.name, this.value});

  ToAddrsNames.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
