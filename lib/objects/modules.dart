// To parse this JSON data, do
//
//     final modules = modulesFromJson(jsonString);

import 'dart:convert';

// Modules modulesFromJson(String str) => Modules.fromJson(json.decode(str));
//
// String modulesToJson(Modules data) => json.encode(data.toJson());

class Modules {
  List<Module> modules;

  Modules({
    this.modules
  });



  factory Modules.fromJson(Map<String, dynamic> json) => Modules(
    modules: List<Module>.from(json["modules"].map((x) => Module.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "modules": List<dynamic>.from(modules.map((x) => x.toJson())),
  };
}

class Module {

  String moduleKey;
  String moduleLabel;
  bool favoriteEnabled;
  List<Acl> acls;

  Module(
      {
    this.moduleKey,
    this.moduleLabel,
    this.favoriteEnabled,
    this.acls,
  });



  factory Module.fromJson(Map<String, dynamic> json) => Module(
    moduleKey: json["module_key"],
    moduleLabel: json["module_label"],
    favoriteEnabled: json["favorite_enabled"],
    acls: List<Acl>.from(json["acls"].map((x) => Acl.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "module_key": moduleKey,
    "module_label": moduleLabel,
    "favorite_enabled": favoriteEnabled,
    "acls": List<dynamic>.from(acls.map((x) => x.toJson())),
  };
}

class Acl {
  Acl({
    this.action,
    this.access,
  });

  Action action;
  bool access;

  factory Acl.fromJson(Map<String, dynamic> json) => Acl(
    action: actionValues.map[json["action"]],
    access: json["access"],
  );

  Map<String, dynamic> toJson() => {
    "action": actionValues.reverse[action],
    "access": access,
  };
}

enum Action { EDIT, DELETE, LIST, VIEW, IMPORT, EXPORT }

final actionValues = EnumValues({
  "delete": Action.DELETE,
  "edit": Action.EDIT,
  "export": Action.EXPORT,
  "import": Action.IMPORT,
  "list": Action.LIST,
  "view": Action.VIEW
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }



  // factory Modules.fromMap(Map<String, dynamic> json) => new Module(
  //   moduleKey: json["module_key"],
  //   moduleLabel: json["module_label"],);

}
