class EventStatusStruct {
  EventStatusName eventStatusName;

  EventStatusStruct({this.eventStatusName});

  EventStatusStruct.fromJson(Map<String, dynamic> json) {
    eventStatusName = json['event_status_name'] != null
        ? new EventStatusName.fromJson(json['event_status_name'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.eventStatusName != null) {
      data['event_status_name'] = this.eventStatusName.toJson();
    }
    return data;
  }
}

class EventStatusName {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  String len;

  EventStatusName(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  EventStatusName.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class Options {
  Invited invited;
  Invited notInvited;
  Invited attended;
  Invited notAttended;

  Options({this.invited, this.notInvited, this.attended, this.notAttended});

  Options.fromJson(Map<String, dynamic> json) {
    invited =
    json['Invited'] != null ? new Invited.fromJson(json['Invited']) : null;
    notInvited = json['Not Invited'] != null
        ? new Invited.fromJson(json['Not Invited'])
        : null;
    attended = json['Attended'] != null
        ? new Invited.fromJson(json['Attended'])
        : null;
    notAttended = json['Not Attended'] != null
        ? new Invited.fromJson(json['Not Attended'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.invited != null) {
      data['Invited'] = this.invited.toJson();
    }
    if (this.notInvited != null) {
      data['Not Invited'] = this.notInvited.toJson();
    }
    if (this.attended != null) {
      data['Attended'] = this.attended.toJson();
    }
    if (this.notAttended != null) {
      data['Not Attended'] = this.notAttended.toJson();
    }
    return data;
  }
}

class Invited {
  String name;
  String value;

  Invited({this.name, this.value});

  Invited.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

