class CallsListResponse {
  int resultCount;
  String totalCount;
  int nextOffset;
  List<EntryList> entryList;

  CallsListResponse(
      {this.resultCount, this.totalCount, this.nextOffset, this.entryList});

  CallsListResponse.fromJson(Map<String, dynamic> json) {
    resultCount = json['result_count'];
    totalCount = json['total_count'];
    nextOffset = json['next_offset'];
    if (json['entry_list'] != null) {
      entryList = new List<EntryList>();
      json['entry_list'].forEach((v) {
        entryList.add(new EntryList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result_count'] = this.resultCount;
    data['total_count'] = this.totalCount;
    data['next_offset'] = this.nextOffset;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class EntryList {
  String id;
  String moduleName;
  NameValueList nameValueList;

  EntryList({this.id, this.moduleName, this.nameValueList});

  EntryList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    moduleName = json['module_name'];
    nameValueList = json['name_value_list'] != null
        ? new NameValueList.fromJson(json['name_value_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['module_name'] = this.moduleName;
    if (this.nameValueList != null) {
      data['name_value_list'] = this.nameValueList.toJson();
    }
    return data;
  }
}

class NameValueList {
  String assignedUserName;
  String modifiedByName;
  String createdByName;
  String id;
  String name;
  String dateEntered;
  String dateModified;
  String modifiedUserId;
  String createdBy;
  String description;
  String deleted;
  String assignedUserId;
  String durationHours;
  String durationMinutes;
  String dateStart;
  String dateEnd;
  String parentType;
  String parentName;
  String status;
  String direction;
  String parentId;
  bool reminderChecked;
  String reminderTime;
  bool emailReminderChecked;
  String emailReminderTime;
  String emailReminderSent;
  String reminders;
  String outlookId;
  String acceptStatus;
  String contactName;
  String contactId;
  String repeatType;
  String repeatInterval;
  String repeatDow;
  String repeatUntil;
  String repeatCount;
  String repeatParentId;
  String recurringSource;
  String rescheduleHistory;
  String rescheduleCount;

  NameValueList(
      {this.assignedUserName,
        this.modifiedByName,
        this.createdByName,
        this.id,
        this.name,
        this.dateEntered,
        this.dateModified,
        this.modifiedUserId,
        this.createdBy,
        this.description,
        this.deleted,
        this.assignedUserId,
        this.durationHours,
        this.durationMinutes,
        this.dateStart,
        this.dateEnd,
        this.parentType,
        this.parentName,
        this.status,
        this.direction,
        this.parentId,
        this.reminderChecked,
        this.reminderTime,
        this.emailReminderChecked,
        this.emailReminderTime,
        this.emailReminderSent,
        this.reminders,
        this.outlookId,
        this.acceptStatus,
        this.contactName,
        this.contactId,
        this.repeatType,
        this.repeatInterval,
        this.repeatDow,
        this.repeatUntil,
        this.repeatCount,
        this.repeatParentId,
        this.recurringSource,
        this.rescheduleHistory,
        this.rescheduleCount});

  NameValueList.fromJson(Map<String, dynamic> json) {
    assignedUserName = json['assigned_user_name'];
    modifiedByName = json['modified_by_name'];
    createdByName = json['created_by_name'];
    id = json['id'];
    name = json['name'];
    dateEntered = json['date_entered'];
    dateModified = json['date_modified'];
    modifiedUserId = json['modified_user_id'];
    createdBy = json['created_by'];
    description = json['description'];
    deleted = json['deleted'];
    assignedUserId = json['assigned_user_id'];
    durationHours = json['duration_hours'];
    durationMinutes = json['duration_minutes'];
    dateStart = json['date_start'];
    dateEnd = json['date_end'];
    parentType = json['parent_type'];
    parentName = json['parent_name'];
    status = json['status'];
    direction = json['direction'];
    parentId = json['parent_id'];
    reminderChecked = json['reminder_checked'];
    reminderTime = json['reminder_time'];
    emailReminderChecked = json['email_reminder_checked'];
    emailReminderTime = json['email_reminder_time'];
    emailReminderSent = json['email_reminder_sent'];
    reminders = json['reminders'];
    outlookId = json['outlook_id'];
    acceptStatus = json['accept_status'];
    contactName = json['contact_name'];
    contactId = json['contact_id'];
    repeatType = json['repeat_type'];
    repeatInterval = json['repeat_interval'];
    repeatDow = json['repeat_dow'];
    repeatUntil = json['repeat_until'];
    repeatCount = json['repeat_count'];
    repeatParentId = json['repeat_parent_id'];
    recurringSource = json['recurring_source'];
    rescheduleHistory = json['reschedule_history'];
    rescheduleCount = json['reschedule_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['assigned_user_name'] = this.assignedUserName;
    data['modified_by_name'] = this.modifiedByName;
    data['created_by_name'] = this.createdByName;
    data['id'] = this.id;
    data['name'] = this.name;
    data['date_entered'] = this.dateEntered;
    data['date_modified'] = this.dateModified;
    data['modified_user_id'] = this.modifiedUserId;
    data['created_by'] = this.createdBy;
    data['description'] = this.description;
    data['deleted'] = this.deleted;
    data['assigned_user_id'] = this.assignedUserId;
    data['duration_hours'] = this.durationHours;
    data['duration_minutes'] = this.durationMinutes;
    data['date_start'] = this.dateStart;
    data['date_end'] = this.dateEnd;
    data['parent_type'] = this.parentType;
    data['parent_name'] = this.parentName;
    data['status'] = this.status;
    data['direction'] = this.direction;
    data['parent_id'] = this.parentId;
    data['reminder_checked'] = this.reminderChecked;
    data['reminder_time'] = this.reminderTime;
    data['email_reminder_checked'] = this.emailReminderChecked;
    data['email_reminder_time'] = this.emailReminderTime;
    data['email_reminder_sent'] = this.emailReminderSent;
    data['reminders'] = this.reminders;
    data['outlook_id'] = this.outlookId;
    data['accept_status'] = this.acceptStatus;
    data['contact_name'] = this.contactName;
    data['contact_id'] = this.contactId;
    data['repeat_type'] = this.repeatType;
    data['repeat_interval'] = this.repeatInterval;
    data['repeat_dow'] = this.repeatDow;
    data['repeat_until'] = this.repeatUntil;
    data['repeat_count'] = this.repeatCount;
    data['repeat_parent_id'] = this.repeatParentId;
    data['recurring_source'] = this.recurringSource;
    data['reschedule_history'] = this.rescheduleHistory;
    data['reschedule_count'] = this.rescheduleCount;
    return data;
  }
}
