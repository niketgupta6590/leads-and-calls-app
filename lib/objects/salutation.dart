class SalutationStruct {
  Salutation salutation;

  SalutationStruct({this.salutation});

  SalutationStruct.fromJson(Map<String, dynamic> json) {
    salutation = json['salutation'] != null
        ? new Salutation.fromJson(json['salutation'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.salutation != null) {
      data['salutation'] = this.salutation.toJson();
    }
    return data;
  }
}

class Salutation {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  int len;

  Salutation(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  Salutation.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class Options {
  Mr mr;
  Mr ms;
  Mr mrs;
  Mr miss;
  Mr dr;
  Mr prof;

  Options({this.mr, this.ms, this.mrs, this.miss, this.dr, this.prof});

  Options.fromJson(Map<String, dynamic> json) {
    mr = json['Mr.'] != null ? new Mr.fromJson(json['Mr.']) : null;
    ms = json['Ms.'] != null ? new Mr.fromJson(json['Ms.']) : null;
    mrs = json['Mrs.'] != null ? new Mr.fromJson(json['Mrs.']) : null;
    miss = json['Miss'] != null ? new Mr.fromJson(json['Miss']) : null;
    dr = json['Dr.'] != null ? new Mr.fromJson(json['Dr.']) : null;
    prof = json['Prof.'] != null ? new Mr.fromJson(json['Prof.']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mr != null) {
      data['Mr.'] = this.mr.toJson();
    }
    if (this.ms != null) {
      data['Ms.'] = this.ms.toJson();
    }
    if (this.mrs != null) {
      data['Mrs.'] = this.mrs.toJson();
    }
    if (this.miss != null) {
      data['Miss'] = this.miss.toJson();
    }
    if (this.dr != null) {
      data['Dr.'] = this.dr.toJson();
    }
    if (this.prof != null) {
      data['Prof.'] = this.prof.toJson();
    }
    return data;
  }
}

class Mr {
  String name;
  String value;

  Mr({this.name, this.value});

  Mr.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

