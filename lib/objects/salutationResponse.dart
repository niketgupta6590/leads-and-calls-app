class DomResponse {
  DomResponse({
    this.moduleName,
    this.tableName,
    this.moduleFields,
    this.linkFields,
  });

  String moduleName;
  String tableName;
  ModuleFields moduleFields;
  List<dynamic> linkFields;

  factory DomResponse.fromJson(Map<String, dynamic> json) => DomResponse(
    moduleName: json["module_name"],
    tableName: json["table_name"],
    moduleFields: ModuleFields.fromJson(json["module_fields"]),
    linkFields: List<dynamic>.from(json["link_fields"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "module_name": moduleName,
    "table_name": tableName,
    "module_fields": moduleFields.toJson(),
    "link_fields": List<dynamic>.from(linkFields.map((x) => x)),
  };
}

class ModuleFields {
  ModuleFields({
    this.salutation,
    // this.title,
    // this.leadSource,
    // this.status,
    // this.propertyC,
    // this.propertyStatusC,
    // this.subPropertyTypeC,
  });

  Salutation salutation;
  // Title title;
  // LeadSource leadSource;
  // Status status;
  // PropertyC propertyC;
  // PropertyStatusC propertyStatusC;
  // Salutation subPropertyTypeC;

  factory ModuleFields.fromJson(Map<String, dynamic> json) => ModuleFields(
    salutation: Salutation.fromJson(json["salutation"]),
    // title: Title.fromJson(json["title"]),
    // leadSource: LeadSource.fromJson(json["lead_source"]),
    // status: Status.fromJson(json["status"]),
    // propertyC: PropertyC.fromJson(json["property_c"]),
    // propertyStatusC: PropertyStatusC.fromJson(json["property_status_c"]),
    // subPropertyTypeC: Salutation.fromJson(json["sub_property_type_c"]),
  );

  Map<String, dynamic> toJson() => {
    "salutation": salutation.toJson(),
    // "title": title.toJson(),
    // "lead_source": leadSource.toJson(),
    // "status": status.toJson(),
    // "property_c": propertyC.toJson(),
    // "property_status_c": propertyStatusC.toJson(),
    // "sub_property_type_c": subPropertyTypeC.toJson(),
  };
}
class Salutation {
  Salutation({
    this.name,
    this.type,
    this.group,
    this.idName,
    this.label,
    this.required,
    this.options,
    this.relatedModule,
    this.calculated,
    this.len,
  });

  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Map<String, Option> options;
  String relatedModule;
  bool calculated;
  int len;

  factory Salutation.fromJson(Map<String, dynamic> json) => Salutation(
    name: json["name"],
    type: json["type"],
    group: json["group"],
    idName: json["id_name"],
    label: json["label"],
    required: json["required"],
    options: Map.from(json["options"]).map((k, v) => MapEntry<String, Option>(k, Option.fromJson(v))),
    relatedModule: json["related_module"],
    calculated: json["calculated"],
    len: json["len"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "type": type,
    "group": group,
    "id_name": idName,
    "label": label,
    "required": required,
    "options": Map.from(options).map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
    "related_module": relatedModule,
    "calculated": calculated,
    "len": len,
  };
}
class Option {
  Option({
    this.name,
    this.value,
  });

  String name;
  String value;

  factory Option.fromJson(Map<String, dynamic> json) => Option(
    name: json["name"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "value": value,
  };
}
