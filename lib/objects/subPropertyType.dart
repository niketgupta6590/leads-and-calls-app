class SubPropertyTypeStruct {
  SubPropertyTypeC subPropertyTypeC;

  SubPropertyTypeStruct({this.subPropertyTypeC});

  SubPropertyTypeStruct.fromJson(Map<String, dynamic> json) {
    subPropertyTypeC = json['sub_property_type_c'] != null
        ? new SubPropertyTypeC.fromJson(json['sub_property_type_c'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.subPropertyTypeC != null) {
      data['sub_property_type_c'] = this.subPropertyTypeC.toJson();
    }
    return data;
  }
}

class SubPropertyTypeC {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  int len;

  SubPropertyTypeC(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  SubPropertyTypeC.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class Options {
  OneBHK oneBHK;
  OneBHK twoBHK;
  OneBHK threeBHK;
  OneBHK fourBHK;
  OneBHK fiveBHK;
  OneBHK fiveBHKS;
  OneBHK flat;
  OneBHK houseVilla;
  OneBHK officeSpace;
  OneBHK shopShowroom;
  OneBHK commercialLand;
  OneBHK warehouseGodown;
  OneBHK industrialBuilding;
  OneBHK industrialShed;
  OneBHK agriculturalLand;
  OneBHK farmHouse;
  OneBHK shop;
  OneBHK rK;
  OneBHK apartment;
  OneBHK hotelApartment;
  OneBHK duplex;
  OneBHK penthouse;
  OneBHK townhouse;
  OneBHK villa;
  OneBHK mansion;
  OneBHK land;
  OneBHK office;
  OneBHK commercial;

  Options(
      {this.oneBHK,
        this.twoBHK,
        this.threeBHK,
        this.fourBHK,
        this.fiveBHK,
        this.fiveBHKS,
        this.flat,
        this.houseVilla,
        this.officeSpace,
        this.shopShowroom,
        this.commercialLand,
        this.warehouseGodown,
        this.industrialBuilding,
        this.industrialShed,
        this.agriculturalLand,
        this.farmHouse,
        this.shop,
        this.rK,
        this.apartment,
        this.hotelApartment,
        this.duplex,
        this.penthouse,
        this.townhouse,
        this.villa,
        this.mansion,
        this.land,
        this.office,
        this.commercial});

  Options.fromJson(Map<String, dynamic> json) {
    oneBHK =
    json['oneBHK'] != null ? new OneBHK.fromJson(json['oneBHK']) : null;
    twoBHK =
    json['twoBHK'] != null ? new OneBHK.fromJson(json['twoBHK']) : null;
    threeBHK =
    json['threeBHK'] != null ? new OneBHK.fromJson(json['threeBHK']) : null;
    fourBHK =
    json['fourBHK'] != null ? new OneBHK.fromJson(json['fourBHK']) : null;
    fiveBHK =
    json['fiveBHK'] != null ? new OneBHK.fromJson(json['fiveBHK']) : null;
    fiveBHKS =
    json['fiveBHKS'] != null ? new OneBHK.fromJson(json['fiveBHKS']) : null;
    flat = json['Flat'] != null ? new OneBHK.fromJson(json['Flat']) : null;
    houseVilla = json['House_Villa'] != null
        ? new OneBHK.fromJson(json['House_Villa'])
        : null;
    officeSpace = json['Office_space'] != null
        ? new OneBHK.fromJson(json['Office_space'])
        : null;
    shopShowroom = json['Shop_Showroom'] != null
        ? new OneBHK.fromJson(json['Shop_Showroom'])
        : null;
    commercialLand = json['Commercial_Land'] != null
        ? new OneBHK.fromJson(json['Commercial_Land'])
        : null;
    warehouseGodown = json['Warehouse_Godown'] != null
        ? new OneBHK.fromJson(json['Warehouse_Godown'])
        : null;
    industrialBuilding = json['Industrial_Building'] != null
        ? new OneBHK.fromJson(json['Industrial_Building'])
        : null;
    industrialShed = json['Industrial_Shed'] != null
        ? new OneBHK.fromJson(json['Industrial_Shed'])
        : null;
    agriculturalLand = json['Agricultural_Land'] != null
        ? new OneBHK.fromJson(json['Agricultural_Land'])
        : null;
    farmHouse = json['Farm_House'] != null
        ? new OneBHK.fromJson(json['Farm_House'])
        : null;
    shop = json['Shop'] != null ? new OneBHK.fromJson(json['Shop']) : null;
    rK = json['RK'] != null ? new OneBHK.fromJson(json['RK']) : null;
    apartment = json['Apartment'] != null
        ? new OneBHK.fromJson(json['Apartment'])
        : null;
    hotelApartment = json['Hotel_Apartment'] != null
        ? new OneBHK.fromJson(json['Hotel_Apartment'])
        : null;
    duplex =
    json['Duplex'] != null ? new OneBHK.fromJson(json['Duplex']) : null;
    penthouse = json['Penthouse'] != null
        ? new OneBHK.fromJson(json['Penthouse'])
        : null;
    townhouse = json['Townhouse'] != null
        ? new OneBHK.fromJson(json['Townhouse'])
        : null;
    villa = json['Villa'] != null ? new OneBHK.fromJson(json['Villa']) : null;
    mansion =
    json['Mansion'] != null ? new OneBHK.fromJson(json['Mansion']) : null;
    land = json['Land'] != null ? new OneBHK.fromJson(json['Land']) : null;
    office =
    json['Office'] != null ? new OneBHK.fromJson(json['Office']) : null;
    commercial = json['Commercial'] != null
        ? new OneBHK.fromJson(json['Commercial'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.oneBHK != null) {
      data['oneBHK'] = this.oneBHK.toJson();
    }
    if (this.twoBHK != null) {
      data['twoBHK'] = this.twoBHK.toJson();
    }
    if (this.threeBHK != null) {
      data['threeBHK'] = this.threeBHK.toJson();
    }
    if (this.fourBHK != null) {
      data['fourBHK'] = this.fourBHK.toJson();
    }
    if (this.fiveBHK != null) {
      data['fiveBHK'] = this.fiveBHK.toJson();
    }
    if (this.fiveBHKS != null) {
      data['fiveBHKS'] = this.fiveBHKS.toJson();
    }
    if (this.flat != null) {
      data['Flat'] = this.flat.toJson();
    }
    if (this.houseVilla != null) {
      data['House_Villa'] = this.houseVilla.toJson();
    }
    if (this.officeSpace != null) {
      data['Office_space'] = this.officeSpace.toJson();
    }
    if (this.shopShowroom != null) {
      data['Shop_Showroom'] = this.shopShowroom.toJson();
    }
    if (this.commercialLand != null) {
      data['Commercial_Land'] = this.commercialLand.toJson();
    }
    if (this.warehouseGodown != null) {
      data['Warehouse_Godown'] = this.warehouseGodown.toJson();
    }
    if (this.industrialBuilding != null) {
      data['Industrial_Building'] = this.industrialBuilding.toJson();
    }
    if (this.industrialShed != null) {
      data['Industrial_Shed'] = this.industrialShed.toJson();
    }
    if (this.agriculturalLand != null) {
      data['Agricultural_Land'] = this.agriculturalLand.toJson();
    }
    if (this.farmHouse != null) {
      data['Farm_House'] = this.farmHouse.toJson();
    }
    if (this.shop != null) {
      data['Shop'] = this.shop.toJson();
    }
    if (this.rK != null) {
      data['RK'] = this.rK.toJson();
    }
    if (this.apartment != null) {
      data['Apartment'] = this.apartment.toJson();
    }
    if (this.hotelApartment != null) {
      data['Hotel_Apartment'] = this.hotelApartment.toJson();
    }
    if (this.duplex != null) {
      data['Duplex'] = this.duplex.toJson();
    }
    if (this.penthouse != null) {
      data['Penthouse'] = this.penthouse.toJson();
    }
    if (this.townhouse != null) {
      data['Townhouse'] = this.townhouse.toJson();
    }
    if (this.villa != null) {
      data['Villa'] = this.villa.toJson();
    }
    if (this.mansion != null) {
      data['Mansion'] = this.mansion.toJson();
    }
    if (this.land != null) {
      data['Land'] = this.land.toJson();
    }
    if (this.office != null) {
      data['Office'] = this.office.toJson();
    }
    if (this.commercial != null) {
      data['Commercial'] = this.commercial.toJson();
    }
    return data;
  }
}

class OneBHK {
  String name;
  String value;

  OneBHK({this.name, this.value});

  OneBHK.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

