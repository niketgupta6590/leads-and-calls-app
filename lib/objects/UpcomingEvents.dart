class UpcomingEvents {
  String id;
  String module;
  String dateDue;
  String summary;

  UpcomingEvents({this.id, this.module, this.dateDue, this.summary});

  UpcomingEvents.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    module = json['module'];
    dateDue = json['date_due'];
    summary = json['summary'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['module'] = this.module;
    data['date_due'] = this.dateDue;
    data['summary'] = this.summary;
    return data;
  }
}
