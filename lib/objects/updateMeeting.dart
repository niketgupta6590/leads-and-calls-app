class UpdateMeetingResponse {
  String id;
  EntryLisst entryList;

  UpdateMeetingResponse({this.id, this.entryList});

  UpdateMeetingResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    entryList = json['entry_list'] != null
        ? new EntryLisst.fromJson(json['entry_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.toJson();
    }
    return data;
  }
}

class EntryLisst {
  AssignedUserName assignedUserName;
  AssignedUserName modifiedByName;
  AssignedUserName createdByName;
  AssignedUserName assignedUserId;
  AssignedUserName name;
  AssignedUserName dateStart;
  AssignedUserName dateEnd;
  AssignedUserName durationHours;
  AssignedUserName durationMinutes;
  AssignedUserName location;
  AssignedUserName parentType;
  AssignedUserName status;
  AssignedUserName description;
  AssignedUserName dateModified;
  AssignedUserName id;

  EntryLisst(
      {this.assignedUserName,
        this.modifiedByName,
        this.createdByName,
        this.assignedUserId,
        this.name,
        this.dateStart,
        this.dateEnd,
        this.durationHours,
        this.durationMinutes,
        this.location,
        this.parentType,
        this.status,
        this.description,
        this.dateModified,
        this.id});

  EntryLisst.fromJson(Map<String, dynamic> json) {
    assignedUserName = json['assigned_user_name'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_name'])
        : null;
    modifiedByName = json['modified_by_name'] != null
        ? new AssignedUserName.fromJson(json['modified_by_name'])
        : null;
    createdByName = json['created_by_name'] != null
        ? new AssignedUserName.fromJson(json['created_by_name'])
        : null;
    assignedUserId = json['assigned_user_id'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_id'])
        : null;
    name = json['name'] != null
        ? new AssignedUserName.fromJson(json['name'])
        : null;
    dateStart = json['date_start'] != null
        ? new AssignedUserName.fromJson(json['date_start'])
        : null;
    dateEnd = json['date_end'] != null
        ? new AssignedUserName.fromJson(json['date_end'])
        : null;
    durationHours = json['duration_hours'] != null
        ? new AssignedUserName.fromJson(json['duration_hours'])
        : null;
    durationMinutes = json['duration_minutes'] != null
        ? new AssignedUserName.fromJson(json['duration_minutes'])
        : null;
    location = json['location'] != null
        ? new AssignedUserName.fromJson(json['location'])
        : null;
    parentType = json['parent_type'] != null
        ? new AssignedUserName.fromJson(json['parent_type'])
        : null;
    status = json['status'] != null
        ? new AssignedUserName.fromJson(json['status'])
        : null;
    description = json['description'] != null
        ? new AssignedUserName.fromJson(json['description'])
        : null;
    dateModified = json['date_modified'] != null
        ? new AssignedUserName.fromJson(json['date_modified'])
        : null;
    id = json['id'] != null ? new AssignedUserName.fromJson(json['id']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.assignedUserName != null) {
      data['assigned_user_name'] = this.assignedUserName.toJson();
    }
    if (this.modifiedByName != null) {
      data['modified_by_name'] = this.modifiedByName.toJson();
    }
    if (this.createdByName != null) {
      data['created_by_name'] = this.createdByName.toJson();
    }
    if (this.assignedUserId != null) {
      data['assigned_user_id'] = this.assignedUserId.toJson();
    }
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.dateStart != null) {
      data['date_start'] = this.dateStart.toJson();
    }
    if (this.dateEnd != null) {
      data['date_end'] = this.dateEnd.toJson();
    }
    if (this.durationHours != null) {
      data['duration_hours'] = this.durationHours.toJson();
    }
    if (this.durationMinutes != null) {
      data['duration_minutes'] = this.durationMinutes.toJson();
    }
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    if (this.parentType != null) {
      data['parent_type'] = this.parentType.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.description != null) {
      data['description'] = this.description.toJson();
    }
    if (this.dateModified != null) {
      data['date_modified'] = this.dateModified.toJson();
    }
    if (this.id != null) {
      data['id'] = this.id.toJson();
    }
    return data;
  }
}

class AssignedUserName {
  String name;
  String value;

  AssignedUserName({this.name, this.value});

  AssignedUserName.fromJson(Map<dynamic, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
