class DropDownCalls {
  String moduleName;
  String tableName;
  ModuleFields moduleFields;

  DropDownCalls({this.moduleName, this.tableName, this.moduleFields});

  DropDownCalls.fromJson(Map<String, dynamic> json) {
    moduleName = json['module_name'];
    tableName = json['table_name'];
    moduleFields = json['module_fields'] != null
        ? new ModuleFields.fromJson(json['module_fields'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['module_name'] = this.moduleName;
    data['table_name'] = this.tableName;
    if (this.moduleFields != null) {
      data['module_fields'] = this.moduleFields.toJson();
    }
    return data;
  }
}

class ModuleFields {
  DurationMinutes durationMinutes;
  ParentName parentName;
  Status status;
  Direction direction;
  DurationMinutes reminderChecked;
  ReminderTime reminderTime;
  DurationMinutes emailReminderChecked;
  ReminderTime emailReminderTime;
  ReminderTime emailReminderSent;

  ModuleFields(
      {this.durationMinutes,
        this.parentName,
        this.status,
        this.direction,
        this.reminderChecked,
        this.reminderTime,
        this.emailReminderChecked,
        this.emailReminderTime,
        this.emailReminderSent});

  ModuleFields.fromJson(Map<String, dynamic> json) {
    durationMinutes = json['duration_minutes'] != null
        ? new DurationMinutes.fromJson(json['duration_minutes'])
        : null;
    parentName = json['parent_name'] != null
        ? new ParentName.fromJson(json['parent_name'])
        : null;
    status =
    json['status'] != null ? new Status.fromJson(json['status']) : null;
    direction = json['direction'] != null
        ? new Direction.fromJson(json['direction'])
        : null;
    reminderChecked = json['reminder_checked'] != null
        ? new DurationMinutes.fromJson(json['reminder_checked'])
        : null;
    reminderTime = json['reminder_time'] != null
        ? new ReminderTime.fromJson(json['reminder_time'])
        : null;
    emailReminderChecked = json['email_reminder_checked'] != null
        ? new DurationMinutes.fromJson(json['email_reminder_checked'])
        : null;
    emailReminderTime = json['email_reminder_time'] != null
        ? new ReminderTime.fromJson(json['email_reminder_time'])
        : null;
    emailReminderSent = json['email_reminder_sent'] != null
        ? new ReminderTime.fromJson(json['email_reminder_sent'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.durationMinutes != null) {
      data['duration_minutes'] = this.durationMinutes.toJson();
    }
    if (this.parentName != null) {
      data['parent_name'] = this.parentName.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.direction != null) {
      data['direction'] = this.direction.toJson();
    }
    if (this.reminderChecked != null) {
      data['reminder_checked'] = this.reminderChecked.toJson();
    }
    if (this.reminderTime != null) {
      data['reminder_time'] = this.reminderTime.toJson();
    }
    if (this.emailReminderChecked != null) {
      data['email_reminder_checked'] = this.emailReminderChecked.toJson();
    }
    if (this.emailReminderTime != null) {
      data['email_reminder_time'] = this.emailReminderTime.toJson();
    }
    if (this.emailReminderSent != null) {
      data['email_reminder_sent'] = this.emailReminderSent.toJson();
    }
    return data;
  }
}

class DurationMinutes {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  String len;

  DurationMinutes(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  DurationMinutes.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options = json['options'].cast<String>();
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    data['options'] = this.options;
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class ParentName {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  String len;
  String typeName;

  ParentName(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len,
        this.typeName});

  ParentName.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options = json['options'].cast<String>();
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
    typeName = json['type_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    data['options'] = this.options;
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    data['type_name'] = this.typeName;
    return data;
  }
}

class Status {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  int len;
  String defaultValue;

  Status(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len,
        this.defaultValue});

  Status.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options = json['options'].cast<String>();
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
    defaultValue = json['default_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    data['options'] = this.options;
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    data['default_value'] = this.defaultValue;
    return data;
  }
}

class Direction {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  int len;

  Direction(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  Direction.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options = json['options'].cast<String>();
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    data['options'] = this.options;
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class ReminderTime {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  String len;
  int defaultValue;

  ReminderTime(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len,
        this.defaultValue});

  ReminderTime.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options = json['options'].cast<String>();
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
    defaultValue = json['default_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    data['options'] = this.options;
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    data['default_value'] = this.defaultValue;
    return data;
  }
}
