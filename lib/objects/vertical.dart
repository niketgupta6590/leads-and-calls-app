class VerticalStruct {
  VerticalC verticalC;

  VerticalStruct({this.verticalC});

  VerticalStruct.fromJson(Map<String, dynamic> json) {
    verticalC = json['vertical_c'] != null
        ? new VerticalC.fromJson(json['vertical_c'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.verticalC != null) {
      data['vertical_c'] = this.verticalC.toJson();
    }
    return data;
  }
}

class VerticalC {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  int len;

  VerticalC(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  VerticalC.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class Options {
  RealEstate realEstate;
  RealEstate projectManagement;
  RealEstate recruitment;
  RealEstate faqManager;
  RealEstate restaurants;
  RealEstate casino;
  RealEstate travel;
  RealEstate basicModule;
  RealEstate website;

  Options(
      {this.realEstate,
        this.projectManagement,
        this.recruitment,
        this.faqManager,
        this.restaurants,
        this.casino,
        this.travel,
        this.basicModule,
        this.website});

  Options.fromJson(Map<String, dynamic> json) {
    realEstate = json['Real_Estate'] != null
        ? new RealEstate.fromJson(json['Real_Estate'])
        : null;
    projectManagement = json['Project_Management'] != null
        ? new RealEstate.fromJson(json['Project_Management'])
        : null;
    recruitment = json['Recruitment'] != null
        ? new RealEstate.fromJson(json['Recruitment'])
        : null;
    faqManager = json['Faq_Manager'] != null
        ? new RealEstate.fromJson(json['Faq_Manager'])
        : null;
    restaurants = json['Restaurants'] != null
        ? new RealEstate.fromJson(json['Restaurants'])
        : null;
    casino =
    json['Casino'] != null ? new RealEstate.fromJson(json['Casino']) : null;
    travel =
    json['Travel'] != null ? new RealEstate.fromJson(json['Travel']) : null;
    basicModule = json['Basic_Module'] != null
        ? new RealEstate.fromJson(json['Basic_Module'])
        : null;
    website = json['Website'] != null
        ? new RealEstate.fromJson(json['Website'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.realEstate != null) {
      data['Real_Estate'] = this.realEstate.toJson();
    }
    if (this.projectManagement != null) {
      data['Project_Management'] = this.projectManagement.toJson();
    }
    if (this.recruitment != null) {
      data['Recruitment'] = this.recruitment.toJson();
    }
    if (this.faqManager != null) {
      data['Faq_Manager'] = this.faqManager.toJson();
    }
    if (this.restaurants != null) {
      data['Restaurants'] = this.restaurants.toJson();
    }
    if (this.casino != null) {
      data['Casino'] = this.casino.toJson();
    }
    if (this.travel != null) {
      data['Travel'] = this.travel.toJson();
    }
    if (this.basicModule != null) {
      data['Basic_Module'] = this.basicModule.toJson();
    }
    if (this.website != null) {
      data['Website'] = this.website.toJson();
    }
    return data;
  }
}

class RealEstate {
  String name;
  String value;

  RealEstate({this.name, this.value});

  RealEstate.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

