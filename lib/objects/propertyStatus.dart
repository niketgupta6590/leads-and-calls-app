class PropertyStatusStruct {
  PropertyStatusC propertyStatusC;

  PropertyStatusStruct({this.propertyStatusC});

  PropertyStatusStruct.fromJson(Map<String, dynamic> json) {
    propertyStatusC = json['property_status_c'] != null
        ? new PropertyStatusC.fromJson(json['property_status_c'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.propertyStatusC != null) {
      data['property_status_c'] = this.propertyStatusC.toJson();
    }
    return data;
  }
}

class PropertyStatusC {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  int len;

  PropertyStatusC(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  PropertyStatusC.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class Options {
  ReadyToMoveResaleFreehold readyToMoveResaleFreehold;
  ReadyToMoveResaleFreehold readyToMoveNewProperty;
  ReadyToMoveResaleFreehold underConstructionNewProperty;
  ReadyToMoveResaleFreehold temporarilyOffTheMarket;
  ReadyToMoveResaleFreehold pending;
  ReadyToMoveResaleFreehold soldOut;
  ReadyToMoveResaleFreehold expired;
  ReadyToMoveResaleFreehold cancelled;

  Options(
      {this.readyToMoveResaleFreehold,
        this.readyToMoveNewProperty,
        this.underConstructionNewProperty,
        this.temporarilyOffTheMarket,
        this.pending,
        this.soldOut,
        this.expired,
        this.cancelled});

  Options.fromJson(Map<String, dynamic> json) {
    readyToMoveResaleFreehold = json['Ready_To_Move_Resale_Freehold'] != null
        ? new ReadyToMoveResaleFreehold.fromJson(
        json['Ready_To_Move_Resale_Freehold'])
        : null;
    readyToMoveNewProperty = json['Ready_To_Move_New_Property'] != null
        ? new ReadyToMoveResaleFreehold.fromJson(
        json['Ready_To_Move_New_Property'])
        : null;
    underConstructionNewProperty =
    json['Under_Construction_New_Property'] != null
        ? new ReadyToMoveResaleFreehold.fromJson(
        json['Under_Construction_New_Property'])
        : null;
    temporarilyOffTheMarket = json['Temporarily_Off_The_Market'] != null
        ? new ReadyToMoveResaleFreehold.fromJson(
        json['Temporarily_Off_The_Market'])
        : null;
    pending = json['Pending'] != null
        ? new ReadyToMoveResaleFreehold.fromJson(json['Pending'])
        : null;
    soldOut = json['SoldOut'] != null
        ? new ReadyToMoveResaleFreehold.fromJson(json['SoldOut'])
        : null;
    expired = json['Expired'] != null
        ? new ReadyToMoveResaleFreehold.fromJson(json['Expired'])
        : null;
    cancelled = json['Cancelled'] != null
        ? new ReadyToMoveResaleFreehold.fromJson(json['Cancelled'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.readyToMoveResaleFreehold != null) {
      data['Ready_To_Move_Resale_Freehold'] =
          this.readyToMoveResaleFreehold.toJson();
    }
    if (this.readyToMoveNewProperty != null) {
      data['Ready_To_Move_New_Property'] = this.readyToMoveNewProperty.toJson();
    }
    if (this.underConstructionNewProperty != null) {
      data['Under_Construction_New_Property'] =
          this.underConstructionNewProperty.toJson();
    }
    if (this.temporarilyOffTheMarket != null) {
      data['Temporarily_Off_The_Market'] =
          this.temporarilyOffTheMarket.toJson();
    }
    if (this.pending != null) {
      data['Pending'] = this.pending.toJson();
    }
    if (this.soldOut != null) {
      data['SoldOut'] = this.soldOut.toJson();
    }
    if (this.expired != null) {
      data['Expired'] = this.expired.toJson();
    }
    if (this.cancelled != null) {
      data['Cancelled'] = this.cancelled.toJson();
    }
    return data;
  }
}

class ReadyToMoveResaleFreehold {
  String name;
  String value;

  ReadyToMoveResaleFreehold({this.name, this.value});

  ReadyToMoveResaleFreehold.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

