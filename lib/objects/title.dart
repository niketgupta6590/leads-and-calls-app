class TitleStruct {
  TitlesC titlesC;

  TitleStruct({this.titlesC});

  TitleStruct.fromJson(Map<String, dynamic> json) {
    titlesC = json['titles_c'] != null
        ? new TitlesC.fromJson(json['titles_c'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.titlesC != null) {
      data['titles_c'] = this.titlesC.toJson();
    }
    return data;
  }
}

class TitlesC {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  int len;
  String defaultValue;

  TitlesC(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len,
        this.defaultValue});

  TitlesC.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
    defaultValue = json['default_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    data['default_value'] = this.defaultValue;
    return data;
  }
}

class Options {
  QAAnalyst qAAnalyst;
  QAAnalyst qAAssociate;
  QAAnalyst qAAuditor;
  QAAnalyst qACalibrationTechnician;
  QAAnalyst qAChampion;
  QAAnalyst qAConsultant;
  QAAnalyst qACoordinator;
  QAAnalyst qADirector;
  QAAnalyst qAInstructor;
  QAAnalyst qAInspector;
  QAAnalyst qAManager;
  QAAnalyst qAProcessEngineer;
  QAAnalyst qualityEngineer;
  QAAnalyst qAReliabilitySafetyEngineer;
  QAAnalyst qASpecialist;
  QAAnalyst qASupervisor;
  QAAnalyst qATechnician;
  QAAnalyst qAVicePresidentExecutive;
  QAAnalyst qualitySupplierEngineer;
  QAAnalyst sixSigmaEngineer;
  QAAnalyst quality;
  QAAnalyst blackBelt;
  QAAnalyst qAProjectEngineer;
  QAAnalyst qAManufacturingEngineer;
  QAAnalyst uIDesigner;
  QAAnalyst uXDesigner;
  QAAnalyst accessibiltySpecialist;
  QAAnalyst interactionDesigner;
  QAAnalyst frontEndDesigner;
  QAAnalyst computerGraphicsAnimator;
  QAAnalyst seniorSystemDesigner;
  QAAnalyst systemDesigner;
  QAAnalyst webDesigner;
  QAAnalyst entryLevelDesigner;
  QAAnalyst designer;
  QAAnalyst webPageDesigner;
  QAAnalyst webGraphicDesigner;
  QAAnalyst layoutDesigner;
  QAAnalyst coverDesigner;
  QAAnalyst artDirector;
  QAAnalyst internetSiteDesigner;
  QAAnalyst intranetSiteDesigner;
  QAAnalyst webSiteDesigner;
  QAAnalyst fullStackDeveloper;
  QAAnalyst frontEndDeveloper;
  QAAnalyst mobileDeveloper;
  QAAnalyst softwareDeveloper;
  QAAnalyst wordPressDeveloper;
  QAAnalyst frameWorkSpecialist;
  QAAnalyst pythonDeveloper;
  QAAnalyst gameDeveloper;
  QAAnalyst mobileAppDeveloper;
  QAAnalyst webAnalyticsDeveloper;
  QAAnalyst nETDeveloper;
  QAAnalyst javaDeveloper;
  QAAnalyst associateDeveloper;
  QAAnalyst applicationDeveloper;
  QAAnalyst pHPDeveloper;
  QAAnalyst webSiteDeveloper;
  QAAnalyst webDeveloper;
  QAAnalyst juniorDeveloper;
  QAAnalyst juniorSoftwareDeveloper;
  QAAnalyst juniorSoftwareEngineer;
  QAAnalyst technicalLead;
  QAAnalyst mobileSocialMediaDeveloper;
  QAAnalyst informationArchitect;
  QAAnalyst marketingTechnologist;
  QAAnalyst sEOConsultant;
  QAAnalyst digitalMarketingManager;
  QAAnalyst socialMediaManager;
  QAAnalyst growthHacker;
  QAAnalyst socialMediaEventsManager;
  QAAnalyst mediaManager;
  QAAnalyst blogger;
  QAAnalyst communityManager;
  QAAnalyst communityModerator;
  QAAnalyst contentStrategist;
  QAAnalyst contentManager;
  QAAnalyst analyticsManager;
  QAAnalyst blogEditor;
  QAAnalyst brandAmbassador;
  QAAnalyst brandPromoter;
  QAAnalyst clientEngagementManager;
  QAAnalyst clientServicesCoordinator;
  QAAnalyst directorOfSocialMedia;
  QAAnalyst digitalMediaCoordinator;
  QAAnalyst socialMediaLead;
  QAAnalyst socialMediaSpecialist;
  QAAnalyst socialMediaAnalyst;
  QAAnalyst searchAndSocialMediaOptimizer;
  QAAnalyst internetMediaAssociate;
  QAAnalyst socialMediaRepresentative;
  QAAnalyst socialMediaExpert;
  QAAnalyst socialDigitalMediaManager;
  QAAnalyst brandProjectManager;
  QAAnalyst businessSystemAnalyst;
  QAAnalyst systemEngineer;
  QAAnalyst systemAdministrator;
  QAAnalyst seniorWebAdministrator;
  QAAnalyst webAdministrator;
  QAAnalyst seniorSystemAdministrator;
  QAAnalyst seniorNetworkSystemAdministrator;
  QAAnalyst networkSystemsAdministrator;
  QAAnalyst iTSystemsAdministrator;
  QAAnalyst customerSupportAdministrator;
  QAAnalyst administrativeCoordinator;
  QAAnalyst administrativeSpecialist;
  QAAnalyst administrativeServicesManager;
  QAAnalyst administrativeServicesOfficer;
  QAAnalyst administrativeSupportManager;
  QAAnalyst contractAdministrator;
  QAAnalyst dataEntry;
  QAAnalyst seniorAdministrativeCoordinator;
  QAAnalyst seniorAdministrativeServicesOfficer;
  QAAnalyst seniorExecutiveAssistant;
  QAAnalyst seniorSupportSpecialist;
  QAAnalyst administrativeSupportSupervisor;
  QAAnalyst securityAnalyst;
  QAAnalyst iTSecurityEngineer;
  QAAnalyst securityArchitect;
  QAAnalyst securitySpecialist;
  QAAnalyst securitySystemsAdministrator;
  QAAnalyst seniorITSecurityConsultant;
  QAAnalyst seniorInformationSecurityRiskOfficer;
  QAAnalyst principalInformationAssuranceOfficer;
  QAAnalyst chiefInformationSecurityConsultant;
  QAAnalyst principalCyberSecurityManager;
  QAAnalyst regionalInformationSecurityAnalyst;
  QAAnalyst seniorInformationSecurityAssuranceConsultant;
  QAAnalyst seniorITSecurityOperationsSpecialist;
  QAAnalyst cryptanalyst;
  QAAnalyst cryptographerCryptologist;
  QAAnalyst sourceCodeAuditor;
  QAAnalyst virusTechnician;
  QAAnalyst penetrationTester;
  QAAnalyst vulnerabilityAssessor;
  QAAnalyst computerSecurityIncidentResponder;
  QAAnalyst intrusionDetectionSpecialist;
  QAAnalyst dataBaseAdministrator;
  QAAnalyst dataArchitect;
  QAAnalyst dataModeler;
  QAAnalyst dataAnalyst;
  QAAnalyst dataScientist;
  QAAnalyst administrator;
  QAAnalyst data;
  QAAnalyst database;
  QAAnalyst analyst;
  QAAnalyst eDPElectronicDataProcessing;
  QAAnalyst electronicDataProcessingEDPSystems;
  QAAnalyst architect;
  QAAnalyst dataAdministrator;
  QAAnalyst dataAnalystInformaticsAndSystems;
  QAAnalyst databaseAdministratorDBA;
  QAAnalyst databaseAnalyst;
  QAAnalyst databaseAnalystDBA;
  QAAnalyst databaseArchitect;
  QAAnalyst databaseArchitectDBA;
  QAAnalyst databaseDesigner;
  QAAnalyst databaseManagementSupervisorComputerSystems;
  QAAnalyst databaseManager;
  QAAnalyst dataCustodian;
  QAAnalyst dataDictionaryAdministrator;
  QAAnalyst dataMiner;
  QAAnalyst dataMiningAnalyst;
  QAAnalyst dataProcessingSpecialist;
  QAAnalyst dataWarehouseAnalyst;
  QAAnalyst dBADatabaseAdministrator;
  QAAnalyst dBADatabaseAnalyst;
  QAAnalyst dBADatabaseArchitect;
  QAAnalyst eDPElectronicDataProcessingAnalyst;
  QAAnalyst eDPElectronicDataProcessingSpecialist;
  QAAnalyst eDPElectronicDataProcessingSystemsAnalyst;
  QAAnalyst electronicDataProcessingEDPSpecialist;
  QAAnalyst electronicDataProcessingEDPSystemsAnalyst;
  QAAnalyst informationResourceAnalyst;
  QAAnalyst manager;
  QAAnalyst specialist;
  QAAnalyst electronicDataProcessingEDP;
  QAAnalyst supervisor;
  QAAnalyst databaseManagementComputerSystems;
  QAAnalyst systemsAnalyst;
  QAAnalyst juniorResearchAssociate;
  QAAnalyst researchAssociate;
  QAAnalyst seniorResearchAssociate;
  QAAnalyst technicalArchitectDatabase;
  QAAnalyst cloudArchitect;
  QAAnalyst devopsManager;
  QAAnalyst agileProjectManager;
  QAAnalyst productManager;
  QAAnalyst technicalAccountManager;
  QAAnalyst contentArchitect;

  Options(
      {this.qAAnalyst,
        this.qAAssociate,
        this.qAAuditor,
        this.qACalibrationTechnician,
        this.qAChampion,
        this.qAConsultant,
        this.qACoordinator,
        this.qADirector,
        this.qAInstructor,
        this.qAInspector,
        this.qAManager,
        this.qAProcessEngineer,
        this.qualityEngineer,
        this.qAReliabilitySafetyEngineer,
        this.qASpecialist,
        this.qASupervisor,
        this.qATechnician,
        this.qAVicePresidentExecutive,
        this.qualitySupplierEngineer,
        this.sixSigmaEngineer,
        this.quality,
        this.blackBelt,
        this.qAProjectEngineer,
        this.qAManufacturingEngineer,
        this.uIDesigner,
        this.uXDesigner,
        this.accessibiltySpecialist,
        this.interactionDesigner,
        this.frontEndDesigner,
        this.computerGraphicsAnimator,
        this.seniorSystemDesigner,
        this.systemDesigner,
        this.webDesigner,
        this.entryLevelDesigner,
        this.designer,
        this.webPageDesigner,
        this.webGraphicDesigner,
        this.layoutDesigner,
        this.coverDesigner,
        this.artDirector,
        this.internetSiteDesigner,
        this.intranetSiteDesigner,
        this.webSiteDesigner,
        this.fullStackDeveloper,
        this.frontEndDeveloper,
        this.mobileDeveloper,
        this.softwareDeveloper,
        this.wordPressDeveloper,
        this.frameWorkSpecialist,
        this.pythonDeveloper,
        this.gameDeveloper,
        this.mobileAppDeveloper,
        this.webAnalyticsDeveloper,
        this.nETDeveloper,
        this.javaDeveloper,
        this.associateDeveloper,
        this.applicationDeveloper,
        this.pHPDeveloper,
        this.webSiteDeveloper,
        this.webDeveloper,
        this.juniorDeveloper,
        this.juniorSoftwareDeveloper,
        this.juniorSoftwareEngineer,
        this.technicalLead,
        this.mobileSocialMediaDeveloper,
        this.informationArchitect,
        this.marketingTechnologist,
        this.sEOConsultant,
        this.digitalMarketingManager,
        this.socialMediaManager,
        this.growthHacker,
        this.socialMediaEventsManager,
        this.mediaManager,
        this.blogger,
        this.communityManager,
        this.communityModerator,
        this.contentStrategist,
        this.contentManager,
        this.analyticsManager,
        this.blogEditor,
        this.brandAmbassador,
        this.brandPromoter,
        this.clientEngagementManager,
        this.clientServicesCoordinator,
        this.directorOfSocialMedia,
        this.digitalMediaCoordinator,
        this.socialMediaLead,
        this.socialMediaSpecialist,
        this.socialMediaAnalyst,
        this.searchAndSocialMediaOptimizer,
        this.internetMediaAssociate,
        this.socialMediaRepresentative,
        this.socialMediaExpert,
        this.socialDigitalMediaManager,
        this.brandProjectManager,
        this.businessSystemAnalyst,
        this.systemEngineer,
        this.systemAdministrator,
        this.seniorWebAdministrator,
        this.webAdministrator,
        this.seniorSystemAdministrator,
        this.seniorNetworkSystemAdministrator,
        this.networkSystemsAdministrator,
        this.iTSystemsAdministrator,
        this.customerSupportAdministrator,
        this.administrativeCoordinator,
        this.administrativeSpecialist,
        this.administrativeServicesManager,
        this.administrativeServicesOfficer,
        this.administrativeSupportManager,
        this.contractAdministrator,
        this.dataEntry,
        this.seniorAdministrativeCoordinator,
        this.seniorAdministrativeServicesOfficer,
        this.seniorExecutiveAssistant,
        this.seniorSupportSpecialist,
        this.administrativeSupportSupervisor,
        this.securityAnalyst,
        this.iTSecurityEngineer,
        this.securityArchitect,
        this.securitySpecialist,
        this.securitySystemsAdministrator,
        this.seniorITSecurityConsultant,
        this.seniorInformationSecurityRiskOfficer,
        this.principalInformationAssuranceOfficer,
        this.chiefInformationSecurityConsultant,
        this.principalCyberSecurityManager,
        this.regionalInformationSecurityAnalyst,
        this.seniorInformationSecurityAssuranceConsultant,
        this.seniorITSecurityOperationsSpecialist,
        this.cryptanalyst,
        this.cryptographerCryptologist,
        this.sourceCodeAuditor,
        this.virusTechnician,
        this.penetrationTester,
        this.vulnerabilityAssessor,
        this.computerSecurityIncidentResponder,
        this.intrusionDetectionSpecialist,
        this.dataBaseAdministrator,
        this.dataArchitect,
        this.dataModeler,
        this.dataAnalyst,
        this.dataScientist,
        this.administrator,
        this.data,
        this.database,
        this.analyst,
        this.eDPElectronicDataProcessing,
        this.electronicDataProcessingEDPSystems,
        this.architect,
        this.dataAdministrator,
        this.dataAnalystInformaticsAndSystems,
        this.databaseAdministratorDBA,
        this.databaseAnalyst,
        this.databaseAnalystDBA,
        this.databaseArchitect,
        this.databaseArchitectDBA,
        this.databaseDesigner,
        this.databaseManagementSupervisorComputerSystems,
        this.databaseManager,
        this.dataCustodian,
        this.dataDictionaryAdministrator,
        this.dataMiner,
        this.dataMiningAnalyst,
        this.dataProcessingSpecialist,
        this.dataWarehouseAnalyst,
        this.dBADatabaseAdministrator,
        this.dBADatabaseAnalyst,
        this.dBADatabaseArchitect,
        this.eDPElectronicDataProcessingAnalyst,
        this.eDPElectronicDataProcessingSpecialist,
        this.eDPElectronicDataProcessingSystemsAnalyst,
        this.electronicDataProcessingEDPSpecialist,
        this.electronicDataProcessingEDPSystemsAnalyst,
        this.informationResourceAnalyst,
        this.manager,
        this.specialist,
        this.electronicDataProcessingEDP,
        this.supervisor,
        this.databaseManagementComputerSystems,
        this.systemsAnalyst,
        this.juniorResearchAssociate,
        this.researchAssociate,
        this.seniorResearchAssociate,
        this.technicalArchitectDatabase,
        this.cloudArchitect,
        this.devopsManager,
        this.agileProjectManager,
        this.productManager,
        this.technicalAccountManager,
        this.contentArchitect});

  Options.fromJson(Map<String, dynamic> json) {
    qAAnalyst = json['QA Analyst'] != null
        ? new QAAnalyst.fromJson(json['QA Analyst'])
        : null;
    qAAssociate = json['QA Associate'] != null
        ? new QAAnalyst.fromJson(json['QA Associate'])
        : null;
    qAAuditor = json['QA Auditor'] != null
        ? new QAAnalyst.fromJson(json['QA Auditor'])
        : null;
    qACalibrationTechnician = json['QA Calibration technician'] != null
        ? new QAAnalyst.fromJson(json['QA Calibration technician'])
        : null;
    qAChampion = json['QA Champion'] != null
        ? new QAAnalyst.fromJson(json['QA Champion'])
        : null;
    qAConsultant = json['QA Consultant'] != null
        ? new QAAnalyst.fromJson(json['QA Consultant'])
        : null;
    qACoordinator = json['QA Coordinator'] != null
        ? new QAAnalyst.fromJson(json['QA Coordinator'])
        : null;
    qADirector = json['QA Director'] != null
        ? new QAAnalyst.fromJson(json['QA Director'])
        : null;
    qAInstructor = json['QA Instructor'] != null
        ? new QAAnalyst.fromJson(json['QA Instructor'])
        : null;
    qAInspector = json['QA Inspector'] != null
        ? new QAAnalyst.fromJson(json['QA Inspector'])
        : null;
    qAManager = json['QA Manager'] != null
        ? new QAAnalyst.fromJson(json['QA Manager'])
        : null;
    qAProcessEngineer = json['QA Process engineer'] != null
        ? new QAAnalyst.fromJson(json['QA Process engineer'])
        : null;
    qualityEngineer = json['Quality Engineer'] != null
        ? new QAAnalyst.fromJson(json['Quality Engineer'])
        : null;
    qAReliabilitySafetyEngineer = json['QA Reliability/Safety Engineer'] != null
        ? new QAAnalyst.fromJson(json['QA Reliability/Safety Engineer'])
        : null;
    qASpecialist = json['QA Specialist'] != null
        ? new QAAnalyst.fromJson(json['QA Specialist'])
        : null;
    qASupervisor = json['QA Supervisor'] != null
        ? new QAAnalyst.fromJson(json['QA Supervisor'])
        : null;
    qATechnician = json['QA Technician'] != null
        ? new QAAnalyst.fromJson(json['QA Technician'])
        : null;
    qAVicePresidentExecutive = json['QA Vice President/Executive'] != null
        ? new QAAnalyst.fromJson(json['QA Vice President/Executive'])
        : null;
    qualitySupplierEngineer = json['Quality Supplier Engineer'] != null
        ? new QAAnalyst.fromJson(json['Quality Supplier Engineer'])
        : null;
    sixSigmaEngineer = json['Six Sigma Engineer'] != null
        ? new QAAnalyst.fromJson(json['Six Sigma Engineer'])
        : null;
    quality = json['Quality'] != null
        ? new QAAnalyst.fromJson(json['Quality'])
        : null;
    blackBelt = json['Black Belt'] != null
        ? new QAAnalyst.fromJson(json['Black Belt'])
        : null;
    qAProjectEngineer = json['QA Project Engineer'] != null
        ? new QAAnalyst.fromJson(json['QA Project Engineer'])
        : null;
    qAManufacturingEngineer = json['QA Manufacturing Engineer'] != null
        ? new QAAnalyst.fromJson(json['QA Manufacturing Engineer'])
        : null;
    uIDesigner = json['UI Designer'] != null
        ? new QAAnalyst.fromJson(json['UI Designer'])
        : null;
    uXDesigner = json['UX Designer'] != null
        ? new QAAnalyst.fromJson(json['UX Designer'])
        : null;
    accessibiltySpecialist = json['Accessibilty Specialist'] != null
        ? new QAAnalyst.fromJson(json['Accessibilty Specialist'])
        : null;
    interactionDesigner = json['Interaction Designer'] != null
        ? new QAAnalyst.fromJson(json['Interaction Designer'])
        : null;
    frontEndDesigner = json['Front-End Designer'] != null
        ? new QAAnalyst.fromJson(json['Front-End Designer'])
        : null;
    computerGraphicsAnimator = json['Computer Graphics Animator'] != null
        ? new QAAnalyst.fromJson(json['Computer Graphics Animator'])
        : null;
    seniorSystemDesigner = json['Senior System Designer'] != null
        ? new QAAnalyst.fromJson(json['Senior System Designer'])
        : null;
    systemDesigner = json['System Designer'] != null
        ? new QAAnalyst.fromJson(json['System Designer'])
        : null;
    webDesigner = json['Web designer'] != null
        ? new QAAnalyst.fromJson(json['Web designer'])
        : null;
    entryLevelDesigner = json['Entry-level designer'] != null
        ? new QAAnalyst.fromJson(json['Entry-level designer'])
        : null;
    designer = json['Designer'] != null
        ? new QAAnalyst.fromJson(json['Designer'])
        : null;
    webPageDesigner = json['Web page designer'] != null
        ? new QAAnalyst.fromJson(json['Web page designer'])
        : null;
    webGraphicDesigner = json['Web graphic designer'] != null
        ? new QAAnalyst.fromJson(json['Web graphic designer'])
        : null;
    layoutDesigner = json['layout designer'] != null
        ? new QAAnalyst.fromJson(json['layout designer'])
        : null;
    coverDesigner = json['Cover designer'] != null
        ? new QAAnalyst.fromJson(json['Cover designer'])
        : null;
    artDirector = json['Art Director'] != null
        ? new QAAnalyst.fromJson(json['Art Director'])
        : null;
    internetSiteDesigner = json['Internet site designer'] != null
        ? new QAAnalyst.fromJson(json['Internet site designer'])
        : null;
    intranetSiteDesigner = json['Intranet site designer'] != null
        ? new QAAnalyst.fromJson(json['Intranet site designer'])
        : null;
    webSiteDesigner = json['Web site designer'] != null
        ? new QAAnalyst.fromJson(json['Web site designer'])
        : null;
    fullStackDeveloper = json['Full-Stack Developer'] != null
        ? new QAAnalyst.fromJson(json['Full-Stack Developer'])
        : null;
    frontEndDeveloper = json['Front-End Developer'] != null
        ? new QAAnalyst.fromJson(json['Front-End Developer'])
        : null;
    mobileDeveloper = json['Mobile Developer'] != null
        ? new QAAnalyst.fromJson(json['Mobile Developer'])
        : null;
    softwareDeveloper = json['Software Developer'] != null
        ? new QAAnalyst.fromJson(json['Software Developer'])
        : null;
    wordPressDeveloper = json['WordPress Developer'] != null
        ? new QAAnalyst.fromJson(json['WordPress Developer'])
        : null;
    frameWorkSpecialist = json['Frame Work Specialist'] != null
        ? new QAAnalyst.fromJson(json['Frame Work Specialist'])
        : null;
    pythonDeveloper = json['Python Developer'] != null
        ? new QAAnalyst.fromJson(json['Python Developer'])
        : null;
    gameDeveloper = json['Game Developer'] != null
        ? new QAAnalyst.fromJson(json['Game Developer'])
        : null;
    mobileAppDeveloper = json['Mobile App Developer'] != null
        ? new QAAnalyst.fromJson(json['Mobile App Developer'])
        : null;
    webAnalyticsDeveloper = json['Web Analytics Developer'] != null
        ? new QAAnalyst.fromJson(json['Web Analytics Developer'])
        : null;
    nETDeveloper = json['.NET Developer'] != null
        ? new QAAnalyst.fromJson(json['.NET Developer'])
        : null;
    javaDeveloper = json['Java Developer'] != null
        ? new QAAnalyst.fromJson(json['Java Developer'])
        : null;
    associateDeveloper = json['Associate Developer'] != null
        ? new QAAnalyst.fromJson(json['Associate Developer'])
        : null;
    applicationDeveloper = json['Application Developer'] != null
        ? new QAAnalyst.fromJson(json['Application Developer'])
        : null;
    pHPDeveloper = json['PHP Developer'] != null
        ? new QAAnalyst.fromJson(json['PHP Developer'])
        : null;
    webSiteDeveloper = json['Web site developer'] != null
        ? new QAAnalyst.fromJson(json['Web site developer'])
        : null;
    webDeveloper = json['Web developer'] != null
        ? new QAAnalyst.fromJson(json['Web developer'])
        : null;
    juniorDeveloper = json['Junior Developer'] != null
        ? new QAAnalyst.fromJson(json['Junior Developer'])
        : null;
    juniorSoftwareDeveloper = json['Junior Software Developer'] != null
        ? new QAAnalyst.fromJson(json['Junior Software Developer'])
        : null;
    juniorSoftwareEngineer = json['Junior Software Engineer'] != null
        ? new QAAnalyst.fromJson(json['Junior Software Engineer'])
        : null;
    technicalLead = json['Technical Lead'] != null
        ? new QAAnalyst.fromJson(json['Technical Lead'])
        : null;
    mobileSocialMediaDeveloper = json['Mobile Social Media Developer'] != null
        ? new QAAnalyst.fromJson(json['Mobile Social Media Developer'])
        : null;
    informationArchitect = json['Information Architect'] != null
        ? new QAAnalyst.fromJson(json['Information Architect'])
        : null;
    marketingTechnologist = json['Marketing Technologist'] != null
        ? new QAAnalyst.fromJson(json['Marketing Technologist'])
        : null;
    sEOConsultant = json['SEO Consultant'] != null
        ? new QAAnalyst.fromJson(json['SEO Consultant'])
        : null;
    digitalMarketingManager = json['Digital Marketing Manager'] != null
        ? new QAAnalyst.fromJson(json['Digital Marketing Manager'])
        : null;
    socialMediaManager = json['Social Media Manager'] != null
        ? new QAAnalyst.fromJson(json['Social Media Manager'])
        : null;
    growthHacker = json['Growth Hacker'] != null
        ? new QAAnalyst.fromJson(json['Growth Hacker'])
        : null;
    socialMediaEventsManager = json['Social Media Events Manager'] != null
        ? new QAAnalyst.fromJson(json['Social Media Events Manager'])
        : null;
    mediaManager = json['Media Manager'] != null
        ? new QAAnalyst.fromJson(json['Media Manager'])
        : null;
    blogger = json['Blogger'] != null
        ? new QAAnalyst.fromJson(json['Blogger'])
        : null;
    communityManager = json['Community Manager'] != null
        ? new QAAnalyst.fromJson(json['Community Manager'])
        : null;
    communityModerator = json['Community Moderator'] != null
        ? new QAAnalyst.fromJson(json['Community Moderator'])
        : null;
    contentStrategist = json['Content Strategist'] != null
        ? new QAAnalyst.fromJson(json['Content Strategist'])
        : null;
    contentManager = json['Content Manager'] != null
        ? new QAAnalyst.fromJson(json['Content Manager'])
        : null;
    analyticsManager = json['Analytics Manager'] != null
        ? new QAAnalyst.fromJson(json['Analytics Manager'])
        : null;
    blogEditor = json['Blog Editor'] != null
        ? new QAAnalyst.fromJson(json['Blog Editor'])
        : null;
    brandAmbassador = json['Brand Ambassador'] != null
        ? new QAAnalyst.fromJson(json['Brand Ambassador'])
        : null;
    brandPromoter = json['Brand Promoter'] != null
        ? new QAAnalyst.fromJson(json['Brand Promoter'])
        : null;
    clientEngagementManager = json['Client Engagement Manager'] != null
        ? new QAAnalyst.fromJson(json['Client Engagement Manager'])
        : null;
    clientServicesCoordinator = json['Client Services Coordinator'] != null
        ? new QAAnalyst.fromJson(json['Client Services Coordinator'])
        : null;
    directorOfSocialMedia = json['Director of Social Media'] != null
        ? new QAAnalyst.fromJson(json['Director of Social Media'])
        : null;
    digitalMediaCoordinator = json['Digital Media Coordinator'] != null
        ? new QAAnalyst.fromJson(json['Digital Media Coordinator'])
        : null;
    socialMediaLead = json['Social Media Lead'] != null
        ? new QAAnalyst.fromJson(json['Social Media Lead'])
        : null;
    socialMediaSpecialist = json['Social Media Specialist'] != null
        ? new QAAnalyst.fromJson(json['Social Media Specialist'])
        : null;
    socialMediaAnalyst = json['Social Media Analyst'] != null
        ? new QAAnalyst.fromJson(json['Social Media Analyst'])
        : null;
    searchAndSocialMediaOptimizer =
    json['Search and Social Media Optimizer'] != null
        ? new QAAnalyst.fromJson(json['Search and Social Media Optimizer'])
        : null;
    internetMediaAssociate = json['Internet Media Associate'] != null
        ? new QAAnalyst.fromJson(json['Internet Media Associate'])
        : null;
    socialMediaRepresentative = json['Social Media Representative'] != null
        ? new QAAnalyst.fromJson(json['Social Media Representative'])
        : null;
    socialMediaExpert = json['Social Media Expert'] != null
        ? new QAAnalyst.fromJson(json['Social Media Expert'])
        : null;
    socialDigitalMediaManager = json['Social & Digital Media Manager'] != null
        ? new QAAnalyst.fromJson(json['Social & Digital Media Manager'])
        : null;
    brandProjectManager = json['Brand & Project Manager'] != null
        ? new QAAnalyst.fromJson(json['Brand & Project Manager'])
        : null;
    businessSystemAnalyst = json['Business System Analyst'] != null
        ? new QAAnalyst.fromJson(json['Business System Analyst'])
        : null;
    systemEngineer = json['System Engineer'] != null
        ? new QAAnalyst.fromJson(json['System Engineer'])
        : null;
    systemAdministrator = json['System Administrator'] != null
        ? new QAAnalyst.fromJson(json['System Administrator'])
        : null;
    seniorWebAdministrator = json['Senior Web Administrator'] != null
        ? new QAAnalyst.fromJson(json['Senior Web Administrator'])
        : null;
    webAdministrator = json['Web Administrator'] != null
        ? new QAAnalyst.fromJson(json['Web Administrator'])
        : null;
    seniorSystemAdministrator = json['Senior System Administrator'] != null
        ? new QAAnalyst.fromJson(json['Senior System Administrator'])
        : null;
    seniorNetworkSystemAdministrator =
    json['Senior Network System Administrator'] != null
        ? new QAAnalyst.fromJson(
        json['Senior Network System Administrator'])
        : null;
    networkSystemsAdministrator = json['Network Systems Administrator'] != null
        ? new QAAnalyst.fromJson(json['Network Systems Administrator'])
        : null;
    iTSystemsAdministrator = json['IT Systems Administrator'] != null
        ? new QAAnalyst.fromJson(json['IT Systems Administrator'])
        : null;
    customerSupportAdministrator =
    json['Customer Support Administrator'] != null
        ? new QAAnalyst.fromJson(json['Customer Support Administrator'])
        : null;
    administrativeCoordinator = json['Administrative Coordinator'] != null
        ? new QAAnalyst.fromJson(json['Administrative Coordinator'])
        : null;
    administrativeSpecialist = json['Administrative Specialist'] != null
        ? new QAAnalyst.fromJson(json['Administrative Specialist'])
        : null;
    administrativeServicesManager =
    json['Administrative Services Manager'] != null
        ? new QAAnalyst.fromJson(json['Administrative Services Manager'])
        : null;
    administrativeServicesOfficer =
    json['Administrative Services Officer'] != null
        ? new QAAnalyst.fromJson(json['Administrative Services Officer'])
        : null;
    administrativeSupportManager =
    json['Administrative Support Manager'] != null
        ? new QAAnalyst.fromJson(json['Administrative Support Manager'])
        : null;
    contractAdministrator = json['Contract Administrator'] != null
        ? new QAAnalyst.fromJson(json['Contract Administrator'])
        : null;
    dataEntry = json['Data Entry'] != null
        ? new QAAnalyst.fromJson(json['Data Entry'])
        : null;
    seniorAdministrativeCoordinator =
    json['Senior Administrative Coordinator'] != null
        ? new QAAnalyst.fromJson(json['Senior Administrative Coordinator'])
        : null;
    seniorAdministrativeServicesOfficer =
    json['Senior Administrative Services Officer'] != null
        ? new QAAnalyst.fromJson(
        json['Senior Administrative Services Officer'])
        : null;
    seniorExecutiveAssistant = json['Senior Executive Assistant'] != null
        ? new QAAnalyst.fromJson(json['Senior Executive Assistant'])
        : null;
    seniorSupportSpecialist = json['Senior Support Specialist'] != null
        ? new QAAnalyst.fromJson(json['Senior Support Specialist'])
        : null;
    administrativeSupportSupervisor =
    json['Administrative Support Supervisor'] != null
        ? new QAAnalyst.fromJson(json['Administrative Support Supervisor'])
        : null;
    securityAnalyst = json['Security Analyst'] != null
        ? new QAAnalyst.fromJson(json['Security Analyst'])
        : null;
    iTSecurityEngineer = json['IT Security Engineer'] != null
        ? new QAAnalyst.fromJson(json['IT Security Engineer'])
        : null;
    securityArchitect = json['Security Architect'] != null
        ? new QAAnalyst.fromJson(json['Security Architect'])
        : null;
    securitySpecialist = json['Security Specialist'] != null
        ? new QAAnalyst.fromJson(json['Security Specialist'])
        : null;
    securitySystemsAdministrator =
    json['Security Systems Administrator'] != null
        ? new QAAnalyst.fromJson(json['Security Systems Administrator'])
        : null;
    seniorITSecurityConsultant = json['Senior IT Security Consultant'] != null
        ? new QAAnalyst.fromJson(json['Senior IT Security Consultant'])
        : null;
    seniorInformationSecurityRiskOfficer =
    json['Senior Information Security Risk Officer'] != null
        ? new QAAnalyst.fromJson(
        json['Senior Information Security Risk Officer'])
        : null;
    principalInformationAssuranceOfficer =
    json['Principal Information Assurance Officer'] != null
        ? new QAAnalyst.fromJson(
        json['Principal Information Assurance Officer'])
        : null;
    chiefInformationSecurityConsultant =
    json['Chief Information Security Consultant'] != null
        ? new QAAnalyst.fromJson(
        json['Chief Information Security Consultant'])
        : null;
    principalCyberSecurityManager =
    json['Principal Cyber Security Manager'] != null
        ? new QAAnalyst.fromJson(json['Principal Cyber Security Manager'])
        : null;
    regionalInformationSecurityAnalyst =
    json['Regional Information Security Analyst'] != null
        ? new QAAnalyst.fromJson(
        json['Regional Information Security Analyst'])
        : null;
    seniorInformationSecurityAssuranceConsultant =
    json['Senior Information Security Assurance Consultant'] != null
        ? new QAAnalyst.fromJson(
        json['Senior Information Security Assurance Consultant'])
        : null;
    seniorITSecurityOperationsSpecialist =
    json['Senior IT Security Operations Specialist'] != null
        ? new QAAnalyst.fromJson(
        json['Senior IT Security Operations Specialist'])
        : null;
    cryptanalyst = json['Cryptanalyst'] != null
        ? new QAAnalyst.fromJson(json['Cryptanalyst'])
        : null;
    cryptographerCryptologist = json['Cryptographer/Cryptologist'] != null
        ? new QAAnalyst.fromJson(json['Cryptographer/Cryptologist'])
        : null;
    sourceCodeAuditor = json['Source Code Auditor'] != null
        ? new QAAnalyst.fromJson(json['Source Code Auditor'])
        : null;
    virusTechnician = json['Virus Technician'] != null
        ? new QAAnalyst.fromJson(json['Virus Technician'])
        : null;
    penetrationTester = json['Penetration Tester'] != null
        ? new QAAnalyst.fromJson(json['Penetration Tester'])
        : null;
    vulnerabilityAssessor = json['Vulnerability Assessor'] != null
        ? new QAAnalyst.fromJson(json['Vulnerability Assessor'])
        : null;
    computerSecurityIncidentResponder =
    json['Computer Security Incident Responder'] != null
        ? new QAAnalyst.fromJson(
        json['Computer Security Incident Responder'])
        : null;
    intrusionDetectionSpecialist =
    json['Intrusion Detection Specialist'] != null
        ? new QAAnalyst.fromJson(json['Intrusion Detection Specialist'])
        : null;
    dataBaseAdministrator = json['Data Base Administrator'] != null
        ? new QAAnalyst.fromJson(json['Data Base Administrator'])
        : null;
    dataArchitect = json['Data Architect'] != null
        ? new QAAnalyst.fromJson(json['Data Architect'])
        : null;
    dataModeler = json['Data Modeler'] != null
        ? new QAAnalyst.fromJson(json['Data Modeler'])
        : null;
    dataAnalyst = json['Data Analyst'] != null
        ? new QAAnalyst.fromJson(json['Data Analyst'])
        : null;
    dataScientist = json['Data Scientist'] != null
        ? new QAAnalyst.fromJson(json['Data Scientist'])
        : null;
    administrator = json['administrator'] != null
        ? new QAAnalyst.fromJson(json['administrator'])
        : null;
    data = json['data'] != null ? new QAAnalyst.fromJson(json['data']) : null;
    database = json['database'] != null
        ? new QAAnalyst.fromJson(json['database'])
        : null;
    analyst = json['analyst'] != null
        ? new QAAnalyst.fromJson(json['analyst'])
        : null;
    eDPElectronicDataProcessing =
    json['EDP (electronic data processing)'] != null
        ? new QAAnalyst.fromJson(json['EDP (electronic data processing)'])
        : null;
    electronicDataProcessingEDPSystems =
    json['electronic data processing (EDP) systems'] != null
        ? new QAAnalyst.fromJson(
        json['electronic data processing (EDP) systems'])
        : null;
    architect = json['architect'] != null
        ? new QAAnalyst.fromJson(json['architect'])
        : null;
    dataAdministrator = json['data administrator'] != null
        ? new QAAnalyst.fromJson(json['data administrator'])
        : null;
    dataAnalystInformaticsAndSystems =
    json['data analyst - informatics and systems'] != null
        ? new QAAnalyst.fromJson(
        json['data analyst - informatics and systems'])
        : null;
    databaseAdministratorDBA = json['database administrator (DBA)'] != null
        ? new QAAnalyst.fromJson(json['database administrator (DBA)'])
        : null;
    databaseAnalyst = json['database analyst'] != null
        ? new QAAnalyst.fromJson(json['database analyst'])
        : null;
    databaseAnalystDBA = json['database analyst (DBA)'] != null
        ? new QAAnalyst.fromJson(json['database analyst (DBA)'])
        : null;
    databaseArchitect = json['database architect'] != null
        ? new QAAnalyst.fromJson(json['database architect'])
        : null;
    databaseArchitectDBA = json['database architect (DBA)'] != null
        ? new QAAnalyst.fromJson(json['database architect (DBA)'])
        : null;
    databaseDesigner = json['database designer'] != null
        ? new QAAnalyst.fromJson(json['database designer'])
        : null;
    databaseManagementSupervisorComputerSystems =
    json['database management supervisor - computer systems'] != null
        ? new QAAnalyst.fromJson(
        json['database management supervisor - computer systems'])
        : null;
    databaseManager = json['database manager'] != null
        ? new QAAnalyst.fromJson(json['database manager'])
        : null;
    dataCustodian = json['data custodian'] != null
        ? new QAAnalyst.fromJson(json['data custodian'])
        : null;
    dataDictionaryAdministrator = json['data dictionary administrator'] != null
        ? new QAAnalyst.fromJson(json['data dictionary administrator'])
        : null;
    dataMiner = json['data miner'] != null
        ? new QAAnalyst.fromJson(json['data miner'])
        : null;
    dataMiningAnalyst = json['data mining analyst'] != null
        ? new QAAnalyst.fromJson(json['data mining analyst'])
        : null;
    dataProcessingSpecialist = json['data processing specialist'] != null
        ? new QAAnalyst.fromJson(json['data processing specialist'])
        : null;
    dataWarehouseAnalyst = json['data warehouse analyst'] != null
        ? new QAAnalyst.fromJson(json['data warehouse analyst'])
        : null;
    dBADatabaseAdministrator = json['DBA (database administrator)'] != null
        ? new QAAnalyst.fromJson(json['DBA (database administrator)'])
        : null;
    dBADatabaseAnalyst = json['DBA (database analyst)'] != null
        ? new QAAnalyst.fromJson(json['DBA (database analyst)'])
        : null;
    dBADatabaseArchitect = json['DBA (database architect)'] != null
        ? new QAAnalyst.fromJson(json['DBA (database architect)'])
        : null;
    designer = json['designer'] != null
        ? new QAAnalyst.fromJson(json['designer'])
        : null;
    eDPElectronicDataProcessingAnalyst =
    json['EDP (electronic data processing) analyst'] != null
        ? new QAAnalyst.fromJson(
        json['EDP (electronic data processing) analyst'])
        : null;
    eDPElectronicDataProcessingSpecialist =
    json['EDP (electronic data processing) specialist'] != null
        ? new QAAnalyst.fromJson(
        json['EDP (electronic data processing) specialist'])
        : null;
    eDPElectronicDataProcessingSystemsAnalyst =
    json['EDP (electronic data processing) systems analyst'] != null
        ? new QAAnalyst.fromJson(
        json['EDP (electronic data processing) systems analyst'])
        : null;
    electronicDataProcessingEDPSpecialist =
    json['electronic data processing (EDP) specialist'] != null
        ? new QAAnalyst.fromJson(
        json['electronic data processing (EDP) specialist'])
        : null;
    electronicDataProcessingEDPSystemsAnalyst =
    json['electronic data processing (EDP) systems analyst'] != null
        ? new QAAnalyst.fromJson(
        json['electronic data processing (EDP) systems analyst'])
        : null;
    informationResourceAnalyst = json['information resource analyst'] != null
        ? new QAAnalyst.fromJson(json['information resource analyst'])
        : null;
    manager = json['manager'] != null
        ? new QAAnalyst.fromJson(json['manager'])
        : null;
    specialist = json['specialist'] != null
        ? new QAAnalyst.fromJson(json['specialist'])
        : null;
    electronicDataProcessingEDP =
    json['electronic data processing (EDP)'] != null
        ? new QAAnalyst.fromJson(json['electronic data processing (EDP)'])
        : null;
    supervisor = json['supervisor'] != null
        ? new QAAnalyst.fromJson(json['supervisor'])
        : null;
    databaseManagementComputerSystems =
    json['database management - computer systems'] != null
        ? new QAAnalyst.fromJson(
        json['database management - computer systems'])
        : null;
    systemsAnalyst = json['systems analyst'] != null
        ? new QAAnalyst.fromJson(json['systems analyst'])
        : null;
    juniorResearchAssociate = json['Junior Research Associate'] != null
        ? new QAAnalyst.fromJson(json['Junior Research Associate'])
        : null;
    researchAssociate = json['Research Associate'] != null
        ? new QAAnalyst.fromJson(json['Research Associate'])
        : null;
    seniorResearchAssociate = json['Senior Research Associate'] != null
        ? new QAAnalyst.fromJson(json['Senior Research Associate'])
        : null;
    technicalArchitectDatabase = json['technical architect - database'] != null
        ? new QAAnalyst.fromJson(json['technical architect - database'])
        : null;
    cloudArchitect = json['Cloud Architect'] != null
        ? new QAAnalyst.fromJson(json['Cloud Architect'])
        : null;
    devopsManager = json['Devops Manager'] != null
        ? new QAAnalyst.fromJson(json['Devops Manager'])
        : null;
    agileProjectManager = json['Agile Project Manager'] != null
        ? new QAAnalyst.fromJson(json['Agile Project Manager'])
        : null;
    productManager = json['Product Manager'] != null
        ? new QAAnalyst.fromJson(json['Product Manager'])
        : null;
    technicalAccountManager = json['Technical Account Manager'] != null
        ? new QAAnalyst.fromJson(json['Technical Account Manager'])
        : null;
    contentArchitect = json['Content Architect'] != null
        ? new QAAnalyst.fromJson(json['Content Architect'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.qAAnalyst != null) {
      data['QA Analyst'] = this.qAAnalyst.toJson();
    }
    if (this.qAAssociate != null) {
      data['QA Associate'] = this.qAAssociate.toJson();
    }
    if (this.qAAuditor != null) {
      data['QA Auditor'] = this.qAAuditor.toJson();
    }
    if (this.qACalibrationTechnician != null) {
      data['QA Calibration technician'] = this.qACalibrationTechnician.toJson();
    }
    if (this.qAChampion != null) {
      data['QA Champion'] = this.qAChampion.toJson();
    }
    if (this.qAConsultant != null) {
      data['QA Consultant'] = this.qAConsultant.toJson();
    }
    if (this.qACoordinator != null) {
      data['QA Coordinator'] = this.qACoordinator.toJson();
    }
    if (this.qADirector != null) {
      data['QA Director'] = this.qADirector.toJson();
    }
    if (this.qAInstructor != null) {
      data['QA Instructor'] = this.qAInstructor.toJson();
    }
    if (this.qAInspector != null) {
      data['QA Inspector'] = this.qAInspector.toJson();
    }
    if (this.qAManager != null) {
      data['QA Manager'] = this.qAManager.toJson();
    }
    if (this.qAProcessEngineer != null) {
      data['QA Process engineer'] = this.qAProcessEngineer.toJson();
    }
    if (this.qualityEngineer != null) {
      data['Quality Engineer'] = this.qualityEngineer.toJson();
    }
    if (this.qAReliabilitySafetyEngineer != null) {
      data['QA Reliability/Safety Engineer'] =
          this.qAReliabilitySafetyEngineer.toJson();
    }
    if (this.qASpecialist != null) {
      data['QA Specialist'] = this.qASpecialist.toJson();
    }
    if (this.qASupervisor != null) {
      data['QA Supervisor'] = this.qASupervisor.toJson();
    }
    if (this.qATechnician != null) {
      data['QA Technician'] = this.qATechnician.toJson();
    }
    if (this.qAVicePresidentExecutive != null) {
      data['QA Vice President/Executive'] =
          this.qAVicePresidentExecutive.toJson();
    }
    if (this.qualitySupplierEngineer != null) {
      data['Quality Supplier Engineer'] = this.qualitySupplierEngineer.toJson();
    }
    if (this.sixSigmaEngineer != null) {
      data['Six Sigma Engineer'] = this.sixSigmaEngineer.toJson();
    }
    if (this.quality != null) {
      data['Quality'] = this.quality.toJson();
    }
    if (this.blackBelt != null) {
      data['Black Belt'] = this.blackBelt.toJson();
    }
    if (this.qAProjectEngineer != null) {
      data['QA Project Engineer'] = this.qAProjectEngineer.toJson();
    }
    if (this.qAManufacturingEngineer != null) {
      data['QA Manufacturing Engineer'] = this.qAManufacturingEngineer.toJson();
    }
    if (this.uIDesigner != null) {
      data['UI Designer'] = this.uIDesigner.toJson();
    }
    if (this.uXDesigner != null) {
      data['UX Designer'] = this.uXDesigner.toJson();
    }
    if (this.accessibiltySpecialist != null) {
      data['Accessibilty Specialist'] = this.accessibiltySpecialist.toJson();
    }
    if (this.interactionDesigner != null) {
      data['Interaction Designer'] = this.interactionDesigner.toJson();
    }
    if (this.frontEndDesigner != null) {
      data['Front-End Designer'] = this.frontEndDesigner.toJson();
    }
    if (this.computerGraphicsAnimator != null) {
      data['Computer Graphics Animator'] =
          this.computerGraphicsAnimator.toJson();
    }
    if (this.seniorSystemDesigner != null) {
      data['Senior System Designer'] = this.seniorSystemDesigner.toJson();
    }
    if (this.systemDesigner != null) {
      data['System Designer'] = this.systemDesigner.toJson();
    }
    if (this.webDesigner != null) {
      data['Web designer'] = this.webDesigner.toJson();
    }
    if (this.entryLevelDesigner != null) {
      data['Entry-level designer'] = this.entryLevelDesigner.toJson();
    }
    if (this.designer != null) {
      data['Designer'] = this.designer.toJson();
    }
    if (this.webPageDesigner != null) {
      data['Web page designer'] = this.webPageDesigner.toJson();
    }
    if (this.webGraphicDesigner != null) {
      data['Web graphic designer'] = this.webGraphicDesigner.toJson();
    }
    if (this.layoutDesigner != null) {
      data['layout designer'] = this.layoutDesigner.toJson();
    }
    if (this.coverDesigner != null) {
      data['Cover designer'] = this.coverDesigner.toJson();
    }
    if (this.artDirector != null) {
      data['Art Director'] = this.artDirector.toJson();
    }
    if (this.internetSiteDesigner != null) {
      data['Internet site designer'] = this.internetSiteDesigner.toJson();
    }
    if (this.intranetSiteDesigner != null) {
      data['Intranet site designer'] = this.intranetSiteDesigner.toJson();
    }
    if (this.webSiteDesigner != null) {
      data['Web site designer'] = this.webSiteDesigner.toJson();
    }
    if (this.fullStackDeveloper != null) {
      data['Full-Stack Developer'] = this.fullStackDeveloper.toJson();
    }
    if (this.frontEndDeveloper != null) {
      data['Front-End Developer'] = this.frontEndDeveloper.toJson();
    }
    if (this.mobileDeveloper != null) {
      data['Mobile Developer'] = this.mobileDeveloper.toJson();
    }
    if (this.softwareDeveloper != null) {
      data['Software Developer'] = this.softwareDeveloper.toJson();
    }
    if (this.wordPressDeveloper != null) {
      data['WordPress Developer'] = this.wordPressDeveloper.toJson();
    }
    if (this.frameWorkSpecialist != null) {
      data['Frame Work Specialist'] = this.frameWorkSpecialist.toJson();
    }
    if (this.pythonDeveloper != null) {
      data['Python Developer'] = this.pythonDeveloper.toJson();
    }
    if (this.gameDeveloper != null) {
      data['Game Developer'] = this.gameDeveloper.toJson();
    }
    if (this.mobileAppDeveloper != null) {
      data['Mobile App Developer'] = this.mobileAppDeveloper.toJson();
    }
    if (this.webAnalyticsDeveloper != null) {
      data['Web Analytics Developer'] = this.webAnalyticsDeveloper.toJson();
    }
    if (this.nETDeveloper != null) {
      data['.NET Developer'] = this.nETDeveloper.toJson();
    }
    if (this.javaDeveloper != null) {
      data['Java Developer'] = this.javaDeveloper.toJson();
    }
    if (this.associateDeveloper != null) {
      data['Associate Developer'] = this.associateDeveloper.toJson();
    }
    if (this.applicationDeveloper != null) {
      data['Application Developer'] = this.applicationDeveloper.toJson();
    }
    if (this.pHPDeveloper != null) {
      data['PHP Developer'] = this.pHPDeveloper.toJson();
    }
    if (this.webSiteDeveloper != null) {
      data['Web site developer'] = this.webSiteDeveloper.toJson();
    }
    if (this.webDeveloper != null) {
      data['Web developer'] = this.webDeveloper.toJson();
    }
    if (this.juniorDeveloper != null) {
      data['Junior Developer'] = this.juniorDeveloper.toJson();
    }
    if (this.juniorSoftwareDeveloper != null) {
      data['Junior Software Developer'] = this.juniorSoftwareDeveloper.toJson();
    }
    if (this.juniorSoftwareEngineer != null) {
      data['Junior Software Engineer'] = this.juniorSoftwareEngineer.toJson();
    }
    if (this.technicalLead != null) {
      data['Technical Lead'] = this.technicalLead.toJson();
    }
    if (this.mobileSocialMediaDeveloper != null) {
      data['Mobile Social Media Developer'] =
          this.mobileSocialMediaDeveloper.toJson();
    }
    if (this.informationArchitect != null) {
      data['Information Architect'] = this.informationArchitect.toJson();
    }
    if (this.marketingTechnologist != null) {
      data['Marketing Technologist'] = this.marketingTechnologist.toJson();
    }
    if (this.sEOConsultant != null) {
      data['SEO Consultant'] = this.sEOConsultant.toJson();
    }
    if (this.digitalMarketingManager != null) {
      data['Digital Marketing Manager'] = this.digitalMarketingManager.toJson();
    }
    if (this.socialMediaManager != null) {
      data['Social Media Manager'] = this.socialMediaManager.toJson();
    }
    if (this.growthHacker != null) {
      data['Growth Hacker'] = this.growthHacker.toJson();
    }
    if (this.socialMediaEventsManager != null) {
      data['Social Media Events Manager'] =
          this.socialMediaEventsManager.toJson();
    }
    if (this.mediaManager != null) {
      data['Media Manager'] = this.mediaManager.toJson();
    }
    if (this.blogger != null) {
      data['Blogger'] = this.blogger.toJson();
    }
    if (this.communityManager != null) {
      data['Community Manager'] = this.communityManager.toJson();
    }
    if (this.communityModerator != null) {
      data['Community Moderator'] = this.communityModerator.toJson();
    }
    if (this.contentStrategist != null) {
      data['Content Strategist'] = this.contentStrategist.toJson();
    }
    if (this.contentManager != null) {
      data['Content Manager'] = this.contentManager.toJson();
    }
    if (this.analyticsManager != null) {
      data['Analytics Manager'] = this.analyticsManager.toJson();
    }
    if (this.blogEditor != null) {
      data['Blog Editor'] = this.blogEditor.toJson();
    }
    if (this.brandAmbassador != null) {
      data['Brand Ambassador'] = this.brandAmbassador.toJson();
    }
    if (this.brandPromoter != null) {
      data['Brand Promoter'] = this.brandPromoter.toJson();
    }
    if (this.clientEngagementManager != null) {
      data['Client Engagement Manager'] = this.clientEngagementManager.toJson();
    }
    if (this.clientServicesCoordinator != null) {
      data['Client Services Coordinator'] =
          this.clientServicesCoordinator.toJson();
    }
    if (this.directorOfSocialMedia != null) {
      data['Director of Social Media'] = this.directorOfSocialMedia.toJson();
    }
    if (this.digitalMediaCoordinator != null) {
      data['Digital Media Coordinator'] = this.digitalMediaCoordinator.toJson();
    }
    if (this.socialMediaLead != null) {
      data['Social Media Lead'] = this.socialMediaLead.toJson();
    }
    if (this.socialMediaSpecialist != null) {
      data['Social Media Specialist'] = this.socialMediaSpecialist.toJson();
    }
    if (this.socialMediaAnalyst != null) {
      data['Social Media Analyst'] = this.socialMediaAnalyst.toJson();
    }
    if (this.searchAndSocialMediaOptimizer != null) {
      data['Search and Social Media Optimizer'] =
          this.searchAndSocialMediaOptimizer.toJson();
    }
    if (this.internetMediaAssociate != null) {
      data['Internet Media Associate'] = this.internetMediaAssociate.toJson();
    }
    if (this.socialMediaRepresentative != null) {
      data['Social Media Representative'] =
          this.socialMediaRepresentative.toJson();
    }
    if (this.socialMediaExpert != null) {
      data['Social Media Expert'] = this.socialMediaExpert.toJson();
    }
    if (this.socialDigitalMediaManager != null) {
      data['Social & Digital Media Manager'] =
          this.socialDigitalMediaManager.toJson();
    }
    if (this.brandProjectManager != null) {
      data['Brand & Project Manager'] = this.brandProjectManager.toJson();
    }
    if (this.businessSystemAnalyst != null) {
      data['Business System Analyst'] = this.businessSystemAnalyst.toJson();
    }
    if (this.systemEngineer != null) {
      data['System Engineer'] = this.systemEngineer.toJson();
    }
    if (this.systemAdministrator != null) {
      data['System Administrator'] = this.systemAdministrator.toJson();
    }
    if (this.seniorWebAdministrator != null) {
      data['Senior Web Administrator'] = this.seniorWebAdministrator.toJson();
    }
    if (this.webAdministrator != null) {
      data['Web Administrator'] = this.webAdministrator.toJson();
    }
    if (this.seniorSystemAdministrator != null) {
      data['Senior System Administrator'] =
          this.seniorSystemAdministrator.toJson();
    }
    if (this.seniorNetworkSystemAdministrator != null) {
      data['Senior Network System Administrator'] =
          this.seniorNetworkSystemAdministrator.toJson();
    }
    if (this.networkSystemsAdministrator != null) {
      data['Network Systems Administrator'] =
          this.networkSystemsAdministrator.toJson();
    }
    if (this.iTSystemsAdministrator != null) {
      data['IT Systems Administrator'] = this.iTSystemsAdministrator.toJson();
    }
    if (this.customerSupportAdministrator != null) {
      data['Customer Support Administrator'] =
          this.customerSupportAdministrator.toJson();
    }
    if (this.administrativeCoordinator != null) {
      data['Administrative Coordinator'] =
          this.administrativeCoordinator.toJson();
    }
    if (this.administrativeSpecialist != null) {
      data['Administrative Specialist'] =
          this.administrativeSpecialist.toJson();
    }
    if (this.administrativeServicesManager != null) {
      data['Administrative Services Manager'] =
          this.administrativeServicesManager.toJson();
    }
    if (this.administrativeServicesOfficer != null) {
      data['Administrative Services Officer'] =
          this.administrativeServicesOfficer.toJson();
    }
    if (this.administrativeSupportManager != null) {
      data['Administrative Support Manager'] =
          this.administrativeSupportManager.toJson();
    }
    if (this.contractAdministrator != null) {
      data['Contract Administrator'] = this.contractAdministrator.toJson();
    }
    if (this.dataEntry != null) {
      data['Data Entry'] = this.dataEntry.toJson();
    }
    if (this.seniorAdministrativeCoordinator != null) {
      data['Senior Administrative Coordinator'] =
          this.seniorAdministrativeCoordinator.toJson();
    }
    if (this.seniorAdministrativeServicesOfficer != null) {
      data['Senior Administrative Services Officer'] =
          this.seniorAdministrativeServicesOfficer.toJson();
    }
    if (this.seniorExecutiveAssistant != null) {
      data['Senior Executive Assistant'] =
          this.seniorExecutiveAssistant.toJson();
    }
    if (this.seniorSupportSpecialist != null) {
      data['Senior Support Specialist'] = this.seniorSupportSpecialist.toJson();
    }
    if (this.administrativeSupportSupervisor != null) {
      data['Administrative Support Supervisor'] =
          this.administrativeSupportSupervisor.toJson();
    }
    if (this.securityAnalyst != null) {
      data['Security Analyst'] = this.securityAnalyst.toJson();
    }
    if (this.iTSecurityEngineer != null) {
      data['IT Security Engineer'] = this.iTSecurityEngineer.toJson();
    }
    if (this.securityArchitect != null) {
      data['Security Architect'] = this.securityArchitect.toJson();
    }
    if (this.securitySpecialist != null) {
      data['Security Specialist'] = this.securitySpecialist.toJson();
    }
    if (this.securitySystemsAdministrator != null) {
      data['Security Systems Administrator'] =
          this.securitySystemsAdministrator.toJson();
    }
    if (this.seniorITSecurityConsultant != null) {
      data['Senior IT Security Consultant'] =
          this.seniorITSecurityConsultant.toJson();
    }
    if (this.seniorInformationSecurityRiskOfficer != null) {
      data['Senior Information Security Risk Officer'] =
          this.seniorInformationSecurityRiskOfficer.toJson();
    }
    if (this.principalInformationAssuranceOfficer != null) {
      data['Principal Information Assurance Officer'] =
          this.principalInformationAssuranceOfficer.toJson();
    }
    if (this.chiefInformationSecurityConsultant != null) {
      data['Chief Information Security Consultant'] =
          this.chiefInformationSecurityConsultant.toJson();
    }
    if (this.principalCyberSecurityManager != null) {
      data['Principal Cyber Security Manager'] =
          this.principalCyberSecurityManager.toJson();
    }
    if (this.regionalInformationSecurityAnalyst != null) {
      data['Regional Information Security Analyst'] =
          this.regionalInformationSecurityAnalyst.toJson();
    }
    if (this.seniorInformationSecurityAssuranceConsultant != null) {
      data['Senior Information Security Assurance Consultant'] =
          this.seniorInformationSecurityAssuranceConsultant.toJson();
    }
    if (this.seniorITSecurityOperationsSpecialist != null) {
      data['Senior IT Security Operations Specialist'] =
          this.seniorITSecurityOperationsSpecialist.toJson();
    }
    if (this.cryptanalyst != null) {
      data['Cryptanalyst'] = this.cryptanalyst.toJson();
    }
    if (this.cryptographerCryptologist != null) {
      data['Cryptographer/Cryptologist'] =
          this.cryptographerCryptologist.toJson();
    }
    if (this.sourceCodeAuditor != null) {
      data['Source Code Auditor'] = this.sourceCodeAuditor.toJson();
    }
    if (this.virusTechnician != null) {
      data['Virus Technician'] = this.virusTechnician.toJson();
    }
    if (this.penetrationTester != null) {
      data['Penetration Tester'] = this.penetrationTester.toJson();
    }
    if (this.vulnerabilityAssessor != null) {
      data['Vulnerability Assessor'] = this.vulnerabilityAssessor.toJson();
    }
    if (this.computerSecurityIncidentResponder != null) {
      data['Computer Security Incident Responder'] =
          this.computerSecurityIncidentResponder.toJson();
    }
    if (this.intrusionDetectionSpecialist != null) {
      data['Intrusion Detection Specialist'] =
          this.intrusionDetectionSpecialist.toJson();
    }
    if (this.dataBaseAdministrator != null) {
      data['Data Base Administrator'] = this.dataBaseAdministrator.toJson();
    }
    if (this.dataArchitect != null) {
      data['Data Architect'] = this.dataArchitect.toJson();
    }
    if (this.dataModeler != null) {
      data['Data Modeler'] = this.dataModeler.toJson();
    }
    if (this.dataAnalyst != null) {
      data['Data Analyst'] = this.dataAnalyst.toJson();
    }
    if (this.dataScientist != null) {
      data['Data Scientist'] = this.dataScientist.toJson();
    }
    if (this.administrator != null) {
      data['administrator'] = this.administrator.toJson();
    }
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.database != null) {
      data['database'] = this.database.toJson();
    }
    if (this.analyst != null) {
      data['analyst'] = this.analyst.toJson();
    }
    if (this.eDPElectronicDataProcessing != null) {
      data['EDP (electronic data processing)'] =
          this.eDPElectronicDataProcessing.toJson();
    }
    if (this.electronicDataProcessingEDPSystems != null) {
      data['electronic data processing (EDP) systems'] =
          this.electronicDataProcessingEDPSystems.toJson();
    }
    if (this.architect != null) {
      data['architect'] = this.architect.toJson();
    }
    if (this.dataAdministrator != null) {
      data['data administrator'] = this.dataAdministrator.toJson();
    }
    if (this.dataAnalystInformaticsAndSystems != null) {
      data['data analyst - informatics and systems'] =
          this.dataAnalystInformaticsAndSystems.toJson();
    }
    if (this.databaseAdministratorDBA != null) {
      data['database administrator (DBA)'] =
          this.databaseAdministratorDBA.toJson();
    }
    if (this.databaseAnalyst != null) {
      data['database analyst'] = this.databaseAnalyst.toJson();
    }
    if (this.databaseAnalystDBA != null) {
      data['database analyst (DBA)'] = this.databaseAnalystDBA.toJson();
    }
    if (this.databaseArchitect != null) {
      data['database architect'] = this.databaseArchitect.toJson();
    }
    if (this.databaseArchitectDBA != null) {
      data['database architect (DBA)'] = this.databaseArchitectDBA.toJson();
    }
    if (this.databaseDesigner != null) {
      data['database designer'] = this.databaseDesigner.toJson();
    }
    if (this.databaseManagementSupervisorComputerSystems != null) {
      data['database management supervisor - computer systems'] =
          this.databaseManagementSupervisorComputerSystems.toJson();
    }
    if (this.databaseManager != null) {
      data['database manager'] = this.databaseManager.toJson();
    }
    if (this.dataCustodian != null) {
      data['data custodian'] = this.dataCustodian.toJson();
    }
    if (this.dataDictionaryAdministrator != null) {
      data['data dictionary administrator'] =
          this.dataDictionaryAdministrator.toJson();
    }
    if (this.dataMiner != null) {
      data['data miner'] = this.dataMiner.toJson();
    }
    if (this.dataMiningAnalyst != null) {
      data['data mining analyst'] = this.dataMiningAnalyst.toJson();
    }
    if (this.dataProcessingSpecialist != null) {
      data['data processing specialist'] =
          this.dataProcessingSpecialist.toJson();
    }
    if (this.dataWarehouseAnalyst != null) {
      data['data warehouse analyst'] = this.dataWarehouseAnalyst.toJson();
    }
    if (this.dBADatabaseAdministrator != null) {
      data['DBA (database administrator)'] =
          this.dBADatabaseAdministrator.toJson();
    }
    if (this.dBADatabaseAnalyst != null) {
      data['DBA (database analyst)'] = this.dBADatabaseAnalyst.toJson();
    }
    if (this.dBADatabaseArchitect != null) {
      data['DBA (database architect)'] = this.dBADatabaseArchitect.toJson();
    }
    if (this.designer != null) {
      data['designer'] = this.designer.toJson();
    }
    if (this.eDPElectronicDataProcessingAnalyst != null) {
      data['EDP (electronic data processing) analyst'] =
          this.eDPElectronicDataProcessingAnalyst.toJson();
    }
    if (this.eDPElectronicDataProcessingSpecialist != null) {
      data['EDP (electronic data processing) specialist'] =
          this.eDPElectronicDataProcessingSpecialist.toJson();
    }
    if (this.eDPElectronicDataProcessingSystemsAnalyst != null) {
      data['EDP (electronic data processing) systems analyst'] =
          this.eDPElectronicDataProcessingSystemsAnalyst.toJson();
    }
    if (this.electronicDataProcessingEDPSpecialist != null) {
      data['electronic data processing (EDP) specialist'] =
          this.electronicDataProcessingEDPSpecialist.toJson();
    }
    if (this.electronicDataProcessingEDPSystemsAnalyst != null) {
      data['electronic data processing (EDP) systems analyst'] =
          this.electronicDataProcessingEDPSystemsAnalyst.toJson();
    }
    if (this.informationResourceAnalyst != null) {
      data['information resource analyst'] =
          this.informationResourceAnalyst.toJson();
    }
    if (this.manager != null) {
      data['manager'] = this.manager.toJson();
    }
    if (this.specialist != null) {
      data['specialist'] = this.specialist.toJson();
    }
    if (this.electronicDataProcessingEDP != null) {
      data['electronic data processing (EDP)'] =
          this.electronicDataProcessingEDP.toJson();
    }
    if (this.supervisor != null) {
      data['supervisor'] = this.supervisor.toJson();
    }
    if (this.databaseManagementComputerSystems != null) {
      data['database management - computer systems'] =
          this.databaseManagementComputerSystems.toJson();
    }
    if (this.systemsAnalyst != null) {
      data['systems analyst'] = this.systemsAnalyst.toJson();
    }
    if (this.juniorResearchAssociate != null) {
      data['Junior Research Associate'] = this.juniorResearchAssociate.toJson();
    }
    if (this.researchAssociate != null) {
      data['Research Associate'] = this.researchAssociate.toJson();
    }
    if (this.seniorResearchAssociate != null) {
      data['Senior Research Associate'] = this.seniorResearchAssociate.toJson();
    }
    if (this.technicalArchitectDatabase != null) {
      data['technical architect - database'] =
          this.technicalArchitectDatabase.toJson();
    }
    if (this.cloudArchitect != null) {
      data['Cloud Architect'] = this.cloudArchitect.toJson();
    }
    if (this.devopsManager != null) {
      data['Devops Manager'] = this.devopsManager.toJson();
    }
    if (this.agileProjectManager != null) {
      data['Agile Project Manager'] = this.agileProjectManager.toJson();
    }
    if (this.productManager != null) {
      data['Product Manager'] = this.productManager.toJson();
    }
    if (this.technicalAccountManager != null) {
      data['Technical Account Manager'] = this.technicalAccountManager.toJson();
    }
    if (this.contentArchitect != null) {
      data['Content Architect'] = this.contentArchitect.toJson();
    }
    return data;
  }
}

class QAAnalyst {
  String name;
  String value;

  QAAnalyst({this.name, this.value});

  QAAnalyst.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

