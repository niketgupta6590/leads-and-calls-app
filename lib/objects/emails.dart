class Emails {
  int resultCount;
  String totalCount;
  int nextOffset;
  List<EntryList> entryList;
//  List<Null> relationshipList;

  Emails(
      {this.resultCount,
        this.totalCount,
        this.nextOffset,
        this.entryList,
        /*this.relationshipList*/});

  Emails.fromJson(Map<String, dynamic> json) {
    resultCount = json['result_count'];
    totalCount = json['total_count'];
    nextOffset = json['next_offset'];
    if (json['entry_list'] != null) {
      entryList = new List<EntryList>();
      json['entry_list'].forEach((v) {
        entryList.add(new EntryList.fromJson(v));
      });
    }
//    if (json['relationship_list'] != null) {
//      relationshipList = new List<Null>();
//      json['relationship_list'].forEach((v) {
//        relationshipList.add(new Null.fromJson(v));
//      });
//    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result_count'] = this.resultCount;
    data['total_count'] = this.totalCount;
    data['next_offset'] = this.nextOffset;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.map((v) => v.toJson()).toList();
    }
//    if (this.relationshipList != null) {
//      data['relationship_list'] =
//          this.relationshipList.map((v) => v.toJson()).toList();
//    }
    return data;
  }
}

class EntryList {
  String id;
  String moduleName;
  NameValueList nameValueList;

  EntryList({this.id, this.moduleName, this.nameValueList});

  EntryList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    moduleName = json['module_name'];
    nameValueList = json['name_value_list'] != null
        ? new NameValueList.fromJson(json['name_value_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['module_name'] = this.moduleName;
    if (this.nameValueList != null) {
      data['name_value_list'] = this.nameValueList.toJson();
    }
    return data;
  }
}

class NameValueList {
  AssignedUserName assignedUserName;
  AssignedUserName modifiedByName;
  AssignedUserName createdByName;
  AssignedUserName id;
  AssignedUserName name;
  AssignedUserName dateEntered;
  AssignedUserName dateModified;
  AssignedUserName modifiedUserId;
  AssignedUserName createdBy;
  AssignedUserName description;
  AssignedUserName deleted;
  AssignedUserName assignedUserId;
  AssignedUserName orphaned;
  AssignedUserName lastSynced;
  AssignedUserName fromAddrName;
  AssignedUserName replyToAddr;
  AssignedUserName toAddrsNames;
  AssignedUserName ccAddrsNames;
  AssignedUserName bccAddrsNames;
  AssignedUserName imapKeywords;
  AssignedUserName rawSource;
  AssignedUserName descriptionHtml;
  AssignedUserName dateSentReceived;
  AssignedUserName messageId;
  AssignedUserName type;
  AssignedUserName status;
  AssignedUserName flagged;
  AssignedUserName replyToStatus;
  AssignedUserName intent;
  AssignedUserName mailboxId;
  AssignedUserName parentName;
  AssignedUserName parentType;
  AssignedUserName parentId;
  AssignedUserName indicator;
  AssignedUserName subject;
  AssignedUserName attachment;
  AssignedUserName uid;
  AssignedUserName msgno;
  AssignedUserName folder;
  AssignedUserName folderType;
  AssignedUserName inboundEmailRecord;
  AssignedUserName isImported;
  AssignedUserName hasAttachment;
  AssignedUserName isOnlyPlainText;
  AssignedUserName categoryId;
  AssignedUserName emailsEmailTemplatesName;
  AssignedUserName optIn;

  NameValueList(
      {this.assignedUserName,
        this.modifiedByName,
        this.createdByName,
        this.id,
        this.name,
        this.dateEntered,
        this.dateModified,
        this.modifiedUserId,
        this.createdBy,
        this.description,
        this.deleted,
        this.assignedUserId,
        this.orphaned,
        this.lastSynced,
        this.fromAddrName,
        this.replyToAddr,
        this.toAddrsNames,
        this.ccAddrsNames,
        this.bccAddrsNames,
        this.imapKeywords,
        this.rawSource,
        this.descriptionHtml,
        this.dateSentReceived,
        this.messageId,
        this.type,
        this.status,
        this.flagged,
        this.replyToStatus,
        this.intent,
        this.mailboxId,
        this.parentName,
        this.parentType,
        this.parentId,
        this.indicator,
        this.subject,
        this.attachment,
        this.uid,
        this.msgno,
        this.folder,
        this.folderType,
        this.inboundEmailRecord,
        this.isImported,
        this.hasAttachment,
        this.isOnlyPlainText,
        this.categoryId,
        this.emailsEmailTemplatesName,
        this.optIn});

  NameValueList.fromJson(Map<String, dynamic> json) {
    assignedUserName = json['assigned_user_name'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_name'])
        : null;
    modifiedByName = json['modified_by_name'] != null
        ? new AssignedUserName.fromJson(json['modified_by_name'])
        : null;
    createdByName = json['created_by_name'] != null
        ? new AssignedUserName.fromJson(json['created_by_name'])
        : null;
    id = json['id'] != null ? new AssignedUserName.fromJson(json['id']) : null;
    name = json['name'] != null
        ? new AssignedUserName.fromJson(json['name'])
        : null;
    dateEntered = json['date_entered'] != null
        ? new AssignedUserName.fromJson(json['date_entered'])
        : null;
    dateModified = json['date_modified'] != null
        ? new AssignedUserName.fromJson(json['date_modified'])
        : null;
    modifiedUserId = json['modified_user_id'] != null
        ? new AssignedUserName.fromJson(json['modified_user_id'])
        : null;
    createdBy = json['created_by'] != null
        ? new AssignedUserName.fromJson(json['created_by'])
        : null;
    description = json['description'] != null
        ? new AssignedUserName.fromJson(json['description'])
        : null;
    deleted = json['deleted'] != null
        ? new AssignedUserName.fromJson(json['deleted'])
        : null;
    assignedUserId = json['assigned_user_id'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_id'])
        : null;
    orphaned = json['orphaned'] != null
        ? new AssignedUserName.fromJson(json['orphaned'])
        : null;
    lastSynced = json['last_synced'] != null
        ? new AssignedUserName.fromJson(json['last_synced'])
        : null;
    fromAddrName = json['from_addr_name'] != null
        ? new AssignedUserName.fromJson(json['from_addr_name'])
        : null;
    replyToAddr = json['reply_to_addr'] != null
        ? new AssignedUserName.fromJson(json['reply_to_addr'])
        : null;
    toAddrsNames = json['to_addrs_names'] != null
        ? new AssignedUserName.fromJson(json['to_addrs_names'])
        : null;
    ccAddrsNames = json['cc_addrs_names'] != null
        ? new AssignedUserName.fromJson(json['cc_addrs_names'])
        : null;
    bccAddrsNames = json['bcc_addrs_names'] != null
        ? new AssignedUserName.fromJson(json['bcc_addrs_names'])
        : null;
    imapKeywords = json['imap_keywords'] != null
        ? new AssignedUserName.fromJson(json['imap_keywords'])
        : null;
    rawSource = json['raw_source'] != null
        ? new AssignedUserName.fromJson(json['raw_source'])
        : null;
    descriptionHtml = json['description_html'] != null
        ? new AssignedUserName.fromJson(json['description_html'])
        : null;
    dateSentReceived = json['date_sent_received'] != null
        ? new AssignedUserName.fromJson(json['date_sent_received'])
        : null;
    messageId = json['message_id'] != null
        ? new AssignedUserName.fromJson(json['message_id'])
        : null;
    type = json['type'] != null
        ? new AssignedUserName.fromJson(json['type'])
        : null;
    status = json['status'] != null
        ? new AssignedUserName.fromJson(json['status'])
        : null;
    flagged = json['flagged'] != null
        ? new AssignedUserName.fromJson(json['flagged'])
        : null;
    replyToStatus = json['reply_to_status'] != null
        ? new AssignedUserName.fromJson(json['reply_to_status'])
        : null;
    intent = json['intent'] != null
        ? new AssignedUserName.fromJson(json['intent'])
        : null;
    mailboxId = json['mailbox_id'] != null
        ? new AssignedUserName.fromJson(json['mailbox_id'])
        : null;
    parentName = json['parent_name'] != null
        ? new AssignedUserName.fromJson(json['parent_name'])
        : null;
    parentType = json['parent_type'] != null
        ? new AssignedUserName.fromJson(json['parent_type'])
        : null;
    parentId = json['parent_id'] != null
        ? new AssignedUserName.fromJson(json['parent_id'])
        : null;
    indicator = json['indicator'] != null
        ? new AssignedUserName.fromJson(json['indicator'])
        : null;
    subject = json['subject'] != null
        ? new AssignedUserName.fromJson(json['subject'])
        : null;
    attachment = json['attachment'] != null
        ? new AssignedUserName.fromJson(json['attachment'])
        : null;
    uid =
    json['uid'] != null ? new AssignedUserName.fromJson(json['uid']) : null;
    msgno = json['msgno'] != null
        ? new AssignedUserName.fromJson(json['msgno'])
        : null;
    folder = json['folder'] != null
        ? new AssignedUserName.fromJson(json['folder'])
        : null;
    folderType = json['folder_type'] != null
        ? new AssignedUserName.fromJson(json['folder_type'])
        : null;
    inboundEmailRecord = json['inbound_email_record'] != null
        ? new AssignedUserName.fromJson(json['inbound_email_record'])
        : null;
    isImported = json['is_imported'] != null
        ? new AssignedUserName.fromJson(json['is_imported'])
        : null;
    hasAttachment = json['has_attachment'] != null
        ? new AssignedUserName.fromJson(json['has_attachment'])
        : null;
    isOnlyPlainText = json['is_only_plain_text'] != null
        ? new AssignedUserName.fromJson(json['is_only_plain_text'])
        : null;
    categoryId = json['category_id'] != null
        ? new AssignedUserName.fromJson(json['category_id'])
        : null;
    emailsEmailTemplatesName = json['emails_email_templates_name'] != null
        ? new AssignedUserName.fromJson(json['emails_email_templates_name'])
        : null;
    optIn = json['opt_in'] != null
        ? new AssignedUserName.fromJson(json['opt_in'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.assignedUserName != null) {
      data['assigned_user_name'] = this.assignedUserName.toJson();
    }
    if (this.modifiedByName != null) {
      data['modified_by_name'] = this.modifiedByName.toJson();
    }
    if (this.createdByName != null) {
      data['created_by_name'] = this.createdByName.toJson();
    }
    if (this.id != null) {
      data['id'] = this.id.toJson();
    }
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.dateEntered != null) {
      data['date_entered'] = this.dateEntered.toJson();
    }
    if (this.dateModified != null) {
      data['date_modified'] = this.dateModified.toJson();
    }
    if (this.modifiedUserId != null) {
      data['modified_user_id'] = this.modifiedUserId.toJson();
    }
    if (this.createdBy != null) {
      data['created_by'] = this.createdBy.toJson();
    }
    if (this.description != null) {
      data['description'] = this.description.toJson();
    }
    if (this.deleted != null) {
      data['deleted'] = this.deleted.toJson();
    }
    if (this.assignedUserId != null) {
      data['assigned_user_id'] = this.assignedUserId.toJson();
    }
    if (this.orphaned != null) {
      data['orphaned'] = this.orphaned.toJson();
    }
    if (this.lastSynced != null) {
      data['last_synced'] = this.lastSynced.toJson();
    }
    if (this.fromAddrName != null) {
      data['from_addr_name'] = this.fromAddrName.toJson();
    }
    if (this.replyToAddr != null) {
      data['reply_to_addr'] = this.replyToAddr.toJson();
    }
    if (this.toAddrsNames != null) {
      data['to_addrs_names'] = this.toAddrsNames.toJson();
    }
    if (this.ccAddrsNames != null) {
      data['cc_addrs_names'] = this.ccAddrsNames.toJson();
    }
    if (this.bccAddrsNames != null) {
      data['bcc_addrs_names'] = this.bccAddrsNames.toJson();
    }
    if (this.imapKeywords != null) {
      data['imap_keywords'] = this.imapKeywords.toJson();
    }
    if (this.rawSource != null) {
      data['raw_source'] = this.rawSource.toJson();
    }
    if (this.descriptionHtml != null) {
      data['description_html'] = this.descriptionHtml.toJson();
    }
    if (this.dateSentReceived != null) {
      data['date_sent_received'] = this.dateSentReceived.toJson();
    }
    if (this.messageId != null) {
      data['message_id'] = this.messageId.toJson();
    }
    if (this.type != null) {
      data['type'] = this.type.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.flagged != null) {
      data['flagged'] = this.flagged.toJson();
    }
    if (this.replyToStatus != null) {
      data['reply_to_status'] = this.replyToStatus.toJson();
    }
    if (this.intent != null) {
      data['intent'] = this.intent.toJson();
    }
    if (this.mailboxId != null) {
      data['mailbox_id'] = this.mailboxId.toJson();
    }
    if (this.parentName != null) {
      data['parent_name'] = this.parentName.toJson();
    }
    if (this.parentType != null) {
      data['parent_type'] = this.parentType.toJson();
    }
    if (this.parentId != null) {
      data['parent_id'] = this.parentId.toJson();
    }
    if (this.indicator != null) {
      data['indicator'] = this.indicator.toJson();
    }
    if (this.subject != null) {
      data['subject'] = this.subject.toJson();
    }
    if (this.attachment != null) {
      data['attachment'] = this.attachment.toJson();
    }
    if (this.uid != null) {
      data['uid'] = this.uid.toJson();
    }
    if (this.msgno != null) {
      data['msgno'] = this.msgno.toJson();
    }
    if (this.folder != null) {
      data['folder'] = this.folder.toJson();
    }
    if (this.folderType != null) {
      data['folder_type'] = this.folderType.toJson();
    }
    if (this.inboundEmailRecord != null) {
      data['inbound_email_record'] = this.inboundEmailRecord.toJson();
    }
    if (this.isImported != null) {
      data['is_imported'] = this.isImported.toJson();
    }
    if (this.hasAttachment != null) {
      data['has_attachment'] = this.hasAttachment.toJson();
    }
    if (this.isOnlyPlainText != null) {
      data['is_only_plain_text'] = this.isOnlyPlainText.toJson();
    }
    if (this.categoryId != null) {
      data['category_id'] = this.categoryId.toJson();
    }
    if (this.emailsEmailTemplatesName != null) {
      data['emails_email_templates_name'] =
          this.emailsEmailTemplatesName.toJson();
    }
    if (this.optIn != null) {
      data['opt_in'] = this.optIn.toJson();
    }
    return data;
  }
}

class AssignedUserName {
  String name;
  String value;

  AssignedUserName({this.name, this.value});

  AssignedUserName.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

