class CallResponse {
  String id;
  tEntryList entryList;

  CallResponse({this.id, this.entryList});

  CallResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    entryList = json['entry_list'] != null
        ? new tEntryList.fromJson(json['entry_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.toJson();
    }
    return data;
  }
}

// ignore: camel_case_types
class tEntryList {
  Name name;
  Name assignedUserId;
  Name parentType;
  Name dateStart;
  Name durationHours;
  Name status;
  Name description;

  tEntryList(
      {this.name,
        this.assignedUserId,
        this.parentType,
        this.dateStart,
        this.durationHours,
        this.status,
        this.description});

  tEntryList.fromJson(Map<String, dynamic> json) {
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    assignedUserId = json['assigned_user_id'] != null
        ? new Name.fromJson(json['assigned_user_id'])
        : null;
    parentType = json['parent_type'] != null
        ? new Name.fromJson(json['parent_type'])
        : null;
    dateStart = json['date_start'] != null
        ? new Name.fromJson(json['date_start'])
        : null;
    durationHours = json['duration_hours'] != null
        ? new Name.fromJson(json['duration_hours'])
        : null;
    status = json['status'] != null ? new Name.fromJson(json['status']) : null;
    description = json['description'] != null
        ? new Name.fromJson(json['description'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.assignedUserId != null) {
      data['assigned_user_id'] = this.assignedUserId.toJson();
    }
    if (this.parentType != null) {
      data['parent_type'] = this.parentType.toJson();
    }
    if (this.dateStart != null) {
      data['date_start'] = this.dateStart.toJson();
    }
    if (this.durationHours != null) {
      data['duration_hours'] = this.durationHours.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.description != null) {
      data['description'] = this.description.toJson();
    }
    return data;
  }
}

class Name {
  String name;
  String value;

  Name({this.name, this.value});

  Name.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
