class Leads {
  int resultCount;
  String totalCount;
  int nextOffset;
  List<EntryList> entryList;
//  List<Null> relationshipList;

  Leads(
      {this.resultCount,
        this.totalCount,
        this.nextOffset,
        this.entryList,
        /*this.relationshipList*/});

  Leads.fromJson(Map<String, dynamic> json) {
    resultCount = json['result_count'];
    totalCount = json['total_count'];
    nextOffset = json['next_offset'];
    if (json['entry_list'] != null) {
      entryList = new List<EntryList>();
      json['entry_list'].forEach((v) {
        entryList.add(new EntryList.fromJson(v));
      });
    }
//    if (json['relationship_list'] != null) {
//      relationshipList = new List<Null>();
//      json['relationship_list'].forEach((v) {
//        relationshipList.add(new Null.fromJson(v));
//      });
//    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result_count'] = this.resultCount;
    data['total_count'] = this.totalCount;
    data['next_offset'] = this.nextOffset;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.map((v) => v.toJson()).toList();
    }
//    if (this.relationshipList != null) {
//      data['relationship_list'] =
//          this.relationshipList.map((v) => v.toJson()).toList();
//    }
    return data;
  }

  String toJsonString() {
    String data = '{"result_count":"$resultCount","total_count":"$totalCount","next_offset":"$nextOffset"';


    if (this.entryList != null) {
      String entryListString='[';
//      data['entry_list'] = this.entryList.map((v) => v.toJson()).toList();
      for(int i=0; i<entryList.length;i++){
        entryListString = entryListString+entryList[i].toJsonString();
        if(i<entryList.length-1){
          entryListString=entryListString+",";
        }
      }
      entryListString = entryListString + ']';
      data = data + ',"entry_list":"$entryListString"';
    }

    return data;
  }
}

class EntryList {
  String id;
  String moduleName;
  NameValueList nameValueList;

  EntryList({this.id, this.moduleName, this.nameValueList});

  EntryList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    moduleName = json['module_name'];
    nameValueList = json['name_value_list'] != null
        ? new NameValueList.fromJson(json['name_value_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['module_name'] = this.moduleName;
    if (this.nameValueList != null) {
      data['name_value_list'] = this.nameValueList.toJson();
    }
    return data;
  }

  String toJsonString(){
    String data = '{"id":"$id","module_name":"$moduleName","name_value_list":"${nameValueList.toJsonString()}"}';
    return data;
  }
}

class NameValueList {
  AssignedUserName assignedUserName;
  AssignedUserName modifiedByName;
  AssignedUserName createdByName;
  AssignedUserName id;
  AssignedUserName name;
  AssignedUserName dateEntered;
  AssignedUserName dateModified;
  AssignedUserName modifiedUserId;
  AssignedUserName createdBy;
  AssignedUserName description;
  AssignedUserName deleted;
  AssignedUserName assignedUserId;
  AssignedUserName salutation;
  AssignedUserName firstName;
  AssignedUserName lastName;
  AssignedUserName fullName;
  AssignedUserName title;
  AssignedUserName photo;
  AssignedUserName department;
  AssignedUserName doNotCall;
  AssignedUserName phoneHome;
  AssignedUserName email;
  AssignedUserName phoneMobile;
  AssignedUserName phoneWork;
  AssignedUserName phoneOther;
  AssignedUserName phoneFax;
  AssignedUserName email1;
  AssignedUserName email2;
  AssignedUserName invalidEmail;
  AssignedUserName emailOptOut;
  AssignedUserName lawfulBasis;
  AssignedUserName dateReviewed;
  AssignedUserName lawfulBasisSource;
  AssignedUserName primaryAddressStreet;
  AssignedUserName primaryAddressStreet2;
  AssignedUserName primaryAddressStreet3;
  AssignedUserName primaryAddressCity;
  AssignedUserName primaryAddressState;
  AssignedUserName primaryAddressPostalcode;
  AssignedUserName primaryAddressCountry;
  AssignedUserName altAddressStreet;
  AssignedUserName altAddressStreet2;
  AssignedUserName altAddressStreet3;
  AssignedUserName altAddressCity;
  AssignedUserName altAddressState;
  AssignedUserName altAddressPostalcode;
  AssignedUserName altAddressCountry;
  AssignedUserName assistant;
  AssignedUserName assistantPhone;
  AssignedUserName emailAddressesNonPrimary;
  AssignedUserName converted;
  AssignedUserName referedBy;
  AssignedUserName leadSource;
  AssignedUserName leadSourceDescription;
  AssignedUserName status;
  AssignedUserName statusDescription;
  AssignedUserName reportsToId;
  AssignedUserName reportToName;
  AssignedUserName accountName;
  AssignedUserName accountDescription;
  AssignedUserName contactId;
  AssignedUserName accountId;
  AssignedUserName opportunityId;
  AssignedUserName opportunityName;
  AssignedUserName opportunityAmount;
  AssignedUserName campaignId;
  AssignedUserName campaignName;
  AssignedUserName cAcceptStatusFields;
  AssignedUserName mAcceptStatusFields;
  AssignedUserName acceptStatusId;
  AssignedUserName acceptStatusName;
  AssignedUserName webtoleadEmail1;
  AssignedUserName webtoleadEmail2;
  AssignedUserName webtoleadEmailOptOut;
  AssignedUserName webtoleadInvalidEmail;
  AssignedUserName birthdate;
  AssignedUserName portalName;
  AssignedUserName portalApp;
  AssignedUserName website;
  AssignedUserName eInviteStatusFields;
  AssignedUserName eventStatusName;
  AssignedUserName eventInviteId;
  AssignedUserName eAcceptStatusFields;
  AssignedUserName eventAcceptStatus;
  AssignedUserName eventStatusId;
  AssignedUserName titlesC;
  AssignedUserName departmentsC;
  AssignedUserName jjwgMapsLngC;
  AssignedUserName isYourOwnBusinessC;
  AssignedUserName jjwgMapsGeocodeStatusC;
  AssignedUserName passwordC;
  AssignedUserName userTypeC;
  AssignedUserName selectTypeC;
  AssignedUserName jjwgMapsAddressC;
  AssignedUserName jjwgMapsLatC;
  AssignedUserName verticalC;
  AssignedUserName confirmPasswordC;

  NameValueList(
      {this.assignedUserName,
        this.modifiedByName,
        this.createdByName,
        this.id,
        this.name,
        this.dateEntered,
        this.dateModified,
        this.modifiedUserId,
        this.createdBy,
        this.description,
        this.deleted,
        this.assignedUserId,
        this.salutation,
        this.firstName,
        this.lastName,
        this.fullName,
        this.title,
        this.photo,
        this.department,
        this.doNotCall,
        this.phoneHome,
        this.email,
        this.phoneMobile,
        this.phoneWork,
        this.phoneOther,
        this.phoneFax,
        this.email1,
        this.email2,
        this.invalidEmail,
        this.emailOptOut,
        this.lawfulBasis,
        this.dateReviewed,
        this.lawfulBasisSource,
        this.primaryAddressStreet,
        this.primaryAddressStreet2,
        this.primaryAddressStreet3,
        this.primaryAddressCity,
        this.primaryAddressState,
        this.primaryAddressPostalcode,
        this.primaryAddressCountry,
        this.altAddressStreet,
        this.altAddressStreet2,
        this.altAddressStreet3,
        this.altAddressCity,
        this.altAddressState,
        this.altAddressPostalcode,
        this.altAddressCountry,
        this.assistant,
        this.assistantPhone,
        this.emailAddressesNonPrimary,
        this.converted,
        this.referedBy,
        this.leadSource,
        this.leadSourceDescription,
        this.status,
        this.statusDescription,
        this.reportsToId,
        this.reportToName,
        this.accountName,
        this.accountDescription,
        this.contactId,
        this.accountId,
        this.opportunityId,
        this.opportunityName,
        this.opportunityAmount,
        this.campaignId,
        this.campaignName,
        this.cAcceptStatusFields,
        this.mAcceptStatusFields,
        this.acceptStatusId,
        this.acceptStatusName,
        this.webtoleadEmail1,
        this.webtoleadEmail2,
        this.webtoleadEmailOptOut,
        this.webtoleadInvalidEmail,
        this.birthdate,
        this.portalName,
        this.portalApp,
        this.website,
        this.eInviteStatusFields,
        this.eventStatusName,
        this.eventInviteId,
        this.eAcceptStatusFields,
        this.eventAcceptStatus,
        this.eventStatusId,
        this.titlesC,
        this.departmentsC,
        this.jjwgMapsLngC,
        this.isYourOwnBusinessC,
        this.jjwgMapsGeocodeStatusC,
        this.passwordC,
        this.userTypeC,
        this.selectTypeC,
        this.jjwgMapsAddressC,
        this.jjwgMapsLatC,
        this.verticalC,
        this.confirmPasswordC});

  NameValueList.fromJson(Map<String, dynamic> json) {
    assignedUserName = json['assigned_user_name'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_name'])
        : null;
    modifiedByName = json['modified_by_name'] != null
        ? new AssignedUserName.fromJson(json['modified_by_name'])
        : null;
    createdByName = json['created_by_name'] != null
        ? new AssignedUserName.fromJson(json['created_by_name'])
        : null;
    id = json['id'] != null ? new AssignedUserName.fromJson(json['id']) : null;
    name = json['name'] != null
        ? new AssignedUserName.fromJson(json['name'])
        : null;
    dateEntered = json['date_entered'] != null
        ? new AssignedUserName.fromJson(json['date_entered'])
        : null;
    dateModified = json['date_modified'] != null
        ? new AssignedUserName.fromJson(json['date_modified'])
        : null;
    modifiedUserId = json['modified_user_id'] != null
        ? new AssignedUserName.fromJson(json['modified_user_id'])
        : null;
    createdBy = json['created_by'] != null
        ? new AssignedUserName.fromJson(json['created_by'])
        : null;
    description = json['description'] != null
        ? new AssignedUserName.fromJson(json['description'])
        : null;
    deleted = json['deleted'] != null
        ? new AssignedUserName.fromJson(json['deleted'])
        : null;
    assignedUserId = json['assigned_user_id'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_id'])
        : null;
    salutation = json['salutation'] != null
        ? new AssignedUserName.fromJson(json['salutation'])
        : null;
    firstName = json['first_name'] != null
        ? new AssignedUserName.fromJson(json['first_name'])
        : null;
    lastName = json['last_name'] != null
        ? new AssignedUserName.fromJson(json['last_name'])
        : null;
    fullName = json['full_name'] != null
        ? new AssignedUserName.fromJson(json['full_name'])
        : null;
    title = json['title'] != null
        ? new AssignedUserName.fromJson(json['title'])
        : null;
    photo = json['photo'] != null
        ? new AssignedUserName.fromJson(json['photo'])
        : null;
    department = json['department'] != null
        ? new AssignedUserName.fromJson(json['department'])
        : null;
    doNotCall = json['do_not_call'] != null
        ? new AssignedUserName.fromJson(json['do_not_call'])
        : null;
    phoneHome = json['phone_home'] != null
        ? new AssignedUserName.fromJson(json['phone_home'])
        : null;
    email = json['email'] != null
        ? new AssignedUserName.fromJson(json['email'])
        : null;
    phoneMobile = json['phone_mobile'] != null
        ? new AssignedUserName.fromJson(json['phone_mobile'])
        : null;
    phoneWork = json['phone_work'] != null
        ? new AssignedUserName.fromJson(json['phone_work'])
        : null;
    phoneOther = json['phone_other'] != null
        ? new AssignedUserName.fromJson(json['phone_other'])
        : null;
    phoneFax = json['phone_fax'] != null
        ? new AssignedUserName.fromJson(json['phone_fax'])
        : null;
    email1 = json['email1'] != null
        ? new AssignedUserName.fromJson(json['email1'])
        : null;
    email2 = json['email2'] != null
        ? new AssignedUserName.fromJson(json['email2'])
        : null;
    invalidEmail = json['invalid_email'] != null
        ? new AssignedUserName.fromJson(json['invalid_email'])
        : null;
    emailOptOut = json['email_opt_out'] != null
        ? new AssignedUserName.fromJson(json['email_opt_out'])
        : null;
    lawfulBasis = json['lawful_basis'] != null
        ? new AssignedUserName.fromJson(json['lawful_basis'])
        : null;
    dateReviewed = json['date_reviewed'] != null
        ? new AssignedUserName.fromJson(json['date_reviewed'])
        : null;
    lawfulBasisSource = json['lawful_basis_source'] != null
        ? new AssignedUserName.fromJson(json['lawful_basis_source'])
        : null;
    primaryAddressStreet = json['primary_address_street'] != null
        ? new AssignedUserName.fromJson(json['primary_address_street'])
        : null;
    primaryAddressStreet2 = json['primary_address_street_2'] != null
        ? new AssignedUserName.fromJson(json['primary_address_street_2'])
        : null;
    primaryAddressStreet3 = json['primary_address_street_3'] != null
        ? new AssignedUserName.fromJson(json['primary_address_street_3'])
        : null;
    primaryAddressCity = json['primary_address_city'] != null
        ? new AssignedUserName.fromJson(json['primary_address_city'])
        : null;
    primaryAddressState = json['primary_address_state'] != null
        ? new AssignedUserName.fromJson(json['primary_address_state'])
        : null;
    primaryAddressPostalcode = json['primary_address_postalcode'] != null
        ? new AssignedUserName.fromJson(json['primary_address_postalcode'])
        : null;
    primaryAddressCountry = json['primary_address_country'] != null
        ? new AssignedUserName.fromJson(json['primary_address_country'])
        : null;
    altAddressStreet = json['alt_address_street'] != null
        ? new AssignedUserName.fromJson(json['alt_address_street'])
        : null;
    altAddressStreet2 = json['alt_address_street_2'] != null
        ? new AssignedUserName.fromJson(json['alt_address_street_2'])
        : null;
    altAddressStreet3 = json['alt_address_street_3'] != null
        ? new AssignedUserName.fromJson(json['alt_address_street_3'])
        : null;
    altAddressCity = json['alt_address_city'] != null
        ? new AssignedUserName.fromJson(json['alt_address_city'])
        : null;
    altAddressState = json['alt_address_state'] != null
        ? new AssignedUserName.fromJson(json['alt_address_state'])
        : null;
    altAddressPostalcode = json['alt_address_postalcode'] != null
        ? new AssignedUserName.fromJson(json['alt_address_postalcode'])
        : null;
    altAddressCountry = json['alt_address_country'] != null
        ? new AssignedUserName.fromJson(json['alt_address_country'])
        : null;
    assistant = json['assistant'] != null
        ? new AssignedUserName.fromJson(json['assistant'])
        : null;
    assistantPhone = json['assistant_phone'] != null
        ? new AssignedUserName.fromJson(json['assistant_phone'])
        : null;
    emailAddressesNonPrimary = json['email_addresses_non_primary'] != null
        ? new AssignedUserName.fromJson(json['email_addresses_non_primary'])
        : null;
    converted = json['converted'] != null
        ? new AssignedUserName.fromJson(json['converted'])
        : null;
    referedBy = json['refered_by'] != null
        ? new AssignedUserName.fromJson(json['refered_by'])
        : null;
    leadSource = json['lead_source'] != null
        ? new AssignedUserName.fromJson(json['lead_source'])
        : null;
    leadSourceDescription = json['lead_source_description'] != null
        ? new AssignedUserName.fromJson(json['lead_source_description'])
        : null;
    status = json['status'] != null
        ? new AssignedUserName.fromJson(json['status'])
        : null;
    statusDescription = json['status_description'] != null
        ? new AssignedUserName.fromJson(json['status_description'])
        : null;
    reportsToId = json['reports_to_id'] != null
        ? new AssignedUserName.fromJson(json['reports_to_id'])
        : null;
    reportToName = json['report_to_name'] != null
        ? new AssignedUserName.fromJson(json['report_to_name'])
        : null;
    accountName = json['account_name'] != null
        ? new AssignedUserName.fromJson(json['account_name'])
        : null;
    accountDescription = json['account_description'] != null
        ? new AssignedUserName.fromJson(json['account_description'])
        : null;
    contactId = json['contact_id'] != null
        ? new AssignedUserName.fromJson(json['contact_id'])
        : null;
    accountId = json['account_id'] != null
        ? new AssignedUserName.fromJson(json['account_id'])
        : null;
    opportunityId = json['opportunity_id'] != null
        ? new AssignedUserName.fromJson(json['opportunity_id'])
        : null;
    opportunityName = json['opportunity_name'] != null
        ? new AssignedUserName.fromJson(json['opportunity_name'])
        : null;
    opportunityAmount = json['opportunity_amount'] != null
        ? new AssignedUserName.fromJson(json['opportunity_amount'])
        : null;
    campaignId = json['campaign_id'] != null
        ? new AssignedUserName.fromJson(json['campaign_id'])
        : null;
    campaignName = json['campaign_name'] != null
        ? new AssignedUserName.fromJson(json['campaign_name'])
        : null;
    cAcceptStatusFields = json['c_accept_status_fields'] != null
        ? new AssignedUserName.fromJson(json['c_accept_status_fields'])
        : null;
    mAcceptStatusFields = json['m_accept_status_fields'] != null
        ? new AssignedUserName.fromJson(json['m_accept_status_fields'])
        : null;
    acceptStatusId = json['accept_status_id'] != null
        ? new AssignedUserName.fromJson(json['accept_status_id'])
        : null;
    acceptStatusName = json['accept_status_name'] != null
        ? new AssignedUserName.fromJson(json['accept_status_name'])
        : null;
    webtoleadEmail1 = json['webtolead_email1'] != null
        ? new AssignedUserName.fromJson(json['webtolead_email1'])
        : null;
    webtoleadEmail2 = json['webtolead_email2'] != null
        ? new AssignedUserName.fromJson(json['webtolead_email2'])
        : null;
    webtoleadEmailOptOut = json['webtolead_email_opt_out'] != null
        ? new AssignedUserName.fromJson(json['webtolead_email_opt_out'])
        : null;
    webtoleadInvalidEmail = json['webtolead_invalid_email'] != null
        ? new AssignedUserName.fromJson(json['webtolead_invalid_email'])
        : null;
    birthdate = json['birthdate'] != null
        ? new AssignedUserName.fromJson(json['birthdate'])
        : null;
    portalName = json['portal_name'] != null
        ? new AssignedUserName.fromJson(json['portal_name'])
        : null;
    portalApp = json['portal_app'] != null
        ? new AssignedUserName.fromJson(json['portal_app'])
        : null;
    website = json['website'] != null
        ? new AssignedUserName.fromJson(json['website'])
        : null;
    eInviteStatusFields = json['e_invite_status_fields'] != null
        ? new AssignedUserName.fromJson(json['e_invite_status_fields'])
        : null;
    eventStatusName = json['event_status_name'] != null
        ? new AssignedUserName.fromJson(json['event_status_name'])
        : null;
    eventInviteId = json['event_invite_id'] != null
        ? new AssignedUserName.fromJson(json['event_invite_id'])
        : null;
    eAcceptStatusFields = json['e_accept_status_fields'] != null
        ? new AssignedUserName.fromJson(json['e_accept_status_fields'])
        : null;
    eventAcceptStatus = json['event_accept_status'] != null
        ? new AssignedUserName.fromJson(json['event_accept_status'])
        : null;
    eventStatusId = json['event_status_id'] != null
        ? new AssignedUserName.fromJson(json['event_status_id'])
        : null;
    titlesC = json['titles_c'] != null
        ? new AssignedUserName.fromJson(json['titles_c'])
        : null;
    departmentsC = json['departments_c'] != null
        ? new AssignedUserName.fromJson(json['departments_c'])
        : null;
    jjwgMapsLngC = json['jjwg_maps_lng_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_lng_c'])
        : null;
    isYourOwnBusinessC = json['is_your_own_business_c'] != null
        ? new AssignedUserName.fromJson(json['is_your_own_business_c'])
        : null;
    jjwgMapsGeocodeStatusC = json['jjwg_maps_geocode_status_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_geocode_status_c'])
        : null;
    passwordC = json['password_c'] != null
        ? new AssignedUserName.fromJson(json['password_c'])
        : null;
    userTypeC = json['user_type_c'] != null
        ? new AssignedUserName.fromJson(json['user_type_c'])
        : null;
    selectTypeC = json['select_type_c'] != null
        ? new AssignedUserName.fromJson(json['select_type_c'])
        : null;
    jjwgMapsAddressC = json['jjwg_maps_address_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_address_c'])
        : null;
    jjwgMapsLatC = json['jjwg_maps_lat_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_lat_c'])
        : null;
    verticalC = json['vertical_c'] != null
        ? new AssignedUserName.fromJson(json['vertical_c'])
        : null;
    confirmPasswordC = json['confirm_password_c'] != null
        ? new AssignedUserName.fromJson(json['confirm_password_c'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.assignedUserName != null) {
      data['assigned_user_name'] = this.assignedUserName.toJson();
    }
    if (this.modifiedByName != null) {
      data['modified_by_name'] = this.modifiedByName.toJson();
    }
    if (this.createdByName != null) {
      data['created_by_name'] = this.createdByName.toJson();
    }
    if (this.id != null) {
      data['id'] = this.id.toJson();
    }
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.dateEntered != null) {
      data['date_entered'] = this.dateEntered.toJson();
    }
    if (this.dateModified != null) {
      data['date_modified'] = this.dateModified.toJson();
    }
    if (this.modifiedUserId != null) {
      data['modified_user_id'] = this.modifiedUserId.toJson();
    }
    if (this.createdBy != null) {
      data['created_by'] = this.createdBy.toJson();
    }
    if (this.description != null) {
      data['description'] = this.description.toJson();
    }
    if (this.deleted != null) {
      data['deleted'] = this.deleted.toJson();
    }
    if (this.assignedUserId != null) {
      data['assigned_user_id'] = this.assignedUserId.toJson();
    }
    if (this.salutation != null) {
      data['salutation'] = this.salutation.toJson();
    }
    if (this.firstName != null) {
      data['first_name'] = this.firstName.toJson();
    }
    if (this.lastName != null) {
      data['last_name'] = this.lastName.toJson();
    }
    if (this.fullName != null) {
      data['full_name'] = this.fullName.toJson();
    }
    if (this.title != null) {
      data['title'] = this.title.toJson();
    }
    if (this.photo != null) {
      data['photo'] = this.photo.toJson();
    }
    if (this.department != null) {
      data['department'] = this.department.toJson();
    }
    if (this.doNotCall != null) {
      data['do_not_call'] = this.doNotCall.toJson();
    }
    if (this.phoneHome != null) {
      data['phone_home'] = this.phoneHome.toJson();
    }
    if (this.email != null) {
      data['email'] = this.email.toJson();
    }
    if (this.phoneMobile != null) {
      data['phone_mobile'] = this.phoneMobile.toJson();
    }
    if (this.phoneWork != null) {
      data['phone_work'] = this.phoneWork.toJson();
    }
    if (this.phoneOther != null) {
      data['phone_other'] = this.phoneOther.toJson();
    }
    if (this.phoneFax != null) {
      data['phone_fax'] = this.phoneFax.toJson();
    }
    if (this.email1 != null) {
      data['email1'] = this.email1.toJson();
    }
    if (this.email2 != null) {
      data['email2'] = this.email2.toJson();
    }
    if (this.invalidEmail != null) {
      data['invalid_email'] = this.invalidEmail.toJson();
    }
    if (this.emailOptOut != null) {
      data['email_opt_out'] = this.emailOptOut.toJson();
    }
    if (this.lawfulBasis != null) {
      data['lawful_basis'] = this.lawfulBasis.toJson();
    }
    if (this.dateReviewed != null) {
      data['date_reviewed'] = this.dateReviewed.toJson();
    }
    if (this.lawfulBasisSource != null) {
      data['lawful_basis_source'] = this.lawfulBasisSource.toJson();
    }
    if (this.primaryAddressStreet != null) {
      data['primary_address_street'] = this.primaryAddressStreet.toJson();
    }
    if (this.primaryAddressStreet2 != null) {
      data['primary_address_street_2'] = this.primaryAddressStreet2.toJson();
    }
    if (this.primaryAddressStreet3 != null) {
      data['primary_address_street_3'] = this.primaryAddressStreet3.toJson();
    }
    if (this.primaryAddressCity != null) {
      data['primary_address_city'] = this.primaryAddressCity.toJson();
    }
    if (this.primaryAddressState != null) {
      data['primary_address_state'] = this.primaryAddressState.toJson();
    }
    if (this.primaryAddressPostalcode != null) {
      data['primary_address_postalcode'] =
          this.primaryAddressPostalcode.toJson();
    }
    if (this.primaryAddressCountry != null) {
      data['primary_address_country'] = this.primaryAddressCountry.toJson();
    }
    if (this.altAddressStreet != null) {
      data['alt_address_street'] = this.altAddressStreet.toJson();
    }
    if (this.altAddressStreet2 != null) {
      data['alt_address_street_2'] = this.altAddressStreet2.toJson();
    }
    if (this.altAddressStreet3 != null) {
      data['alt_address_street_3'] = this.altAddressStreet3.toJson();
    }
    if (this.altAddressCity != null) {
      data['alt_address_city'] = this.altAddressCity.toJson();
    }
    if (this.altAddressState != null) {
      data['alt_address_state'] = this.altAddressState.toJson();
    }
    if (this.altAddressPostalcode != null) {
      data['alt_address_postalcode'] = this.altAddressPostalcode.toJson();
    }
    if (this.altAddressCountry != null) {
      data['alt_address_country'] = this.altAddressCountry.toJson();
    }
    if (this.assistant != null) {
      data['assistant'] = this.assistant.toJson();
    }
    if (this.assistantPhone != null) {
      data['assistant_phone'] = this.assistantPhone.toJson();
    }
    if (this.emailAddressesNonPrimary != null) {
      data['email_addresses_non_primary'] =
          this.emailAddressesNonPrimary.toJson();
    }
    if (this.converted != null) {
      data['converted'] = this.converted.toJson();
    }
    if (this.referedBy != null) {
      data['refered_by'] = this.referedBy.toJson();
    }
    if (this.leadSource != null) {
      data['lead_source'] = this.leadSource.toJson();
    }
    if (this.leadSourceDescription != null) {
      data['lead_source_description'] = this.leadSourceDescription.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.statusDescription != null) {
      data['status_description'] = this.statusDescription.toJson();
    }
    if (this.reportsToId != null) {
      data['reports_to_id'] = this.reportsToId.toJson();
    }
    if (this.reportToName != null) {
      data['report_to_name'] = this.reportToName.toJson();
    }
    if (this.accountName != null) {
      data['account_name'] = this.accountName.toJson();
    }
    if (this.accountDescription != null) {
      data['account_description'] = this.accountDescription.toJson();
    }
    if (this.contactId != null) {
      data['contact_id'] = this.contactId.toJson();
    }
    if (this.accountId != null) {
      data['account_id'] = this.accountId.toJson();
    }
    if (this.opportunityId != null) {
      data['opportunity_id'] = this.opportunityId.toJson();
    }
    if (this.opportunityName != null) {
      data['opportunity_name'] = this.opportunityName.toJson();
    }
    if (this.opportunityAmount != null) {
      data['opportunity_amount'] = this.opportunityAmount.toJson();
    }
    if (this.campaignId != null) {
      data['campaign_id'] = this.campaignId.toJson();
    }
    if (this.campaignName != null) {
      data['campaign_name'] = this.campaignName.toJson();
    }
    if (this.cAcceptStatusFields != null) {
      data['c_accept_status_fields'] = this.cAcceptStatusFields.toJson();
    }
    if (this.mAcceptStatusFields != null) {
      data['m_accept_status_fields'] = this.mAcceptStatusFields.toJson();
    }
    if (this.acceptStatusId != null) {
      data['accept_status_id'] = this.acceptStatusId.toJson();
    }
    if (this.acceptStatusName != null) {
      data['accept_status_name'] = this.acceptStatusName.toJson();
    }
    if (this.webtoleadEmail1 != null) {
      data['webtolead_email1'] = this.webtoleadEmail1.toJson();
    }
    if (this.webtoleadEmail2 != null) {
      data['webtolead_email2'] = this.webtoleadEmail2.toJson();
    }
    if (this.webtoleadEmailOptOut != null) {
      data['webtolead_email_opt_out'] = this.webtoleadEmailOptOut.toJson();
    }
    if (this.webtoleadInvalidEmail != null) {
      data['webtolead_invalid_email'] = this.webtoleadInvalidEmail.toJson();
    }
    if (this.birthdate != null) {
      data['birthdate'] = this.birthdate.toJson();
    }
    if (this.portalName != null) {
      data['portal_name'] = this.portalName.toJson();
    }
    if (this.portalApp != null) {
      data['portal_app'] = this.portalApp.toJson();
    }
    if (this.website != null) {
      data['website'] = this.website.toJson();
    }
    if (this.eInviteStatusFields != null) {
      data['e_invite_status_fields'] = this.eInviteStatusFields.toJson();
    }
    if (this.eventStatusName != null) {
      data['event_status_name'] = this.eventStatusName.toJson();
    }
    if (this.eventInviteId != null) {
      data['event_invite_id'] = this.eventInviteId.toJson();
    }
    if (this.eAcceptStatusFields != null) {
      data['e_accept_status_fields'] = this.eAcceptStatusFields.toJson();
    }
    if (this.eventAcceptStatus != null) {
      data['event_accept_status'] = this.eventAcceptStatus.toJson();
    }
    if (this.eventStatusId != null) {
      data['event_status_id'] = this.eventStatusId.toJson();
    }
    if (this.titlesC != null) {
      data['titles_c'] = this.titlesC.toJson();
    }
    if (this.departmentsC != null) {
      data['departments_c'] = this.departmentsC.toJson();
    }
    if (this.jjwgMapsLngC != null) {
      data['jjwg_maps_lng_c'] = this.jjwgMapsLngC.toJson();
    }
    if (this.isYourOwnBusinessC != null) {
      data['is_your_own_business_c'] = this.isYourOwnBusinessC.toJson();
    }
    if (this.jjwgMapsGeocodeStatusC != null) {
      data['jjwg_maps_geocode_status_c'] = this.jjwgMapsGeocodeStatusC.toJson();
    }
    if (this.passwordC != null) {
      data['password_c'] = this.passwordC.toJson();
    }
    if (this.userTypeC != null) {
      data['user_type_c'] = this.userTypeC.toJson();
    }
    if (this.selectTypeC != null) {
      data['select_type_c'] = this.selectTypeC.toJson();
    }
    if (this.jjwgMapsAddressC != null) {
      data['jjwg_maps_address_c'] = this.jjwgMapsAddressC.toJson();
    }
    if (this.jjwgMapsLatC != null) {
      data['jjwg_maps_lat_c'] = this.jjwgMapsLatC.toJson();
    }
    if (this.verticalC != null) {
      data['vertical_c'] = this.verticalC.toJson();
    }
    if (this.confirmPasswordC != null) {
      data['confirm_password_c'] = this.confirmPasswordC.toJson();
    }
    return data;
  }

  String toJsonString() {
    String data = null;
    if (this.assignedUserName != null) {
//      data['assigned_user_name'] = this.assignedUserName.toJson();
    data = '{';
      data = '$data"assigned_user_name":"${assignedUserName.toJsonString()}",';
    }
    if (this.modifiedByName != null) {
      if(data==null)
      data = '{';

      data = '$data"modified_by_name":"${modifiedByName.toJsonString()}",';
    }
    if (this.createdByName != null) {
      if(data==null)
        data = '{';
      data = '$data"created_by_name":"${createdByName.toJsonString()}",';
    }
    if (this.id != null) {
      if(data==null)
        data = '{';
      data = '$data"id":"${id.toJsonString()}",';
    }
    if (this.name != null) {
      if(data==null)
        data = '{';
      data = '$data"name":"${name.toJsonString()}",';
    }
    if (this.dateEntered != null) {
//      data['date_entered'] = this.dateEntered.toJson();
      if(data==null)
        data = '{';
      data = '$data"date_entered":"${dateEntered.toJsonString()}",';
    }
    if (this.dateModified != null) {
//      data['date_modified'] = this.dateModified.toJson();
      if(data==null)
        data = '{';
      data = '$data"date_modified":"${dateModified.toJsonString()}",';
    }
    if (this.modifiedUserId != null) {
      if(data==null)
        data = '{';
      data = '$data"modified_user_id":"${modifiedUserId.toJsonString()}",';
    }
    if (this.createdBy != null) {
      if(data==null)
        data = '{';
      data = '$data"created_by":"${createdBy.toJsonString()}",';
    }
    if (this.description != null) {
      if(data==null)
        data = '{';
      data = '$data"description":"${description.toJsonString()}",';
    }
    if (this.deleted != null) {
      if(data==null)
        data = '{';
      data = '$data"deleted":"${deleted.toJsonString()}",';
    }
    if (this.assignedUserId != null) {
      if(data==null)
        data = '{';
      data = '$data"assigned_user_id":"${assignedUserId.toJsonString()}",';
    }
    if (this.salutation != null) {
      if(data==null)
        data = '{';
      data = '$data"salutation":"${salutation.toJsonString()}",';
    }
    if (this.firstName != null) {
      if(data==null)
        data = '{';
      data = '$data"first_name":"${firstName.toJsonString()}",';
    }
    if (this.lastName != null) {
      if(data==null)
        data = '{';
      data = '$data"last_name":"${lastName.toJsonString()}",';
    }
    if (this.fullName != null) {
      if(data==null)
        data = '{';
      data = '$data"full_name":"${fullName.toJsonString()}",';
    }
    if (this.title != null) {
      if(data==null)
        data = '{';
      data = '$data"title":"${title.toJsonString()}",';
    }
    if (this.photo != null) {
      if(data==null)
        data = '{';
      data = '$data"photo":"${photo.toJsonString()}",';
    }
    if (this.department != null) {
      if(data==null)
        data = '{';
      data = '$data"department":"${department.toJsonString()}",';
    }
    if (this.doNotCall != null) {
      if(data==null)
        data = '{';
      data = '$data"do_not_call":"${doNotCall.toJsonString()}",';
    }
    if (this.phoneHome != null) {
      if(data==null)
        data = '{';
      data = '$data"phone_home":"${phoneHome.toJsonString()}",';
    }
    if (this.email != null) {
      if(data==null)
        data = '{';
      data = '$data"email":"${email.toJsonString()}",';
    }
    if (this.phoneMobile != null) {
      if(data==null)
        data = '{';
      data = '$data"phone_mobile":"${phoneMobile.toJsonString()}",';
    }
    if (this.phoneWork != null) {
      if(data==null)
        data = '{';
      data = '$data"phone_work":"${phoneWork.toJsonString()}",';
    }
    if (this.phoneOther != null) {
      if(data==null)
        data = '{';
      data = '$data"phone_other":"${phoneOther.toJsonString()}",';
    }
    if (this.phoneFax != null) {
      if(data==null)
        data = '{';
      data = '$data"phone_fax":"${phoneFax.toJsonString()}",';
    }
    if (this.email1 != null) {
      if(data==null)
        data = '{';
      data = '$data"email1":"${email1.toJsonString()}",';
    }
    if (this.email2 != null) {
      if(data==null)
        data = '{';
      data = '$data"email2":"${email2.toJsonString()}",';
    }
    if (this.invalidEmail != null) {
      if(data==null)
        data = '{';
      data = '$data"invalid_email":"${invalidEmail.toJsonString()}",';
    }
    if (this.emailOptOut != null) {
      if(data==null)
        data = '{';
      data = '$data"email_opt_out":"${emailOptOut.toJsonString()}",';
    }
    if (this.lawfulBasis != null) {
      if(data==null)
        data = '{';
      data = '$data"lawful_basis":"${lawfulBasis.toJsonString()}",';
    }
    if (this.dateReviewed != null) {
      if(data==null)
        data = '{';
      data = '$data"date_reviewed":"${dateReviewed.toJsonString()}",';
    }
    if (this.lawfulBasisSource != null) {
      if(data==null)
        data = '{';
      data = '$data"lawful_basis_source":"${lawfulBasisSource.toJsonString()}",';
    }
    if (this.primaryAddressStreet != null) {
      if(data==null)
        data = '{';
      data = '$data"primary_address_street":"${primaryAddressStreet.toJsonString()}",';
    }
    if (this.primaryAddressStreet2 != null) {
      if(data==null)
        data = '{';
      data = '$data"primary_address_street_2":"${primaryAddressStreet2.toJsonString()}",';
    }
    if (this.primaryAddressStreet3 != null) {
      if(data==null)
        data = '{';
      data = '$data"primary_address_street_3":"${primaryAddressStreet3.toJsonString()}",';
    }
    if (this.primaryAddressCity != null) {
      if(data==null)
        data = '{';
      data = '$data"primary_address_city":"${primaryAddressCity.toJsonString()}",';
    }
    if (this.primaryAddressState != null) {
      if(data==null)
        data = '{';
      data = '$data"primary_address_state":"${primaryAddressState.toJsonString()}",';
    }
    if (this.primaryAddressPostalcode != null) {
      if(data==null)
        data = '{';
      data = '$data"primary_address_postalcode":"${primaryAddressPostalcode.toJsonString()}",';
    }
    if (this.primaryAddressCountry != null) {
      if(data==null)
        data = '{';
      data = '$data"primary_address_country":"${primaryAddressCountry.toJsonString()}",';
    }
    if (this.altAddressStreet != null) {
      if(data==null)
        data = '{';
      data = '$data"alt_address_street":"${altAddressStreet.toJsonString()}",';
    }
    if (this.altAddressStreet2 != null) {
      if(data==null)
        data = '{';
      data = '$data"alt_address_street_2":"${altAddressStreet2.toJsonString()}",';
    }
    if (this.altAddressStreet3 != null) {
      if(data==null)
        data = '{';
      data = '$data"alt_address_street_3":"${altAddressStreet3.toJsonString()}",';
    }
    if (this.altAddressCity != null) {
      if(data==null)
        data = '{';
      data = '$data"alt_address_city":"${altAddressCity.toJsonString()}",';
    }
    if (this.altAddressState != null) {
      if(data==null)
        data = '{';
      data = '$data"alt_address_state":"${altAddressState.toJsonString()}",';
    }
    if (this.altAddressPostalcode != null) {
      if(data==null)
        data = '{';
      data = '$data"alt_address_postalcode":"${altAddressPostalcode.toJsonString()}",';
    }
    if (this.altAddressCountry != null) {
      if(data==null)
        data = '{';
      data = '$data"alt_address_country":"${altAddressCountry.toJsonString()}",';
    }
    if (this.assistant != null) {
      if(data==null)
        data = '{';
      data = '$data"assistant":"${assistant.toJsonString()}",';
    }
    if (this.assistantPhone != null) {
      if(data==null)
        data = '{';
      data = '$data"assistant_phone":"${assistantPhone.toJsonString()}",';
    }
    if (this.emailAddressesNonPrimary != null) {
      if(data==null)
        data = '{';
      data = '$data"email_addresses_non_primary":"${emailAddressesNonPrimary.toJsonString()}",';
    }
    if (this.converted != null) {
      if(data==null)
        data = '{';
      data = '$data"converted":"${converted.toJsonString()}",';
    }
    if (this.referedBy != null) {
      if(data==null)
        data = '{';
      data = '$data"refered_by":"${referedBy.toJsonString()}",';
    }
    if (this.leadSource != null) {
      if(data==null)
        data = '{';
      data = '$data"lead_source":"${leadSource.toJsonString()}",';
    }
    if (this.leadSourceDescription != null) {
      if(data==null)
        data = '{';
      data = '$data"lead_source_description":"${leadSourceDescription.toJsonString()}",';
    }
    if (this.status != null) {
      if(data==null)
        data = '{';
      data = '$data"status":"${status.toJsonString()}",';
    }
    if (this.statusDescription != null) {
      if(data==null)
        data = '{';
      data = '$data"status_description":"${statusDescription.toJsonString()}",';
    }
    if (this.reportsToId != null) {
      if(data==null)
        data = '{';
      data = '$data"reports_to_id":"${reportsToId.toJsonString()}",';
    }
    if (this.reportToName != null) {
      if(data==null)
        data = '{';
      data = '$data"report_to_name":"${reportToName.toJsonString()}",';
    }
    if (this.accountName != null) {
      if(data==null)
        data = '{';
      data = '$data"account_name":"${accountName.toJsonString()}",';
    }
    if (this.accountDescription != null) {
      if(data==null)
        data = '{';
      data = '$data"account_description":"${accountDescription.toJsonString()}",';
    }
    if (this.contactId != null) {
      if(data==null)
        data = '{';
      data = '$data"contact_id":"${contactId.toJsonString()}",';
    }
    if (this.accountId != null) {
      if(data==null)
        data = '{';
      data = '$data"account_id":"${accountId.toJsonString()}",';
    }
    if (this.opportunityId != null) {
      if(data==null)
        data = '{';
      data = '$data"opportunity_id":"${opportunityId.toJsonString()}",';
    }
    if (this.opportunityName != null) {
      if(data==null)
        data = '{';
      data = '$data"opportunity_name":"${opportunityName.toJsonString()}",';
    }
    if (this.opportunityAmount != null) {
      if(data==null)
        data = '{';
      data = '$data"opportunity_amount":"${opportunityAmount.toJsonString()}",';
    }
    if (this.campaignId != null) {
      if(data==null)
        data = '{';
      data = '$data"campaign_id":"${campaignId.toJsonString()}",';
    }
    if (this.campaignName != null) {
      if(data==null)
        data = '{';
      data = '$data"campaign_name":"${campaignName.toJsonString()}",';
    }
    if (this.cAcceptStatusFields != null) {
      if(data==null)
        data = '{';
      data = '$data"c_accept_status_fields":"${cAcceptStatusFields.toJsonString()}",';
    }
    if (this.mAcceptStatusFields != null) {
      if(data==null)
        data = '{';
      data = '$data"m_accept_status_fields":"${mAcceptStatusFields.toJsonString()}",';
    }
    if (this.acceptStatusId != null) {
      if(data==null)
        data = '{';
      data = '$data"accept_status_id":"${acceptStatusId.toJsonString()}",';
    }
    if (this.acceptStatusName != null) {
      if(data==null)
        data = '{';
      data = '$data"accept_status_name":"${acceptStatusName.toJsonString()}",';
    }
    if (this.webtoleadEmail1 != null) {
      if(data==null)
        data = '{';
      data = '$data"webtolead_email1":"${webtoleadEmail1.toJsonString()}",';
    }
    if (this.webtoleadEmail2 != null) {
      if(data==null)
        data = '{';
      data = '$data"webtolead_email2":"${webtoleadEmail2.toJsonString()}",';
    }
    if (this.webtoleadEmailOptOut != null) {
      if(data==null)
        data = '{';
      data = '$data"webtolead_email_opt_out":"${webtoleadEmailOptOut.toJsonString()}",';
    }
    if (this.webtoleadInvalidEmail != null) {
      if(data==null)
        data = '{';
      data = '$data"webtolead_invalid_email":"${webtoleadInvalidEmail.toJsonString()}",';
    }
    if (this.birthdate != null) {
      if(data==null)
        data = '{';
      data = '$data"birthdate":"${birthdate.toJsonString()}",';
    }
    if (this.portalName != null) {
      if(data==null)
        data = '{';
      data = '$data"portal_name":"${portalName.toJsonString()}",';
    }
    if (this.portalApp != null) {
      if(data==null)
        data = '{';
      data = '$data"portal_app":"${portalApp.toJsonString()}",';
    }
    if (this.website != null) {
      if(data==null)
        data = '{';
      data = '$data"website":"${website.toJsonString()}",';
    }
    if (this.eInviteStatusFields != null) {
      if(data==null)
        data = '{';
      data = '$data"e_invite_status_fields":"${eInviteStatusFields.toJsonString()}",';
    }
    if (this.eventStatusName != null) {
      if(data==null)
        data = '{';
      data = '$data"event_status_name":"${eventStatusName.toJsonString()}",';
    }
    if (this.eventInviteId != null) {
      if(data==null)
        data = '{';
      data = '$data"event_invite_id":"${eventInviteId.toJsonString()}",';
    }
    if (this.eAcceptStatusFields != null) {
      if(data==null)
        data = '{';
      data = '$data"e_accept_status_fields":"${eAcceptStatusFields.toJsonString()}",';
    }
    if (this.eventAcceptStatus != null) {
      if(data==null)
        data = '{';
      data = '$data"event_accept_status":"${eventAcceptStatus.toJsonString()}",';
    }
    if (this.eventStatusId != null) {
      if(data==null)
        data = '{';
      data = '$data"event_status_id":"${eventStatusId.toJsonString()}",';
    }
    if (this.titlesC != null) {
      if(data==null)
        data = '{';
      data = '$data"titles_c":"${titlesC.toJsonString()}",';
    }
    if (this.departmentsC != null) {
      if(data==null)
        data = '{';
      data = '$data"departments_c":"${departmentsC.toJsonString()}",';
    }
    if (this.jjwgMapsLngC != null) {
      if(data==null)
        data = '{';
      data = '$data"jjwg_maps_lng_c":"${jjwgMapsLngC.toJsonString()}",';
    }
    if (this.isYourOwnBusinessC != null) {
      if(data==null)
        data = '{';
      data = '$data"is_your_own_business_c":"${isYourOwnBusinessC.toJsonString()}",';
    }
    if (this.jjwgMapsGeocodeStatusC != null) {
      if(data==null)
        data = '{';
      data = '$data"jjwg_maps_geocode_status_c":"${jjwgMapsGeocodeStatusC.toJsonString()}",';
    }
    if (this.passwordC != null) {
      if(data==null)
        data = '{';
      data = '$data"password_c":"${passwordC.toJsonString()}",';
    }
    if (this.userTypeC != null) {
      if(data==null)
        data = '{';
      data = '$data"user_type_c":"${userTypeC.toJsonString()}",';
    }
    if (this.selectTypeC != null) {
      if(data==null)
        data = '{';
      data = '$data"select_type_c":"${selectTypeC.toJsonString()}",';
    }
    if (this.jjwgMapsAddressC != null) {
      if(data==null)
        data = '{';
      data = '$data"jjwg_maps_address_c":"${jjwgMapsAddressC.toJsonString()}",';
    }
    if (this.jjwgMapsLatC != null) {
      if(data==null)
        data = '{';
      data = '$data"jjwg_maps_lat_c":"${jjwgMapsLatC.toJsonString()}",';
    }
    if (this.verticalC != null) {
      if(data==null)
        data = '{';
      data = '$data"vertical_c":"${verticalC.toJsonString()}",';
    }
    if (this.confirmPasswordC != null) {
      if(data==null)
        data = '{';
      data = '$data"confirm_password_c":"${confirmPasswordC.toJsonString()}",';
    }

    data = data.substring(0, data.length - 1);

    data = data + '}';
    return data;
  }
}

class AssignedUserName {
  String name;
  String value;

  AssignedUserName({this.name, this.value});

  AssignedUserName.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }

  String toJsonString() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['name'] = this.name;
//    data['value'] = this.value;

    String data = '{"name":"$name","value":"${value}';
    return data;
  }
}






