class ReturnedGenericObjects {
  int _resultCount;
  String _totalCount;
  int _nextOffset;
  List<EntryList> _entryList;
//  List<Map> _relationshipList;

  ReturnedGenericObjects(
      {int resultCount,
        String totalCount,
        int nextOffset,
        List<EntryList> entryList,
        List<Null> relationshipList}) {
    this._resultCount = resultCount;
    this._totalCount = totalCount;
    this._nextOffset = nextOffset;
    this._entryList = entryList;
//    this._relationshipList = relationshipList;
  }

  int get resultCount => _resultCount;
  set resultCount(int resultCount) => _resultCount = resultCount;
  String get totalCount => _totalCount;
  set totalCount(String totalCount) => _totalCount = totalCount;
  int get nextOffset => _nextOffset;
  set nextOffset(int nextOffset) => _nextOffset = nextOffset;
  List<EntryList> get entryList => _entryList;
  set entryList(List<EntryList> entryList) => _entryList = entryList;
//  List<Null> get relationshipList => _relationshipList;
//  set relationshipList(List<Null> relationshipList) =>
//      _relationshipList = relationshipList;

  ReturnedGenericObjects.fromJson(Map<String, dynamic> json) {
    _resultCount = json['result_count'];
    _totalCount = json['total_count'];
    _nextOffset = json['next_offset'];
    if (json['entry_list'] != null) {
      _entryList = new List<EntryList>();
      json['entry_list'].forEach((v) {
        _entryList.add(new EntryList.fromJson(v));
      });
    }
//    if (json['relationship_list'] != null) {
//      _relationshipList = new List<Null>();
//      json['relationship_list'].forEach((v) {
//        _relationshipList.add(new Null.fromJson(v));
//      });
//    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result_count'] = this._resultCount;
    data['total_count'] = this._totalCount;
    data['next_offset'] = this._nextOffset;
    if (this._entryList != null) {
      data['entry_list'] = this._entryList.map((v) => v.toJson()).toList();
    }
//    if (this._relationshipList != null) {
//      data['relationship_list'] =
//          this._relationshipList.map((v) => v.toJson()).toList();
//    }
    return data;
  }
}

class EntryList {
  String _id;
  String _moduleName;
  NameValueList _nameValueList;

  EntryList({String id, String moduleName, NameValueList nameValueList}) {
    this._id = id;
    this._moduleName = moduleName;
    this._nameValueList = nameValueList;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get moduleName => _moduleName;
  set moduleName(String moduleName) => _moduleName = moduleName;
  NameValueList get nameValueList => _nameValueList;
  set nameValueList(NameValueList nameValueList) =>
      _nameValueList = nameValueList;

  EntryList.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _moduleName = json['module_name'];
    _nameValueList = json['name_value_list'] != null
        ? new NameValueList.fromJson(json['name_value_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['module_name'] = this._moduleName;
    if (this._nameValueList != null) {
      data['name_value_list'] = this._nameValueList.toJson();
    }
    return data;
  }
}

class NameValueList {
  Id _id;
  Id _name;

  NameValueList({Id id, Id name}) {
    this._id = id;
    this._name = name;
  }

  Id get id => _id;
  set id(Id id) => _id = id;
  Id get name => _name;
  set name(Id name) => _name = name;

  NameValueList.fromJson(Map<String, dynamic> json) {
    _id = json['id'] != null ? new Id.fromJson(json['id']) : null;
    _name = json['name'] != null ? new Id.fromJson(json['name']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._id != null) {
      data['id'] = this._id.toJson();
    }
    if (this._name != null) {
      data['name'] = this._name.toJson();
    }
    return data;
  }
}

class Id {
  String _name;
  String _value;

  Id({String name, String value}) {
    this._name = name;
    this._value = value;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get value => _value;
  set value(String value) => _value = value;

  Id.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    data['value'] = this._value;
    return data;
  }
}