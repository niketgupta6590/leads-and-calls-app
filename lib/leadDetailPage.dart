import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:leadcalls/EditOption/LeadEdit.dart';
import 'package:leadcalls/Home.dart';
import 'package:leadcalls/Reused/LeadResponse.dart';
import 'package:leadcalls/menus/sharePage.dart';
import 'package:leadcalls/menus/webview.dart';

import 'package:leadcalls/utils/ColorConstants.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'objects/DeleteRecord.dart';
import 'objects/modules.dart';
import 'package:url_launcher/url_launcher.dart';

import 'objects/leads.dart';
import 'utils/Constants.dart';

class LeadDetailPage extends StatefulWidget {
  EntryList lead;
  LeadDetailPage(this.lead);

  @override
  _LeadDetailPageState createState() => _LeadDetailPageState();
}

class _LeadDetailPageState extends State<LeadDetailPage> {
  LeadResponse leadResponse = LeadResponse();
  final bool isdeleted = false;

  String leadRecordId;
  String leadname;

  TextEditingController _phoncontroller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.lead.toJson());

    // leadResponse.getLeads(context);
  }

  Future<void> deletedialog(context) {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Do you want to delete?"),
            actions: [
              FlatButton(
                child: Text("Yes"),
                onPressed: () {
                  _deleteItem();
                },
              ),
              FlatButton(
                child: Text("Cancel"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          );
        });
  }

  Future<void> _deleteItem() async {
    // set up POST request arguments
    String url =
        'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    String restData =
        '{"session":"$userSession","module_name":"Leads","name_value_list":[{"name":"id","value":"$leadRecordId"},{"name":"deleted","value":"1"}]}';

    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "set_entry",
      "rest_data": "$restData"
    };
    print(restData);

    FormData formData = FormData.fromMap(body);
    // make POST request
    Dio dio = new Dio();

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // Do something before request is sent
      print(options.data);
      print(options);
      return options; //continue
    }, onResponse: (Response response) async {
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
//    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());

      if (responseMap != null) {
        DeleteRecord deleteRecord =
        DeleteRecord.fromJson(json.decode(response.toString()));

        String desc = "Delete successfully..";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.greenAccent,
            textColor: Colors.white,
            fontSize: 16.0);

        // Navigator.of(context).pushNamed('/leadListPage').then((bool) {
        //   if(isdeleted){
        Navigator.of(context).pop();
        Navigator.pop(context, true);

        ///  Navigator.pushNamed(context, Home.id);
        //   }
        // });

      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        if (desc.contains("session ID is invalid")) {
          // Navigator.pushAndRemoveUntil(
          //   context,
          //   MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
          //   ModalRoute.withName('/'),
          // );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    leadRecordId = widget.lead.id;
    leadname = widget.lead.nameValueList.fullName.value;
    Widget body = new Container(
      margin: EdgeInsets.only(left: 0, right: 0, top: 8, bottom: 8),
      child: ListView(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            height: 28.0,
            color: ColorConstants.primaryColor,
            child: Text(
              "OVERVIEW",
              textAlign: TextAlign.center,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text("Full Name"),
            trailing: Text(
              widget.lead.nameValueList.fullName.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(

            leading: Text('Phone '),
            trailing: Text(
              widget.lead.nameValueList.phoneWork.value,
            ),
            onTap: () {
    FlutterPhoneDirectCaller.callNumber(
        widget.lead.nameValueList.phoneWork.value,
            );
            }
    ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Lead Source '),
            trailing: Text(
              widget.lead.nameValueList.leadSource.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Status '),
            trailing: Text(
              widget.lead.nameValueList.status.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text("Email"),
            trailing: Text(
              widget.lead.nameValueList.email.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Short Description'),
            trailing: Text(
              widget.lead.nameValueList.description.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          Container(
            alignment: Alignment.center,
            height: 28.0,
            color: ColorConstants.primaryColor,
            child: Text(
              "INTERNAL",
              textAlign: TextAlign.center,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Date Modified'),
            trailing: Text(
              widget.lead.nameValueList.dateModified.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Date Created'),
            trailing: Text(
              widget.lead.nameValueList.dateEntered.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Created By'),
            trailing: Text(
              widget.lead.nameValueList.createdByName.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Modified By'),
            trailing: Text(
              widget.lead.nameValueList.modifiedByName.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Assigned to'),
            trailing: Text(
              widget.lead.nameValueList.assignedUserName.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Postal Code'),
            trailing: Text(
              widget.lead.nameValueList.primaryAddressPostalcode.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Lead Details'),
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: choiceAction,
            itemBuilder: (BuildContext context) {
              return Constants.choices.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          )
        ],
      ),
      body: body,
    );
  }

  choiceAction(String choice) {
    if (choice == Constants.webView) {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => webview(),
          )
      );    } else if (choice == Constants.share) {
     // WebviewPage();
    } else if (choice == Constants.share) {
      share(context);
    } else if (choice == Constants.edit) {
      _editData();
    } else if (choice == Constants.delete) {
      deletedialog(context);
    }
  }

  _editData() {
    String subject = widget.lead.nameValueList.fullName.value;
    String id = widget.lead.nameValueList.id.value;
    String desc = widget.lead.nameValueList.description.value;
    String createdby = widget.lead.nameValueList.createdByName.value;
    String modifyby = widget.lead.nameValueList.modifiedByName.value;
    String assignusername = widget.lead.nameValueList.assignedUserName.value;
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) =>
            LeadEdit(firstnametext: subject,
                createdby: createdby,
                modifyby: modifyby,
                assusrname: assignusername,
                desc: desc),
      ),
    );
  }

  share(BuildContext context) {
    RenderBox box = context.findRenderObject();

    final String data =
        '${widget.lead.moduleName}\n Full Name - ${widget.lead.nameValueList
        .fullName.value}\n '
        'Phone - ${widget.lead.nameValueList.phoneWork.value}\n '
        'Status - ${widget.lead.nameValueList.status.value}\n '
        'Email - ${widget.lead.nameValueList.email.value}\n '
        'Short Description - ${widget.lead.nameValueList.description.value}\n '
        'Email - ${widget.lead.nameValueList.email.value}\n'
        'Date Modified - ${widget.lead.nameValueList.dateModified.value}\n'
        'Date Created - ${widget.lead.nameValueList.dateEntered.value}\n'
        'Created By - ${widget.lead.nameValueList.createdByName.value}\n'
        'Modified By - ${widget.lead.nameValueList.modifiedByName.value}\n'
        'Assigned to - ${widget.lead.nameValueList.assignedUserName.value}\n';

    Share.share(
      data,
      sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size,
    );
  }

  _calling() {
    String num = "tel:"+_phoncontroller.text;
    launch(num);
  }
}
