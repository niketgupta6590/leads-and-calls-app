import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:leadcalls/Reused/Dialogs.dart';
import 'package:leadcalls/Res/Colors.dart';
import 'package:leadcalls/utils/ColorConstants.dart';
import 'package:leadcalls/utils/Constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Temps.dart';
import 'dashboardPage.dart';
import 'objects/UserResponse.dart';

class LoginPage extends StatefulWidget {
  static const id = 'login';
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  TextEditingController emailController;
  TextEditingController passwordController;
  bool emailError = false;
  bool passwordError = false;
  Dialogs dialogs = Dialogs();

  @override
  void initState() {
    super.initState();
    emailController =  TextEditingController();
    passwordController =  TextEditingController();
    emailController.text = 'webapp-api@ideadunes.com';
    passwordController.text = 'Password123';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstants.primaryColor,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: double.infinity,
              alignment: Alignment.center,
              height: 200,
              color: ColorConstants.primaryColor,
              child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('CONTACTS\nMANAGEMENT',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: cWhite,
                          fontSize: 30,
                          fontWeight: FontWeight.bold))),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                    color: cWhite,
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(20))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(height: 20.0),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        controller: emailController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Email Address'),
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        obscureText: true,
                        controller: passwordController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Confirm Password'),
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Container(
                        padding: EdgeInsets.all(10),
                        child: RaisedButton(
                            textColor: Colors.white,
                            color: ColorConstants.primaryColor,
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Text('Login'),
                            ),
                            onPressed: () => login())),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomSheet: Container(
        padding: EdgeInsets.all(20),
        color: cWhite,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

              Text('Powered by \n    v 1.0.0'),

                SizedBox(
                  width: 18,
                ),
            Image.asset(
              'images/powered.png',
              width: 100,
            ),


          ],
        ),
      ),
    );
  }

  void login() {
    String email = emailController.text;
    String password = passwordController.text;

    if (email.isEmpty) {
      setState(() {
        emailError = true;
      });
      return;
    } else {
      emailError = false;
    }

    if (password.isEmpty) {
      setState(() {
        passwordError = true;
      });
      return;
    } else {
      passwordError = false;
    }

    _makePostRequest(email, password);
  }

  _makePostRequest(String email, String password) async {
    dialogs.loading(context);
    String url =
       'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';

    String userAuthString =
        '{"user_auth":{"user_name":"$email","password":"$password","encryption":"PLAIN"},"application":"MyRestAPI"}';
    Map<String, Object> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "login",
      "rest_data": "$userAuthString"
    };

    FormData formData = FormData.fromMap(body);
    Dio dio = new Dio();

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      return options; //continue
    }, onResponse: (Response response) async {
      return response; // continue
    }, onError: (DioError e) async {
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());
      if (responseMap != null && responseMap.containsKey('id')) {
        UserResponse userResponse =
            UserResponse.fromJson(json.decode(response.toString()));
        Fluttertoast.showToast(
            msg: "Welcome ${userResponse.nameValueList.userName.value}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.greenAccent,
            textColor: Colors.black,
            fontSize: 16.0);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool(Constants.loginStatusPrefKey, true);
        prefs.setString(Constants.id, userResponse.id);
        prefs.setString(Constants.userId, userResponse.nameValueList.userId.value);
        prefs.setString(Constants.userName, userResponse.nameValueList.userName.value);
        tUserSession = prefs.getString(Constants.id);
        tUserId = prefs.getString(Constants.userId);
        tUserName = prefs.getString(Constants.userName);
        dialogs.dismiss(context);
        Navigator.pushReplacement(context,
            PageRouteBuilder(pageBuilder: (_, __, ___) => DashboardPage()));
        //
      } else {
        dialogs.dismiss(context);
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    }
  }

  Future<dynamic> readResponse(HttpClientResponse response) {
    var completer = new Completer();
    var contents = new StringBuffer();
    response.transform(utf8.decoder).listen((data) {
      contents.write(data);
    }, onDone: () => completer.complete(contents.toString()));
    return completer.future;
  }
}
