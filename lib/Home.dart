import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:leadcalls/Screens/Details.dart';
import 'package:leadcalls/Screens/Lister.dart';
import 'package:leadcalls/callsListPage.dart';
import 'package:leadcalls/emailsListPage.dart';
import 'package:leadcalls/meetingsListPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Temps.dart';
import 'db/ModuleDb.dart';
import 'leadListPage.dart';
import 'loginPage.dart';
import 'objects/callListResponse.dart';
import 'objects/createResponse.dart';
import 'objects/emails.dart';
import 'objects/leads.dart';
import 'objects/meetings.dart';
import 'utils/ColorConstants.dart';
import 'utils/Constants.dart';

class Home extends StatefulWidget {
  static const id = 'home';

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Leads leads;
  Meetings meetings;
  Emails emails;
  CallsListResponse callListResponse;
  String userName = " ";

  int currentIndex = 0;
  int lOpen = 0, lAssigned = 0, lHot = 0, lCold = 0;
  int cPlanned = 0, cHeld = 0;
  int mPlanned = 0, mUnHeld = 0, Held = 0;
  int eNew = 0, eSent = 0;
  List<ModuleDb> modules = [];
  int currItem = 0;

  @override
  void initState() {
    super.initState();
    getLeads();
    getUserName();
    getCalls();
    getMeetings();
    getEmails();
  }

  @override
  Widget build(BuildContext context) {
    Widget listTitle = ListView(
      padding: const EdgeInsets.all(8.0),
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: GestureDetector(
            onTap: () {
            Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Lister('Leads')));
            },
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'Leads',
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.w600),
                        )),
                        leads == null
                            ? CircularProgressIndicator()
                            : Text(
                                'Total - ${leads == null ? 0 : leads.totalCount}',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            color: ColorConstants.blueLight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'Open',
                                    style: TextStyle(),
                                  ),
                                  Text(
                                    lOpen.toString() ?? '-',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            color: ColorConstants.greenLight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'Assigned',
                                    style: TextStyle(),
                                  ),
                                  Text(
                                    lAssigned.toString() ?? '-',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            color: ColorConstants.redLight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'Hot',
                                    style: TextStyle(),
                                  ),
                                  Text(
                                    lHot.toString() ?? '-',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            color: ColorConstants.greyLight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'Cold',
                                    style: TextStyle(),
                                  ),
                                  Text(
                                    lCold.toString() ?? '-',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: GestureDetector(
            onTap: () {
              callListResponse == null
                  ? () {}
                  : Navigator.push(
                      context,
                      PageRouteBuilder(
                          pageBuilder: (_, __, ___) =>
                              Lister('Calls')));
            },
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Calls',
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.w600),
                          ),
                        ),
                        callListResponse == null
                            ? CircularProgressIndicator()
                            : Text(
                                'Total - ${callListResponse == null ? 0 : callListResponse.totalCount}',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            color: ColorConstants.blueLight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'Planned',
                                    style: TextStyle(),
                                  ),
                                  Text(
                                    cPlanned.toString() ?? '-',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            color: ColorConstants.greenLight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'Held',
                                    style: TextStyle(),
                                  ),
                                  Text(
                                    cHeld.toString() ?? '-',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: GestureDetector(
            onTap: () {
              meetings == null
                  ? () {}
                  : Navigator.push(
                      context,
                      PageRouteBuilder(
                          pageBuilder: (_, __, ___) =>
                             Lister('Meetings')));
            },
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Meetings',
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.w600),
                          ),
                        ),
                        meetings == null
                            ? CircularProgressIndicator()
                            : Text(
                                'Total - ${meetings == null ? 0 : meetings.totalCount == null ? 0 : meetings.totalCount}',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            color: ColorConstants.blueLight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'Planned',
                                    style: TextStyle(),
                                  ),
                                  Text(
                                    mPlanned.toString() ?? '-',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            color: ColorConstants.greenLight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'Not Held',
                                    style: TextStyle(),
                                  ),
                                  Text(
                                    mUnHeld.toString() ?? '-',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            color: ColorConstants.greenLight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'Held',
                                    style: TextStyle(),
                                  ),
                                  Text(
                                    Held.toString() ?? '-',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: GestureDetector(
            onTap: () {
              emails == null
                  ? () {}
                  : Navigator.push(
                      context,
                      PageRouteBuilder(
                          pageBuilder: (_, __, ___) => Lister('Emails')));
            },
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Emails',
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.w600),
                          ),
                        ),
                        emails == null
                            ? CircularProgressIndicator()
                            : Text(
                                'Total - ${emails == null ? 0 : emails.totalCount}',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            color: ColorConstants.blueLight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'New',
                                    style: TextStyle(),
                                  ),
                                  Text(
                                    eNew.toString() ?? '-',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            color: ColorConstants.greenLight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'Sent',
                                    style: TextStyle(),
                                  ),
                                  Text(
                                    eSent.toString() ?? '-',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );

    return Scaffold(
        body: Container(
         child: listTitle,
    ));
  }

  Future<void> getLeads() async {
    // set up POST request arguments
    String url =
        'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';

    String restData =
        '{"session":"$tUserSession","module_name":"Leads","query":"leads.assigned_user_id=\'$tUserId\'"}';

    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_entry_list",
      "rest_data": "$restData"
    };

    FormData formData = FormData.fromMap(body);

    try {
      // make POST request
      Dio dio = new Dio();

      dio.interceptors
          .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
        // Do something before request is sent
        return options; //continue
        // If you want to resolve the request with some custom data，
        // you can return a `Response` object or return `dio.resolve(data)`.
        // If you want to reject the request with a error message,
        // you can return a `DioError` object or return `dio.reject(errMsg)`
      }, onResponse: (Response response) async {
        // Do something with response data
        return response; // continue
      }, onError: (DioError e) async {
        // Do something with response error
        return e; //continue
      }));

      Response response = await dio.post(url, data: formData);
//    // check the status code for the result
      int statusCode = response.statusCode;

      if (statusCode == 200) {
        Map responseMap = json.decode(response.toString());
        if (responseMap != null && responseMap.containsKey('result_count')) {
          leads = Leads.fromJson(json.decode(response.toString()));
          String status;
          for (int i = 0; i < leads.entryList.length; i++) {
            status =
                leads.entryList[i].nameValueList.status.value.toLowerCase();
            if (status == 'open') {
              lOpen++;
            } else if (status == 'assigned') {
              lAssigned++;
            } else if (status == 'hot') {
              lHot++;
            } else if (status == 'cold') {
              lCold++;
            }
            setState(() {});
          }
        } else {
          String desc = responseMap.containsKey('description')
              ? responseMap['description']
              : "Something went wrong. Try again.";

          Fluttertoast.showToast(
              msg: "$desc",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIos: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);

          if (desc.contains("session ID is invalid")) {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              ModalRoute.withName('/'),
            );
          }
        }
      }
    } catch (e) {
      print('leading');
      print(e);
    }
  }

  Future<void> getCalls() async {
    // set up POST request arguments
    String url =
        'http://uat.ideadunes.com/projects/devs/testapi_giproperties/general.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    int offset = 0;

    String restData =
        '{"session":"$userSession","module_name":"Calls","query":"assigned_user_id=\'$userID\'","order_by":"status","offset":"$offset","max_results":"50","deleted":"0","Favorites":false}';
    Map<String, Object> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_entry_list",
      "rest_data": "$restData"
    };

    FormData formData = FormData.fromMap(body);

    // make POST request
    Dio dio = new Dio();

    Response response = await dio.post(url, data: formData);
//    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());

      if (responseMap != null && responseMap.containsKey('result_count')) {
        callListResponse =
            CallsListResponse.fromJson(json.decode(response.toString()));
        String status;
        print(callListResponse.entryList.length);
        for (int i = 0; i < callListResponse.entryList.length; i++) {
          status =
              callListResponse.entryList[i].nameValueList.status.toLowerCase();
          if (status == 'planned') {
            cPlanned++;
          } else if (status == 'held') {
            cHeld++;
          }
          if (callListResponse == null) {
            setState(() {
              callListResponse = new CallsListResponse();
            });
            callListResponse = new CallsListResponse();
            setState(() {
              callListResponse = new CallsListResponse();
            });
          }
        }
      } else {
        setState(() {
          callListResponse = new CallsListResponse();
        });
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);

        // SharedPreferences prefs = await SharedPreferences.getInstance();
        // prefs.setString(Constants.module, calls.entryList[0].moduleName);

        if (desc.contains("session ID is invalid")) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
            ModalRoute.withName('/'),
          );
        }
      }
    }
  }

  Future<void> getMeetings() async {
    // set up POST request arguments
    String url =
        'http://uat.ideadunes.com/projects/devs/testapi_giproperties/meetings.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);
    int offset = 0;

    String restData =
        '{"session":"$userSession","module_name":"Meetings","query":"assigned_user_id=\'$userID\'","order_by":"","offset":"$offset","max_results":"50","deleted":"0","Favorites":false}';

    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_entry_list",
      "rest_data": "$restData"
    };

    FormData formData = FormData.fromMap(body);

    // make POST request
    Dio dio = new Dio();

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // Do something before request is sent
      return options; //continue
      // If you want to resolve the request with some custom data，
      // you can return a `Response` object or return `dio.resolve(data)`.
      // If you want to reject the request with a error message,
      // you can return a `DioError` object or return `dio.reject(errMsg)`
    }, onResponse: (Response response) async {
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
//    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());

      if (responseMap != null &&
          responseMap.containsKey('entry_list') &&
          responseMap['entry_list'].length > 0) {
        meetings = Meetings.fromJson(json.decode(response.toString()));
        String status;
        for (int i = 0; i < meetings.entryList.length; i++) {
          status =
              meetings.entryList[i].nameValueList.status.value.toLowerCase();
          if (status == 'planned') {
            mPlanned++;
          } else if (status == 'not held') {
            mUnHeld++;
          } else if (status == 'held') {
            Held++;
          }
          setState(() {});
        }
      } else {
        setState(() {
          meetings = new Meetings();
        });
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";
        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);

        if (desc.contains("session ID is invalid")) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
            ModalRoute.withName('/'),
          );
        }
      }
    }
  }

  Future<void> getEmails() async {
    // set up POST request arguments
    String url =
        'http://uat.ideadunes.com/projects/devs/testapi_giproperties/emails.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    int offset = 0;

    String restData =
        '{"session":"$userSession","module_name":"Emails","query":"assigned_user_id=\'$userID\'","order_by":"status","offset":"$offset","max_results":"50","deleted":"0","Favorites":false}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_entry_list",
      "rest_data": "$restData"
    };

    FormData formData = FormData.fromMap(body);
    // make POST request
    Dio dio = new Dio();

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // Do something before request is sent
      return options; //continue
      // If you want to resolve the request with some custom data，
      // you can return a `Response` object or return `dio.resolve(data)`.
      // If you want to reject the request with a error message,
      // you can return a `DioError` object or return `dio.reject(errMsg)`
    }, onResponse: (Response response) async {
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
//    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());
      if (responseMap != null && responseMap.containsKey('result_count')) {
        emails = Emails.fromJson(json.decode(response.toString()));

        // List jsonDecodeList<Emails>(Emails fromJson(Map<String, dynamic> decodedItem)) {
        //   List mapList = jsonDecode(this);
        //   final List<Emails> decodedList = [];
        //
        //   //mapping
        //   mapList.forEach((mapItem) {
        //     if (mapItem is Map<String, dynamic>) {
        //       decodedList.add(fromJson(mapItem));
        //     }
        //   });
        //   return decodedList;
        // }
        //

        String status;
        for (int i = 0; i < emails.entryList.length; i++) {
          status = emails.entryList[i].nameValueList.status.value.toLowerCase();
          if (status == 'new') {
            eNew++;
          } else if (status == 'sent') {
            eSent++;
          }
          setState(() {});
        }
      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);

        if (desc.contains("session ID is invalid")) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
            ModalRoute.withName('/'),
          );
        }
      }
    }
  }

  Future<void> getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      userName = prefs.getString(Constants.userName);
    });
  }
}
