import 'package:flutter/material.dart';
import 'Colors.dart';

const sIdleText = TextStyle(
  color: cBlack,
);

const sRoundedTextField = InputDecoration(
    hintStyle: TextStyle(
        color: cLightGrey,
        fontWeight: FontWeight.w300
    ),
    contentPadding: EdgeInsets.symmetric(horizontal: 15),
    enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(18)),
        borderSide: BorderSide(
            color: cDarkPink,
            width: 2
        )
    ),
    focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(18)),
        borderSide: BorderSide(
            color: cWhite,
            width: 2
        )
    )
);