
import 'package:flutter/cupertino.dart';

const cPink = Color(0xFFBF606B);
const cDarkPink = Color(0xFF9D5059);
const cLightPink = Color(0xFFDA727E);
const cBlack = Color(0xFF000000);
const cWhite = Color(0xFFFFFFFF);
const cLightGrey = Color(0xFFCECECE);
const cGrey = Color(0xFF9F9F9F);
