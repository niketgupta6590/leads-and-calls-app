import 'package:dio/dio.dart';

class ServiceGenerator{

  static const BASE_URL = 'http://uat.ideadunes.com/projects/devs/testapi_giproperties/';
  static const BASE_URL_COMMON = 'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/';
  static const BASE_URL_SET_ENTRY = 'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';

  static Future<dynamic> getReq(String api) async {
    try {
      Response response = await Dio().post(BASE_URL+api);
      return response;
    } catch (e) {
      print(e);
      return 'err';
    }
  }

  static Future<dynamic> getReqWithParams(String api, FormData data) async {
    try {
      Response response = await Dio().post(BASE_URL_COMMON+api,data: data);
      print(response);
      return response;
    } catch (e) {
      print(e);
      return 'err';
    }
  }

  //  static postData() async{
  //    FormData formData = FormData.fromMap({});
  //      formData.files.addAll([
  //        MapEntry("image_files", await MultipartFile.fromFile(item.path))]);
  //
  //    var Dio dio = new Dio()..options.baseUrl = "http://serverURL:port";
  //    dio.post("/uploadFile", data: formData).then((response) {
  //      print(response);
  //    }).catchError((error) => print(error));
  // }
}